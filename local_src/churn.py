import re


# Regular expression pattern for the desired lines
pattern = r'when_Hoffman_.* <= .*'

# Open the input file for reading
with open('Hoffman.vhd', 'r') as file:
    lines = file.readlines()

# Use regular expressions to find matching lines
matching_lines = [line for line in lines if re.search(pattern, line)]

# Print or process the matching lines
replaces = [] 
for line in matching_lines:
    parts = line.split('<=')
    if len(parts) > 1:
        rightmost_portion = parts[-1].strip()
        search_pattern = parts[0][2:-1] + " = '1'"
        rightmost_portion = rightmost_portion.split(';')
        replaces.append((search_pattern,rightmost_portion[0])) 

# Create a function to perform the replacement
def multiple_replace(input_text, replace_pairs):
    # Create a regular expression pattern to match the keys (input strings)
    pattern = re.compile("|".join(map(re.escape, [pair[0] for pair in replace_pairs])))
    
    # Use a function as the replacement to look up the value for each match
    def replace(match):
        return next(replace[1] for replace in replace_pairs if replace[0] == match.group(0))
    
    # Perform the replacement for each line in the input_text array
    output_text = []
    for line in input_text:
        modified_line = pattern.sub(replace, line)
        output_text.append(modified_line)
    
    return output_text

new_lines = multiple_replace(lines, replaces)

# If you want to save the matching lines to a new file, you can do so like this:
with open('output_file.txt', 'w') as output_file:
    output_file.writelines(new_lines)
