
# VexRiscV Hoffman RISC-V processor on S-100 T35 Module 

  This repo contains an implementation of [Hoffman Basic SOC](vexriscv/src/main/scala/vexriscv/hoffman/Hoffman.scala) for the [S-100 T35 FPGA BOARD](https://github.com/s100projects/T35_FPGA_MODULE/) board. It also contains a GHDL VPI simulation written in c++. The simulation is particularly helpful because it is much faster to iterate on the build//compile/test process with the simulator than it is with the synthesis in the Efinity IDE which is slow. It enables you to inspect your vhdl code as it is being executed and to put in log statements and trace your code. 

  ![main window](ghdl/images/process_diagram.jpg)

## TLDR 

In the outflow directory is a Hoffman.bit and Hoffman.hex. Depending on how you flash the T-35 select one or the other. 
Then flash to an SST39SF040 or equivalent the lisp.bin ( or lisp.hex ) also in the outflow directory. 

I found that the TL86Plus didn't seem to like the .hex file when burning it to the flash so I used the lisp.bin file. 

Make the small adapter board which basiclly just shifts a few of the pins. The SDRAM socket is almost pin compatible with the SST39SF040 512kB flash rom but not quite [Adapter](kicad/s100_fpga_adapter_board)

Boot the T-35 up in the FPGA ZBC Z80 board with a terminal on the usb and you will see the monitor come up. Hit 'b' for boot. If things have gone according to plan will boot off the flash and you will now be in a line by line lisp interp ...  

## Notes

  - Uses on board uart on the T-35 module or serial I/O card on port 0x0AC

    [ 1111 1111 ] - default test loop with on board uart 
    [ 1111 1110 ] - monitor up on board uart 
    [ 1111 1100 ] - monitor up on serial I/O card on 0x0AC 

  - Make sure you use the pure RISC-V 32 gcc and not the RISC-V 64 gcc with 32 support as it is broken
  - Make sure you use the ghdl-gcc for the simulation and not mcode or something else. 
  - The monitor.c example demonstrates basic I/O and Uart with a simple system monitor. 
  - The C stack currently supports a basic malloc/sbrk/math/printf/scanf and is on top of the nano_newlib 
  - Build tree has not been tested with Windows given that this is LibC orientated it is Linux orientated stack

## Features

  * [Hoffman CPU Core](vexriscv/src/main/scala/vexriscv/hoffman/Hoffman.scala), supporting rv32imaf/ilp32f (More on this is covered below). 
  * 1 UART connected to pins rxd:GPIOL_45 / txd:GPIOL_43
  * Serial I/O card will work on port 0x0AC 
  * GPIO Port connected to the 7 segments of the T35 Module display    
  * 2.5Mhz CPU Clocked from external clock on the T-35 board.  
  * FPU 

## Dependencies

  - Risc-V 32 Tool Chain ( supplies libc in the form of nano_newlib ) 
  - Scala ( sbt ) 
  - JRE >= 8 
  - Efinity  

## Hardware 
  - The S100 SBC Z80 FPGA board [Fgpa Board](http://www.s100computers.com/My%20System%20Pages/FPGA%20Z80%20SBC/FPGA%20Z80%20SBC.htm) 
  - For LiSP you will need the small flash adapter board [Adapter](kicad/s100_fpga_adapter_board)

## Overview Of Steps To Build For Efinity Or Simulator 

  - build the monitor file ( monitor.c ) 
  - build the Hoffman scala core which ingests the monitor for either Efinity or Simulator ( both have different timing requirements )  
  - Open Efinity IDE and do synthesis for FPGA *or* build for simulator and run simulator 

## Efinity => FPGA ( quick start ) 

  Getting demo going will need Efinity installed and T-35 module plugged in to the JTAG. The Repo contains complete project for the Efinix IDE and this resides in the root. 

  > git clone https://gitlab.com/mixotricha/risc-v-s100_t35/
  
  > checkout stable 

   Open the project file (hoffman_soc.xml) in the Efinity IDE and run the complete build flow in the Efinix IDE and then open the programmer and flash it to the T-35 module. 

   - If successful you should see the system monitor come up on the uart 38400/8/n/1 after a reset. 
   - By altering the dip switches as described above can switch to Serial I/O board on port 0x0AC 

  When the system monitor is up and running you should see something like the following in your terminal. You can go through some basic functions to test the memory. Test the math functions. Note that if you do the memory test (r) you are effectively shot gunning straight in to memory which will spork malloc. If you test with malloc you will have controlled access to memory. You can also step through memory and enlarge the block window.  

  ![main window](ghdl/images/simple_monitor.jpg)

  ***MAKE SURE IF YOU BUILT MONITOR FIRST THAT YOU BUILT THE CORRECT VERSION OF MONITOR AND NOT SIMULATOR VERSION***

## Building Monitor 

  [monitor.c](vexriscv/src/main/c/hoffman/monitor/monitor.c) is the monitor example. To build this you will need to install the risc-v 32 tool chain and it will need to be in path so that the Makefile can find it. Note that the tool chain also builds the libraries you need ( nano_newlib ) 

   To configure the Risc-V 32 tool chain : 

  > git clone https://github.com/riscv-collab/riscv-gnu-toolchain
  
  > ./configure --prefix=(LOCATION)/riscv --target=riscv32-unknown-elf --with-arch=rv32imaf --with-abi=ilp32f --disable-linux 
  
  > make newlib 
 
  ( this will probably take a long time to build ) 
  
  > make install 

  Then ( Ubuntu used as example ) you will want something like 

  > export PATH=$PATH:/(LOCATION)/riscv/bin so that the Makefile can find the Risc-V 32 toolchain. 

  Note that if you make LOCATION the location where your risc-v 32 tool chain was cloned you wont need to 'sudo' to install it and can keep things nice and tidy. 

   Now from the root of the 'risc-v-s100_t35/' respository :

  > make monitor_clean // to clean the monitor build dir 
  
  > make monitor_efinity *or* make monitor_sim

  ***MAKE SURE YOU BUILD MONITOR FOR APPROPRIATE TARGET OR IT WONT RUN***

  For more details about the risc-v 32 tool chain follow the steps here : https://github.com/riscv-collab/riscv-gnu-toolchain 

## Copying the Lisp Rom To A Flash chip.  

 The Lisp rom is here [monitor.c](vexriscv/src/main/c/hoffman/lisp/lisp.bin)
 I used a 32 pin rom the SST39SF040. The ROM plugs into the SRAM socket on the S100 SBC FPGA board. 

## Building the Hoffman Risc-V 32 scala for efinity

  You will need a JRE that is >= 8 and you will need Scala installed ( see relevant documentation for your Linux on my Ubuntu both dependencies were met by by default ). 

  From the root of the respository :

  > make monitor_efinity
  
  > make hoffman_efinity_vhdl

  This will put the Hoffman.vhd targeted for efinity under local_src 

  You can then follow the steps in Efinity => FPGA to put your build on the T-35 module 

## Building the Hoffman Risc-V 32 core for simulator 

  From the root of the respository :

  > make monitor_clean // ------------------------------------------------------------------------------------------------
  
  > make monitor_sim   // you can skip building monitor if you just want to bring up simulator with already built hex file 
  
  > make hoffman_sim_vhdl
  
  > make sim_run

  This will bring up the simulator and you can then click the play button for it to start running in the interface. You will see this is an order of magnitude slower than the actual real hardware so be prepared to give it awhile. You can also bring up the inspector to see the core running. 

## More Info On The Hoffman Core 

   I developed the Hoffman core based on [Briey](https://github.com/SpinalHDL/VexRiscv/blob/master/src/main/scala/vexriscv/demo/Briey.scala). It contains a functional FPU and sdram has been turned off ( for now ). It also contains an addition to support GPIO on the T-35 module. It is orchestrated off the VexRiscV family of cores which are in turn on top of SpinalHDL. This is a high level orchestration which means that you can stay out of the weeds and write less VHDL or Verilog ( you can switch to target either as your preference ) and configure your core in a much more abstracted way. Of course this has a price and the price is you will have to absorb yet another API and set of tools if you want to work on the core directly. However you can also skip this and move to playing with the C code if you want. It alo means the output VHDL or Verilog becomes a black box which reduces its human readability significantly. Efinity themselves have used the VexRiscV to create the Sapphire SOC but I do not believe that they provide a complete tool chain for it as I have done ( just the end result as verilog). The gain to be had by having decided to orchestrate the core this way is the potential to be able to bring together multiple cores ( Z80 / 68k series / Sparc ) and to have those cores all working together on a combined bus and to be able to share data with each other via an AXI bridge in a common build structure. Also features that might be harder to implement or that are missing become available on the bus like the FPU. Those features may even be shareable between cores. The possibility exists for example to add a Z80 instruction decoder and address bus to the Hoffman. For more details on the [VexRiscV](https://github.com/SpinalHDL/VexRiscv) cores and on [SpinalHDL](https://github.com/SpinalHDL/SpinalHDL).

## More Info About Why You Might Want This ? 

  Well you are now in the land of 'C' and on top of a small Libc ( nano_newlib ) rather than in the land of pure assembly and hdl and this makes lots of existing code available to you and lets you step out of some of the bare metal of the S-100 systems to get something other than CPM done. You can still inline assembly in your code if you want and you can see a few small simple examples of this in the monitor.c. With further work ( access to the memory and disk ) this core should now be able to bring up FreeRTOS or a Linux however at the moment the Hoffman is using the internal memory lanes on FPGA and so space is limited. 

  My ambition is to bring up a Lisp on my S-100 machine as a pure L box. What I think I will do next is to turn back to the hardware and 

  - Get it to see the SDRAM on FPGA and on 4MB card 
  - Get it to see IDE on the FPGA board or elsewhere  

## Segment Display

  The segments display willrotate through hex chars when you push 'k' in the monitor. 

## UART 
  
  Uart is set to 38400 8,N,1. It is clocked off the : io_axiClk define in the vhdl file.

## UART 
Serial I/O board set to port 0x0AC 38400,8,N,1. For whatever reason I do not seem able to pick up status 
bit off my serial board so the char loop waits for a new character to be pressed. This is acceptable within 
the monitor but will definitely need to be fixed. It is possibly a hardware fault in my serial I/O code itself. 

## Reset line 

  This is now set with a weak pull up. Without this the processor seems to halt. The pin assignment for that is : resetCtrl_systemReset

![main window](ghdl/images/hoffman.jpg) 

( grundle grundle grundle blap ! ) 





 
 

