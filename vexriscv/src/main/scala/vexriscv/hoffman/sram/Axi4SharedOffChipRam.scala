/***************************************************************************
 *   Damien Towning         (connolly.damien@gmail.com) 2024               *
 *                                                                         *
 *   This file is part of the T-35 Risc-V Hoffman project                  *
 *                                                                         *
 *   This code is free software; you can redistribute it and/or            *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/

package vexriscv.hoffman.sram

import spinal.core._
import spinal.core.fiber._
import spinal.lib._
import spinal.lib.io._
import spinal.lib.bus.amba3.apb._
import spinal.lib.bus.amba4.axi._
import spinal.lib.fsm._


// Sram config object
object Axi4SharedOffChipRam{
    def getConfigs(addressAxiWidth: Int, dataWidth: Int, idWidth: Int): (Axi4Config) =
    (
      Axi4Config(
        addressWidth = addressAxiWidth,
        dataWidth    = dataWidth,
        idWidth      = idWidth,
        useLock      = false,
        useRegion    = false,
        useCache     = false,
        useProt      = false,
        useQos       = false
      )
    )
}

case class SRAM() extends Bundle { 
}

// SRAM axi driver class 

case class Axi4SharedOffChipRam( addressAxiWidth: Int, dataWidth: Int, idWidth: Int,arwStage : Boolean = false) extends Component {
  
  val (axiConfig) = Axi4SharedOffChipRam.getConfigs(addressAxiWidth,dataWidth,idWidth)

  val io = new Bundle {
    val axi = slave(Axi4Shared(axiConfig)) // axi slave object
    
    val cs     = out Bool()   // chip select signal 
    val oe     = out Bool()   // read enable signal 
    val wr     = out Bool()   // write enable signal 
    val boe    = out Bits(8 bits) // bidirectional bits signals 

    val addr   = out UInt(32 bits) // address bus 
    val wrdata = out Bits(8 bits) // write bus 
    val rddata = in Bits(8 bits)  // read bus    
  }
 
  val lenBurst          = Reg(cloneOf(io.axi.arw.len))     // length of burst
  val arwPayload        = Reg(cloneOf(io.axi.arw.payload)) // payload object 
  
  val readData          = Reg(Bits(32 bits)) init(0x0) // data to be read
  val writeData         = Reg(Bits(8 bits))            // data to be written 
 
  val nxtAddr           = Reg(UInt(32 bits)) // next address 

  val csReg             = Reg(Bool())       // Chip Select 
  val oeReg             = Reg(Bool())       // Chip Read
  val wrReg             = Reg(Bool())       // Chip Write
  val boeReg            = Reg(Bits(8 bits)) // BiDir port

  val readNextByte      = Reg(Bool()) init(True)    // axi state machine has loaded up address 
  val readBytesDone     = Reg(Bool()) init(False)   // read state machine has read bytes  
  val readByteCounter   = Reg(UInt(2 bits)) init(0) // How many bytes read

  val writeNextByte     = Reg(Bool()) init(True)    // axi state machine has loaded up address 
  val writeBytesDone    = Reg(Bool()) init(False)   // write state machine has written bytes 
  val writeByteCounter  = Reg(UInt(4 bits)) init(0) // How many bytes written 
  val writeByteRange    = Reg(UInt(4 bits)) init(0) // How many bytes relative to strobe
  val strobe            = Reg(Bits(4 bits))         // write strobe 

  //val byte_a = Reg(Bits(8 bits)) // LSB 
  //val byte_b = Reg(Bits(8 bits)) // ..
  //val byte_c = Reg(Bits(8 bits)) // .. 
  //val byte_d = Reg(Bits(8 bits)) // MSB 

  def isEndBurst = lenBurst === 0 // Burst end response 

  // Control signals for the axi bus 
  io.axi.arw.ready  := False
  io.axi.w.ready    := False
  io.axi.b.valid    := False
  io.axi.b.resp     := Axi4.resp.OKAY
  io.axi.b.id       := arwPayload.id
  io.axi.r.valid    := False
  io.axi.r.resp     := Axi4.resp.OKAY
  io.axi.r.id       := arwPayload.id
  io.axi.r.last     := isEndBurst && !arwPayload.write

  io.addr := arwPayload.addr.resized  // address bus 
  
  // wiring signals to registers 
  io.cs   := csReg  
  io.oe   := oeReg              
  io.wr   := wrReg    
  io.boe  := boeReg 
  strobe := io.axi.w.payload.strb // strobe 

  io.axi.r.data := readData // reading
  io.wrdata := writeData // writing 

  // SRAM read state machine 
  val sram_read = new StateMachine {
     
    val READIDLE: State = new State with EntryPoint {
      whenIsActive {
        when(!readNextByte && writeNextByte) {
          readByteCounter := 0
          goto(READDELAY)
        }
      }
    }
    
    val READDELAY: State = new StateDelay(60) {
      whenIsActive {
        boeReg := "00000000"
        csReg := True
        oeReg := True
        wrReg := True
      }
      whenCompleted {
        goto(READBYTE)
      }
    }

    val READBYTE: State = new StateDelay(60) {
      whenIsActive {
        boeReg := "00000000"
        csReg := False
        oeReg := False
        wrReg := True
      }
      whenCompleted {
        when(!readNextByte) {
          val readDataParts = readData.subdivideIn(4 slices)
          readDataParts(readByteCounter) := io.rddata
          readByteCounter := readByteCounter + 1
          readNextByte := True
          when(readByteCounter === 3) {
            readBytesDone := True
            goto(READIDLE)
          } otherwise {
            goto(READDELAY)
          }
        }
      }
    }
  }
  
  // SRAM write state machine 
  val sram_write = new StateMachine { 
    
    val WRITEIDLE: State = new State with EntryPoint { 
      whenIsActive {
        when(!writeNextByte && readNextByte) {
          switch(strobe) {
            is(0x0) { goto(WRITEIDLE) }
            for(i <- List(0x1, 0x2, 0x4, 0x8) ) {
              is(i) { writeByteCounter := i + 1; writeByteRange := 1; goto(WRITEDELAY) }
            }
            for(i <- List(0x3,0x6,0xC)) {
              is(i) { writeByteCounter := i + 1; writeByteRange := 1; goto(WRITEDELAY) }
            }
            is(0xF) { writeByteCounter := 1; writeByteRange := 3; goto(WRITEDELAY) }
          }
        }
      }
    }
      
    val WRITEDELAY: State = new StateDelay(60) {
      whenIsActive {
        assert(False,"NO!",NOTE)
        boeReg := "11111111"
        csReg := True
        oeReg := True
        wrReg := True
      }
      whenCompleted {
        goto(WRITEBYTE)
      }
    }

    val WRITEBYTE: State = new StateDelay(60) {
      whenIsActive {
        boeReg := "11111111"
        csReg := False
        oeReg := True
        wrReg := False
      }
      whenCompleted {    
        when(!writeNextByte) {    
          when( writeByteCounter <= (writeByteCounter+writeByteRange) ) {  
            switch(writeByteCounter) {   
              is(1) { assert(False, "write byte:1", NOTE); writeData := io.axi.w.data(7 downto 0)   }                
              is(2) { assert(False, "write byte:2", NOTE); writeData := io.axi.w.data(15 downto 8)  }
              is(3) { assert(False, "write byte:3", NOTE); writeData := io.axi.w.data(23 downto 16) }
              is(4) { assert(False, "write byte:4", NOTE); writeData := io.axi.w.data(31 downto 24) }
            } 
            writeByteCounter := writeByteCounter + 1  
            writeNextByte := True
          }   
        }.otherwise { 
          writeBytesDone := True
          goto(WRITEIDLE)
        }
      }
    }      
  }
  
     
  // Axi sram state machine  
  val fsm = new StateMachine {

    // At idle commence 
    val SETUP: State = new StateDelay(10) with EntryPoint {
      whenIsActive { 
      }
      whenCompleted{
        arwPayload  := io.axi.arw
        lenBurst  := io.axi.arw.len 
        when(io.axi.arw.valid){  
          io.axi.arw.ready := True  
          goto(ACCESS)
        } 
      }
    }

    // Initiate read or write access 
    val ACCESS: State = new State {
      whenIsActive {    
        io.axi.w.ready  := io.axi.w.valid & arwPayload.write
        when(arwPayload.write && (io.axi.w.last || arwPayload.len === 0)){ 
          writeNextByte := False
          goto(WRITE)
        }
        when(!arwPayload.write){
          nxtAddr := Axi4.incr(arwPayload.addr, arwPayload.burst, arwPayload.len, arwPayload.size, 4)
          readNextByte := False // pick up first byte before entering address increment state    
          goto(READ)
        }
      }
    }

    // Enter read cycle 
    val READ: State = new State {
      whenIsActive {
        when(readBytesDone) {
          readBytesDone := False 
          io.axi.r.valid := True
          when(io.axi.r.ready) {
            when(isEndBurst) {
              goto(SETUP)
            }.otherwise {
              arwPayload.addr := nxtAddr
              lenBurst := lenBurst - 1
              goto(ACCESS)
            }
          }
        }.otherwise { 
          // assert each new address for any state machines in vicinity 
          when(readNextByte) {      
            readNextByte := False 
            arwPayload.addr := arwPayload.addr + 1
            goto(READ)
          }
        }    
      }
    }

    // Enter write cycle 
    val WRITE: State = new State { 
      whenIsActive {
        when(writeBytesDone) {
          writeBytesDone := False 
          when(arwPayload.write){ 
            io.axi.b.valid := True
            when(io.axi.b.ready){ goto(SETUP) }
          }
        }.otherwise { 
          // assert each new address for any state machines in vicinity 
          when(writeNextByte) {      
            writeNextByte := False 
            arwPayload.addr := arwPayload.addr + 1  
            goto(WRITE)
          }
        }    
      }  
    }



  }
}
      


  /*val StateMachineA = new StateMachine {

    // At idle 
    val IDLE: State = new State with EntryPoint {
      whenIsActive { 
        when(begin) {
          stepA := True
          stepB := False
          byteCount := 4 
          goto(STEPA) 
        }
      }
    }

    // STEP A COMMENCE 
    val STEPA: State = new State
      whenIsActive { 
        when(ByteCount > 0 ) {
          when(stepA) { 
            stepA := False
            stepB := True
            byteCount := ByteCount - 1 
          }.otherwise { 
            goto(STEPA) 
          }
        }.otherwise { 
          assert(False,"All Done",NOTE) 
      }
    }
  }
  

  val StateMachineB = new StateMachine {

    // At idle 
    val IDLE: State = new State with EntryPoint {
      whenIsActive { 
        when(stepB) {
          goto(STEPB) 
        }
      }
    }

    val STEPB: State = new StateDelay(60)  {
      whenIsActive { 
        csReg := False 
        oeReg := False 
        wrReg := True 
      }
      whenCompleted { 
        switch(byteCount) { 
          is(0) { 
            assert(False,"end",NOTE) 
          } 
          is(1) { assert(False,"byte:1",NOTE) StepA := True  } 
          is(2) { assert(False,"byte:2",NOTE) StepA := True  } 
          is(3) { assert(False,"byte:3",NOTE) StepA := True  } 
          is(4) { assert(False,"byte:4",NOTE) StepA := True  } 
        } 
      }
    }

  }

*/

  




/*case class readByte() extends Component {
    
    val io = new Bundle {
      val rddata = in Bits(8 bits)  // read bus 
      val byteOut = out Bits(8 bits)
      val cs     = out Bool()   // chip select signal 
      val oe     = out Bool()   // read enable signal 
      val wr     = out Bool()   // write enable signal  
      val boe    = out Bits(8 bits) // bidirectional bits signals 
    }

    val csReg      = Reg(Bool()) init(True) // chip select
    val oeReg      = Reg(Bool()) init(True) // read enable 
    val wrReg      = Reg(Bool()) init(True) // write enable 
    val boeReg     = Reg(Bits(8 bits))      // bit state bidirectional bus 
    val rddataReg = Reg(Bits(8 bits)) 
    
    io.cs  := csReg 
    io.oe  := oeReg 
    io.wr  := wrReg 
    io.boe := boeReg 
    io.byteOut := rddataReg

    val sram = new StateMachine {
      
      val IDLE: State = new StateDelay(60) with EntryPoint {
        whenIsActive {     
          boeReg := B"00000000"
          csReg := False
          oeReg := False 
          wrReg := True 
        }
        whenCompleted{      
          goto(GETBYTE)
        }
      }

      val GETBYTE: State = new StateDelay(30) {
        whenIsActive { 
          rddataReg := io.rddata
        }
        whenCompleted{      
          csReg := True
          oeReg := True 
          wrReg := True
        }
      }

    }

}*/


 


 //val led_state = out Bool()
    //val led_b_state = out Bool()

    // control for memory on FPGA board ( not over bus ) 
    //val biData_IN      = master(TriStateArray(8 bits)) // ram bi rd on FPGA board
    //val biData_OUT     = master(TriStateArray(8 bits)) // ram bi wr on FPGA board 
    //val CS      = out Bool() // chip select
    //val OE      = out Bool() // read bit of ram 
    //val WR      = out Bool() // write bit of ram 

    //val ADDR       = out Bits(19 bits)
    //val ADDR   = out UInt(19 bits)
    //val IN         = in  Bits(8 bits) 
    //val OUT        = out UInt(8 bits) 
    //val BOE  = out Bits(8 bits)

    //val ram_A16       = out Bool() 
    //val ram_A17       = out Bool() 
    //val ram_A18       = out Bool()   
   
  /*val busdrive = new ClockingArea(axiClockDomain) {
      
      val pwrReg     = Reg(Bool) init(True)   addTag(crossClockDomain)
      val psyncReg   = Reg(Bool) init(False)  addTag(crossClockDomain) 
      val pstvalReg  = Reg(Bool) init(True)   addTag(crossClockDomain)
      val soutReg    = Reg(Bool) init(False)  addTag(crossClockDomain)
      val sinpReg    = Reg(Bool) init(False)  addTag(crossClockDomain)
      val pDBINReg   = Reg(Bool) init(False)  addTag(crossClockDomain)
      val counterReg = Reg(UInt(8 bits)) init(0) addTag(crossClockDomain)    
      when ( io.S100STATUS.write(axi.z80_n_wr) === False ) {
        switch(counterReg) {
          is(0) {
             pwrReg     := True
             psyncReg   := True 
             pstvalReg  := False 
             soutReg    := False 
             sinpReg    := False 
             pDBINReg   := False 
             counterReg := counterReg + 1; 
          }
          is(1) {
            pwrReg     := False
            psyncReg   := False 
            pstvalReg  := True 
            soutReg    := True  
            sinpReg    := False 
            pDBINReg   := False 
            counterReg := counterReg + 1;
          }
          default {
            pwrReg     := True
            psyncReg   := False
            pstvalReg  := True 
            soutReg    := False
            sinpReg    := False 
            pDBINReg   := False 
            counterReg := counterReg + 1;  
          }
        }
      }.elsewhen ( io.S100STATUS.write(axi.z80_n_rd) === False ) {
        switch ( counterReg ) {  // psync high and pstval low 
      
          is(0) {
            //pwrReg     := True
            psyncReg   := True 
            pstvalReg  := False   
            pDBINReg   := False 
            sinpReg    := False
            soutReg    := False
            counterReg := counterReg + 1;
          }
          is(1) { 
            //pwrReg     := True
            psyncReg   := False
            pstvalReg  := True
            pDBINReg   := True
            sinpReg    := True
            soutReg    := False
            counterReg := counterReg + 1;
          }
          is(2) { 
            //pwrReg     := True
            psyncReg   := False 
            pstvalReg  := True  
            pDBINReg   := False 
            sinpReg    := False
            soutReg    := False
            counterReg := counterReg + 1;
          }
          //is(3) { 
            //pwrReg     := True
            //psyncReg   := False 
           // pstvalReg  := True  
           // pDBINReg   := True 
           // sinpReg    := True
           // soutReg    := False
           // counterReg := counterReg + 1;
          //}
          //is(4) { 
            //pwrReg     := True
            //psyncReg   := False 
            //pstvalReg  := True 
            //pDBINReg   := True 
            //sinpReg    := True
            //soutReg    := False
            //counterReg := counterReg + 1;
          //}
          //is(5) { 
            //pwrReg     := True
          //  psyncReg   := False 
          //  pstvalReg  := True 
          //  pDBINReg   := False
          //  sinpReg    := True
          //  soutReg    := False
           // counterReg := counterReg + 1;
         // }
          //is(6) { 
            //pwrReg     := True
           // psyncReg   := False //. 
           // pstvalReg  := True 
           // pDBINReg   := False 
           // sinpReg    := False
           // soutReg    := False
           // counterReg := counterReg + 1;
          //}
          //is(7) { 
            //pwrReg     := True
            //psyncReg   := False //.
            //pstvalReg  := True //. 
            //pDBINReg   := False 
            //sinpReg    := False
            //soutReg    := False
            //counterReg := counterReg + 1;
          //}
          default { // back to initial state
            //pwrReg     := True
            psyncReg   := False
            pstvalReg  := True 
            pDBINReg   := False 
            soutReg    := False
            sinpReg    := False 
            counterReg := counterReg + 1;
          } 
        } 
      }.otherwise  { // back to initial state 
         pwrReg     := True
         psyncReg   := False
         pstvalReg  := True 
         soutReg    := False
         sinpReg    := False 
         pDBINReg   := False 
         counterReg := 0 
      } 
       
  }

  io.ieee696.clkOut := axi.clkFlp
  io.ieee696.phi    := axi.clkFlp

  io.ieee696.pwr          :=  busdrive.pwrReg
  io.ieee696.psync        :=  busdrive.psyncReg 
  io.ieee696.pstval       :=  busdrive.pstvalReg
  io.ieee696.sOUT         :=  busdrive.soutReg 
  io.ieee696.sINP         :=  busdrive.sinpReg 
  io.ieee696.pDBIN        :=  busdrive.pDBINReg 

//io.sdram          <> axi.sdramCtrl.io.sdram
  //io.vga            <> axi.vgaCtrl.io.vga
  io.ieee696.DO  <> axi.gpioIODOCtrl.io.gpio
  io.ieee696.DI  <> axi.gpioIODICtrl.io.gpio
  
  // Table of status bits ( must be mirrored in monitor ) 
      val output_add_en  = 19  // GPIOB_TXN17
      val output_di_en   = 18  // GPIOL_166 
      val output_do_en   = 17  // GPIOL_144
      val output_ctl_en  = 16  // GPIOR_120  
      val output_stat_en = 15  // GPIOB_TXN19
      val bd_oe       = 14     
      val r_n_cs      = 13 
      val r_n_oe      = 12
      val r_n_wr      = 11
      val r_A16       = 10 
      val r_A17       = 9 
      val r_A18       = 8 
      val z80_n_m1       = 7   
      val z80_n_iorq     = 6 
      val z80_n_wr       = 5 
      val z80_n_rfsh     = 4 
      val z80_n_mreq     = 3 
      val z80_n_rd       = 2 
      val liorq          = 1 
      val inta           = 0 

      val clkFlp     = Reg(Bool) init(False) //addTag(crossClockDomain)
      
      when ( clkFlp === False ) {
        clkFlp := True
      }.otherwise { 
        clkFlp := False
      }

      //when ( io.S100STATUS.write(bd_oe) === False ) { 
      //  io.biData_OE := 0xFF
      //}.otherwise { 
      //  io.biData_OE := 0x00
      //}

        io.S100STATUS  <> axi.gpioS100STATUSCtrl.io.gpio
  
  io.OUTPUT_ADD_EN        :=  io.S100STATUS.write(axi.output_add_en)
  io.OUTPUT_DO_EN         :=  io.S100STATUS.write(axi.output_do_en)
  io.OUTPUT_DI_EN         :=  io.S100STATUS.write(axi.output_di_en)
  //io.OUTPUT_CTL_EN        :=  io.S100STATUS.write(axi.output_ctl_en)
  //io.OUTPUT_STATUS_EN     :=  io.S100STATUS.write(axi.output_stat_en)

  io.OUTPUT_CTL_EN        :=  False
  io.OUTPUT_STATUS_EN     :=  False

            //gpioS100ADRCtrl.io.apb     -> (0x03000, 4 kB), 
          //gpioIODOCtrl.io.apb        -> (0x04000, 4 kB),
          //gpioIODICtrl.io.apb        -> (0x05000, 4 kB),
          //gpioS100STATUSCtrl.io.apb  -> (0x06000, 4 kB), 
          //gpioBiDataINCtrl.io.apb    -> (0x07000, 4 kB), 
          //gpioBiDataOUTCtrl.io.apb   -> (0x08000, 4 kB),           
     //,vgaCtrl.io.apb   -> (0x30000, 4 kB)

           //axiCrossbar.addPipelining(vgaCtrl.io.axi)((ctrl,crossbar) => {
      //  ctrl.readCmd.halfPipe()    >>  crossbar.readCmd
      //  ctrl.readRsp               <<  crossbar.readRsp
      //})

            /*axiCrossbar.addPipelining(sdramCtrl.io.axi)((crossbar,ctrl) => {
        crossbar.sharedCmd.halfPipe()  >>  ctrl.sharedCmd
        crossbar.writeData            >/-> ctrl.writeData
        crossbar.writeRsp              <<  ctrl.writeRsp
        crossbar.readRsp               <<  ctrl.readRsp
      })*/

              //,vgaCtrl.io.axi  -> List(            sdramCtrl.io.axi)

      //val vgaCtrlConfig = Axi4VgaCtrlGenerics(
      //  axiAddressWidth = 32,
      //  axiDataWidth    = 32,
      //  burstLength     = 8,
      //  frameSizeMax    = 2048*1512*2,
      //  fifoSize        = 512,
      //  rgbConfig       = vgaRgbConfig,
      //  vgaClock        = vgaClockDomain
      //)
      //val vgaCtrl = Axi4VgaCtrl(vgaCtrlConfig)

            //val gpioS100ADRCtrl     = Apb3Gpio( gpioWidth = 19, withReadSync = true ) 
      val gpioIODOCtrl        = Apb3Gpio( gpioWidth = 8,  withReadSync = true )
      val gpioIODICtrl        = Apb3Gpio( gpioWidth = 8,  withReadSync = true )
     //val gpioBiDataINCtrl    = Apb3Gpio( gpioWidth = 8,  withReadSync = true ) 
      //val gpioBiDataOUTCtrl   = Apb3Gpio( gpioWidth = 8,  withReadSync = true )   
      val gpioS100STATUSCtrl  = Apb3Gpio( gpioWidth = 20, withReadSync = true )
      //val busconfig = Apb3Config( addressWidth = BUSWIDTH, dataWidth = 32 )
      //val gpioConfig = Gpio.Parameter( width = BUSWIDTH,  output = Seq.range(0, BUSWIDTH) , input = Seq() )
      //val gpioSEG7Ctrl = Apb3Gpio2(gpioConfig,busconfig)
      //val gpioSBCLEDSCtrl = Apb3Gpio2(gpioConfig,busconfig)

            //val sdramCtrl = Axi4SharedSdramCtrl(
      //  axiDataWidth = 32,
      //  axiIdWidth   = 4,
      //  layout       = sdramLayout,
      //  timing       = sdramTimings,
      //  CAS          = 3
      //)

        //val vgaClockDomain = ClockDomain(
  //  clock = io.vgaClk,
  //  reset = resetCtrl.vgaReset
  //)

    //val freq: HertzNumber = 5 MHz
  //val busClockDomain = ClockDomain(
  //  clock = io.busClk,
  //  reset = resetCtrl.axiReset,
  //  frequency = FixedFrequency(freq)
  //)

  */