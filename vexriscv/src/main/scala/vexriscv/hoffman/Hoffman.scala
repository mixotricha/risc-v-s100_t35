/***************************************************************************
 *   Damien Towning         (connolly.damien@gmail.com) 2024               *
 *                                                                         *
 *   This file is part of the T-35 Risc-V Hoffman project                  *
 *                                                                         *
 *   This code is free software; you can redistribute it and/or            *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/

package vexriscv.hoffman


import vexriscv.plugin._
import vexriscv._
import vexriscv.ip.{DataCacheConfig, InstructionCacheConfig}
import spinal.core._
import spinal.lib._

import spinal.lib.bus.amba3.apb._
import spinal.lib.bus.amba4.axi._
import spinal.lib.com.jtag.Jtag
import spinal.lib.com.jtag.sim.JtagTcp
//import spinal.lib.com.uart.sim.{UartDecoder, UartEncoder}
//import spinal.lib.com.uart.{Apb3UartCtrl, Uart, UartCtrlGenerics, UartCtrlMemoryMappedConfig}

import spinal.lib.com.uart._

import spinal.lib.graphic.RgbConfig
//import spinal.lib.graphic.vga.{Axi4VgaCtrl, Axi4VgaCtrlGenerics, Vga}
import spinal.lib.io._

//import vexriscv.hoffman.sdram._
//import vexriscv.hoffman.sdram.SdramLayout
//import vexriscv.hoffman.sdram.SdramLayout.SdramGeneration.SDR
//import vexriscv.hoffman.sdram.sdr.sim.SdramModel
//import vexriscv.hoffman.sdram.sdr.{Axi4SharedSdramCtrl, IS42x320D, SdramInterface, SdramTimings}

import spinal.lib.bus.amba3.ahblite._

import vexriscv.hoffman.sram.Axi4SharedOffChipRam
//import vexriscv.hoffman.sram.AhbLiteOffChipRam

import spinal.lib.misc.HexTools
import spinal.lib.soc.pinsec.{PinsecTimerCtrl, PinsecTimerCtrlExternal}
import spinal.lib.system.debugger.{JtagAxi4SharedDebugger, JtagBridge, SystemDebugger, SystemDebuggerConfig}

import scala.collection.mutable.ArrayBuffer
import scala.collection.Seq
import vexriscv.ip.fpu.FpuParameter




case class HoffmanConfig(axiFrequency : HertzNumber,
                       SramSize : BigInt,
                       FpgaRamSize : BigInt, 
                       //sdramLayout: SdramLayout,
                       //sdramTimings: SdramTimings,
                       cpuPlugins : ArrayBuffer[Plugin[VexRiscv]],
                       uartCtrlConfig : UartCtrlMemoryMappedConfig
                       )

object HoffmanConfig{

  def default = {
    val config = HoffmanConfig (
      axiFrequency = 2.5 MHz,
      SramSize  = 514 kB,
      FpgaRamSize = 75+2 kB, // 10k + 32 stack and 32 heap 
      //sdramLayout = IS42x320D.layout,
      //sdramTimings = IS42x320D.timingGrade7,
      uartCtrlConfig = UartCtrlMemoryMappedConfig(
        uartCtrlConfig = UartCtrlGenerics(
          dataWidthMax      = 8,
          clockDividerWidth = 20,
          preSamplingSize   = 1,
          samplingSize      = 3,
          postSamplingSize  = 1
        ),
        initConfig = UartCtrlInitConfig(
          baudrate = 38400,
          dataLength = 7,  //7 => 8 bits
          parity = UartParityType.NONE,
          stop = UartStopType.ONE
        ),
        busCanWriteClockDividerConfig = false,
        busCanWriteFrameConfig = false,
        txFifoDepth = 16,
        rxFifoDepth = 16
      ),
      cpuPlugins = ArrayBuffer(
        new PcManagerSimplePlugin(0x80000000l, false),
        //          new IBusSimplePlugin(
        //            interfaceKeepData = false,
        //            catchAccessFault = true
        //          ),
        new IBusCachedPlugin(
          resetVector = 0x80000000l,

          prediction = STATIC,
          config = InstructionCacheConfig(
            cacheSize = 4096,
            bytePerLine =32,
            wayCount = 1,
            addressWidth = 32,
            cpuDataWidth = 32,
            memDataWidth = 32,
            catchIllegalAccess = true,
            catchAccessFault = true,
            asyncTagMemory = false,
            twoCycleRam = true,
            twoCycleCache = true
          )
          //            askMemoryTranslation = true,
          //            memoryTranslatorPortConfig = MemoryTranslatorPortConfig(
          //              portTlbSize = 4
          //            )
        ),
        //                    new DBusSimplePlugin(
        //                      catchAddressMisaligned = true,
        //                      catchAccessFault = true
        //                    ),
        new DBusCachedPlugin(
          config = new DataCacheConfig(
            cacheSize         = 4096,
            bytePerLine       = 32,
            wayCount          = 1,
            addressWidth      = 32,
            cpuDataWidth      = 32,
            memDataWidth      = 32,
            catchAccessError  = true,
            catchIllegal      = true,
            catchUnaligned    = true
          ),
          memoryTranslatorPortConfig = null
          //            memoryTranslatorPortConfig = MemoryTranslatorPortConfig(
          //              portTlbSize = 6
          //            )
        ),
        new StaticMemoryTranslatorPlugin(
          ioRange      = _(31 downto 28) === 0xF
        ),
        new DecoderSimplePlugin(
          catchIllegalInstruction = true
        ),
        new RegFilePlugin(
          regFileReadyKind = plugin.SYNC,
          zeroBoot = false
        ),
        new IntAluPlugin,
        new SrcPlugin(
          separatedAddSub = false,
          executeInsertion = true
        ),
        new FullBarrelShifterPlugin,
        new MulPlugin,
        new DivPlugin,
        new HazardSimplePlugin(
          bypassExecute           = true,
          bypassMemory            = true,
          bypassWriteBack         = true,
          bypassWriteBackBuffer   = true,
          pessimisticUseSrc       = false,
          pessimisticWriteRegFile = false,
          pessimisticAddressMatch = false
        ),
        new BranchPlugin(
          earlyBranch = false,
          catchAddressMisaligned = true
        ),
        new CsrPlugin(
          config = CsrPluginConfig(
            catchIllegalAccess = false,
            mvendorid      = null,
            marchid        = null,
            mimpid         = null,
            mhartid        = null,
            misaExtensionsInit = 66,
            misaAccess     = CsrAccess.NONE,
            mtvecAccess    = CsrAccess.NONE,
            mtvecInit      = 0x80000020l,
            mepcAccess     = CsrAccess.READ_WRITE,
            mscratchGen    = false,
            mcauseAccess   = CsrAccess.READ_ONLY,
            mbadaddrAccess = CsrAccess.READ_ONLY,
            mcycleAccess   = CsrAccess.NONE,
            minstretAccess = CsrAccess.NONE,
            ecallGen       = false,
            wfiGenAsWait         = false,
            ucycleAccess   = CsrAccess.NONE,
            uinstretAccess = CsrAccess.NONE
          )
        ),
        new FpuPlugin(
           externalFpu = false,
          simHalt = false,
          p = FpuParameter(withDouble = false)
        ),
        new YamlPlugin("cpu0.yaml")
      )
    )
    config
  }
}

case class IEEE696() extends Bundle { 
  // S-100 bus 

  //val DO = master(TriStateArray(8 bits)) // GPIOL_94 
  //val DI = master(TriStateArray(8 bits)) // GPIOT_RXN00
 
  //val clkOut      = out Bool()
 
  //val sOUT   = out Bool() // GPIOT_RXN09
  //val sINP   = out Bool() // GPIOT_RXN10
  //val pDBIN  = out Bool() // GPIOT_RXP21 
  //val psync  = out Bool() // GPIOT_RXN18
  //val pstval = out Bool() // GPIOT_RXN21
  //val phi    = out Bool() // GPIOT_RXP16
  //val pwr    = out Bool() // GPIOT_RXP20 

} 

class Hoffman(val config: HoffmanConfig) extends Component{

  //Legacy constructor
  def this(axiFrequency: HertzNumber) {
    this(HoffmanConfig.default.copy(axiFrequency = axiFrequency))
  }

  import config._
  val debug = false
  val interruptCount = 4
  //def vgaRgbConfig = RgbConfig(5,6,5)

  val io = new Bundle{
    
    //Clocks / reset
    val asyncReset = in Bool()
    
    val axiClk     = in Bool()
    //val busClk     = in Bool()
    //val vgaClk     = in Bool()
    
    //Peripherals IO
 
    val SEG7           = master(TriStateArray(8 bits)) // segment display on T-35 module 
    val SBCLEDS        = master(TriStateArray(8 bits)) // LED row on FPGA board
    val IOBYTES        = master(TriStateArray(8 bits)) // DIP SWITTCH on FPGA board
    
    
    val S100STATUS  = in Bits(32 bits) // Bus stats bits map 
        
    val ieee696 = IEEE696() // Interface for S-100 bus 

    // enable transciever logic on FPGA main board 
    //val OUTPUT_ADD_EN    = out Bool()
    //val OUTPUT_STATUS_EN = out Bool()
    //val OUTPUT_CTL_EN    = out Bool()
    //val OUTPUT_DO_EN     = out Bool()
    //val OUTPUT_DI_EN     = out Bool()

    // address bus 
    val S100ADR = out UInt(19 bits)
   
    // data bus for ram 
    val biData_IN  = in Bits(8 bits)  
    val biData_OUT = out Bits(8 bits)
  
    // control for memory on FPGA board ( not over bus ) 
    val ram_cs    = out Bool() 
    val ram_oe    = out Bool()
    val ram_wr    = out Bool() // write bit of ram 
    val biData_OE = out Bits(8 bits)
    
    val ram_A16       = out Bool() 
    val ram_A17       = out Bool() 
    val ram_A18       = out Bool()   
   
    // Jtag and Uart 
    val jtag         = slave(Jtag())
    val uart         = master(Uart())

    // More complex IO 
    //val vga        = master(Vga(vgaRgbConfig))
    //val sdram      = master(SdramInterface(sdramLayout))

    val timerExternal = in(PinsecTimerCtrlExternal())
    val coreInterrupt = in Bool()
  
  }

  val resetCtrlClockDomain = ClockDomain(
    clock = io.axiClk,
    config = ClockDomainConfig(
      resetKind = BOOT
    )
  )

  val resetCtrl = new ClockingArea(resetCtrlClockDomain) {
    val systemResetUnbuffered  = False
    //    val coreResetUnbuffered = False

    //Implement an counter to keep the reset axiResetOrder high 64 cycles
    // Also this counter will automaticly do a reset when the system boot.
    val systemResetCounter = Reg(UInt(6 bits)) init(0)
    when(systemResetCounter =/= U(systemResetCounter.range -> true)){
      systemResetCounter := systemResetCounter + 1
      systemResetUnbuffered := True
    }
    when(BufferCC(io.asyncReset)){
      systemResetCounter := 0
    }

    //Create all reset used later in the design
    val systemReset  = RegNext(systemResetUnbuffered)
    val axiReset     = RegNext(systemResetUnbuffered)
    //val vgaReset     = BufferCC(axiReset)
  }

  val axiClockDomain = ClockDomain(
    clock = io.axiClk,
    reset = resetCtrl.axiReset,
    frequency = FixedFrequency(axiFrequency) //The frequency information is used by the SDRAM controller
  )

  val debugClockDomain = ClockDomain(
    clock = io.axiClk,
    reset = resetCtrl.systemReset,
    frequency = FixedFrequency(axiFrequency)
  )


  val axi = new ClockingArea(axiClockDomain) {
 
      val fpgaMem = Axi4SharedOnChipRam(
        dataWidth = 32,
        byteCount = FpgaRamSize,
        idWidth = 4
      )

      val externSram = Axi4SharedOffChipRam( 
        dataWidth = 32,
        addressAxiWidth = 32, 
        idWidth = 4
      )

      //val s100Bus = Axi4SharedS100Bus( 
      //  dataWidth = 32,
      //  addressAxiWidth = 32, 
      //  idWidth = 4
      //)

      val apbBridge = Axi4SharedToApb3Bridge(
        addressWidth = 19,
        dataWidth    = 32,
        idWidth      = 4
      )

      val gpioSEG7Ctrl        = Apb3Gpio( gpioWidth = 8,  withReadSync = true )
      val gpioSBCLEDSCtrl     = Apb3Gpio( gpioWidth = 8,  withReadSync = true )
      val gpioIOBYTESCtrl     = Apb3Gpio( gpioWidth = 8,  withReadSync = true )
      val gpioS100STATUSCtrl  = Apb3Gpio( gpioWidth = 32, withReadSync = true )

      val timerCtrl = PinsecTimerCtrl()
      val uartCtrl = Apb3UartCtrl(uartCtrlConfig)
      uartCtrl.io.apb.addAttribute(Verilator.public)

      val core = new Area{
        val config = VexRiscvConfig(
          plugins = cpuPlugins += new DebugPlugin(debugClockDomain)
        )

        val cpu = new VexRiscv(config)
        var iBus : Axi4ReadOnly = null
        var dBus : Axi4Shared = null
        //var cBus : AhbLite3 = null
        for(plugin <- config.plugins) plugin match{
          case plugin : IBusSimplePlugin => iBus = plugin.iBus.toAxi4ReadOnly()
          case plugin : IBusCachedPlugin => iBus = plugin.iBus.toAxi4ReadOnly()
          case plugin : DBusSimplePlugin => dBus = plugin.dBus.toAxi4Shared()
          case plugin : DBusCachedPlugin => dBus = plugin.dBus.toAxi4Shared(true)
          case plugin : CsrPlugin        => {
            plugin.externalInterrupt := BufferCC(io.coreInterrupt)
            plugin.timerInterrupt := timerCtrl.io.interrupt
          }
          case plugin : DebugPlugin      => debugClockDomain{
            resetCtrl.axiReset setWhen(RegNext(plugin.io.resetOut))
            io.jtag <> plugin.io.bus.fromJtag()
          }
          case _ =>
        }
      }

      val axiCrossbar = Axi4CrossbarFactory()

      axiCrossbar.addSlaves(
        fpgaMem.io.axi        -> (0x80000000L,   FpgaRamSize),
        externSram.io.axi     ->  (0x20000000L,   SramSize),
        //s100Bus.io.axi        ->  (0x30000000L,   SramSize),
        apbBridge.io.axi      -> (0xF0000000L,   1 MB)
      )

      //axiCrossbar.addConnections(
      //  core.iBus       -> List(fpgaMem.io.axi,externSram.io.axi,s100Bus.io.axi),
      //  core.dBus       -> List(fpgaMem.io.axi,externSram.io.axi,s100Bus.io.axi,apbBridge.io.axi)
      //)
   
      axiCrossbar.addConnections(
        core.iBus       -> List(fpgaMem.io.axi,externSram.io.axi),
        core.dBus       -> List(fpgaMem.io.axi,externSram.io.axi,apbBridge.io.axi)
      )
      axiCrossbar.addPipelining(apbBridge.io.axi)((crossbar,bridge) => {
        crossbar.sharedCmd.halfPipe() >> bridge.sharedCmd
        crossbar.writeData.halfPipe() >> bridge.writeData
        crossbar.writeRsp             << bridge.writeRsp
        crossbar.readRsp              << bridge.readRsp
      })

      axiCrossbar.addPipelining(fpgaMem.io.axi)((crossbar,ctrl) => {
        crossbar.sharedCmd.halfPipe()  >>  ctrl.sharedCmd
        crossbar.writeData            >/-> ctrl.writeData
        crossbar.writeRsp              <<  ctrl.writeRsp
        crossbar.readRsp               <<  ctrl.readRsp
      })

      //axiCrossbar.addPipelining(externSram.io.axi)((crossbar,ctrl) => {
      //  crossbar.sharedCmd.halfPipe()  >>  ctrl.sharedCmd
      //  crossbar.writeData            >/-> ctrl.writeData
      //  crossbar.writeRsp              <<  ctrl.writeRsp
      //  crossbar.readRsp               <<  ctrl.readRsp
      //})
  
      axiCrossbar.addPipelining(core.dBus)((cpu,crossbar) => {
        cpu.sharedCmd             >>  crossbar.sharedCmd
        cpu.writeData             >>  crossbar.writeData
        cpu.writeRsp              <<  crossbar.writeRsp
        cpu.readRsp               <-< crossbar.readRsp //Data cache directly use read responses without buffering, so pipeline it for FMax
      })

      axiCrossbar.build()

      val apbDecoder = Apb3Decoder(
        master = apbBridge.io.apb,
        slaves = List(
          gpioSEG7Ctrl.io.apb        -> (0x00000, 16 Bytes),
          gpioSBCLEDSCtrl.io.apb     -> (0x01000, 16 Bytes),
          gpioIOBYTESCtrl.io.apb     -> (0x02000, 16 Bytes),
          gpioS100STATUSCtrl.io.apb  -> (0x06000, 16 Bytes), 
          uartCtrl.io.apb            -> (0x10000, 16 Bytes),
          timerCtrl.io.apb           -> (0x20000, 16 Bytes)
        )
      )
      
      val addrReg        = Reg(UInt(19 bits)) // next address 
      val csReg          = Reg(Bool()) 
      val oeReg          = Reg(Bool()) 
      val wrReg          = Reg(Bool()) 
      val biData_OE_Reg  = Reg(Bits(8 bits))
      
      val biData_IN_Reg  = Reg(Bits(8 bits))  
      val biData_OUT_Reg = Reg(Bits(8 bits))

      io.S100ADR       := addrReg
      io.ram_A16       := io.S100ADR(16) 
      io.ram_A17       := io.S100ADR(17) 
      io.ram_A18       := io.S100ADR(18) 
  
      io.ram_cs            := csReg 
      io.ram_oe            := oeReg
      io.ram_wr            := wrReg
      io.biData_OE         := biData_OE_Reg   
      
      io.biData_OUT        := biData_OUT_Reg 

      externSram.io.rddata := biData_IN_Reg 
      //s100Bus.io.rddata    := biData_IN_Reg 

      when( externSram.io.addr >= U"x20000000" && 
          externSram.io.addr <= U"x2007ffff" ) {
        //assert(False,"SRAM",NOTE)
        addrReg := externSram.io.addr.resized
        csReg                := externSram.io.cs 
        oeReg                := externSram.io.oe
        wrReg                := externSram.io.wr
        biData_OE_Reg        := externSram.io.boe 
        biData_IN_Reg        := io.biData_IN 
        biData_OUT_Reg       := externSram.io.wrdata
      }

      //when( s100Bus.io.addr >= U"x30000000" && 
      //    s100Bus.io.addr <= U"x3007ffff" ) {
      //  assert(False,"BUS",NOTE)
      //  addrReg := s100Bus.io.addr.resized
      //  csReg             := s100Bus.io.cs 
      //  oeReg             := s100Bus.io.oe
      //  wrReg             := s100Bus.io.wr
      //  biData_OE_Reg     := s100Bus.io.boe 
      //  biData_IN_Reg     := io.biData_IN
      //  biData_OUT_Reg    := s100Bus.io.wrdata
      //}
    
      //io.ieee696.clkOut := io.axiClk

  }

  io.timerExternal  <> axi.timerCtrl.io.external
  io.uart           <> axi.uartCtrl.io.uart 

  io.SEG7        <> axi.gpioSEG7Ctrl.io.gpio 
  io.SBCLEDS     <> axi.gpioSBCLEDSCtrl.io.gpio 
  io.IOBYTES     <> axi.gpioIOBYTESCtrl.io.gpio 

  io.S100STATUS  <> axi.gpioS100STATUSCtrl.io.gpio.read
    
}



// This is a bit ugly in that we are basically duplicating lots of code to get different builds
// but I do not know enough about Scala yet to do this more elgantly ... 

// ============================================================================================
// Efinity T-35 Target  
// ============================================================================================

object HoffmanWithMemoryInit{
  def main(args: Array[String]) {
    val config = SpinalConfig()
    config.generateVhdl({
      val toplevel = new Hoffman(
        HoffmanConfig.default.copy(
          uartCtrlConfig = UartCtrlMemoryMappedConfig(
            uartCtrlConfig = UartCtrlGenerics(
              dataWidthMax      = 8,
              clockDividerWidth = 20,
              preSamplingSize   = 1,
              samplingSize      = 3,
              postSamplingSize  = 1
            ),
            initConfig = UartCtrlInitConfig(
              baudrate = 38400,
              dataLength = 7,  //7 => 8 bits
              parity = UartParityType.NONE,
              stop = UartStopType.ONE
            ),
            busCanWriteClockDividerConfig = false,
            busCanWriteFrameConfig = false,
            txFifoDepth = 16,
            rxFifoDepth = 16
          )
        )
      )
      HexTools.initRam(toplevel.axi.fpgaMem.ram, "src/main/c/hoffman/monitor/monitor.hex", 0x80000000l)
      toplevel
    })
  }
}

// ============================================================================================
// GHDL Simulator 
// ============================================================================================
object HoffmanWithMemoryInitGhdl{
   def main(args: Array[String]) {
    val config = SpinalConfig()
    config.generateVhdl({
      val toplevel = new Hoffman(
        HoffmanConfig.default.copy(
          uartCtrlConfig = UartCtrlMemoryMappedConfig(
            uartCtrlConfig = UartCtrlGenerics(
              dataWidthMax      = 8,
              clockDividerWidth = 20,
              preSamplingSize   = 1,
              samplingSize      = 3,
              postSamplingSize  = 1
            ),
            initConfig = UartCtrlInitConfig(
              baudrate = 115200,
              dataLength = 7,  //7 => 8 bits
              parity = UartParityType.NONE,
              stop = UartStopType.ONE
            ),
            busCanWriteClockDividerConfig = false,
            busCanWriteFrameConfig = false,
            txFifoDepth = 16,
            rxFifoDepth = 16
          )
        )
      )
      HexTools.initRam(toplevel.axi.fpgaMem.ram, "src/main/c/hoffman/monitor/monitor.hex", 0x80000000l)
      toplevel
    })
  }
}



