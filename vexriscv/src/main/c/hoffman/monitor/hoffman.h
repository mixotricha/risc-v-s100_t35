#ifndef __MURAX_H__
#define __MURAX_H__

#include "timer.h"
#include "prescaler.h"
#include "interrupt.h"
#include "gpio.h"

#ifndef SIMULATOR 
	#define SCALAR 1      // Relative Scalar division for efinity build 
#else
	#define SCALAR 10000    // Relative Scalar division for simulator. Use for any of your own timing since 
#endif

#define SEG7       ((Gpio_Reg*)(0xF0000000)) // segments
#define SBCLEDS    ((Gpio_Reg*)(0xF0001000)) // sbc leds
#define IOBYTES    ((Gpio_Reg*)(0xF0002000)) // iobytes
#define S100ADR    ((Gpio_Reg*)(0xF0003000)) // addr bus ( 19 of 24 bits )
#define S100DODATA ((Gpio_Reg*)(0xF0004000)) // bus ( data bits )
#define S100DIDATA ((Gpio_Reg*)(0xF0005000)) // bus ( data bits )
#define S100STATUS ((Gpio_Reg*)(0xF0006000)) // bus ( status bits )
#define BIDATAIN   ((Gpio_Reg*)(0xF0007000)) // ram bi rd on FPGA board 
#define BIDATAOUT  ((Gpio_Reg*)(0xF0008000)) // ram bi wr on FPGA board  
#define UART_BASE  ((Uart_Reg*)(0xF0010000)) // on board uart 

#define BINARY_TO_HEX(binary) \
    ((binary & 0b1000 ? 0x8 : 0) | \
     (binary & 0b0100 ? 0x4 : 0) | \
     (binary & 0b0010 ? 0x2 : 0) | \
     (binary & 0b0001 ? 0x1 : 0))

#define TIMER_PRESCALER ((Prescaler_Reg*)0xF0020000)
#define TIMER_INTERRUPT ((InterruptCtrl_Reg*)0xF0020010)
#define TIMER_A ((Timer_Reg*)0xF0020040)
#define TIMER_B ((Timer_Reg*)0xF0020050)


#endif /* __MURAX_H__ */
