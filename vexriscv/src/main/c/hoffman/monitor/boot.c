/***************************************************************************
 *   Damien Towning         (connolly.damien@gmail.com) 2023               *
 *                                                                         *
 *   This file is part of the T-35 Risc-V Hoffman project                  *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <assert.h>
#include <locale.h>
#include <errno.h>
#include <sys/types.h>
#include <string.h>

#include "mempattern.h"
#include "io.h"
#include "console.h"
#include "hoffman.h"

extern char *_heap_start; 
extern char *_heap_end;
extern char *_heap_size; 
extern char *_stack_start; 
extern void* _sbrk(incr);

int port = 0; // Global for console 
//typedef void (*FunctionPointer)(int);
#define reverse_bytes_32(num) ( ((num & 0xFF000000) >> 24) | ((num & 0x00FF0000) >> 8) | ((num & 0x0000FF00) << 8) | ((num & 0x000000FF) << 24) )

// ======================================
// Do a delay 
// ======================================
void delay(uint32_t loops){
    for(int i=0;i<loops;i++){
        int tmp = SEG7->OUTPUT;
    }
}

// ======================================
// write a word out to gpio 
// ======================================
inline void _write_word(void* address,uint32_t value)
{
  *(( volatile uint32_t* )( address ))=value;
}

// ======================================
// 0 = on / 1 = off 
// ---------    
//    -0- 
// 5 |   | 1
//    -6- 
// 4 |   | 2
//    -3-  .7 
// ---------   
// ======================================

// ======================================
// print out table of symbols 
// ======================================
void display_symbols() { 
  
  int i=0;
  int many_times = 595535 / SCALAR; // How many times to display a character is easier than a wait 
                           // and ensures some wasteful loop is not optimised out 

  // Remember each symbol has the dp leading 
  unsigned char symbols[15] = {0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,0xF8,0x80,0x90,0x88,0x83,0xC6,0xA1,0x86,0x7F};
                            
  // enable the write SEG7 pins   
  //_write_word((void*)SEG7+GPIO_OUTPUT_EN,0xFF);

  // Write out to the segment display a set of symbols. We write them multiple times so we can see them. 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[0]; } // 0 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[1]; } // 1 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[2]; } // 2 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[3]; } // 3 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[4]; } // 4 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[5]; } // 5 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[6]; } // 6 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[7]; } // 7 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[8]; } // 8 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[9]; } // 9 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[10]; } // A 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[11]; } // B 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[12]; } // C 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[13]; } // D 
  
}

// ======================================
// Generate a heart beat and count through 
// sbc leds and T-35 module segments 
// ======================================
void heart_beat() { 
  int i=0;
  int ii = 0; 
  int many_times = 595535 / SCALAR; // How many times to display a character is easier than a wait 
                                    
  // Remember each symbol has the dp leading disabled 
  unsigned char symbols[16] = {0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,0xF8,0x80,0x90,0x88,0x83,0xC6,0xA1,0x86,0x8E};
  
  SEG7->OUTPUT_ENABLE = 0x00000000;
  SBCLEDS->OUTPUT_ENABLE = 0x00000000;

  for ( i = 0; i < 16; i++ ) { 
   for ( ii = 0; ii < many_times; ii++ )  { SEG7->OUTPUT=0x79; SBCLEDS->OUTPUT=symbols[i]; } // 1. , symbol[i]  
   for ( ii = 0; ii < many_times; ii++ )  { SEG7->OUTPUT=0xF9; SBCLEDS->OUTPUT=symbols[i]; } // 1  , symbol[i]  
  }
   
}

// ==============================================
// display state of iobytes dipswitch on sbc leds
// while counting on T-35 module segment display 
// ==============================================
void show_iobytes() { 
  int i=0;
  int ii = 0; 
  int many_times = 595535 / SCALAR; // How many times to display a character is easier than a wait 
                                    // and ensures some wasteful loop is not optimised out 
  // Remember each symbol has the dp leading disabled 
  unsigned char symbols[16] = {0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,0xF8,0x80,0x90,0x88,0x83,0xC6,0xA1,0x86,0x8E};
  
  SEG7->OUTPUT_ENABLE = 0x00000000;
  SBCLEDS->OUTPUT_ENABLE = 0x00000000;

  for ( i = 0; i < 16; i++ ) { 
   for ( ii = 0; ii < many_times; ii++ )  { SEG7->OUTPUT=symbols[i]; SBCLEDS->OUTPUT=IOBYTES->INPUT; } // symbol[i] , iobytes  
   for ( ii = 0; ii < many_times; ii++ )  { SEG7->OUTPUT=symbols[i]; SBCLEDS->OUTPUT=IOBYTES->INPUT; } // symbol[i] , iobytes 
  } 

}

// write to address bus 19 bits  
void _write_address_bus(int addr) { 
  // put an address on the bs
  S100ADR->OUTPUT_ENABLE = 0x00000000;
  S100ADR->OUTPUT=addr; 
}

//void _write_data_bus(int bank_a,int bank_b) {}
//void _read_data_bus() { }

// ======================================
// Test fpu 
// ======================================
void test_fpu()
{
    float a = 2.5;
    float b = 3.0;
    float result;
    asm volatile(
        "fmul.s %[result], %[a], %[b]\n\t" // multiply a and b, result stored in result
        : [result] "=f" (result)           // output: result is a float
        : [a] "f" (a), [b] "f" (b)         // inputs: a and b are floats
    );
    // One might think checking the FCSR would be more elgant but is it big endian? Little endian? 
    // I have considered it not reliable. Multiplication seems like a good test. 
    if (result == 7.5) {
        console_write(port,"Floating-point unit available!\r\n");
    } else {
        console_write(port,"No floating-point unit available.\r\n");
    }
}

// ======================================
// Convert a float to a char 
// ======================================
char *float_to_char(float num, int precision) { 
    char* buffer = malloc(256 * sizeof(char)); // allocate buffer to hold the converted number
    int whole = (int)num; // integer part of the float number
    float decimal = num - whole; // decimal part of the float number
    int len = sprintf(buffer, "%d.", whole); // convert the integer part to string
    for (int i = 0; i < precision; i++) { // convert the decimal part to string
        decimal *= 10;
        int digit = (int)decimal;
        buffer[len++] = digit + '0';
        decimal -= digit;
    }
    buffer[len] = '\0'; // add null terminator to the string  
    return buffer;  
}

// ======================================
// Do a math test
// ======================================
void math_test() { 

  char* result;

  console_write(port,"Doing simple math test\r\n"); 
  result = float_to_char(3.14159, 5);
  console_write(port,"Simple test of output of floating point value 3.14159 is %s with precision of 5\r\n", result);
  free(result);

  //result = float_to_char( sqrt(60.8),6);
  console_write(port,"sqrt(60.8) %s with precision of 6\r\n", result);
  free(result);

}

// ======================================
// input and return a hex value 
// ======================================
int get_hex_value() { 

    char input[10]; // Assuming the maximum length of input is 10 characters
    char c;
    int i = 0;

    while ((c = readchar()) != '\r') {
        if (i < 9) { // Ensure we don't exceed array bounds
            input[i++] = c;
        }
    }
    input[i] = '\0'; // Null-terminate the string
    
    // Check if the input starts with "0x" and has at least two additional characters
    if (i >= 3 && input[0] == '0' && (input[1] == 'x' || input[1] == 'X')) {
        int hexValue = 0;
        for (int j = 2; j < i; j++) {
            char hexChar = toupper(input[j]); // Convert to uppercase
            int hexDigit;
            if (isdigit(hexChar)) {
                hexDigit = hexChar - '0';
            } else if (hexChar >= 'A' && hexChar <= 'F') {
                hexDigit = hexChar - 'A' + 10;
            } else {
                return -1;
            }
            hexValue = (hexValue << 4) | hexDigit; // Shift and combine
        }
        return hexValue;
    } else {
        return -1;
    }

    return -1;
}

// ======================================
//  write to io port 
// ======================================
void io_port() { 
    int addr = -1; 
    while ( addr == -1 ) {  
        console_write(port,"Input IO port address:"); 
        addr = get_hex_value(); 
    }
    console_write(port,"Got to here %d\r\n",addr); 
    _write_address_bus(addr);  
}

// ======================================
// print out a hex value 
// ======================================
void hex_dump(void *mem,int numWords)
{ 
  uint32_t *pmem = mem;
  int i;
    for(i=0;i<numWords;i++) {
      if ((i % 4)==0) { // Write Memory address for every four words
        console_write(port,"\r\n%lx : ",(uint32_t)&pmem[i]);
      }
      console_write(port," %lx ",reverse_bytes_32(pmem[i]));
    }
}

typedef struct {
    int id;
    char name[20];
    double value;
} ComplexObject;

// ======================================
// do a malloc test
// ======================================
void malloc_test() { 

  //free(0); 

  printk("Doing a malloc test\r\n");   
  
  //srand(5764); 

  int i = 0; 
    int byte_size = 5;
    //int byte_size = rand() % 11;
    int* ptr0 = (int *) malloc(sizeof(int)*byte_size);
    int* ptr1 = (int *) malloc(sizeof(int)*byte_size);
    int* ptr2 = (int *) malloc(sizeof(int)*byte_size);
    if (ptr0 == NULL || ptr1 == NULL || ptr2 == NULL ) {
      printk("Memory allocation failed\r\n");
      printk("ptr0 = %p\r\n", ptr0);
      printk("ptr1 = %p\r\n", ptr1);
      printk("ptr2 = %p\r\n", ptr2);
      return 1;
    }
    printk("ptr0 = %p\r\n", ptr0);
    printk("ptr1 = %p\r\n", ptr1);
    printk("ptr2 = %p\r\n", ptr2);

    // Set the second byte of each pointer to a different value
    for ( i = 0; i < byte_size; i++ ) { 
      *( (int*) ptr0 + i ) = i;
      *( (int*) ptr1 + i ) = i;
      *( (int*) ptr2 + i ) = i;
    }
    printk("\r\nptr0:");
    for ( i = 0; i < byte_size; i++ ) { 
      printk(" %x ", *((int*)ptr0 + i));
    }   
    printk("\r\nptr1:"); 
    for ( i = 0; i < byte_size; i++ ) { 
      printk(" %x ", *((int*)ptr1 + i));
    }
    printk("\r\nptr2:");
    for ( i = 0; i < byte_size; i++ ) { 
      printk(" %x ", *((int*)ptr2 + i));
    }
    printk("\r\n"); 
    free(ptr0); 
    free(ptr1); 
    free(ptr2);
    
    printk("More complex memory test using a complex object...\r\n");     
 
    void* obj_ptr = malloc(sizeof(ComplexObject));

    if(obj_ptr != NULL) { 
        printk("Complex object malloced\r\n"); 
    }
    else { 
        printk("Failed to malloc complex object\r\n"); 
    }
     
    // Initialize the object
    ComplexObject* obj = (ComplexObject*) obj_ptr;
    obj->id = 42;
    strcpy(obj->name, "Test Object");
    obj->value = 3.14;

    // Verify the object's values
    printk("obj->id    : %d == 42\r\n" , obj->id); 
    printk("obj->name  : %s == 'Test Object'\r\n", obj->name ); 
    printk("obj->value : %f == 3.14\r\n",obj->value);     
    free(obj_ptr);

    // Our malloc at this point is not smart enough to reclaim memory. 
    // it just wraps sbrk 
    //printk("Test of free\r\n"); 
    //void* aptr1 = malloc(16);
    //if ( aptr1 != NULL ) { printk("Ptr1 allocated\r\n"); }
    //void* aptr2 = malloc(16);
    //if ( aptr2 != NULL ) { printk("Ptr2 allocated\r\n"); }
    // Free the first block
    //free(aptr1);
    // Try to allocate a new block of the same size
    //void* aptr3 = malloc(16);
    //if ( aptr3 != NULL ) { printk("Ptr3 allocated\r\n"); }
    // Check if the new block is the same as the freed block
    //if ( aptr1 == aptr3 ) { 
    //    printk("Memory released correctly\r\n"); 
    //}
    //free(aptr2);
    //free(aptr3);

    return 0;

}

// ======================================
// cycle check for speed test
// ======================================
static inline unsigned long rdcycle(void)
{
    unsigned long cycles;
    asm volatile ("rdcycle %0" : "=r" (cycles));
    return cycles;
}

// ======================================
// get cpu speed
// ======================================
void get_cpu_speed() { 

    unsigned long cycles;
    unsigned long start_time, end_time;
    float cpu_speed;

    // Get the starting cycle count
    start_time = rdcycle();

    // Perform some dummy operations to consume CPU cycles
    int i;
    //for (i = 0; i < 1000000; i++) {
    //    asm volatile ("");
    //}
    //sqrt(60); 
    //sqrt(60); 
    //sqrt(60); 
    
    // Get the ending cycle count
    end_time = rdcycle();

    // Calculate the CPU speed in MHz
    cycles = end_time - start_time;
    cpu_speed = (float)cycles / 1000000;

    char* result;
    result = float_to_char(cpu_speed, 3);
    // Print the CPU speed
    console_write(port,"CPU speed is %s MHz\r\n", result);
    free(result); 

}

// ======================================
// print system info 
// ======================================
void system_info() {

    console_write(port,"T-35 : Smoogle The S-100 Thing Is Hurfling\r\n");
    
    get_cpu_speed();
    console_write(port,"Scalar value is %d\r\n",SCALAR); 
    console_write(port,"heap start: %p\r\n", (void*) &_heap_start); 
    console_write(port,"heap end: %p\r\n", (void*) &_heap_end);
     console_write(port,"heap size: %p\r\n", (void*) &_heap_size);  
    console_write(port,"stack start: %p\r\n", (void *)&_stack_start );
  
    console_write(port,"The size of an int is: %d bytes\r\n", sizeof(int));

     #ifdef _REENTRANT
        #define IS_REENTRANT 1
    #else
        #define IS_REENTRANT 0
    #endif

    console_write(port,"Nano newlib is %s reentrant\r\n", IS_REENTRANT ? "" : " not");

    #if defined(__riscv_flen) && __riscv_flen == 32
        console_write(port,"Floating-point support is available and is 32-bit in Nano Newlib\r\n");
  
    #elif defined(__riscv_flen) && __riscv_flen == 64
        console_write(port,"Floating-point support is available and is 64-bit in Nano Newlib\r\n");
    #else
        console_write(port,"Floating-point support is not available in Nano Newlib\r\n");
    #endif

    #ifdef SIMULATOR
        console_write(port,"Compiled for Simulator\r\n"); 
    #endif

    //#ifdef _WANT_IO_C99_FORMATS
    //  console_write(port,"_WANT_IO_C99_FORMATS\r\n"); 
    //#endif  

    //#ifdef STRING_ONLY
        //console_write(port,"STRING_ONLY is defined\r\n");
    //#else
        //console_write(port,"STRING_ONLY is not defined\r\n");
    //#endif  

}



// ======================================
// enter main 
// ======================================
void main() {
  
       

    console_write(port,"heap start: %p\r\n", (void*) &_heap_start); 
    console_write(port,"heap end: %p\r\n", (void*) &_heap_end); 
      console_write(port,"heap size: %p\r\n", (void*) &_heap_size);
    console_write(port,"stack start: %p\r\n", (void *)&_stack_start );

    //TxD on pin GPIOL_43
    //RxD on pin GPIOL_45
    S100STATUS->OUTPUT_ENABLE = 0x00;
    S100STATUS->OUTPUT = BUSIDLE;    
 
    //system_info(); 
    char c = 0, old_c = 0;
    char d = 0; 
    char old_d = 0;  
    int iobytes = 0; 
    int blocksz = 6;

    int *memptr = &_heap_start;
     //int *memptr = &sram;
    //uint32_t *memptr = malloc(100); 
    //console_write(port,"memory allocated at: %lx\r\n",memptr);
    //FunctionPointer ptrFunction = (FunctionPointer)0x80000400;
  
    while(1) {  
        if ( IOBYTES->INPUT == 0xFF ) { 
            console_write(port,"Houston the clock is running!\r\n"); 
            heart_beat(); 
            show_iobytes(); 
            while(1) { 
                console_write(port,">"); 
                c=readchar();
                console_write(port,"echo: %c\r\n",c);
            }
        }
        else if (IOBYTES->INPUT == 0xFB ){ 
            while(1) { 
                heart_beat();
            } 
        } 
        else { 
            if ( IOBYTES->INPUT == 0xFC ) { 
                port = 0x0AC; // switch to serial I/O board  
            } 
            //console_write(port,"\r\n(r)-memtest (d)-memmap (i)-block_size (j)-info (k)-segments (l)-malloc (m)-math");
            //console_write(port,"\r\n(e)-hexvalue (h)-sram write (n)-sram read"); 
            console_write(port,"\r\n>"); 
            //while ( c == old_c ) { 
                c=console_read(port);
            //}
            //old_c = c; 
            console_write(port,"%c\r\n",c); 
        
            if ( (c=='r') || (c=='R')) {
                typedef void (*Reset_Handler)(void);
                Reset_Handler reset_handler = (Reset_Handler)0x20000000;
                reset_handler();      
                 //ptrFunction(42); 
                //console_write(port,"\r\nWriting test pattern...\r\n"); 
                //writepattern(memptr,blocksz);
                //console_write(port,"Read Back Errors: %d \r\n",verifypattern(memptr,blocksz));  
            } 
            if ( (c=='d') || (c=='D')) {
                hex_dump(memptr,blocksz);
            }
            if ( (c=='i') || (c=='I')){ 
                blocksz+=64; 
            }
            if ( (c=='j') || (c=='j')){ 
                system_info(); 
            }
            if ( (c=='k') || (c=='k')){ 
                display_symbols();
            }
            if ( (c=='L') || (c=='l')){ 
                malloc_test();
            }
            if ( (c=='M') || (c=='m')){ 
                math_test();
            }
            if ( (c=='e') || (c=='E')){ 
                //console_write(0x0AC, "\r\nHello World...\r\n"); 
                int mc = 0;  
                for (  mc = 0; mc < 510; mc++ ) { io_mem_out(mc,mc); } 
            }
            if ( (c=='f') || (c=='F')){    
                int value = io_mem_in(0x01);
                console_write(port," %lx \r\n", value );                  
            }
            if ( (c=='g') || (c=='G')){    
               console_write(port,"Writing to address bus\r\n");
                volatile uint8_t *memory_location = (uint8_t *)0x20000000;
                *memory_location = 'b'; 
            }

            // This is resulting in one byte writes 
            if ( (c=='o') ){    
                char *memory_location = ( char *)0x20000000 ;
                for (  int i = 0; i < 5; i++ ) { 
                 *(memory_location+i) = 0x60+i;
                 //uint8_t value = 0x60+i; // don't generate a read for the memory map by fetching ... 
                 //console_write(port,"%lx: %lx\r\n",memory_location+i,value);  
                }
            }
            if ( (c=='p') ){    
                short int *memory_location = ( short int *)0x20000000 ;
                for (  int i = 0; i < 5; i++ ) { 
                    *(memory_location+i) = 0xcc60+i;
                }
            }
            if ( (c=='q') ){    
                int *memory_location = ( int *)0x20000000; 
                for (  int i = 0; i < 5; i++ ) { 
                  *(memory_location+i) = 0xdeedbbcc+i;
                }
            }
 
            // This is resulting in four byte reads 
            if ( (c=='n') || (c=='N')){   
                char *memory_location = ( char *)0x20000000; 

                int i = 0;
                int j = 0;
                int k = 0; 
                console_write(port,"\r\n%lx: ",memory_location+i); 
                while ( i < 32 && k == 0 ) {    
                    char value = *(memory_location+i);
                    console_write(port," %lx ",value);    
                    j++;     
                    if ( j > 15 ) { 
                        j = 0; 
                        console_write(port,"\r\n%lx: ",memory_location+i);
                    }
                    i++;               
                }   
            }

            if ( (c=='z') || (c=='Z')){   
                char *memory_location = ( char *)0x20000000;   
                int i = 0, j = 0; 
                char value = 0;
                while( i < (512*1024) && j == 0 ) {
                  value = *(memory_location+i);    
                  console_write(port,"%lx: %lx\r\n",memory_location+i,value);          
                  if ( value != 0xAAAAAAAA ) { 
                    console_write(port,"%lx: %lx ( failed ) \r\n",memory_location+i,value); 
                    j = 1; 
                  } 
                  i++;   
                }    
            }
               
        } 
    }
    
}

// ======================================
// handle irq
// ======================================
void irqCallback(){
}

