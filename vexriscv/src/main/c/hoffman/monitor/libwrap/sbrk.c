/* See LICENSE of license details. */

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include "console.h"

//#include "../uart.h"
//#include "../console.h"

//caddr_t __wrap_sbrk(ptrdiff_t incr)
//{
//  printk("nbytes %d\r\n",incr); 
//  if (!_heap_end) _heap_end = (caddr_t)&_end;
//  _prev_heap_end = _heap_end; 
//  _heap_end += incr;
//  return _heap_end;
//}

/*void *__wrap_sbrk(incr)
     int incr;
{
   //extern char   end; 
   //static char * heap_end;
   //char *        prev_heap_end;

   //if (_heap_end == 0) _heap_end = &_end;

  // _prev_heap_end = _heap_end;
  // _heap_end += incr;

  // return (void *) _prev_heap_end;
//}*/



//void *__wrap_sbrk(ptrdiff_t incr)
//{  
//  printk("Well got to here at least..."); 
//  extern char _end[];
//  extern char _heap_end[];
//  static char *curbrk = _end;
//  if (curbrk + incr < _end) return NULL - 1;
//  curbrk += incr;
//  return curbrk - incr;
//}

//extern caddr_t _end;
//extern static caddr_t _heap_end = NULL;
//extern caddr_t _prev_heap_end;

//caddr_t sbrk(int incr)
//{

 //   printk("Well got to here at least..."); 
//
  //  if (_heap_end == NULL) {
    //    _heap_end = &_end;
   // }

   // _prev_heap_end = _heap_end;

   // if (brk(_prev_heap_end + incr) == -1) {
    //    errno = ENOMEM;
      //  return (caddr_t) -1;
   // }

   // _heap_end += incr;
   // return _prev_heap_end;
//}

extern char *_end; /* Set by linker.  */
extern char *end; /* Set by linker.  */

extern char *_heap_end;
extern char *_heap_start; 
extern char *_prev_heap_end;
extern char *_stack_start;

char *heap_ptr;

/*
 * sbrk -- changes heap size size. Get nbytes more
 *         RAM. We just increment a pointer in what's
 *         left of memory on the board.
 */

//heap start: 0x80010400
//heap end: 0x80020400
//heap size: 0x10000
//stack start: 0x80030400

char *__wrap__sbrk(int nbytes)
{
  //printk("Using this sbrk\r\n"); 
  char        *base;
  if (!heap_ptr) heap_ptr = (char *)&_heap_start;
  //heap_ptr = (void *) ((((unsigned long) heap_ptr) + 7) & ~7);
  
  if ( (heap_ptr+nbytes) > (char *)&_heap_end ) { 
    printk("Tried to go beyond bottom of heap(+): %p\r\n", (void*) &heap_ptr);
    return NULL; 
  } 

  if ( (heap_ptr+nbytes) < (char *)&_heap_start ) { 
    printk("Tried to go above top of heap(-): %p\r\n", (void*) &heap_ptr);
    return NULL;  
  }

  base = heap_ptr;
  heap_ptr += nbytes;

  return base;
}

void *__wrap__free_r(void *ptr) {
  //printk("Using this free\r\n"); 
  if (ptr == NULL) return;
  char *base = (char *)&_heap_start;
  // Check if the pointer is within the heap range
  if (ptr >= base && ptr < heap_ptr) {
    // Find the size of the block to free
    size_t size = (size_t)(heap_ptr - (char *)ptr);
    // Update the heap pointer
    heap_ptr -= size;
  }
}


char *__wrap__malloc_r(int dumb, size_t size) {
    //printk("Using this malloc: %d \r\n",size); 
    //void *ptr = sbrk(0);
    char *request = sbrk(size);
    if (request == (void *) -1) return NULL; // out of memory
    return request; 
}
