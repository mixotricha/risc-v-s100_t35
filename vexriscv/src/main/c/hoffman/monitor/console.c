// "Borrowed" from RISC-V proxy kernel
// See LICENSE for license details.



#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <ctype.h>
#include "console.h"
#include "io.h" 


/*static void vprintk(const char* s, va_list vl)
{
  static char out[256]; // make buffer so that it works also when low on stack space
  vsnprintf(out, sizeof(out), s, vl);
  write_console(out);
}

void printk(const char* s, ...)
{
  
  static char out[256]; // make buffer so that it works also when low on stack space
  va_list vl;

  va_start(vl, s);
  vsnprintf(out, sizeof(out), s , vl);
  va_end(vl);
  write_console(out); 
}*/

void print(const char*str){
  while(*str){
    uart_write(UART,*str);
    str++;
  }
}

void println(const char*str){
  print(str);
  uart_write(UART,'\n');
}


void printk(const char* s, ...)
{
  static char out[256]; // make buffer so that it works also when low on stack space
  va_list vl;
  va_start(vl, s);
  vsnprintf(out, sizeof(out), s , vl);
  va_end(vl);
  print(out); 
}

void io_print(int port,const char*str){
  while(*str){
    io_out(port,*str);
    str++;
  }
}

void console_write(int port, const char* s, ...)
{
  static char out[256]; // make buffer so that it works also when low on stack space
  va_list vl;
  va_start(vl, s);
  vsnprintf(out, sizeof(out), s , vl);
  va_end(vl);
  if ( port == 0 ) { 
    print(out); 
  }  
  else { 
    io_print(port,out); 
  }
}


// =====================================
// blocking read a char from uart on fpga board
// =====================================
char readchar()
{
  // read a single character from the UART
  uint32_t data = uart_read(UART);
  //printk("Received data: 0x%08X\n", data);
  char c = (char)(data & 0xFF);
  //char c = (char)((data >> 24) & 0xFF);
  //printk("char: (%c)\n",c);
  return c; 
}

// ==========================================
// blocking read a char from an i/o port on s-100 bus
// ==========================================
int console_read(int port)
{
  int chr = 0;
  //int status = 0 , old_status = 0; 
  if ( port == 0 ) { 
    uint32_t data = uart_read(UART);
    chr = (char)(data & 0xFF);
  }
  else { 
    //while(1) { 
      //status = io_in(0x0AA); 
      //if ( status != old_status ) { 
        chr = io_in(port);
        return chr; 
      //}  
      //old_status = status;   
    //}
  }
  return chr;     
}