#include "hoffman.h" 

#define bitset(byte,nbit)   ((byte) |=  (1<<(nbit)))
#define bitclear(byte,nbit) ((byte) &= ~(1<<(nbit)))
#define bitflip(byte,nbit)  ((byte) ^=  (1<<(nbit)))
#define bitcheck(byte,nbit) ((byte) &   (1<<(nbit)))

#define snogbit(byte, nbit, value) ((byte) = ((byte) & ~(1 << (nbit))) | ((value) << (nbit)))

#define  ENABLED  0 
#define  DISABLED 1

// Table of status bits ( must be mirrored in monitor ) 
#define OUTPUT_ADD_EN  19 // GPIOB_TXN17
#define OUTPUT_DI_EN   18 // GPIOL_166 
#define OUTPUT_DO_EN   17 // GPIOL_144
#define OUTPUT_CTL_EN  16  // GPIOR_120  
#define OUTPUT_STAT_EN 15  // GPIOB_TXN19
// memory onboard FPGA 
#define BD_OE        14
#define R_N_CS       13 
#define R_N_OE       12
#define R_N_WR       11
#define R_A16        10 
#define R_A17        9 
#define R_A18        8 
// Bus control signals 
#define Z80_N_M1     7   
#define Z80_N_IORQ   6 
#define Z80_N_WR     5 
#define Z80_N_RFSH   4 
#define Z80_N_MREQ   3 
#define Z80_N_RD     2 
#define LIORQ        1 
#define INTA         0 

//00100000111111111011
//01011111111111111111
#define BUSIDLE 0x1FFFF

void io_out(int port,int value);
int io_in(int port);
int io_mem_in(int addr);
void io_mem_out(int addr, int value);