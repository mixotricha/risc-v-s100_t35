#ifndef __CONSOLE_H
#define __CONSOLE_H

#include <stdint.h>
#include "uart.h"

#define UART      ((Uart_Reg*)(0xF0010000))

void printk(const char* s, ...);
void console_write(int port, const char* s, ...);
char readchar();
int console_read(int port);

#endif