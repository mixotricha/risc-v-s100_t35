#from io import StringIO

from intelhex import IntelHex
ih = IntelHex()  
ih.loadhex("lisp.hex") 

print("int sram[(512*1024)+1] = {")
for byte in ih.tobinstr():
    print(byte,",",end="")
print("0};")

#print(ih.tobinarray(1))

#ih.tofile("foo.bin", format='hex')
#sio = StringIO()
#ih.write_hex_file(sio)
#hexstr = sio.getvalue()
#sio.close()
#print(hexstr) 


#ih = IntelHex()
#ih[0] = 0x55
#sio = StringIO()
#ih.write_hex_file(sio)
#hexstr = sio.getvalue()
#sio.close()