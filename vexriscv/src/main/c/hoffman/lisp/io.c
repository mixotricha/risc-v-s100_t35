#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <ctype.h>
#include "io.h"

// ==============================================
// Write a value to I/O address on s-100 bus 
// ==============================================
void io_out(int port,int value) { 
  
  unsigned int status = BUSIDLE; //0x23FF;
 
  snogbit( status, OUTPUT_ADD_EN  , ENABLED  ); // output address 
  snogbit( status, OUTPUT_DI_EN   , DISABLED ); // data in 
  snogbit( status, OUTPUT_DO_EN   , ENABLED  ); // data out  
  snogbit( status, OUTPUT_CTL_EN  , ENABLED  ); // ctl transc 
  snogbit( status, OUTPUT_STAT_EN , ENABLED  ); // stat transc  
  snogbit( status, Z80_N_IORQ     , DISABLED ); // irq  
  snogbit( status, Z80_N_RD       , DISABLED ); // rd frm bus  
  snogbit( status, Z80_N_WR       , ENABLED  ); // wrt to bus  

  // gpio state 
  S100ADR->OUTPUT_ENABLE    = 0x00;

  S100DODATA->OUTPUT_ENABLE = 0x00; // enabled 
  S100DIDATA->OUTPUT_ENABLE = 0xFF; // disabled 
  
  S100STATUS->OUTPUT_ENABLE = 0x00;
 
  S100ADR->OUTPUT    = port;  
  S100DODATA->OUTPUT = value; 
  S100STATUS->OUTPUT = status;    
  S100STATUS->OUTPUT = BUSIDLE; //0x23FF; 

}

// ==============================================
// Read a value from I/O address on s-100 bus 
// ==============================================
int io_in(int port) { 
  
  int DI = 0; 

  unsigned int status = BUSIDLE;

  snogbit( status, OUTPUT_ADD_EN  , ENABLED ); // output address 
  snogbit( status, OUTPUT_DI_EN   , ENABLED ); // data in 
  snogbit( status, OUTPUT_DO_EN   , DISABLED ); // data out  
  snogbit( status, OUTPUT_CTL_EN  , ENABLED  ); // ctl transc 
  snogbit( status, OUTPUT_STAT_EN , ENABLED  ); // stat transc  

  snogbit( status, Z80_N_IORQ     , DISABLED ); // irq no assert 
  snogbit( status, Z80_N_RD       , ENABLED ); // rd frm bus  
  snogbit( status, Z80_N_WR       , DISABLED ); // wrt to bus  

  // gpio state 
  S100ADR->OUTPUT_ENABLE    = 0x00;
  S100STATUS->OUTPUT_ENABLE = 0x00;

  S100DODATA->OUTPUT_ENABLE = 0xFF; // disabled 
  S100DIDATA->OUTPUT_ENABLE = 0xFF; // disabled 
 
  S100ADR->OUTPUT    = port;  
  S100STATUS->OUTPUT = status;    
  DI                 = S100DIDATA->INPUT; 
  S100STATUS->OUTPUT = BUSIDLE; 
  return DI; 

}


// ==============================================
// Write a value to biData on FPGA board  
// ==============================================
void io_mem_out(int addr, int value) { 
  
  unsigned int status = BUSIDLE; 
 
  //snogbit( status, OUTPUT_ADD_EN  , DISABLED  ); // output address 
  //snogbit( status, OUTPUT_CTL_EN  , DISABLED  ); // ctl transc 
  //snogbit( status, BD_OE  , ENABLED          ); // switch bidirectional bus to output  
  //snogbit( status, R_N_CS         , ENABLED  ); // enable ram chip 
  //snogbit( status, R_N_WR         , ENABLED  );  // enable write 
  //snogbit( status, R_N_OE         , DISABLED ); // disabled read 
  //snogbit( status, OUTPUT_STAT_EN , ENABLED  ); // stat transc  

  S100ADR->OUTPUT_ENABLE   = 0x00;
  BIDATAOUT->OUTPUT_ENABLE = 0x00; // enabled 
  BIDATAIN->OUTPUT_ENABLE  = 0xFF; // disabled  
  S100STATUS->OUTPUT_ENABLE = 0x00;
 
 S100STATUS->OUTPUT = 0x217FB;
 S100ADR->OUTPUT    = addr;   
 BIDATAOUT->OUTPUT = value; 

  S100STATUS->OUTPUT = BUSIDLE; 
}

// ==============================================
// Read a value from biData on FPGA board 
// ==============================================
int io_mem_in(int addr) { 
  
  int DI = 0; 

  unsigned int status = BUSIDLE; 
 
  //snogbit( status, OUTPUT_ADD_EN  , DISABLED  ); // output address 
  //snogbit( status, OUTPUT_CTL_EN  , ENABLED  ); // ctl transc 
  //snogbit( status, BD_OE          , DISABLED ); // switch bidirectional bus to input  
  //snogbit( status, R_N_CS         , ENABLED ); // enable ram chip 
  //snogbit( status, R_N_WR         , DISABLED ); // disable write 
  //snogbit( status, R_N_OE         , ENABLED ); // enable read 
  //snogbit( status, OUTPUT_STAT_EN , ENABLED ); // stat transc  

  S100ADR->OUTPUT_ENABLE   = 0x00;
  BIDATAOUT->OUTPUT_ENABLE = 0xFF; // disabled 
  BIDATAIN->OUTPUT_ENABLE  = 0x00; // enabled  
  S100STATUS->OUTPUT_ENABLE = 0x00; 
  S100ADR->OUTPUT_ENABLE    = 0x00;
  
  S100STATUS->OUTPUT = 0x24FFB;    
  S100ADR->OUTPUT    = addr; 
  DI                 = BIDATAIN->INPUT; 
  S100STATUS->OUTPUT = BUSIDLE; 
  return DI; 

}
