/***************************************************************************
 *   Damien Towning         (connolly.damien@gmail.com) 2023               *
 *                                                                         *
 *   This file is part of the T-35 Risc-V Hoffman project                  *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this library; see the file COPYING.LIB. If not,    *
 *   write to the Free Software Foundation, Inc., 59 Temple Place,         *
 *   Suite 330, Boston, MA  02111-1307, USA                                *
 *                                                                         *
 ***************************************************************************/

//#include <stdio.h>
//#include <ctype.h>
//#include <stdlib.h>
//#include <stdarg.h>
//#include <setjmp.h>
//#include <stdint.h>
//#include <stdbool.h>
//#include <limits.h>
//#include <math.h>
//#include <float.h>
//#include <string.h>
//#include <assert.h>
//#include <locale.h>
//#include <errno.h>
//#include <sys/types.h>
//#include <string.h>

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <assert.h>
#include <locale.h>
#include <errno.h>
#include <sys/types.h>

#include "io.h"
#include "console.h"
#include "hoffman.h"
//#include "memory.h" 

extern char *_heap_start; 
extern char *_heap_end;
extern char *_heap_size; 
extern char *_stack_start;

int port = 0; // Global for console 

//======================================================================
// Lisp objects
//======================================================================

// The Lisp object type
enum {
    // Regular objects visible from the user
    TINT = 1,
    TCELL,
    TSYMBOL,
    TPRIMITIVE,
    TFUNCTION,
    TMACRO,
    TSPECIAL,
    TENV,
};

// Subtypes for TSPECIAL
enum {
    TNIL = 1,
    TDOT,
    TCPAREN,
    TTRUE,
};

struct Obj;

// Typedef for the primitive function.
typedef struct Obj *Primitive(struct Obj *env, struct Obj *args);

// The object type
typedef struct Obj {
    // The first word of the object represents the type of the object. Any code that handles object
    // needs to check its type first, then access the following union members.
    int type;

    // Object values.
    union {
        // Int
        int value;
        // Cell
        struct {
            struct Obj *car;
            struct Obj *cdr;
        };
        // Symbol
        char name[1];
        // Primitive
        Primitive *fn;
        // Function or Macro
        struct {
            struct Obj *params;
            struct Obj *body;
            struct Obj *env;
        };
        // Subtype for special type
        int subtype;
        // Environment frame. This is a linked list of association lists
        // containing the mapping from symbols to their value.
        struct {
            struct Obj *vars;
            struct Obj *up;
        };
        // Forwarding pointer
        void *moved;
    };
} Obj;

// Constants
static Obj *Nil;
static Obj *Dot;
static Obj *Cparen;
static Obj *True;

// The list containing all symbols. Such data structure is traditionally called the "obarray", but I
// avoid using it as a variable name as this is not an array but a list.
static Obj *Symbols;

static void error(char *fmt, ...) __attribute((noreturn));

//======================================================================
// Constructors
//======================================================================

static Obj *alloc(int type, size_t size) {
    // Add the size of the type tag.
    size += offsetof(Obj, value);

    // Allocate the object.
    Obj *obj = malloc(size);
    obj->type = type;
    return obj;
}

static Obj *make_int(int value) {
    Obj *r = alloc(TINT, sizeof(int));
    r->value = value;
    return r;
}

static Obj *make_symbol(char *name) {
    Obj *sym = alloc(TSYMBOL, strlen(name) + 1);
    strcpy(sym->name, name);
    return sym;
}

static Obj *make_primitive(Primitive *fn) {
    Obj *r = alloc(TPRIMITIVE, sizeof(Primitive *));
    r->fn = fn;
    return r;
}

static Obj *make_function(int type, Obj *params, Obj *body, Obj *env) {
    assert(type == TFUNCTION || type == TMACRO);
    Obj *r = alloc(type, sizeof(Obj *) * 3);
    r->params = params;
    r->body = body;
    r->env = env;
    return r;
}

static Obj *make_special(int subtype) {
    Obj *r = malloc(sizeof(void *) * 2);
    r->type = TSPECIAL;
    r->subtype = subtype;
    return r;
}

struct Obj *make_env(Obj *vars, Obj *up) {
    Obj *r = alloc(TENV, sizeof(Obj *) * 2);
    r->vars = vars;
    r->up = up;
    return r;
}

static Obj *cons(Obj *car, Obj *cdr) {
    Obj *cell = alloc(TCELL, sizeof(Obj *) * 2);
    cell->car = car;
    cell->cdr = cdr;
    return cell;
}

// Returns ((x . y) . a)
static Obj *acons(Obj *x, Obj *y, Obj *a) {
    return cons(cons(x, y), a);
}

//======================================================================
// Parser
//
// This is a hand-written recursive-descendent parser.
//======================================================================

static Obj *read(void);

static void error(char *fmt, ...) {
    //va_list ap;
    //va_start(ap, fmt);
    //vfprintf(stderr, fmt, ap);
    //va_end(ap); 
    printk("Error: %s \r\n", fmt);
    typedef void (*Reset_Handler)(void);
    Reset_Handler reset_handler = (Reset_Handler)0x80000000;
    reset_handler(); 
}


int buffer_pos = 0; 
int buffer_size = 0; 
char buffer[255] = {-1};
// --------------------------------------------------------------
// Read out of the the buffer
// --------------------------------------------------------------
static int read_buff(void) { 
    if ( buffer_pos < buffer_size ) buffer_pos++; 
    return buffer[buffer_pos-1]; 
}

// --------------------------------------------------------------
// Read a string into the buffer
// --------------------------------------------------------------
void read_string() {   
    int c = 0; 
    buffer_size = 0;  
    buffer_pos = 0;  

    printk("code :"); 
    while ( c != '\n' && buffer_size < 255 ) { 
        c = readchar();
        printk("%c",c); 
        if ( c == '\r' ) c = '\n';
        buffer[buffer_size] = c;  
        buffer_size++; 
    }
} 

// --------------------------------------------------------------
// Instead of ungetc we use a limited size buffer on stack for now
// --------------------------------------------------------------
static int peek(void) {  
    return buffer[buffer_pos];  
}


// Skips the input until newline is found. Newline is one of \r, \r\n or \n.
static void skip_line(void) {
    while(1) { 
        int c = read_buff();
        printk("|||%c",c);
        if (c == EOF || c == '\n')
            return;
        if (c == '\r') {
            if (peek() == '\n')
                read_buff();
            return;
        }
    }
}

// Reads a list. Note that '(' has already been read.
static Obj *read_list(void) {
    Obj *obj = read();
    if (!obj)
        error("Unclosed parenthesis");
    if (obj == Dot)
        error("Stray dot");
    if (obj == Cparen)
        return Nil;
    Obj *head, *tail;
    head = tail = cons(obj, Nil); 
    while(1) { 
        Obj *obj = read();
        if (!obj)
            error("Unclosed parenthesis");
        if (obj == Cparen)
            return head;
        if (obj == Dot) {
            tail->cdr = read();
            if (read() != Cparen)
                error("Closed parenthesis expected after dot");
            return head;
        }
        tail->cdr = cons(obj, Nil);
        tail = tail->cdr;
    }
}

// May create a new symbol. If there's a symbol with the same name, it will not create a new symbol
// but return the existing one.
static Obj *intern(char *name) {
    for (Obj *p = Symbols; p != Nil; p = p->cdr)
        if (strcmp(name, p->car->name) == 0)
            return p->car;
    Obj *sym = make_symbol(name);
    Symbols = cons(sym, Symbols);
    return sym;
}

// Reader marcro ' (single quote). It reads an expression and returns (quote <expr>).
static Obj *read_quote(void) {
    Obj *sym = intern("quote");
    return cons(sym, cons(read(), Nil));
}

static int read_number(int val) {
    while (isdigit(peek()))
        val = val * 10 + (read_buff() - '0');
    return val;
}

#define SYMBOL_MAX_LEN 200

static Obj *read_symbol(char c) {
    char buf[SYMBOL_MAX_LEN + 1];
    int len = 1;
    buf[0] = c;
    while (isalnum(peek()) || peek() == '-') {
        if (SYMBOL_MAX_LEN <= len)
            error("Symbol name too long");
        buf[len++] = read_buff();
    }
    buf[len] = '\0';
    return intern(buf);
}

static Obj *read(void) {
     
    while(1) {
        int c = read_buff(); 
        if (c == ' ' || c == '\n' || c == '\r' || c == '\t') { 
            continue;
        }
        if (c == EOF) { 
            return NULL;
        }
        if (c == ';') {
            skip_line();
            continue;
        }
        if (c == '(') {
            return read_list();
        }
        if (c == ')') { 
            return Cparen;
        }
        if (c == '.') { 
            return Dot;
        }
        if (c == '\'') { 
            return read_quote();
        }
        if (isdigit(c)) { 
            return make_int(read_number(c - '0'));
        }
        if (c == '-') { 
            return make_int(-read_number(0));
        }
        if (isalpha(c) || strchr("+=!@#$%^&*", c)) { 
            return read_symbol(c);
        }
        error("Don't know how to handle %c", c);
    }
}

// Prints the given object.
static void print(Obj *obj) {
    switch (obj->type) {
    case TINT:
        printk("%d", obj->value);
        return;
    case TCELL:
        printk("(");
        while(1) { 
            print(obj->car);
            if (obj->cdr == Nil)
                break;
            if (obj->cdr->type != TCELL) {
                printk(" . ");
                print(obj->cdr);
                break;
            }
            printk(" ");
            obj = obj->cdr;
        }
        printk(")");
        return;
    case TSYMBOL:
        printk("%s", obj->name);
        return;
    case TPRIMITIVE:
        printk("<primitive>");
        return;
    case TFUNCTION:
        printk("<function>");
        return;
    case TMACRO:
        printk("<macro>");
        return;
    case TSPECIAL:
        if (obj == Nil)
            printk("()");
        else if (obj == True)
            printk("t");
        else
            error("Bug: print: Unknown subtype: %d", obj->subtype);
        return;
    default:
        error("Bug: print: Unknown tag type: %d", obj->type);
    }
}

static int list_length(Obj *list) {
    int len = 0;
    while(1) { 
        if (list == Nil)
            return len;
        if (list->type != TCELL)
            error("length: cannot handle dotted list");
        list = list->cdr;
        len++;
    }
}

//======================================================================
// Evaluator
//======================================================================

static Obj *eval(Obj *env, Obj *obj);

static void add_variable(Obj *env, Obj *sym, Obj *val) {
    env->vars = acons(sym, val, env->vars);
}

// Returns a newly created environment frame.
static Obj *push_env(Obj *env, Obj *vars, Obj *values) {
    if (list_length(vars) != list_length(values))
        error("Cannot apply function: number of argument does not match");
    Obj *map = Nil;
    for (Obj *p = vars, *q = values; p != Nil; p = p->cdr, q = q->cdr) {
        Obj *sym = p->car;
        Obj *val = q->car;
        map = acons(sym, val, map);
    }
    return make_env(map, env);
}

// Evaluates the list elements from head and returns the last return value.
static Obj *progn(Obj *env, Obj *list) {
    Obj *r = NULL;
    for (Obj *lp = list; lp != Nil; lp = lp->cdr)
        r = eval(env, lp->car);
    return r;
}

// Evaluates all the list elements and returns their return values as a new list.
static Obj *eval_list(Obj *env, Obj *list) {
    Obj *head = NULL;
    Obj *tail = NULL;
    for (Obj *lp = list; lp != Nil; lp = lp->cdr) {
        Obj *tmp = eval(env, lp->car);
        if (head == NULL) {
            head = tail = cons(tmp, Nil);
        } else {
            tail->cdr = cons(tmp, Nil);
            tail = tail->cdr;
        }
    }
    if (head == NULL)
        return Nil;
    return head;
}

static bool is_list(Obj *obj) {
  return obj == Nil || obj->type == TCELL;
}

// Apply fn with args.
static Obj *apply(Obj *env, Obj *fn, Obj *args) {
    if (!is_list(args))
        error("argument must be a list");
    if (fn->type == TPRIMITIVE)
        return fn->fn(env, args);
    if (fn->type == TFUNCTION) {
        Obj *body = fn->body;
        Obj *params = fn->params;
        Obj *eargs = eval_list(env, args);
        Obj *newenv = push_env(fn->env, params, eargs);
        return progn(newenv, body);
    }
    error("not supported");
}

// Searches for a variable by symbol. Returns null if not found.
static Obj *find(Obj *env, Obj *sym) {
    for (Obj *p = env; p; p = p->up) {
        for (Obj *cell = p->vars; cell != Nil; cell = cell->cdr) {
            Obj *bind = cell->car;
            if (sym == bind->car)
                return bind;
        }
    }
    return NULL;
}

// Expands the given macro application form.
static Obj *macroexpand(Obj *env, Obj *obj) {
    if (obj->type != TCELL || obj->car->type != TSYMBOL)
        return obj;
    Obj *bind = find(env, obj->car);
    if (!bind || bind->cdr->type != TMACRO)
        return obj;
    Obj *args = obj->cdr;
    Obj *body = bind->cdr->body;
    Obj *params = bind->cdr->params;
    Obj *newenv = push_env(env, params, args);
    return progn(newenv, body);
}

// Evaluates the S expression.
static Obj *eval(Obj *env, Obj *obj) {
    switch (obj->type) {
    case TINT:
    case TPRIMITIVE:
    case TFUNCTION:
    case TSPECIAL:
        // Self-evaluating objects
        return obj;
    case TSYMBOL: {
        // Variable
        Obj *bind = find(env, obj);
        if (!bind)
            printk("Damo Undefined symbol: %s", obj->name); 
            //error("Undefined symbol: %s", obj->name);
        return bind->cdr;
    }
    case TCELL: {
        // Function application form
        Obj *expanded = macroexpand(env, obj);
        if (expanded != obj)
            return eval(env, expanded);
        Obj *fn = eval(env, obj->car);
        Obj *args = obj->cdr;
        if (fn->type != TPRIMITIVE && fn->type != TFUNCTION)
            error("The head of a list must be a function");
        return apply(env, fn, args);
    }
    default:
        error("Bug: eval: Unknown tag type: %d", obj->type);
    }
}

//======================================================================
// Functions and special forms
//======================================================================

// 'expr
static Obj *prim_quote(Obj *env, Obj *list) {
    if (list_length(list) != 1)
        error("Malformed quote");
    return list->car;
}

// (list expr ...)
static Obj *prim_list(Obj *env, Obj *list) {
    return eval_list(env, list);
}

// (setq <symbol> expr)
static Obj *prim_setq(Obj *env, Obj *list) {
    if (list_length(list) != 2 || list->car->type != TSYMBOL)
        error("Malformed setq");
    Obj *bind = find(env, list->car);
    if (!bind)
        error("Unbound variable %s", list->car->name);
    Obj *value = eval(env, list->cdr->car);
    bind->cdr = value;
    return value;
}

// (+ <integer> ...)
static Obj *prim_plus(Obj *env, Obj *list) {
    int sum = 0;
    for (Obj *args = eval_list(env, list); args != Nil; args = args->cdr) {
        if (args->car->type != TINT)
            error("+ takes only numbers");
        sum += args->car->value;
    }
    return make_int(sum);
}

static Obj *handle_function(Obj *env, Obj *list, int type) {
    if (list->type != TCELL || !is_list(list->car) || list->cdr->type != TCELL)
        error("Malformed lambda");
    for (Obj *p = list->car; p != Nil; p = p->cdr) {
        if (p->car->type != TSYMBOL)
            error("Parameter must be a symbol");
        if (!is_list(p->cdr))
            error("Parameter list is not a flat list");
    }
    Obj *car = list->car;
    Obj *cdr = list->cdr;
    return make_function(type, car, cdr, env);
}

// (lambda (<symbol> ...) expr ...)
static Obj *prim_lambda(Obj *env, Obj *list) {
    return handle_function(env, list, TFUNCTION);
}

static Obj *handle_defun(Obj *env, Obj *list, int type) {
    if (list->car->type != TSYMBOL || list->cdr->type != TCELL)
        error("Malformed defun");
    Obj *sym = list->car;
    Obj *rest = list->cdr;
    Obj *fn = handle_function(env, rest, type);
    add_variable(env, sym, fn);
    return fn;
}

// (defun <symbol> (<symbol> ...) expr ...)
static Obj *prim_defun(Obj *env, Obj *list) {
    return handle_defun(env, list, TFUNCTION);
}

// (define <symbol> expr)
static Obj *prim_define(Obj *env, Obj *list) {
    if (list_length(list) != 2 || list->car->type != TSYMBOL)
        error("Malformed define");
    Obj *sym = list->car;
    Obj *value = eval(env, list->cdr->car);
    add_variable(env, sym, value);
    return value;
}

// (defmacro <symbol> (<symbol> ...) expr ...)
static Obj *prim_defmacro(Obj *env, Obj *list) {
    return handle_defun(env, list, TMACRO);
}

// (macroexpand expr)
static Obj *prim_macroexpand(Obj *env, Obj *list) {
    if (list_length(list) != 1)
        error("Malformed macroexpand");
    Obj *body = list->car;
    return macroexpand(env, body);
}

// (println expr)
static Obj *prim_println(Obj *env, Obj *list) {
    print(eval(env, list->car));
    printk("\n");
    return Nil;
}

// (if expr expr expr ...)
static Obj *prim_if(Obj *env, Obj *list) {
    if (list_length(list) < 2)
        error("Malformed if");
    Obj *cond = eval(env, list->car);
    if (cond != Nil) {
        Obj *then = list->cdr->car;
        return eval(env, then);
    }
    Obj *els = list->cdr->cdr;
    return els == Nil ? Nil : progn(env, els);
}

// (= <integer> <integer>)
static Obj *prim_num_eq(Obj *env, Obj *list) {
    if (list_length(list) != 2)
        error("Malformed =");
    Obj *values = eval_list(env, list);
    Obj *x = values->car;
    Obj *y = values->cdr->car;
    if (x->type != TINT || y->type != TINT)
        error("= only takes numbers");
    return x->value == y->value ? True : Nil;
}

// (exit)
static Obj *prim_exit(Obj *env, Obj *list) {
    exit(0);
}

static void add_primitive(Obj *env, char *name, Primitive *fn) {
    Obj *sym = intern(name);
    Obj *prim = make_primitive(fn);
    add_variable(env, sym, prim);
}

static void define_constants(Obj *env) {
    Obj *sym = intern("t");
    add_variable(env, sym, True);
}

static void define_primitives(Obj *env) {
    add_primitive(env, "quote", prim_quote);
    add_primitive(env, "list", prim_list);
    add_primitive(env, "setq", prim_setq);
    add_primitive(env, "+", prim_plus);
    add_primitive(env, "define", prim_define);
    add_primitive(env, "defun", prim_defun);
    add_primitive(env, "defmacro", prim_defmacro);
    add_primitive(env, "macroexpand", prim_macroexpand);
    add_primitive(env, "lambda", prim_lambda);
    add_primitive(env, "if", prim_if);
    add_primitive(env, "=", prim_num_eq);
    add_primitive(env, "println", prim_println);
    add_primitive(env, "exit", prim_exit);
}


void test() { 

  int i=0;
  
  int many_times = 595535 / SCALAR; // How many times to display a character is easier than a wait 
                                    // and ensures some wasteful loop is not optimised out 

  // Remember each symbol has the dp leading 
  unsigned char symbols[16] = {0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,0xF8,0x80,0x90,0x88,0x83,0xC6,0xA1,0x86,0x7F};
                            
  // Write out to the segment display a set of symbols. We write them multiple times so we can see them. 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[0]; } // 0 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[1]; } // 1 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[2]; } // 2 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[3]; } // 3 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[4]; } // 4 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[5]; } // 5 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[6]; } // 6 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[7]; } // 7 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[8]; } // 8 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[9]; } // 9 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[10]; } // A 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[11]; } // B 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[12]; } // C 
  for ( i = 0; i < many_times; i++ )  { SEG7->OUTPUT=symbols[13]; } // D 
}

typedef struct {
    int id;
    char name[20];
    double value;
} ComplexObject;


// ======================================
// do a malloc test
// ======================================
void malloc_test() { 

  //free(0); 

  printk("Doing a malloc test\r\n");   
  
  //srand(5764); 

  int i = 0; 
    int byte_size = 5;
    //int byte_size = rand() % 11;
    int* ptr0 = (int *) malloc(sizeof(int)*byte_size);
    int* ptr1 = (int *) malloc(sizeof(int)*byte_size);
    int* ptr2 = (int *) malloc(sizeof(int)*byte_size);
    if (ptr0 == NULL || ptr1 == NULL || ptr2 == NULL ) {
      printk("Memory allocation failed\r\n");
      printk("ptr0 = %p\r\n", ptr0);
      printk("ptr1 = %p\r\n", ptr1);
      printk("ptr2 = %p\r\n", ptr2);
      return 1;
    }
    printk("ptr0 = %p\r\n", ptr0);
    printk("ptr1 = %p\r\n", ptr1);
    printk("ptr2 = %p\r\n", ptr2);

    // Set the second byte of each pointer to a different value
    for ( i = 0; i < byte_size; i++ ) { 
      *( (int*) ptr0 + i ) = i;
      *( (int*) ptr1 + i ) = i;
      *( (int*) ptr2 + i ) = i;
    }
    printk("\r\nptr0:");
    for ( i = 0; i < byte_size; i++ ) { 
      printk(" %x ", *((int*)ptr0 + i));
    }   
    printk("\r\nptr1:"); 
    for ( i = 0; i < byte_size; i++ ) { 
      printk(" %x ", *((int*)ptr1 + i));
    }
    printk("\r\nptr2:");
    for ( i = 0; i < byte_size; i++ ) { 
      printk(" %x ", *((int*)ptr2 + i));
    }
    printk("\r\n"); 
    free(ptr0); 
    free(ptr1); 
    free(ptr2);
    
    printk("More complex memory test using a complex object...\r\n");     
 
    void* obj_ptr = malloc(sizeof(ComplexObject));

    if(obj_ptr != NULL) { 
        printk("Complex object malloced\r\n"); 
    }
    else { 
        printk("Failed to malloc complex object\r\n"); 
    }
     
    // Initialize the object
    ComplexObject* obj = (ComplexObject*) obj_ptr;
    obj->id = 42;
    strcpy(obj->name, "Test Object");
    obj->value = 3.14;

    // Verify the object's values
    printk("obj->id    : %d == 42\r\n" , obj->id); 
    printk("obj->name  : %s == 'Test Object'\r\n", obj->name ); 
    printk("obj->value : %f == 3.14\r\n",obj->value);     
    free(obj_ptr);

    // Our malloc at this point is not smart enough to reclaim memory. 
    // it just wraps sbrk 
    //printk("Test of free\r\n"); 
    //void* aptr1 = malloc(16);
    //if ( aptr1 != NULL ) { printk("Ptr1 allocated\r\n"); }
    //void* aptr2 = malloc(16);
    //if ( aptr2 != NULL ) { printk("Ptr2 allocated\r\n"); }
    // Free the first block
    //free(aptr1);
    // Try to allocate a new block of the same size
    //void* aptr3 = malloc(16);
    //if ( aptr3 != NULL ) { printk("Ptr3 allocated\r\n"); }
    // Check if the new block is the same as the freed block
    //if ( aptr1 == aptr3 ) { 
    //    printk("Memory released correctly\r\n"); 
    //}
    //free(aptr2);
    //free(aptr3);

}

// ======================================
// enter main 
// ======================================
void main() {
  
  //malloc_test(); 

  console_write(0,"heap start: %p\r\n", (void*) &_heap_start); 
  console_write(0,"heap end: %p\r\n", (void*) &_heap_end); 
  console_write(0,"heap size: %p\r\n", (void*) &_heap_size);
  console_write(0,"stack start: %p\r\n", (void *)&_stack_start );

  // Constants and primitives
  printk("Constants and primitives\r\n");

  Nil = make_special(TNIL);
  Dot = make_special(TDOT);
  Cparen = make_special(TCPAREN);
  True = make_special(TTRUE);
  Symbols = Nil;
  
  //printk("Environment\r\n");

  Obj *env = make_env(Nil, NULL);
  printk("A!\r\n");
  define_constants(env);
  printk("B!\r\n");
  define_primitives(env);
  printk("C!\r\n");

  printk("Lisp is started\r\n");
    
  // The main loop

  while(1) { 
    read_string();   
    printk("\r\n");        
    Obj *expr = read();
    if (expr == Cparen) error("Stray close parenthesis");
    if (expr == Dot) error("Stray dot");
    print(eval(env, expr));  
    printk("\r\n"); 
  }

}

// ======================================
// handle irq
// ======================================
void irqCallback(){
}

