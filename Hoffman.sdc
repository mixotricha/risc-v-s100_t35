
# Efinity Interface Designer SDC
# Version: 2023.1.150
# Date: 2023-12-17 16:54

# Copyright (C) 2017 - 2023 Efinix Inc. All rights reserved.

# Device: T35F400
# Project: Hoffman
# Timing Model: I4 (final)

# PLL Constraints
#################
create_clock -period 400 io_axiClk
#create_clock -period 200.0000 io_busClk

# GPIO Constraints
####################

set_output_delay -clock io_axiClk 2.5 [ all_outputs ]
set_input_delay  -clock io_axiClk 2.5 [ all_inputs  ]

# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {pll_clk_in}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {pll_clk_in}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_IOBYTES_read[0]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_IOBYTES_read[0]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_IOBYTES_read[1]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_IOBYTES_read[1]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_IOBYTES_read[2]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_IOBYTES_read[2]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_IOBYTES_read[3]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_IOBYTES_read[3]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_IOBYTES_read[4]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_IOBYTES_read[4]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_IOBYTES_read[5]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_IOBYTES_read[5]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_IOBYTES_read[6]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_IOBYTES_read[6]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_IOBYTES_read[7]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_IOBYTES_read[7]}]

# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_uart_rxd}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_uart_rxd}]

#set_input_delay -clock io_axiClk -min 4 [get_ports {io_uart_rxd}]
#set_input_delay -clock io_axiClk -max 5 [get_ports {io_uart_rxd}]

#set_output_delay -clock io_axiClk -min 4 [get_ports {io_uart_txd}]
#set_output_delay -clock io_axiClk -max 5 [get_ports {io_uart_txd}]

# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DO_write[0]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DO_write[0]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DO_write[1]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DO_write[1]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DO_write[2]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DO_write[2]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DO_write[3]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DO_write[3]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DO_write[4]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DO_write[4]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DO_write[5]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DO_write[5]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DO_write[6]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DO_write[6]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DO_write[7]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DO_write[7]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_half_clkOut}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_half_clkOut}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_OUTPUT_CTL_EN}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_OUTPUT_CTL_EN}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_OUTPUT_DI_EN}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_OUTPUT_DI_EN}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_OUTPUT_DO_EN}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_OUTPUT_DO_EN}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[0]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[0]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[1]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[1]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[2]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[2]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[3]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[3]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[4]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[4]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SBCLEDS_write[0]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SBCLEDS_write[0]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SBCLEDS_write[1]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SBCLEDS_write[1]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SBCLEDS_write[2]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SBCLEDS_write[2]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SBCLEDS_write[3]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SBCLEDS_write[3]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SBCLEDS_write[4]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SBCLEDS_write[4]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SBCLEDS_write[5]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SBCLEDS_write[5]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SBCLEDS_write[6]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SBCLEDS_write[6]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SBCLEDS_write[7]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SBCLEDS_write[7]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SEG7_write[0]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SEG7_write[0]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SEG7_write[1]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SEG7_write[1]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SEG7_write[2]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SEG7_write[2]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SEG7_write[3]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SEG7_write[3]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SEG7_write[4]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SEG7_write[4]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SEG7_write[5]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SEG7_write[5]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SEG7_write[6]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SEG7_write[6]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_SEG7_write[7]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_SEG7_write[7]}]

 #set_output_delay -clock io_axiClk -min 4 [get_ports {io_uart_txd}]
 #set_output_delay -clock io_axiClk -max 5 [get_ports {io_uart_txd}]
 

# LVDS RX GPIO Constraints
############################
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DI_read[0]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DI_read[0]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DI_read[1]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DI_read[1]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DI_read[2]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DI_read[2]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DI_read[3]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DI_read[3]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DI_read[4]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DI_read[4]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DI_read[5]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DI_read[5]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DI_read[6]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DI_read[6]}]
# set_input_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_ieee696_DI_read[7]}]
# set_input_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_ieee696_DI_read[7]}]

 #set_output_delay -clock io_axiClk -max 5 [get_ports {io_ieee696_clkOut}]
 #set_output_delay -clock io_axiClk -min 4 [get_ports {io_ieee696_clkOut}]

 #set_output_delay -clock io_axiClk -max 5 [get_ports {io_ieee696_pDBIN}]
 #set_output_delay -clock io_axiClk -min 4 [get_ports {io_ieee696_pDBIN}]
 
 #set_output_delay -clock io_axiClk -max 5 [get_ports {io_ieee696_phi}]
 #set_output_delay -clock io_axiClk -min 4 [get_ports {io_ieee696_phi}]
 
 #set_output_delay -clock io_axiClk -max 5 [get_ports {io_ieee696_pstval}]
 #set_output_delay -clock io_axiClk -min 4 [get_ports {io_ieee696_pstval}]
 
 #set_output_delay -clock io_axiClk -max 5 [get_ports {io_ieee696_psync}]
 #set_output_delay -clock io_axiClk -min 4 [get_ports {io_ieee696_psync}]
 
 #set_output_delay -clock io_axiClk -max 5 [get_ports {io_ieee696_pwr}]
 #set_output_delay -clock io_axiClk -min 4 [get_ports {io_ieee696_pwr}]
 
 #set_output_delay -clock io_axiClk -max 5 [get_ports {io_ieee696_sINP}]
 #set_output_delay -clock io_axiClk -min 4 [get_ports {io_ieee696_sINP}]
 
 #set_output_delay -clock io_axiClk -max 5 [get_ports {io_ieee696_sOUT}]
 #set_output_delay -clock io_axiClk -min 4 [get_ports {io_ieee696_sOUT}]

# LVDS Rx Constraints
####################

# LVDS TX GPIO Constraints
############################
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_OUTPUT_ADD_EN}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_OUTPUT_ADD_EN}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_OUTPUT_STATUS_EN}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_OUTPUT_STATUS_EN}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[5]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[5]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[6]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[6]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[7]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[7]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[8]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[8]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[9]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[9]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[10]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[10]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[11]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[11]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[12]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[12]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[13]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[13]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[14]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[14]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[15]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[15]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[16]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[16]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[17]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[17]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[18]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[18]}]
# set_output_delay -clock <CLOCK> -max <MAX CALCULATION> [get_ports {io_S100ADR_write[19]}]
# set_output_delay -clock <CLOCK> -min <MIN CALCULATION> [get_ports {io_S100ADR_write[19]}]

# LVDS Tx Constraints
####################
#set_output_delay -clock io_axiClk 2.5[ all_outputs ]
#set_input_delay  -clock io_axiClk 2.5 [ all_inputs  ]