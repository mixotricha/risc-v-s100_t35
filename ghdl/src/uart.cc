#include <cstdlib>
#include <cstdio>
#include <vpi_user.h>
#include "virtual_board.hh"
#include "Instance.hh"

#include <time.h>

//#define SYSCLK       24997500
//#define SYSCLK      25000000 // 25Mhz
#define SYSCLK      2500000  // 2.5Mhz

#define BAUDRATE     (115200)
#define BAUD         (SYSCLK/BAUDRATE)
#define BITS         8 
#define DEBUG        false
#define DEBUG_SND    false
#define DRIFT        -1

//#define DRIFT        4 


Uart::Uart() {
}

// -----------------------------------------
// Just a test to make sure the class is up
// -----------------------------------------
void Uart::test(void) { 
  printf("Test\r\n"); 
}

// ---------------------------------
// Get uart packets sent by the vhdl
// ---------------------------------
unsigned char Uart::recv(int v) { 

  if ( recv_uart_read == false ) { 
    if ( v == 0  ) {   
      if ( DEBUG ) printf("start bit %i: %i\r\n",recv_uart_bit,v);
      recv_uart_read = true;  
      recv_uart_bit++; 
      recv_uart_byte = 0; 
    }
  }
  else if ( recv_uart_read == true ) {
    if ( DEBUG ) { printf("clock: %d %d \r\n",recv_uart_clk,SYSCLK/BAUDRATE); }  
    if ( recv_uart_clk == BAUD ) {   
      //printf("********\r\n");  
      if ( recv_uart_bit == 9 ) {
        if ( v == 1 ) { 
          if ( DEBUG ) printf("stop bit %i: %i\r\n",recv_uart_bit,v);  
          recv_uart_bit = 0;
          recv_uart_read = false; 
          recv_uart_byte = 0; 
        }
        else { 
          if ( DEBUG ) printf("Frame Error %i: %i\r\n",recv_uart_bit,v); 
          recv_uart_read = false; 
          recv_uart_bit = 0; 
        }
      } 
      else { 
        if ( recv_uart_bit == 0 ) recv_uart_byte = 0; 
        if ( v == 1 ) recv_uart_byte |= (1 << (recv_uart_bit-1)); 
        if ( recv_uart_bit == 8 ) {
          recv_uart_byte_done = true;
        }
        recv_uart_bit++; 
      } 
      recv_uart_clk = 0;
    }
    else { 
      recv_uart_clk++; 
    }
  }
  if ( recv_uart_byte_done == true ) { 
    recv_uart_byte_done = false; 
    return recv_uart_byte; 
  }  
  else { 
    return 255; 
  }
}

// ---------------------------------
// send uart packets to the vhdl
// ---------------------------------
int Uart::snd(void) {   
  
  if ( snd_uart_write == false && snd_buffer.length() > 0 ) { 
    for (int i = 0; i < 8; i++)  snd_uart_byte[i+1] = (snd_buffer[0] >> i) & 1; // read bits in to snd_uart_byte
    snd_uart_write = true;  
    snd_current_bit = 1; // idle bit 
    snd_uart_bit = 0;  
  }
  else if ( snd_uart_write == true ) {  
    if ( snd_uart_clk == (BAUD+DRIFT) ) { 
      if ( DEBUG_SND ) printf("[%i][%i,%i,%i,%i][%i,%i,%i,%i][%i]\r\n",snd_uart_byte[0],snd_uart_byte[1],snd_uart_byte[2],snd_uart_byte[3],snd_uart_byte[4],snd_uart_byte[5],snd_uart_byte[6],snd_uart_byte[7],snd_uart_byte[8],snd_uart_byte[9]); 

      if ( snd_uart_bit == 0 ) { snd_current_bit = 0; } // start bit  
      
      if ( snd_uart_bit == 1 ) { snd_current_bit = snd_uart_byte[1]; } // b0
      if ( snd_uart_bit == 2 ) { snd_current_bit = snd_uart_byte[2]; } // b1
      if ( snd_uart_bit == 3 ) { snd_current_bit = snd_uart_byte[3]; } // b2
      if ( snd_uart_bit == 4 ) { snd_current_bit = snd_uart_byte[4]; } // b3
      if ( snd_uart_bit == 5 ) { snd_current_bit = snd_uart_byte[5]; } // b4
      if ( snd_uart_bit == 6 ) { snd_current_bit = snd_uart_byte[6]; } // b5
      if ( snd_uart_bit == 7 ) { snd_current_bit = snd_uart_byte[7]; } // b6
      if ( snd_uart_bit == 8 ) { snd_current_bit = snd_uart_byte[8]; } // b7

      //if ( snd_uart_bit == 1 ) { snd_current_bit = 1; } // b0
      //if ( snd_uart_bit == 2 ) { snd_current_bit = 1; } // b1
      //if ( snd_uart_bit == 3 ) { snd_current_bit = 1; } // b2
      //if ( snd_uart_bit == 4 ) { snd_current_bit = 1; } // b3
      //if ( snd_uart_bit == 5 ) { snd_current_bit = 0; } // b4
      //if ( snd_uart_bit == 6 ) { snd_current_bit = 0; } // b5
      //if ( snd_uart_bit == 7 ) { snd_current_bit = 0; } // b6
      //if ( snd_uart_bit == 8 ) { snd_current_bit = 0; } // b7

      if ( snd_uart_bit == 9 ) { snd_current_bit = 1; } // stop bit 
      if ( snd_uart_bit == 10 ) { // one more bit past stop bit  
        snd_current_bit = 1; // back to idle bit 
        snd_uart_write = false; 
        snd_buffer.erase(0, 1); // done with this byte    
      }   
      snd_uart_bit++;       
      snd_uart_clk = 0; 
    }
    else { 
      snd_uart_clk++; 
    }      
  }
  else { 
    snd_current_bit = 1; // idle bit 
  }
  return snd_current_bit; 
}  
  

void Uart::write(unsigned char snd_byte) {
    snd_buffer += static_cast<char>(snd_byte);
}

int Uart::get_buff_len(void) { 

    return snd_buffer.length();
}    
