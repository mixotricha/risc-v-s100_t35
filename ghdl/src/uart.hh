class Uart {
private:

  int  recv_uart_clk = 0; 
  bool recv_uart_read = false; 
  bool recv_uart_byte_done = false; 
  int  recv_uart_bit = 0; 
  unsigned char recv_uart_byte = 0;
  
  int  snd_uart_clk = 0;   
  bool snd_uart_write = false; 
  bool snd_uart_bit_done = true; 
  int  snd_uart_bit = 0; 
  unsigned char  snd_current_bit = 1; 
  unsigned char  snd_uart_byte[10] = { 0, 0, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1};
  Glib::ustring  snd_buffer; 

public:
  Uart();
  unsigned char recv(int v);
  int snd(void); 
  void write(unsigned char snd_byte); 
  void test(void);
  int get_buff_len(void); 

};



