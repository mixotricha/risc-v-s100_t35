EESchema Schematic File Version 2  date 12/11/2011 10:44:19 AM
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:contrib
LIBS:valves
LIBS:dips-s
LIBS:N8VEM-KiCAD
EELAYER 25  0
EELAYER END
$Descr C 22000 17000
encoding utf-8
Sheet 1 1
Title "noname.sch"
Date "11 dec 2011"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74LS682N IC?
U 1 1 4B83122A
P 3500 6850
AR Path="/384B489BEB" Ref="IC?"  Part="1" 
AR Path="/4B489BEB" Ref="IC?"  Part="1" 
AR Path="/284F4C4B489BEB" Ref="IC?"  Part="1" 
AR Path="/7E4188DA4B489BEB" Ref="IC?"  Part="1" 
AR Path="/69549BC04B489BEB" Ref="IC?"  Part="1" 
AR Path="/773F8EB44B489BEB" Ref="IC?"  Part="1" 
AR Path="/23C9F04B489BEB" Ref="IC?"  Part="1" 
AR Path="/94B489BEB" Ref="IC?"  Part="1" 
AR Path="/773F65F14B489BEB" Ref="IC?"  Part="1" 
AR Path="/23C6504B489BEB" Ref="IC?"  Part="1" 
AR Path="/23CBC44B489BEB" Ref="IC?"  Part="1" 
AR Path="/23C34C4B489BEB" Ref="IC?"  Part="1" 
AR Path="/23BC884B489BEB" Ref="IC?"  Part="1" 
AR Path="/39803EA4B489BEB" Ref="IC?"  Part="1" 
AR Path="/FFFFFFFF4B489BEB" Ref="IC?"  Part="1" 
AR Path="/73254B489BEB" Ref="IC?"  Part="1" 
AR Path="/D058A04B489BEB" Ref="IC?"  Part="1" 
AR Path="/1607D44B489BEB" Ref="IC?"  Part="1" 
AR Path="/D1C3804B489BEB" Ref="IC?"  Part="1" 
AR Path="/24B489BEB" Ref="IC?"  Part="1" 
AR Path="/23D70C4B489BEB" Ref="IC?"  Part="1" 
AR Path="/6FE934E34B489BEB" Ref="IC?"  Part="1" 
AR Path="/14B489BEB" Ref="IC?"  Part="1" 
AR Path="/2104B489BEB" Ref="IC?"  Part="1" 
AR Path="/D1CA184B489BEB" Ref="IC?"  Part="1" 
AR Path="/F4BF4B489BEB" Ref="IC?"  Part="1" 
AR Path="/262F604B489BEB" Ref="IC?"  Part="1" 
AR Path="/22C6E204B489BEB" Ref="IC?"  Part="1" 
AR Path="/773F8EB44B83122A" Ref="IC?"  Part="1" 
AR Path="/23C9F04B83122A" Ref="IC3"  Part="1" 
AR Path="/94B83122A" Ref="IC"  Part="1" 
AR Path="/4B83122A" Ref="IC3"  Part="1" 
AR Path="/773F65F14B83122A" Ref="IC3"  Part="1" 
AR Path="/23C6504B83122A" Ref="IC3"  Part="1" 
AR Path="/FFFFFFFF4B83122A" Ref="IC3"  Part="1" 
AR Path="/24B83122A" Ref="IC3"  Part="1" 
AR Path="/23BC884B83122A" Ref="IC3"  Part="1" 
AR Path="/6FE934E34B83122A" Ref="IC3"  Part="1" 
AR Path="/23C34C4B83122A" Ref="IC3"  Part="1" 
AR Path="/23CBC44B83122A" Ref="IC3"  Part="1" 
F 0 "IC3" H 3200 7775 50  0000 L BNN
F 1 "74LS682N" H 3200 5850 50  0000 L BNN
F 2 "20dip300" H 3500 7000 50  0001 C CNN
	1    3500 6850
	1    0    0    -1  
$EndComp
Text Notes 11150 11700 0    60   ~ 0
FOR RELIABLE OPERATION WITH S-100 68K CPU BOARD\nON 4MB SRAM BOARD, REPLACE THE FOLLOWING DEVICES:\nU12 - SN74F11\nU13 - SN74F11\nU15 - SN74F02\n
NoConn ~ 13950 2100
Connection ~ 13950 2400
Connection ~ 13950 2200
Connection ~ 4150 1250
Wire Wire Line
	4150 1250 4150 1850
Wire Wire Line
	3600 9400 2800 9400
Wire Wire Line
	600  9500 1600 9500
Connection ~ 1600 9500
Connection ~ 1600 9300
Connection ~ 3600 9400
Connection ~ 3600 9150
Wire Wire Line
	3050 8950 3600 8950
Wire Wire Line
	3050 9150 3600 9150
Wire Wire Line
	3050 9600 3600 9600
Wire Wire Line
	4000 3650 4400 3650
Wire Wire Line
	4400 3650 4400 5650
Wire Wire Line
	4400 5650 600  5650
Wire Wire Line
	600  5650 600  6750
Wire Wire Line
	600  6750 1300 6750
Wire Wire Line
	1600 1250 550  1250
Wire Wire Line
	3000 5050 3000 5050
Wire Wire Line
	3000 4850 3000 4850
Connection ~ 2600 5250
Connection ~ 2600 5150
Connection ~ 2600 5050
Connection ~ 2600 4950
Connection ~ 2600 4850
Connection ~ 2600 4750
Connection ~ 2600 4650
Connection ~ 2600 4550
Wire Wire Line
	2600 5450 2600 4550
Connection ~ 20050 12500
Connection ~ 20050 12300
Connection ~ 12300 2950
Wire Wire Line
	12300 1750 13000 1750
Wire Wire Line
	12300 1750 12300 3150
Wire Wire Line
	13650 1200 13000 1200
Connection ~ 8500 1900
Connection ~ 8500 1700
Connection ~ 8500 1500
Connection ~ 8500 1300
Wire Wire Line
	3000 1350 3350 1350
Wire Wire Line
	15450 2750 13950 2750
Wire Wire Line
	13950 2750 13950 2200
Connection ~ 4150 1450
Wire Wire Line
	3000 1150 3950 1150
Wire Wire Line
	550  1150 1600 1150
Wire Wire Line
	5250 1500 5650 1500
Wire Wire Line
	4600 1850 5650 1850
Wire Wire Line
	5250 1350 5650 1350
Wire Wire Line
	8500 1150 8500 1900
Wire Wire Line
	20050 14250 20050 12150
Wire Wire Line
	7350 1700 7600 1700
Wire Wire Line
	7350 1300 7600 1300
Wire Wire Line
	7350 1500 7600 1500
Wire Wire Line
	7350 1900 7600 1900
Wire Wire Line
	8050 16100 8400 16100
Wire Wire Line
	8050 15400 8400 15400
Wire Wire Line
	8050 15600 8400 15600
Wire Wire Line
	8050 15700 8400 15700
Wire Wire Line
	8050 15500 8400 15500
Wire Wire Line
	8050 16000 8400 16000
Wire Wire Line
	7950 16200 8400 16200
Wire Wire Line
	8050 15800 8400 15800
Wire Wire Line
	5700 15800 6050 15800
Wire Wire Line
	5600 16200 6050 16200
Wire Wire Line
	5700 16000 6050 16000
Wire Wire Line
	5700 15500 6050 15500
Wire Wire Line
	5700 15700 6050 15700
Wire Wire Line
	5700 15600 6050 15600
Wire Wire Line
	5700 15400 6050 15400
Wire Wire Line
	5700 16100 6050 16100
Wire Wire Line
	1000 15800 1350 15800
Wire Wire Line
	900  16200 1350 16200
Wire Wire Line
	1000 16000 1350 16000
Wire Wire Line
	1000 15500 1350 15500
Wire Wire Line
	1000 15700 1350 15700
Wire Wire Line
	1000 15600 1350 15600
Wire Wire Line
	1000 15100 1350 15100
Wire Wire Line
	1000 15300 1350 15300
Wire Wire Line
	1000 15200 1350 15200
Wire Wire Line
	1000 15400 1350 15400
Wire Wire Line
	1000 16100 1350 16100
Wire Wire Line
	3350 16100 3700 16100
Wire Wire Line
	3350 15400 3700 15400
Wire Wire Line
	1000 12650 1350 12650
Wire Wire Line
	1000 12250 1350 12250
Wire Wire Line
	1000 11950 1350 11950
Wire Wire Line
	11850 15400 15900 15400
Wire Wire Line
	5250 2250 5650 2250
Wire Wire Line
	3000 4750 3000 4750
Wire Wire Line
	750  3750 1300 3750
Wire Wire Line
	750  3650 1300 3650
Wire Wire Line
	10700 8750 10300 8750
Wire Wire Line
	11850 14550 16350 14550
Wire Wire Line
	16350 15150 11850 15150
Connection ~ 15900 15150
Connection ~ 16350 15150
Connection ~ 16350 14750
Connection ~ 15900 14750
Connection ~ 5500 6150
Connection ~ 10950 2800
Connection ~ 10950 2700
Connection ~ 10950 2600
Connection ~ 10950 2500
Connection ~ 10950 2400
Connection ~ 10950 2300
Connection ~ 10950 2200
Connection ~ 10950 2100
Wire Wire Line
	10950 2100 10950 3000
Wire Wire Line
	10150 10850 9800 10850
Wire Wire Line
	10150 10950 9800 10950
Wire Wire Line
	10150 11150 9800 11150
Wire Wire Line
	10150 11050 9800 11050
Wire Wire Line
	10150 11450 9800 11450
Wire Wire Line
	10150 11550 9800 11550
Wire Wire Line
	10150 11350 9800 11350
Wire Wire Line
	10150 11250 9800 11250
Wire Wire Line
	7800 11250 7450 11250
Wire Wire Line
	7800 11350 7450 11350
Wire Wire Line
	7800 11550 7450 11550
Wire Wire Line
	7800 11450 7450 11450
Wire Wire Line
	7800 11050 7450 11050
Wire Wire Line
	7800 11150 7450 11150
Wire Wire Line
	7800 10950 7450 10950
Wire Wire Line
	7800 10850 7450 10850
Wire Wire Line
	5450 10850 5100 10850
Wire Wire Line
	5450 10950 5100 10950
Wire Wire Line
	5450 11150 5100 11150
Wire Wire Line
	5450 11050 5100 11050
Wire Wire Line
	5450 11450 5100 11450
Wire Wire Line
	5450 11550 5100 11550
Wire Wire Line
	5450 11350 5100 11350
Wire Wire Line
	5450 11250 5100 11250
Wire Wire Line
	3100 11250 2750 11250
Wire Wire Line
	3100 11350 2750 11350
Wire Wire Line
	3100 11550 2750 11550
Wire Wire Line
	3100 11450 2750 11450
Wire Wire Line
	3100 11050 2750 11050
Wire Wire Line
	3100 11150 2750 11150
Wire Wire Line
	3100 10950 2750 10950
Wire Wire Line
	3100 10850 2750 10850
Wire Wire Line
	3100 14400 2750 14400
Wire Wire Line
	3100 14500 2750 14500
Wire Wire Line
	3100 14700 2750 14700
Wire Wire Line
	3100 14600 2750 14600
Wire Wire Line
	3100 14200 2750 14200
Wire Wire Line
	3100 14300 2750 14300
Wire Wire Line
	3100 14100 2750 14100
Wire Wire Line
	3100 14000 2750 14000
Wire Wire Line
	5450 14000 5100 14000
Wire Wire Line
	5450 14100 5100 14100
Wire Wire Line
	5450 14300 5100 14300
Wire Wire Line
	5450 14200 5100 14200
Wire Wire Line
	5450 14600 5100 14600
Wire Wire Line
	5450 14700 5100 14700
Wire Wire Line
	5450 14500 5100 14500
Wire Wire Line
	5450 14400 5100 14400
Wire Wire Line
	7800 14400 7450 14400
Wire Wire Line
	7800 14500 7450 14500
Wire Wire Line
	7800 14700 7450 14700
Wire Wire Line
	7800 14600 7450 14600
Wire Wire Line
	7800 14200 7450 14200
Wire Wire Line
	7800 14300 7450 14300
Wire Wire Line
	7800 14100 7450 14100
Wire Wire Line
	7800 14000 7450 14000
Wire Wire Line
	10150 14000 9800 14000
Wire Wire Line
	10150 14100 9800 14100
Wire Wire Line
	10150 14300 9800 14300
Wire Wire Line
	10150 14200 9800 14200
Wire Wire Line
	10150 14600 9800 14600
Wire Wire Line
	10150 14700 9800 14700
Wire Wire Line
	10150 14500 9800 14500
Wire Wire Line
	10150 14400 9800 14400
Wire Wire Line
	17700 6450 17350 6450
Wire Wire Line
	17700 6550 17350 6550
Wire Wire Line
	17700 6750 17350 6750
Wire Wire Line
	17700 6650 17350 6650
Wire Wire Line
	17700 6250 17350 6250
Wire Wire Line
	17700 6350 17350 6350
Wire Wire Line
	17700 6150 17350 6150
Wire Wire Line
	17700 6050 17350 6050
Wire Wire Line
	15950 5150 15600 5150
Wire Wire Line
	15950 5250 15600 5250
Wire Wire Line
	15950 5450 15600 5450
Wire Wire Line
	15950 5350 15600 5350
Wire Wire Line
	15950 4950 15600 4950
Wire Wire Line
	15950 5050 15600 5050
Wire Wire Line
	15950 4850 15600 4850
Wire Wire Line
	15950 4750 15600 4750
Wire Wire Line
	15600 6350 15950 6350
Wire Wire Line
	15600 6150 15950 6150
Wire Wire Line
	17700 850  17350 850 
Wire Wire Line
	16750 9250 16400 9250
Wire Wire Line
	16800 7900 16450 7900
Wire Wire Line
	18000 12850 17650 12850
Connection ~ 14650 12400
Wire Wire Line
	16450 12950 16450 13100
Wire Wire Line
	16450 13100 14650 13100
Wire Wire Line
	14650 13100 14650 10450
Wire Wire Line
	16450 12250 16450 12400
Wire Wire Line
	16450 12400 14650 12400
Connection ~ 14550 11200
Wire Wire Line
	16450 11650 16400 11650
Wire Wire Line
	16400 11650 16400 11750
Wire Wire Line
	16400 11750 14550 11750
Wire Wire Line
	14550 11750 14550 8900
Wire Wire Line
	18000 11000 17650 11000
Wire Wire Line
	16450 11100 16400 11100
Wire Wire Line
	16400 11100 16400 11200
Wire Wire Line
	16400 11200 14550 11200
Connection ~ 14550 8900
Connection ~ 14300 12150
Wire Wire Line
	15200 12850 14300 12850
Wire Wire Line
	14300 12850 14300 9650
Connection ~ 15100 9650
Wire Wire Line
	13550 9650 15100 9650
Connection ~ 15100 11000
Wire Wire Line
	15200 10450 15100 10450
Connection ~ 14400 11550
Wire Wire Line
	14400 9900 14400 12650
Wire Wire Line
	15000 8050 15000 10450
Wire Wire Line
	15200 11350 14850 11350
Wire Wire Line
	18000 10100 17650 10100
Wire Wire Line
	16450 10250 16450 10350
Wire Wire Line
	16450 10350 16400 10350
Wire Wire Line
	15200 11000 11050 11000
Wire Wire Line
	11050 11000 11050 8650
Wire Wire Line
	11050 8650 10300 8650
Connection ~ 14850 9700
Wire Wire Line
	14850 9700 15200 9700
Wire Wire Line
	15200 9250 15000 9250
Wire Wire Line
	15000 8050 15250 8050
Wire Wire Line
	13500 7900 15000 7900
Wire Wire Line
	15250 7750 15000 7750
Wire Wire Line
	15000 7750 15000 7900
Wire Wire Line
	3000 1850 3250 1850
Wire Wire Line
	3000 1650 3250 1650
Connection ~ 11250 9500
Wire Wire Line
	12350 9500 11250 9500
Connection ~ 11850 8350
Wire Wire Line
	10300 8350 11850 8350
Connection ~ 11550 8900
Wire Wire Line
	12350 10450 11550 10450
Wire Wire Line
	11550 10450 11550 8250
Connection ~ 11700 9600
Wire Wire Line
	11700 9600 8350 9600
Connection ~ 11700 9050
Wire Wire Line
	11700 9800 12350 9800
Wire Wire Line
	11700 8500 14250 8500
Wire Wire Line
	14250 8500 14250 8800
Wire Wire Line
	12300 7750 11350 7750
Wire Wire Line
	8900 8150 8600 8150
Wire Wire Line
	8600 8150 8600 9400
Wire Wire Line
	8600 9400 10950 9400
Wire Wire Line
	10950 9400 10950 8050
Wire Wire Line
	10950 8050 10300 8050
Wire Wire Line
	8900 8650 8800 8650
Wire Wire Line
	8800 8650 8800 9200
Wire Wire Line
	15200 8600 14750 8600
Wire Wire Line
	14750 8600 14750 8250
Wire Wire Line
	14750 8250 10300 8250
Wire Wire Line
	1200 2050 1600 2050
Wire Wire Line
	4600 2750 5650 2750
Wire Wire Line
	2150 8300 2450 8300
Wire Wire Line
	600  8200 1000 8200
Wire Wire Line
	7750 2600 7350 2600
Wire Wire Line
	7750 2200 7350 2200
Wire Wire Line
	12850 3450 12550 3450
Wire Wire Line
	12550 3450 12550 3200
Wire Wire Line
	12200 3150 12200 2100
Connection ~ 12100 2200
Wire Wire Line
	12100 3150 12100 2200
Connection ~ 11900 2400
Wire Wire Line
	11900 3150 11900 2400
Connection ~ 11700 2600
Wire Wire Line
	11700 3150 11700 2600
Connection ~ 11500 2800
Wire Wire Line
	11500 3150 11500 2800
Wire Wire Line
	10600 1200 10100 1200
Wire Wire Line
	8050 10850 8400 10850
Wire Wire Line
	8050 10950 8400 10950
Wire Wire Line
	8050 11150 8400 11150
Wire Wire Line
	8050 11050 8400 11050
Wire Wire Line
	8050 11450 8400 11450
Wire Wire Line
	8050 11550 8400 11550
Wire Wire Line
	8050 11350 8400 11350
Wire Wire Line
	8050 11250 8400 11250
Wire Wire Line
	8050 12050 8400 12050
Wire Wire Line
	8050 12150 8400 12150
Wire Wire Line
	8050 11850 8400 11850
Wire Wire Line
	8050 11950 8400 11950
Wire Wire Line
	8050 11750 8400 11750
Wire Wire Line
	8050 11650 8400 11650
Wire Wire Line
	8050 14800 8400 14800
Wire Wire Line
	8050 14900 8400 14900
Wire Wire Line
	8050 15100 8400 15100
Wire Wire Line
	8050 15000 8400 15000
Wire Wire Line
	8050 15300 8400 15300
Wire Wire Line
	8050 15200 8400 15200
Wire Wire Line
	8050 14400 8400 14400
Wire Wire Line
	8050 14500 8400 14500
Wire Wire Line
	8050 14700 8400 14700
Wire Wire Line
	8050 14600 8400 14600
Wire Wire Line
	8050 14200 8400 14200
Wire Wire Line
	8050 14300 8400 14300
Wire Wire Line
	8050 14100 8400 14100
Wire Wire Line
	8050 14000 8400 14000
Wire Wire Line
	5700 14000 6050 14000
Wire Wire Line
	5700 14100 6050 14100
Wire Wire Line
	5700 14300 6050 14300
Wire Wire Line
	5700 14200 6050 14200
Wire Wire Line
	5700 14600 6050 14600
Wire Wire Line
	5700 14700 6050 14700
Wire Wire Line
	5700 14500 6050 14500
Wire Wire Line
	5700 14400 6050 14400
Wire Wire Line
	5700 15200 6050 15200
Wire Wire Line
	5700 15300 6050 15300
Wire Wire Line
	5700 15000 6050 15000
Wire Wire Line
	5700 15100 6050 15100
Wire Wire Line
	5700 14900 6050 14900
Wire Wire Line
	5700 14800 6050 14800
Wire Wire Line
	5700 11650 6050 11650
Wire Wire Line
	5700 11750 6050 11750
Wire Wire Line
	5700 11950 6050 11950
Wire Wire Line
	5700 11850 6050 11850
Wire Wire Line
	5700 12150 6050 12150
Wire Wire Line
	5700 12050 6050 12050
Wire Wire Line
	5700 11250 6050 11250
Wire Wire Line
	5700 11350 6050 11350
Wire Wire Line
	5700 11550 6050 11550
Wire Wire Line
	5700 11450 6050 11450
Wire Wire Line
	5700 11050 6050 11050
Wire Wire Line
	5700 11150 6050 11150
Wire Wire Line
	5700 10950 6050 10950
Wire Wire Line
	5700 10850 6050 10850
Connection ~ 16350 14550
Connection ~ 16350 14150
Connection ~ 15900 14150
Connection ~ 15900 14550
Connection ~ 15450 14550
Connection ~ 15450 14150
Connection ~ 15000 14150
Connection ~ 15000 14550
Connection ~ 14550 14550
Connection ~ 14550 14150
Connection ~ 14100 14150
Connection ~ 14100 14550
Connection ~ 13650 14550
Connection ~ 13650 14150
Connection ~ 13200 14150
Connection ~ 13200 14550
Connection ~ 12750 14550
Connection ~ 12750 14150
Connection ~ 11850 15150
Connection ~ 13200 15150
Connection ~ 11850 14750
Connection ~ 14550 15150
Connection ~ 12300 15150
Connection ~ 12750 15150
Connection ~ 12750 14750
Connection ~ 12300 14750
Connection ~ 11850 14750
Connection ~ 13200 14750
Connection ~ 13650 14750
Connection ~ 14100 14750
Connection ~ 14100 15150
Connection ~ 13650 15150
Connection ~ 15000 15150
Connection ~ 15450 15150
Connection ~ 15450 14750
Connection ~ 15000 14750
Connection ~ 14550 14750
Wire Wire Line
	15700 3050 15950 3050
Wire Wire Line
	15700 3150 15950 3150
Wire Wire Line
	15700 1850 15950 1850
Wire Wire Line
	15700 1750 15950 1750
Wire Wire Line
	17350 2550 17700 2550
Wire Wire Line
	17350 2650 17700 2650
Wire Wire Line
	17350 2750 17700 2750
Wire Wire Line
	17350 2350 17700 2350
Wire Wire Line
	17350 2450 17700 2450
Wire Wire Line
	17350 2250 17700 2250
Wire Wire Line
	17350 2150 17700 2150
Wire Wire Line
	17350 1250 17700 1250
Wire Wire Line
	17350 1350 17700 1350
Wire Wire Line
	17350 1550 17700 1550
Wire Wire Line
	17350 1450 17700 1450
Wire Wire Line
	17350 1050 17700 1050
Wire Wire Line
	17350 1150 17700 1150
Wire Wire Line
	17350 950  17700 950 
Wire Wire Line
	1000 12850 1350 12850
Wire Wire Line
	900  13050 1350 13050
Wire Wire Line
	15600 4450 15950 4450
Wire Wire Line
	17700 3450 17350 3450
Wire Wire Line
	17700 3550 17350 3550
Wire Wire Line
	17700 3750 17350 3750
Wire Wire Line
	17700 3650 17350 3650
Wire Wire Line
	17700 4050 17350 4050
Wire Wire Line
	17700 4150 17350 4150
Wire Wire Line
	17700 3950 17350 3950
Wire Wire Line
	17700 3850 17350 3850
Wire Wire Line
	17700 4750 17350 4750
Wire Wire Line
	17700 4850 17350 4850
Wire Wire Line
	17700 5050 17350 5050
Wire Wire Line
	17700 4950 17350 4950
Wire Wire Line
	17700 5350 17350 5350
Wire Wire Line
	17700 5450 17350 5450
Wire Wire Line
	17700 5250 17350 5250
Wire Wire Line
	17700 5150 17350 5150
Wire Wire Line
	15950 6950 15600 6950
Wire Wire Line
	15700 1550 15950 1550
Wire Wire Line
	15700 2150 15950 2150
Wire Wire Line
	15700 2350 15950 2350
Wire Wire Line
	15700 2250 15950 2250
Wire Wire Line
	15700 2650 15950 2650
Wire Wire Line
	15700 2750 15950 2750
Wire Wire Line
	15700 2550 15950 2550
Wire Wire Line
	15700 2450 15950 2450
Wire Wire Line
	15700 1150 15950 1150
Wire Wire Line
	15700 1250 15950 1250
Wire Wire Line
	15700 1450 15950 1450
Wire Wire Line
	15700 1350 15950 1350
Wire Wire Line
	15700 950  15950 950 
Wire Wire Line
	15700 1050 15950 1050
Wire Wire Line
	15700 850  15950 850 
Wire Wire Line
	15600 4050 15950 4050
Wire Wire Line
	15600 3850 15950 3850
Wire Wire Line
	15600 3650 15950 3650
Wire Wire Line
	15950 3450 15600 3450
Wire Wire Line
	8400 8050 8900 8050
Wire Wire Line
	8400 8250 8900 8250
Wire Wire Line
	8400 8450 8900 8450
Wire Wire Line
	8400 8550 8900 8550
Wire Wire Line
	8400 9050 8900 9050
Wire Wire Line
	8400 8950 8900 8950
Wire Wire Line
	17000 8700 17400 8700
Connection ~ 20850 15600
Connection ~ 12300 14550
Wire Wire Line
	19850 15600 19850 15500
Wire Wire Line
	18200 7600 18800 7600
Wire Wire Line
	18200 5900 18800 5900
Wire Wire Line
	18200 2600 18800 2600
Connection ~ 11850 14550
Connection ~ 11850 14150
Wire Wire Line
	20850 15650 20850 15600
Wire Wire Line
	19400 700  20000 700 
Wire Wire Line
	19400 900  20000 900 
Wire Wire Line
	19400 800  20000 800 
Wire Wire Line
	19400 1100 20000 1100
Wire Wire Line
	19400 1000 20000 1000
Wire Wire Line
	19400 1500 20000 1500
Wire Wire Line
	19400 1600 20000 1600
Wire Wire Line
	19400 1800 20000 1800
Wire Wire Line
	19400 1700 20000 1700
Wire Wire Line
	19400 1300 20000 1300
Wire Wire Line
	19400 1400 20000 1400
Wire Wire Line
	19400 1200 20000 1200
Wire Wire Line
	19400 1900 20000 1900
Wire Wire Line
	19400 2000 20000 2000
Wire Wire Line
	19400 2200 20000 2200
Wire Wire Line
	19400 2100 20000 2100
Wire Wire Line
	19400 2500 20000 2500
Wire Wire Line
	19400 2600 20000 2600
Wire Wire Line
	19400 2400 20000 2400
Wire Wire Line
	19400 2300 20000 2300
Wire Wire Line
	19400 3900 20000 3900
Wire Wire Line
	19400 4000 20000 4000
Wire Wire Line
	19400 4200 20000 4200
Wire Wire Line
	19400 4100 20000 4100
Wire Wire Line
	19400 3700 20000 3700
Wire Wire Line
	19400 3800 20000 3800
Wire Wire Line
	19400 3600 20000 3600
Wire Wire Line
	19400 3500 20000 3500
Wire Wire Line
	19400 2700 20000 2700
Wire Wire Line
	19400 2800 20000 2800
Wire Wire Line
	19400 3000 20000 3000
Wire Wire Line
	19400 2900 20000 2900
Wire Wire Line
	19400 3300 20000 3300
Wire Wire Line
	19400 3400 20000 3400
Wire Wire Line
	19400 3200 20000 3200
Wire Wire Line
	19400 3100 20000 3100
Wire Wire Line
	19400 4500 20000 4500
Wire Wire Line
	19400 4600 20000 4600
Wire Wire Line
	19400 4800 20000 4800
Wire Wire Line
	19400 4700 20000 4700
Wire Wire Line
	19400 4300 20000 4300
Wire Wire Line
	19400 4400 20000 4400
Wire Wire Line
	19400 4900 20000 4900
Wire Wire Line
	19400 5000 20000 5000
Wire Wire Line
	19400 5200 20000 5200
Wire Wire Line
	19400 5100 20000 5100
Wire Wire Line
	19400 5500 20000 5500
Wire Wire Line
	19400 5600 20000 5600
Wire Wire Line
	19400 5400 20000 5400
Wire Wire Line
	19400 5300 20000 5300
Wire Wire Line
	20000 5800 19400 5800
Wire Wire Line
	19400 5700 20000 5700
Wire Wire Line
	19400 10500 20000 10500
Wire Wire Line
	20000 10600 19400 10600
Wire Wire Line
	19400 10100 20000 10100
Wire Wire Line
	19400 10200 20000 10200
Wire Wire Line
	19400 10400 20000 10400
Wire Wire Line
	19400 10300 20000 10300
Wire Wire Line
	19400 9900 20000 9900
Wire Wire Line
	19400 10000 20000 10000
Wire Wire Line
	19400 9800 20000 9800
Wire Wire Line
	19400 9700 20000 9700
Wire Wire Line
	19400 9200 20000 9200
Wire Wire Line
	19400 9100 20000 9100
Wire Wire Line
	19400 9500 20000 9500
Wire Wire Line
	19400 9600 20000 9600
Wire Wire Line
	19400 9400 20000 9400
Wire Wire Line
	19400 9300 20000 9300
Wire Wire Line
	19400 7900 20000 7900
Wire Wire Line
	19400 8000 20000 8000
Wire Wire Line
	19400 8200 20000 8200
Wire Wire Line
	19400 8100 20000 8100
Wire Wire Line
	19400 7700 20000 7700
Wire Wire Line
	19400 7800 20000 7800
Wire Wire Line
	19400 7600 20000 7600
Wire Wire Line
	19400 7500 20000 7500
Wire Wire Line
	19400 8300 20000 8300
Wire Wire Line
	19400 8400 20000 8400
Wire Wire Line
	19400 8600 20000 8600
Wire Wire Line
	19400 8500 20000 8500
Wire Wire Line
	19400 8900 20000 8900
Wire Wire Line
	19400 9000 20000 9000
Wire Wire Line
	19400 8800 20000 8800
Wire Wire Line
	19400 8700 20000 8700
Wire Wire Line
	19400 7100 20000 7100
Wire Wire Line
	19400 7200 20000 7200
Wire Wire Line
	19400 7400 20000 7400
Wire Wire Line
	19400 7300 20000 7300
Wire Wire Line
	19400 6900 20000 6900
Wire Wire Line
	19400 7000 20000 7000
Wire Wire Line
	19400 6800 20000 6800
Wire Wire Line
	19400 6700 20000 6700
Wire Wire Line
	19400 5900 20000 5900
Wire Wire Line
	19400 6000 20000 6000
Wire Wire Line
	19400 6200 20000 6200
Wire Wire Line
	19400 6100 20000 6100
Wire Wire Line
	19400 6500 20000 6500
Wire Wire Line
	19400 6600 20000 6600
Wire Wire Line
	19400 6400 20000 6400
Wire Wire Line
	19400 6300 20000 6300
Wire Wire Line
	19450 15200 18900 15200
Wire Wire Line
	20850 15200 20250 15200
Wire Wire Line
	12300 14550 12300 14600
Connection ~ 13200 15400
Connection ~ 13650 15400
Connection ~ 14100 15400
Connection ~ 14550 15400
Connection ~ 15000 15400
Connection ~ 15450 15400
Connection ~ 13650 14750
Connection ~ 13650 15150
Connection ~ 15450 15800
Connection ~ 15000 15800
Connection ~ 14550 15800
Connection ~ 14100 15800
Connection ~ 13650 15800
Connection ~ 13200 15800
Connection ~ 19850 15600
Connection ~ 20250 15600
Connection ~ 12300 14150
Connection ~ 12300 14550
Connection ~ 12750 15400
Connection ~ 12750 15800
Wire Wire Line
	19850 14800 21200 14800
Wire Wire Line
	21200 14800 21200 15600
Wire Wire Line
	21200 15600 19450 15600
Connection ~ 12300 15400
Connection ~ 12300 15800
Wire Wire Line
	19050 11300 19050 11500
Connection ~ 19050 11400
Connection ~ 19050 11300
Wire Wire Line
	750  3950 1300 3950
Connection ~ 14850 7900
Wire Wire Line
	15600 3550 15950 3550
Wire Wire Line
	15600 3750 15950 3750
Wire Wire Line
	15600 3950 15950 3950
Wire Wire Line
	15600 4150 15950 4150
Wire Wire Line
	15950 6750 15600 6750
Wire Wire Line
	15950 6550 15600 6550
Wire Wire Line
	15600 6050 15950 6050
Wire Wire Line
	15950 6650 15600 6650
Wire Wire Line
	1000 10850 1350 10850
Wire Wire Line
	1000 10950 1350 10950
Wire Wire Line
	1000 11150 1350 11150
Wire Wire Line
	1000 11050 1350 11050
Wire Wire Line
	1000 11450 1350 11450
Wire Wire Line
	1000 11550 1350 11550
Wire Wire Line
	1000 11350 1350 11350
Wire Wire Line
	1000 11250 1350 11250
Wire Wire Line
	1000 12050 1350 12050
Wire Wire Line
	1000 11850 1350 11850
Wire Wire Line
	1000 11750 1350 11750
Wire Wire Line
	1000 11650 1350 11650
Wire Wire Line
	1000 12550 1350 12550
Wire Wire Line
	1000 12350 1350 12350
Wire Wire Line
	1000 14800 1350 14800
Wire Wire Line
	1000 14900 1350 14900
Wire Wire Line
	1000 15000 1350 15000
Wire Wire Line
	1000 14400 1350 14400
Wire Wire Line
	1000 14500 1350 14500
Wire Wire Line
	1000 14700 1350 14700
Wire Wire Line
	1000 14600 1350 14600
Wire Wire Line
	1000 14200 1350 14200
Wire Wire Line
	1000 14300 1350 14300
Wire Wire Line
	1000 14100 1350 14100
Wire Wire Line
	1000 14000 1350 14000
Wire Wire Line
	15600 7050 15950 7050
Wire Wire Line
	15950 5750 15600 5750
Wire Wire Line
	15600 4350 15950 4350
Wire Wire Line
	3350 14000 3700 14000
Wire Wire Line
	3350 14100 3700 14100
Wire Wire Line
	3350 14300 3700 14300
Wire Wire Line
	3350 14200 3700 14200
Wire Wire Line
	3350 14600 3700 14600
Wire Wire Line
	3350 14700 3700 14700
Wire Wire Line
	3350 14500 3700 14500
Wire Wire Line
	3350 14400 3700 14400
Wire Wire Line
	3350 15200 3700 15200
Wire Wire Line
	3350 15300 3700 15300
Wire Wire Line
	3350 15000 3700 15000
Wire Wire Line
	3350 15100 3700 15100
Wire Wire Line
	3350 14900 3700 14900
Wire Wire Line
	3350 14800 3700 14800
Wire Wire Line
	3350 15600 3700 15600
Wire Wire Line
	3350 15700 3700 15700
Wire Wire Line
	3350 15500 3700 15500
Wire Wire Line
	3350 11650 3700 11650
Wire Wire Line
	3350 11750 3700 11750
Wire Wire Line
	3350 11950 3700 11950
Wire Wire Line
	3350 11850 3700 11850
Wire Wire Line
	3350 12150 3700 12150
Wire Wire Line
	3350 12050 3700 12050
Wire Wire Line
	3350 11250 3700 11250
Wire Wire Line
	3350 11350 3700 11350
Wire Wire Line
	3350 11550 3700 11550
Wire Wire Line
	3350 11450 3700 11450
Wire Wire Line
	3350 11050 3700 11050
Wire Wire Line
	3350 11150 3700 11150
Wire Wire Line
	3350 10950 3700 10950
Wire Wire Line
	3350 10850 3700 10850
Wire Wire Line
	3350 16000 3700 16000
Wire Wire Line
	3250 16200 3700 16200
Wire Wire Line
	12550 2100 11350 2100
Wire Wire Line
	12550 2200 11350 2200
Wire Wire Line
	12550 2400 11350 2400
Wire Wire Line
	12550 2300 11350 2300
Wire Wire Line
	12550 2700 11350 2700
Wire Wire Line
	12550 2800 11350 2800
Wire Wire Line
	12550 2600 11350 2600
Wire Wire Line
	12550 2500 11350 2500
Wire Wire Line
	11600 3150 11600 2700
Connection ~ 11600 2700
Wire Wire Line
	11800 3150 11800 2500
Connection ~ 11800 2500
Wire Wire Line
	12000 3150 12000 2300
Connection ~ 12000 2300
Connection ~ 12200 2100
Wire Wire Line
	12450 3100 12450 3900
Wire Wire Line
	12450 3100 12550 3100
Wire Wire Line
	15150 2300 15450 2300
Wire Wire Line
	7750 2400 7350 2400
Wire Wire Line
	7750 2800 7350 2800
Wire Wire Line
	600  8000 1000 8000
Wire Wire Line
	1200 1550 1600 1550
Wire Wire Line
	1200 1850 1600 1850
Wire Wire Line
	1200 1750 1600 1750
Wire Wire Line
	1200 1650 1600 1650
Wire Wire Line
	1200 2150 1600 2150
Wire Wire Line
	12450 3900 12750 3900
Wire Wire Line
	14250 8800 15200 8800
Wire Wire Line
	10750 8550 10750 9200
Wire Wire Line
	10750 9200 8800 9200
Wire Wire Line
	10850 8250 10850 9300
Connection ~ 10850 8250
Wire Wire Line
	10850 9300 8700 9300
Wire Wire Line
	8700 9300 8700 8350
Wire Wire Line
	8700 8350 8900 8350
Wire Wire Line
	10300 8150 11350 8150
Wire Wire Line
	11350 7750 11350 8750
Wire Wire Line
	11350 8750 12300 8750
Connection ~ 11350 8150
Wire Wire Line
	12300 8050 11700 8050
Wire Wire Line
	11700 9050 12300 9050
Connection ~ 11700 8500
Wire Wire Line
	11700 8050 11700 10600
Wire Wire Line
	11700 10600 12350 10600
Connection ~ 11700 9800
Wire Wire Line
	15000 10450 13550 10450
Connection ~ 14650 10450
Wire Wire Line
	12300 8900 11550 8900
Connection ~ 11550 8250
Wire Wire Line
	12300 7900 11850 7900
Wire Wire Line
	11850 7900 11850 9650
Wire Wire Line
	11850 9650 12350 9650
Wire Wire Line
	10300 8450 11250 8450
Wire Wire Line
	11250 8450 11250 10300
Wire Wire Line
	11250 10300 12350 10300
Wire Wire Line
	3000 1550 3250 1550
Wire Wire Line
	3000 1750 3250 1750
Connection ~ 4150 1850
Connection ~ 4150 1750
Connection ~ 4150 1650
Connection ~ 4150 1550
Wire Wire Line
	13500 8900 15200 8900
Wire Wire Line
	15200 8900 15200 9100
Wire Wire Line
	15250 7900 15100 7900
Wire Wire Line
	15100 7900 15100 8900
Connection ~ 15100 8900
Connection ~ 15000 9250
Wire Wire Line
	14400 9900 15200 9900
Wire Wire Line
	14400 10750 11150 10750
Wire Wire Line
	11150 10750 11150 8550
Wire Wire Line
	11150 8550 10300 8550
Connection ~ 10750 8550
Wire Wire Line
	15200 9400 15100 9400
Wire Wire Line
	15100 9400 15100 10250
Wire Wire Line
	15100 10250 15200 10250
Wire Wire Line
	16150 10100 16450 10100
Wire Wire Line
	16450 9950 16450 9800
Wire Wire Line
	16450 9800 16400 9800
Wire Wire Line
	15200 10800 14850 10800
Wire Wire Line
	14850 11350 14850 7900
Connection ~ 14850 10800
Wire Wire Line
	14400 11550 15200 11550
Connection ~ 14400 10750
Wire Wire Line
	14400 12650 15200 12650
Wire Wire Line
	15100 10450 15100 11950
Wire Wire Line
	15100 11950 15200 11950
Wire Wire Line
	15200 12150 14300 12150
Connection ~ 14300 9650
Wire Wire Line
	16400 10900 16450 10900
Wire Wire Line
	16400 11450 16450 11450
Wire Wire Line
	16400 12050 16450 12050
Wire Wire Line
	18000 11550 17650 11550
Wire Wire Line
	18000 12150 17650 12150
Wire Wire Line
	16400 12750 16450 12750
Wire Wire Line
	15700 2850 15950 2850
Wire Wire Line
	15600 5650 15950 5650
Wire Wire Line
	15600 6250 15950 6250
Wire Wire Line
	15600 6450 15950 6450
Wire Wire Line
	16350 14750 11850 14750
Wire Wire Line
	16350 14150 11850 14150
Wire Wire Line
	12300 15200 12300 15150
Wire Wire Line
	12300 15850 12300 15800
Wire Wire Line
	8900 8750 8400 8750
Wire Wire Line
	2200 8100 2450 8100
Wire Wire Line
	750  4050 1300 4050
Wire Wire Line
	750  4150 1300 4150
Wire Wire Line
	750  4350 1300 4350
Wire Wire Line
	750  4250 1300 4250
Wire Wire Line
	3000 4650 3000 4650
Wire Wire Line
	5250 2400 5650 2400
Wire Wire Line
	4300 6050 4000 6050
Wire Wire Line
	750  3850 1300 3850
Wire Wire Line
	11850 15800 15900 15800
Connection ~ 15900 15400
Connection ~ 15900 15800
Wire Wire Line
	1000 12150 1350 12150
Wire Wire Line
	1000 12450 1350 12450
Wire Wire Line
	1000 12950 1350 12950
Wire Wire Line
	3350 12950 3700 12950
Wire Wire Line
	3350 12450 3700 12450
Wire Wire Line
	3350 12350 3700 12350
Wire Wire Line
	3350 12550 3700 12550
Wire Wire Line
	3250 13050 3700 13050
Wire Wire Line
	3350 12850 3700 12850
Wire Wire Line
	3350 12250 3700 12250
Wire Wire Line
	3350 12650 3700 12650
Wire Wire Line
	5700 12650 6050 12650
Wire Wire Line
	5700 12250 6050 12250
Wire Wire Line
	5700 12850 6050 12850
Wire Wire Line
	5600 13050 6050 13050
Wire Wire Line
	5700 12550 6050 12550
Wire Wire Line
	5700 12350 6050 12350
Wire Wire Line
	5700 12450 6050 12450
Wire Wire Line
	5700 12950 6050 12950
Wire Wire Line
	8050 12950 8400 12950
Wire Wire Line
	8050 12450 8400 12450
Wire Wire Line
	8050 12350 8400 12350
Wire Wire Line
	8050 12550 8400 12550
Wire Wire Line
	7950 13050 8400 13050
Wire Wire Line
	8050 12850 8400 12850
Wire Wire Line
	8050 12250 8400 12250
Wire Wire Line
	8050 12650 8400 12650
Wire Wire Line
	3350 15800 3700 15800
Wire Wire Line
	10600 1400 9600 1400
Wire Wire Line
	8350 2200 8900 2200
Wire Wire Line
	8350 2400 8900 2400
Wire Wire Line
	8350 2800 8900 2800
Wire Wire Line
	8350 2600 8900 2600
Wire Wire Line
	5500 6150 6700 6150
Wire Wire Line
	3250 1450 3000 1450
Connection ~ 20050 13300
Connection ~ 20050 13500
Wire Wire Line
	1600 1450 550  1450
Wire Wire Line
	1600 1350 1200 1350
Wire Wire Line
	11800 1300 11800 1100
Wire Wire Line
	13000 1200 13000 1750
Connection ~ 11800 1300
Connection ~ 11800 1100
Connection ~ 20050 13750
Connection ~ 20050 13900
Connection ~ 20050 14050
Wire Wire Line
	3650 8200 4300 8200
Wire Wire Line
	4300 8200 4300 6250
Wire Wire Line
	12550 2950 12300 2950
Wire Wire Line
	3950 1550 3950 1650
Wire Wire Line
	3000 4550 3000 4550
Wire Wire Line
	3000 4950 3000 4950
Wire Wire Line
	3000 5150 3000 5150
Wire Wire Line
	3000 5250 3000 5250
Wire Wire Line
	1900 4450 1900 3650
Wire Wire Line
	1900 6850 1900 6050
Wire Wire Line
	3000 7650 3000 7650
Wire Wire Line
	3000 7550 3000 7550
Wire Wire Line
	3000 7350 3000 7350
Wire Wire Line
	3000 6950 3000 6950
Wire Wire Line
	750  6250 1300 6250
Wire Wire Line
	3000 7050 3000 7050
Wire Wire Line
	750  6650 1300 6650
Wire Wire Line
	750  6550 1300 6550
Wire Wire Line
	750  6450 1300 6450
Wire Wire Line
	750  6350 1300 6350
Wire Wire Line
	750  6050 1300 6050
Wire Wire Line
	750  6150 1300 6150
Wire Wire Line
	3000 7150 3000 7150
Wire Wire Line
	2600 7850 2600 6950
Connection ~ 2600 6950
Connection ~ 2600 7050
Connection ~ 2600 7150
Connection ~ 2600 7250
Connection ~ 2600 7350
Connection ~ 2600 7450
Connection ~ 2600 7550
Connection ~ 2600 7650
Wire Wire Line
	3000 7250 3000 7250
Wire Wire Line
	3000 7450 3000 7450
Wire Wire Line
	1600 9500 1600 9300
Connection ~ 1900 3650
Connection ~ 1900 3750
Connection ~ 1900 3850
Connection ~ 1900 3950
Connection ~ 1900 4050
Connection ~ 1900 4150
Connection ~ 1900 4250
Connection ~ 1900 4350
Connection ~ 1900 6050
Connection ~ 1900 6150
Connection ~ 1900 6250
Connection ~ 1900 6350
Connection ~ 1900 6450
Connection ~ 1900 6550
Connection ~ 1900 6650
Connection ~ 1900 6750
Wire Wire Line
	2800 9400 2800 9050
Wire Wire Line
	2800 9050 3600 9050
Wire Wire Line
	3250 1250 3000 1250
Connection ~ 20050 12750
Connection ~ 20050 12900
Connection ~ 20050 13050
Wire Wire Line
	8700 4850 9050 4850
Wire Wire Line
	8700 5050 9050 5050
Wire Wire Line
	8700 5350 9050 5350
Wire Wire Line
	8700 5150 9050 5150
Wire Wire Line
	9050 4650 8700 4650
Wire Wire Line
	6950 4650 7300 4650
Wire Wire Line
	6950 4750 7300 4750
Wire Wire Line
	6950 4950 7300 4950
Wire Wire Line
	6950 4850 7300 4850
Wire Wire Line
	6950 5250 7300 5250
Wire Wire Line
	6950 5350 7300 5350
Wire Wire Line
	6950 5150 7300 5150
Wire Wire Line
	6950 5050 7300 5050
Wire Wire Line
	6950 5650 7300 5650
Wire Wire Line
	6950 5550 7300 5550
Wire Wire Line
	9050 4750 8700 4750
Wire Wire Line
	9050 5250 8700 5250
Wire Wire Line
	9050 4950 8700 4950
$Comp
L 74LS244 U?
U 1 1 4B88608D
P 8000 5150
AR Path="/3F8000004B88608D" Ref="U?"  Part="1" 
AR Path="/2930C04B88608D" Ref="U?"  Part="1" 
AR Path="/9607844B88608D" Ref="U"  Part="1" 
AR Path="/773F65F14B88608D" Ref="U2"  Part="1" 
AR Path="/453838314B88608D" Ref="U2"  Part="1" 
AR Path="/23C34C4B88608D" Ref="U2"  Part="1" 
AR Path="/3F754B88608D" Ref="U2"  Part="1" 
AR Path="/24B88608D" Ref="U2"  Part="1" 
AR Path="/23BC884B88608D" Ref="U2"  Part="1" 
AR Path="/6AAC4B88608D" Ref="U2"  Part="1" 
AR Path="/6FE934E34B88608D" Ref="U2"  Part="1" 
AR Path="/FFFFFFC14B88608D" Ref="U2"  Part="1" 
AR Path="/FFFFFFFF4B88608D" Ref="U2"  Part="1" 
AR Path="/23CBC44B88608D" Ref="U2"  Part="1" 
AR Path="/4B88608D" Ref="U2"  Part="1" 
AR Path="/E2F54B88608D" Ref="U2"  Part="1" 
F 0 "U2" H 8050 4950 60  0000 C CNN
F 1 "74LS244" H 8100 4750 60  0000 C CNN
F 2 "20dip300" H 8000 5150 60  0001 C CNN
	1    8000 5150
	1    0    0    -1  
$EndComp
Text Label 7000 4650 0    60   ~ 0
A23
Text Label 7000 4750 0    60   ~ 0
A22
Text Label 7000 4850 0    60   ~ 0
A21
Text Label 7000 4950 0    60   ~ 0
A20
Text Label 7000 5050 0    60   ~ 0
A19
Text Label 7000 5150 0    60   ~ 0
A18
Text Label 7000 5250 0    60   ~ 0
A17
Text Label 7000 5350 0    60   ~ 0
A16
Text Label 7000 5650 0    60   ~ 0
GND
Text Label 7000 5550 0    60   ~ 0
GND
Text Label 8750 5350 0    60   ~ 0
bA16
Text Label 8750 5250 0    60   ~ 0
bA17
Text Label 8750 5150 0    60   ~ 0
bA18
Text Label 8750 5050 0    60   ~ 0
bA19
Text Label 8750 4950 0    60   ~ 0
bA20
Text Label 8750 4850 0    60   ~ 0
bA21
Text Label 8750 4750 0    60   ~ 0
bA22
Text Label 8750 4650 0    60   ~ 0
bA23
NoConn ~ 21250 12400
NoConn ~ 21250 12900
Text Label 700  5650 0    60   ~ 0
LOWER_SELECT
Text Label 650  9500 0    60   ~ 0
4MG_RAM_SELECT
Text Label 3100 9600 0    60   ~ 0
RFU3
Text Label 3100 9150 0    60   ~ 0
TMA0*
Text Label 3100 8950 0    60   ~ 0
PHANTOM*
$Comp
L CONN_2 P?
U 1 1 4B83165A
P 3950 9500
AR Path="/9B00384B83165A" Ref="P?"  Part="1" 
AR Path="/4B83165A" Ref="P55"  Part="1" 
AR Path="/773F8EB44B83165A" Ref="P?"  Part="1" 
AR Path="/94B83165A" Ref="P"  Part="1" 
AR Path="/773F65F14B83165A" Ref="P55"  Part="1" 
AR Path="/23C6504B83165A" Ref="P55"  Part="1" 
AR Path="/FFFFFFFF4B83165A" Ref="P55"  Part="1" 
AR Path="/26D74B83165A" Ref="P55"  Part="1" 
AR Path="/24B83165A" Ref="P55"  Part="1" 
AR Path="/23BC884B83165A" Ref="P55"  Part="1" 
AR Path="/6FE934E34B83165A" Ref="P55"  Part="1" 
AR Path="/23C34C4B83165A" Ref="P55"  Part="1" 
AR Path="/23CBC44B83165A" Ref="P55"  Part="1" 
F 0 "P55" V 3900 9500 40  0000 C CNN
F 1 "CONN_2" V 4000 9500 40  0000 C CNN
F 2 "PIN_ARRAY_2X1" H 3950 9500 60  0001 C CNN
	1    3950 9500
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K?
U 1 1 4B831652
P 3950 9050
AR Path="/9B00384B831652" Ref="K?"  Part="1" 
AR Path="/4B831652" Ref="K1"  Part="1" 
AR Path="/773F8EB44B831652" Ref="K?"  Part="1" 
AR Path="/23C9F04B831652" Ref="K1"  Part="1" 
AR Path="/94B831652" Ref="K"  Part="1" 
AR Path="/773F65F14B831652" Ref="K1"  Part="1" 
AR Path="/23C6504B831652" Ref="K1"  Part="1" 
AR Path="/FFFFFFFF4B831652" Ref="K1"  Part="1" 
AR Path="/24B831652" Ref="K1"  Part="1" 
AR Path="/23BC884B831652" Ref="K1"  Part="1" 
AR Path="/6FE934E34B831652" Ref="K1"  Part="1" 
AR Path="/23C34C4B831652" Ref="K1"  Part="1" 
AR Path="/23CBC44B831652" Ref="K1"  Part="1" 
F 0 "K1" V 3900 9050 50  0000 C CNN
F 1 "CONN_3" V 4000 9050 40  0000 C CNN
F 2 "PIN_ARRAY_3X1" H 3950 9050 60  0001 C CNN
	1    3950 9050
	1    0    0    -1  
$EndComp
Text Label 800  6450 0    60   ~ 0
PHANTOM*
Text Label 800  6350 0    60   ~ 0
bA12
Text Label 800  6050 0    60   ~ 0
bA15
Text Label 800  6150 0    60   ~ 0
bA14
Text Label 800  6550 0    60   ~ 0
TMA0*
Text Label 800  6650 0    60   ~ 0
RFU3
$Comp
L GND #PWR?
U 1 1 4B831229
P 2600 7850
AR Path="/773F8EB44B831229" Ref="#PWR?"  Part="1" 
AR Path="/23C9F04B831229" Ref="#PWR8"  Part="1" 
AR Path="/94B831229" Ref="#PWR"  Part="1" 
AR Path="/4B831229" Ref="#PWR01"  Part="1" 
AR Path="/773F65F14B831229" Ref="#PWR01"  Part="1" 
AR Path="/6684D64B831229" Ref="#PWR01"  Part="1" 
AR Path="/FFFFFFFF4B831229" Ref="#PWR02"  Part="1" 
AR Path="/23BC884B831229" Ref="#PWR01"  Part="1" 
AR Path="/6FE934E34B831229" Ref="#PWR02"  Part="1" 
AR Path="/23C34C4B831229" Ref="#PWR02"  Part="1" 
AR Path="/23CBC44B831229" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 2600 7850 30  0001 C CNN
F 1 "GND" H 2600 7780 30  0001 C CNN
	1    2600 7850
	1    0    0    -1  
$EndComp
Text Label 800  6250 0    60   ~ 0
bA13
NoConn ~ 4000 6250
$Comp
L DIPS_08 SW?
U 1 1 4B831224
P 2800 7300
AR Path="/773F8EB44B831224" Ref="SW?"  Part="1" 
AR Path="/23C9F04B831224" Ref="SW4"  Part="1" 
AR Path="/94B831224" Ref="SW"  Part="1" 
AR Path="/4B831224" Ref="SW4"  Part="1" 
AR Path="/773F65F14B831224" Ref="SW4"  Part="1" 
AR Path="/23C6504B831224" Ref="SW4"  Part="1" 
AR Path="/FFFFFFFF4B831224" Ref="SW4"  Part="1" 
AR Path="/24B831224" Ref="SW4"  Part="1" 
AR Path="/23BC884B831224" Ref="SW4"  Part="1" 
AR Path="/6FE934E34B831224" Ref="SW4"  Part="1" 
AR Path="/23C34C4B831224" Ref="SW4"  Part="1" 
AR Path="/23CBC44B831224" Ref="SW4"  Part="1" 
F 0 "SW4" V 2350 7300 60  0000 C CNN
F 1 "DIPS_08" V 3250 7300 60  0000 C CNN
F 2 "SWDIP8" H 2800 7300 60  0001 C CNN
	1    2800 7300
	0    1    1    0   
$EndComp
$Comp
L CONN_8 P?
U 1 1 4B831223
P 1650 6400
AR Path="/773F8EB44B831223" Ref="P?"  Part="1" 
AR Path="/4B831223" Ref="P50"  Part="1" 
AR Path="/94B831223" Ref="P"  Part="1" 
AR Path="/773F65F14B831223" Ref="P50"  Part="1" 
AR Path="/23C6504B831223" Ref="P50"  Part="1" 
AR Path="/FFFFFFFF4B831223" Ref="P50"  Part="1" 
AR Path="/24B831223" Ref="P50"  Part="1" 
AR Path="/23BC884B831223" Ref="P50"  Part="1" 
AR Path="/6FE934E34B831223" Ref="P50"  Part="1" 
AR Path="/23C34C4B831223" Ref="P50"  Part="1" 
AR Path="/23CBC44B831223" Ref="P50"  Part="1" 
F 0 "P50" V 1600 6400 60  0000 C CNN
F 1 "CONN_8" V 1700 6400 60  0000 C CNN
F 2 "SIL-8" H 1650 6400 60  0001 C CNN
	1    1650 6400
	1    0    0    -1  
$EndComp
$Comp
L CONN_8 P?
U 1 1 4B831222
P 2250 6400
AR Path="/773F8EB44B831222" Ref="P?"  Part="1" 
AR Path="/4B831222" Ref="P52"  Part="1" 
AR Path="/94B831222" Ref="P"  Part="1" 
AR Path="/773F65F14B831222" Ref="P52"  Part="1" 
AR Path="/23C6504B831222" Ref="P52"  Part="1" 
AR Path="/FFFFFFFF4B831222" Ref="P52"  Part="1" 
AR Path="/24B831222" Ref="P52"  Part="1" 
AR Path="/23BC884B831222" Ref="P52"  Part="1" 
AR Path="/23C34C4B831222" Ref="P52"  Part="1" 
AR Path="/6FE934E34B831222" Ref="P52"  Part="1" 
AR Path="/23CBC44B831222" Ref="P52"  Part="1" 
F 0 "P52" V 2200 6400 60  0000 C CNN
F 1 "CONN_8" V 2300 6400 60  0000 C CNN
F 2 "SIL-8" H 2250 6400 60  0001 C CNN
	1    2250 6400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4B831221
P 1900 6850
AR Path="/773F8EB44B831221" Ref="#PWR?"  Part="1" 
AR Path="/4B831221" Ref="#PWR02"  Part="1" 
AR Path="/94B831221" Ref="#PWR"  Part="1" 
AR Path="/773F65F14B831221" Ref="#PWR04"  Part="1" 
AR Path="/6684D64B831221" Ref="#PWR04"  Part="1" 
AR Path="/FFFFFFFF4B831221" Ref="#PWR05"  Part="1" 
AR Path="/23BC884B831221" Ref="#PWR04"  Part="1" 
AR Path="/23C34C4B831221" Ref="#PWR05"  Part="1" 
AR Path="/6FE934E34B831221" Ref="#PWR05"  Part="1" 
AR Path="/23CBC44B831221" Ref="#PWR04"  Part="1" 
F 0 "#PWR02" H 1900 6850 30  0001 C CNN
F 1 "GND" H 1900 6780 30  0001 C CNN
	1    1900 6850
	1    0    0    -1  
$EndComp
$Comp
L CONN_8 P?
U 1 1 4B831220
P 2650 6400
AR Path="/773F8EB44B831220" Ref="P?"  Part="1" 
AR Path="/4B831220" Ref="P54"  Part="1" 
AR Path="/94B831220" Ref="P"  Part="1" 
AR Path="/773F65F14B831220" Ref="P54"  Part="1" 
AR Path="/23C6504B831220" Ref="P54"  Part="1" 
AR Path="/FFFFFFFF4B831220" Ref="P54"  Part="1" 
AR Path="/24B831220" Ref="P54"  Part="1" 
AR Path="/23BC884B831220" Ref="P54"  Part="1" 
AR Path="/23C34C4B831220" Ref="P54"  Part="1" 
AR Path="/6FE934E34B831220" Ref="P54"  Part="1" 
AR Path="/23CBC44B831220" Ref="P54"  Part="1" 
F 0 "P54" V 2600 6400 60  0000 C CNN
F 1 "CONN_8" V 2700 6400 60  0000 C CNN
F 2 "SIL-8" H 2650 6400 60  0001 C CNN
	1    2650 6400
	-1   0    0    -1  
$EndComp
$Comp
L CONN_8 P?
U 1 1 4B8311F4
P 2650 4000
AR Path="/2300384B8311F4" Ref="P?"  Part="1" 
AR Path="/4B8311F4" Ref="P53"  Part="1" 
AR Path="/773F8EB44B8311F4" Ref="P?"  Part="1" 
AR Path="/94B8311F4" Ref="P"  Part="1" 
AR Path="/773F65F14B8311F4" Ref="P53"  Part="1" 
AR Path="/23C6504B8311F4" Ref="P53"  Part="1" 
AR Path="/FFFFFFFF4B8311F4" Ref="P53"  Part="1" 
AR Path="/24B8311F4" Ref="P53"  Part="1" 
AR Path="/23BC884B8311F4" Ref="P53"  Part="1" 
AR Path="/23C34C4B8311F4" Ref="P53"  Part="1" 
AR Path="/6FE934E34B8311F4" Ref="P53"  Part="1" 
AR Path="/23CBC44B8311F4" Ref="P53"  Part="1" 
F 0 "P53" V 2600 4000 60  0000 C CNN
F 1 "CONN_8" V 2700 4000 60  0000 C CNN
F 2 "SIL-8" H 2650 4000 60  0001 C CNN
	1    2650 4000
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4B8311EF
P 1900 4450
AR Path="/9B00384B8311EF" Ref="#PWR?"  Part="1" 
AR Path="/4B8311EF" Ref="#PWR03"  Part="1" 
AR Path="/773F8EB44B8311EF" Ref="#PWR?"  Part="1" 
AR Path="/23C9F04B8311EF" Ref="#PWR1"  Part="1" 
AR Path="/94B8311EF" Ref="#PWR"  Part="1" 
AR Path="/773F65F14B8311EF" Ref="#PWR05"  Part="1" 
AR Path="/6684D64B8311EF" Ref="#PWR05"  Part="1" 
AR Path="/FFFFFFFF4B8311EF" Ref="#PWR06"  Part="1" 
AR Path="/23BC884B8311EF" Ref="#PWR05"  Part="1" 
AR Path="/23C34C4B8311EF" Ref="#PWR06"  Part="1" 
AR Path="/6FE934E34B8311EF" Ref="#PWR06"  Part="1" 
AR Path="/23CBC44B8311EF" Ref="#PWR05"  Part="1" 
F 0 "#PWR03" H 1900 4450 30  0001 C CNN
F 1 "GND" H 1900 4380 30  0001 C CNN
	1    1900 4450
	1    0    0    -1  
$EndComp
$Comp
L CONN_8 P?
U 1 1 4B8311E0
P 2250 4000
AR Path="/384B8311E0" Ref="P?"  Part="1" 
AR Path="/4B8311E0" Ref="P51"  Part="1" 
AR Path="/773F8EB44B8311E0" Ref="P?"  Part="1" 
AR Path="/94B8311E0" Ref="P"  Part="1" 
AR Path="/773F65F14B8311E0" Ref="P51"  Part="1" 
AR Path="/23C6504B8311E0" Ref="P51"  Part="1" 
AR Path="/FFFFFFFF4B8311E0" Ref="P51"  Part="1" 
AR Path="/24B8311E0" Ref="P51"  Part="1" 
AR Path="/23BC884B8311E0" Ref="P51"  Part="1" 
AR Path="/23C34C4B8311E0" Ref="P51"  Part="1" 
AR Path="/6FE934E34B8311E0" Ref="P51"  Part="1" 
AR Path="/23CBC44B8311E0" Ref="P51"  Part="1" 
F 0 "P51" V 2200 4000 60  0000 C CNN
F 1 "CONN_8" V 2300 4000 60  0000 C CNN
F 2 "SIL-8" H 2250 4000 60  0001 C CNN
	1    2250 4000
	1    0    0    -1  
$EndComp
$Comp
L CONN_8 P?
U 1 1 4B8311CE
P 1650 4000
AR Path="/9B00384B8311CE" Ref="P?"  Part="1" 
AR Path="/4B8311CE" Ref="P49"  Part="1" 
AR Path="/773F8EB44B8311CE" Ref="P?"  Part="1" 
AR Path="/94B8311CE" Ref="P"  Part="1" 
AR Path="/773F65F14B8311CE" Ref="P49"  Part="1" 
AR Path="/23C6504B8311CE" Ref="P49"  Part="1" 
AR Path="/FFFFFFFF4B8311CE" Ref="P49"  Part="1" 
AR Path="/24B8311CE" Ref="P49"  Part="1" 
AR Path="/23BC884B8311CE" Ref="P49"  Part="1" 
AR Path="/23C34C4B8311CE" Ref="P49"  Part="1" 
AR Path="/6FE934E34B8311CE" Ref="P49"  Part="1" 
AR Path="/23CBC44B8311CE" Ref="P49"  Part="1" 
F 0 "P49" V 1600 4000 60  0000 C CNN
F 1 "CONN_8" V 1700 4000 60  0000 C CNN
F 2 "SIL-8" H 1650 4000 60  0001 C CNN
	1    1650 4000
	1    0    0    -1  
$EndComp
Text Label 600  1250 0    60   ~ 0
4MG_RAM_SELECT
$Comp
L 74LS240 U?
U 1 1 4B3CDCF5
P 2300 1650
AR Path="/69698AFC4B3CDCF5" Ref="U?"  Part="1" 
AR Path="/393639364B3CDCF5" Ref="U?"  Part="1" 
AR Path="/23D6B44B3CDCF5" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3CDCF5" Ref="U?"  Part="1" 
AR Path="/286058A4B3CDCF5" Ref="U"  Part="1" 
AR Path="/4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/A84B3CDCF5" Ref="U28"  Part="1" 
AR Path="/94B3CDCF5" Ref="U28"  Part="1" 
AR Path="/FFFFFFF04B3CDCF5" Ref="U28"  Part="1" 
AR Path="/14B3CDCF5" Ref="U28"  Part="1" 
AR Path="/6FF0DD404B3CDCF5" Ref="U28"  Part="1" 
AR Path="/23D9304B3CDCF5" Ref="U28"  Part="1" 
AR Path="/23D8D44B3CDCF5" Ref="U28"  Part="1" 
AR Path="/4031DDF34B3CDCF5" Ref="U28"  Part="1" 
AR Path="/3FEFFFFF4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/3FE88B434B3CDCF5" Ref="U28"  Part="1" 
AR Path="/4032778D4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/5AD7153D4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/A4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/403091264B3CDCF5" Ref="U28"  Part="1" 
AR Path="/403051264B3CDCF5" Ref="U28"  Part="1" 
AR Path="/4032F78D4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/403251264B3CDCF5" Ref="U28"  Part="1" 
AR Path="/4032AAC04B3CDCF5" Ref="U28"  Part="1" 
AR Path="/4030D1264B3CDCF5" Ref="U28"  Part="1" 
AR Path="/4031778D4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/3FEA24DD4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/2600004B3CDCF5" Ref="U28"  Part="1" 
AR Path="/6FE934E34B3CDCF5" Ref="U28"  Part="1" 
AR Path="/773F8EB44B3CDCF5" Ref="U28"  Part="1" 
AR Path="/23C9F04B3CDCF5" Ref="U28"  Part="1" 
AR Path="/24B3CDCF5" Ref="U28"  Part="1" 
AR Path="/23BC884B3CDCF5" Ref="U28"  Part="1" 
AR Path="/DC0C124B3CDCF5" Ref="U28"  Part="1" 
AR Path="/23C34C4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/23CBC44B3CDCF5" Ref="U28"  Part="1" 
AR Path="/69549BC04B3CDCF5" Ref="U28"  Part="1" 
AR Path="/23C6504B3CDCF5" Ref="U28"  Part="1" 
AR Path="/39803EA4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/FFFFFFFF4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/D058A04B3CDCF5" Ref="U28"  Part="1" 
AR Path="/1607D44B3CDCF5" Ref="U28"  Part="1" 
AR Path="/D1C3804B3CDCF5" Ref="U28"  Part="1" 
AR Path="/23D70C4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/2104B3CDCF5" Ref="U28"  Part="1" 
AR Path="/D1CA184B3CDCF5" Ref="U28"  Part="1" 
AR Path="/F4BF4B3CDCF5" Ref="U28"  Part="1" 
AR Path="/262F604B3CDCF5" Ref="U28"  Part="1" 
AR Path="/22C6E204B3CDCF5" Ref="U28"  Part="1" 
AR Path="/384B3CDCF5" Ref="U28"  Part="1" 
AR Path="/C50E4B3CDCF5" Ref="U28"  Part="1" 
F 0 "U28" H 2350 1450 60  0000 C CNN
F 1 "74LS240" H 2400 1250 60  0000 C CNN
F 2 "20dip300" H 2300 1650 60  0001 C CNN
	1    2300 1650
	1    0    0    -1  
$EndComp
$Comp
L DIPS_08 SW?
U 1 1 4B815554
P 2800 4900
AR Path="/2300384B815554" Ref="SW?"  Part="1" 
AR Path="/4B815554" Ref="SW3"  Part="1" 
AR Path="/773F8EB44B815554" Ref="SW3"  Part="1" 
AR Path="/23C9F04B815554" Ref="SW3"  Part="1" 
AR Path="/94B815554" Ref="SW"  Part="1" 
AR Path="/773F65F14B815554" Ref="SW3"  Part="1" 
AR Path="/23C34C4B815554" Ref="SW3"  Part="1" 
AR Path="/9F1D4B815554" Ref="SW3"  Part="1" 
AR Path="/24B815554" Ref="SW3"  Part="1" 
AR Path="/23BC884B815554" Ref="SW3"  Part="1" 
AR Path="/6FE934E34B815554" Ref="SW3"  Part="1" 
AR Path="/FFFFFFFF4B815554" Ref="SW3"  Part="1" 
AR Path="/23CBC44B815554" Ref="SW3"  Part="1" 
AR Path="/23C6504B815554" Ref="SW3"  Part="1" 
F 0 "SW3" V 2350 4900 60  0000 C CNN
F 1 "DIPS_08" V 3250 4900 60  0000 C CNN
F 2 "SWDIP8" H 2800 4900 60  0001 C CNN
	1    2800 4900
	0    1    1    0   
$EndComp
$Comp
L LED D?
U 1 1 4B80177F
P 8300 1700
AR Path="/A200384B80177F" Ref="D?"  Part="1" 
AR Path="/4B80177F" Ref="D8"  Part="1" 
AR Path="/313737464B80177F" Ref="D?"  Part="1" 
AR Path="/288A384B80177F" Ref="D?"  Part="1" 
AR Path="/13D09344B80177F" Ref="D"  Part="1" 
AR Path="/7E428DAC4B80177F" Ref="D8"  Part="1" 
AR Path="/100004B80177F" Ref="D8"  Part="1" 
AR Path="/280DC04B80177F" Ref="D8"  Part="1" 
AR Path="/26709064B80177F" Ref="D"  Part="1" 
AR Path="/773F65F14B80177F" Ref="D8"  Part="1" 
AR Path="/114B80177F" Ref="D8"  Part="1" 
AR Path="/2705D84B80177F" Ref="D8"  Part="1" 
AR Path="/28B8B84B80177F" Ref="D8"  Part="1" 
AR Path="/18108AC4B80177F" Ref="D"  Part="1" 
AR Path="/773F8EB44B80177F" Ref="D8"  Part="1" 
AR Path="/23C9F04B80177F" Ref="D8"  Part="1" 
AR Path="/94B80177F" Ref="D"  Part="1" 
AR Path="/28E8A04B80177F" Ref="D8"  Part="1" 
AR Path="/17608284B80177F" Ref="D"  Part="1" 
AR Path="/6FE934E34B80177F" Ref="D8"  Part="1" 
AR Path="/FFFFFFFF4B80177F" Ref="D8"  Part="1" 
AR Path="/3E384B80177F" Ref="D8"  Part="1" 
AR Path="/24B80177F" Ref="D8"  Part="1" 
AR Path="/23BC884B80177F" Ref="D8"  Part="1" 
AR Path="/23C34C4B80177F" Ref="D8"  Part="1" 
AR Path="/23CBC44B80177F" Ref="D8"  Part="1" 
AR Path="/23C6504B80177F" Ref="D8"  Part="1" 
F 0 "D8" H 8300 1800 50  0000 C CNN
F 1 "LED" H 8300 1600 50  0000 C CNN
F 2 "LEDV" H 8300 1700 60  0001 C CNN
	1    8300 1700
	-1   0    0    1   
$EndComp
$Comp
L 74LS32 U?
U 3 1 4B3C0E28
P 4900 6150
AR Path="/23D9D84B3C0E28" Ref="U?"  Part="1" 
AR Path="/394433324B3C0E28" Ref="U?"  Part="1" 
AR Path="/23D6544B3C0E28" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3C0E28" Ref="U?"  Part="1" 
AR Path="/2F007824B3C0E28" Ref="U?"  Part="3" 
AR Path="/4B3C0E28" Ref="U25"  Part="3" 
AR Path="/A84B3C0E28" Ref="U25"  Part="3" 
AR Path="/94B3C0E28" Ref="U25"  Part="3" 
AR Path="/FFFFFFF04B3C0E28" Ref="U25"  Part="3" 
AR Path="/6FF0DD404B3C0E28" Ref="U25"  Part="3" 
AR Path="/14B3C0E28" Ref="U25"  Part="3" 
AR Path="/23D9304B3C0E28" Ref="U25"  Part="3" 
AR Path="/23D8D44B3C0E28" Ref="U25"  Part="3" 
AR Path="/4031DDF34B3C0E28" Ref="U25"  Part="3" 
AR Path="/3FEFFFFF4B3C0E28" Ref="U25"  Part="3" 
AR Path="/3FE88B434B3C0E28" Ref="U25"  Part="3" 
AR Path="/4032778D4B3C0E28" Ref="U25"  Part="3" 
AR Path="/5AD7153D4B3C0E28" Ref="U25"  Part="3" 
AR Path="/A4B3C0E28" Ref="U25"  Part="3" 
AR Path="/403091264B3C0E28" Ref="U25"  Part="3" 
AR Path="/403051264B3C0E28" Ref="U25"  Part="3" 
AR Path="/4032F78D4B3C0E28" Ref="U25"  Part="3" 
AR Path="/403251264B3C0E28" Ref="U25"  Part="3" 
AR Path="/4032AAC04B3C0E28" Ref="U25"  Part="3" 
AR Path="/4030D1264B3C0E28" Ref="U25"  Part="3" 
AR Path="/4031778D4B3C0E28" Ref="U25"  Part="3" 
AR Path="/3FEA24DD4B3C0E28" Ref="U25"  Part="3" 
AR Path="/2600004B3C0E28" Ref="U25"  Part="3" 
AR Path="/6FE934E34B3C0E28" Ref="U25"  Part="3" 
AR Path="/773F8EB44B3C0E28" Ref="U25"  Part="3" 
AR Path="/23C9F04B3C0E28" Ref="U25"  Part="3" 
AR Path="/23BC884B3C0E28" Ref="U25"  Part="3" 
AR Path="/DC0C124B3C0E28" Ref="U25"  Part="3" 
AR Path="/23C34C4B3C0E28" Ref="U25"  Part="3" 
AR Path="/23CBC44B3C0E28" Ref="U25"  Part="3" 
AR Path="/69549BC04B3C0E28" Ref="U25"  Part="3" 
AR Path="/23C6504B3C0E28" Ref="U25"  Part="3" 
AR Path="/39803EA4B3C0E28" Ref="U25"  Part="3" 
AR Path="/FFFFFFFF4B3C0E28" Ref="U25"  Part="3" 
AR Path="/D058A04B3C0E28" Ref="U25"  Part="3" 
AR Path="/1607D44B3C0E28" Ref="U25"  Part="3" 
AR Path="/D1C3804B3C0E28" Ref="U25"  Part="3" 
AR Path="/24B3C0E28" Ref="U25"  Part="3" 
AR Path="/23D70C4B3C0E28" Ref="U25"  Part="3" 
AR Path="/2104B3C0E28" Ref="U25"  Part="3" 
AR Path="/D1CA184B3C0E28" Ref="U25"  Part="3" 
AR Path="/F4BF4B3C0E28" Ref="U25"  Part="3" 
AR Path="/262F604B3C0E28" Ref="U25"  Part="3" 
AR Path="/22C6E204B3C0E28" Ref="U25"  Part="3" 
AR Path="/384B3C0E28" Ref="U25"  Part="3" 
F 0 "U25" H 4900 6200 60  0000 C CNN
F 1 "74LS32" H 4900 6100 60  0000 C CNN
F 2 "14dip300" H 4900 6150 60  0001 C CNN
	3    4900 6150
	1    0    0    -1  
$EndComp
$Comp
L 74LS32 U?
U 4 1 4B800A69
P 20650 13400
AR Path="/9F00384B800A69" Ref="U?"  Part="1" 
AR Path="/4B800A69" Ref="U25"  Part="4" 
AR Path="/304136394B800A69" Ref="U25"  Part="1" 
AR Path="/29D9C04B800A69" Ref="U?"  Part="1" 
AR Path="/D0091A4B800A69" Ref="U"  Part="1" 
AR Path="/7E428DAC4B800A69" Ref="U25"  Part="1" 
AR Path="/7E42B4154B800A69" Ref="U25"  Part="1" 
AR Path="/23D6704B800A69" Ref="U25"  Part="1" 
AR Path="/2851A84B800A69" Ref="U25"  Part="1" 
AR Path="/14708C44B800A69" Ref="U"  Part="4" 
AR Path="/FFFFFFFF4B800A69" Ref="U25"  Part="4" 
AR Path="/262F604B800A69" Ref="U25"  Part="4" 
AR Path="/14B800A69" Ref="U25"  Part="4" 
AR Path="/773F8EB44B800A69" Ref="U25"  Part="4" 
AR Path="/23C9F04B800A69" Ref="U25"  Part="4" 
AR Path="/94B800A69" Ref="U"  Part="4" 
AR Path="/773F65F14B800A69" Ref="U25"  Part="4" 
AR Path="/23C34C4B800A69" Ref="U25"  Part="4" 
AR Path="/24B800A69" Ref="U25"  Part="4" 
AR Path="/23BC884B800A69" Ref="U25"  Part="4" 
AR Path="/23CBC44B800A69" Ref="U25"  Part="4" 
AR Path="/384B800A69" Ref="U25"  Part="4" 
AR Path="/6FE934E34B800A69" Ref="U25"  Part="4" 
AR Path="/23C6504B800A69" Ref="U25"  Part="4" 
F 0 "U25" H 20650 13450 60  0000 C CNN
F 1 "74LS32" H 20650 13350 60  0000 C CNN
F 2 "14dip300" H 20650 13400 60  0001 C CNN
	4    20650 13400
	1    0    0    -1  
$EndComp
$Comp
L 74LS01 U?
U 3 1 4B800993
P 12400 1200
AR Path="/2300384B800993" Ref="U?"  Part="1" 
AR Path="/4B800993" Ref="U10"  Part="3" 
AR Path="/303939334B800993" Ref="U?"  Part="1" 
AR Path="/2921384B800993" Ref="U?"  Part="1" 
AR Path="/F208F04B800993" Ref="U"  Part="3" 
AR Path="/22C6E204B800993" Ref="U10"  Part="3" 
AR Path="/FFFFFFFF4B800993" Ref="U10"  Part="3" 
AR Path="/262F604B800993" Ref="U10"  Part="3" 
AR Path="/14B800993" Ref="U10"  Part="3" 
AR Path="/773F8EB44B800993" Ref="U10"  Part="3" 
AR Path="/23C9F04B800993" Ref="U10"  Part="3" 
AR Path="/94B800993" Ref="U"  Part="3" 
AR Path="/773F65F14B800993" Ref="U10"  Part="3" 
AR Path="/23C34C4B800993" Ref="U10"  Part="3" 
AR Path="/24B800993" Ref="U10"  Part="3" 
AR Path="/23BC884B800993" Ref="U10"  Part="3" 
AR Path="/23CBC44B800993" Ref="U10"  Part="3" 
AR Path="/384B800993" Ref="U10"  Part="3" 
AR Path="/6FE934E34B800993" Ref="U10"  Part="3" 
AR Path="/23C6504B800993" Ref="U10"  Part="3" 
F 0 "U10" H 12400 1250 60  0000 C CNN
F 1 "74LS01" H 12400 1150 60  0000 C CNN
F 2 "14dip300" H 12400 1200 60  0001 C CNN
	3    12400 1200
	1    0    0    -1  
$EndComp
$Comp
L 74LS11 U?
U 3 1 4B8008EE
P 20650 13900
AR Path="/9F00384B8008EE" Ref="U?"  Part="1" 
AR Path="/4B8008EE" Ref="U13"  Part="3" 
AR Path="/303845454B8008EE" Ref="U?"  Part="1" 
AR Path="/2E48D84B8008EE" Ref="U?"  Part="1" 
AR Path="/2F08BC4B8008EE" Ref="U"  Part="3" 
AR Path="/262F604B8008EE" Ref="U13"  Part="3" 
AR Path="/FFFFFFFF4B8008EE" Ref="U13"  Part="3" 
AR Path="/22C6E204B8008EE" Ref="U13"  Part="3" 
AR Path="/14B8008EE" Ref="U13"  Part="3" 
AR Path="/773F8EB44B8008EE" Ref="U13"  Part="3" 
AR Path="/94B8008EE" Ref="U"  Part="3" 
AR Path="/773F65F14B8008EE" Ref="U13"  Part="3" 
AR Path="/23C34C4B8008EE" Ref="U13"  Part="3" 
AR Path="/24B8008EE" Ref="U13"  Part="3" 
AR Path="/23BC884B8008EE" Ref="U13"  Part="3" 
AR Path="/23CBC44B8008EE" Ref="U13"  Part="3" 
AR Path="/384B8008EE" Ref="U13"  Part="3" 
AR Path="/23C9F04B8008EE" Ref="U13"  Part="3" 
AR Path="/6FE934E34B8008EE" Ref="U13"  Part="3" 
AR Path="/23C6504B8008EE" Ref="U13"  Part="3" 
F 0 "U13" H 20650 13950 60  0000 C CNN
F 1 "74LS11" H 20650 13850 60  0000 C CNN
F 2 "14dip300" H 20650 13900 60  0001 C CNN
	3    20650 13900
	1    0    0    -1  
$EndComp
Text Label 3050 1350 0    60   ~ 0
PHI*
Text Label 1250 1350 0    60   ~ 0
PHI
Text Label 600  1450 0    60   ~ 0
BOARD_WAIT
Text Label 14000 2750 0    60   ~ 0
BOARD_WAIT
NoConn ~ 21250 13400
Text Label 3050 1150 0    60   ~ 0
4MG_RAM_SELECT
Text Label 600  1150 0    60   ~ 0
4MG_RAM_SELECT*
Text Label 4650 1850 0    60   ~ 0
4MG_RAM_SELECT*
Text Label 5300 1350 0    60   ~ 0
bA21
Text Label 5300 1500 0    60   ~ 0
bA20
$Comp
L VCC #PWR?
U 1 1 4B800491
P 8500 1150
AR Path="/2300384B800491" Ref="#PWR?"  Part="1" 
AR Path="/4B800491" Ref="#PWR04"  Part="1" 
AR Path="/773F65F14B800491" Ref="#PWR06"  Part="1" 
AR Path="/6684D64B800491" Ref="#PWR06"  Part="1" 
AR Path="/773F8EB44B800491" Ref="#PWR02"  Part="1" 
AR Path="/23C9F04B800491" Ref="#PWR23"  Part="1" 
AR Path="/94B800491" Ref="#PWR"  Part="1" 
AR Path="/23C6504B800491" Ref="#PWR01"  Part="1" 
AR Path="/23C34C4B800491" Ref="#PWR07"  Part="1" 
AR Path="/F4BF4B800491" Ref="#PWR01"  Part="1" 
AR Path="/23BC884B800491" Ref="#PWR06"  Part="1" 
AR Path="/14B800491" Ref="#PWR02"  Part="1" 
AR Path="/FFFFFFFF4B800491" Ref="#PWR07"  Part="1" 
AR Path="/262F604B800491" Ref="#PWR01"  Part="1" 
AR Path="/22C6E204B800491" Ref="#PWR01"  Part="1" 
AR Path="/23CBC44B800491" Ref="#PWR06"  Part="1" 
AR Path="/384B800491" Ref="#PWR02"  Part="1" 
AR Path="/6FF0DD404B800491" Ref="#PWR02"  Part="1" 
AR Path="/6FE934E34B800491" Ref="#PWR02"  Part="1" 
F 0 "#PWR04" H 8500 1250 30  0001 C CNN
F 1 "VCC" H 8500 1250 30  0000 C CNN
	1    8500 1150
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4B3CE98F
P 4150 1250
AR Path="/69698AFC4B3CE98F" Ref="#PWR?"  Part="1" 
AR Path="/393639364B3CE98F" Ref="#PWR?"  Part="1" 
AR Path="/4B3CE98F" Ref="#PWR05"  Part="1" 
AR Path="/A84B3CE98F" Ref="#PWR7"  Part="1" 
AR Path="/94B3CE98F" Ref="#PWR6"  Part="1" 
AR Path="/FFFFFFF04B3CE98F" Ref="#PWR6"  Part="1" 
AR Path="/25E4B3CE98F" Ref="#PWR016"  Part="1" 
AR Path="/6FF0DD404B3CE98F" Ref="#PWR03"  Part="1" 
AR Path="/14B3CE98F" Ref="#PWR03"  Part="1" 
AR Path="/23D9304B3CE98F" Ref="#PWR02"  Part="1" 
AR Path="/23D8D44B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/4031DDF34B3CE98F" Ref="#PWR02"  Part="1" 
AR Path="/3FEFFFFF4B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/3FE88B434B3CE98F" Ref="#PWR02"  Part="1" 
AR Path="/4032778D4B3CE98F" Ref="#PWR08"  Part="1" 
AR Path="/363030314B3CE98F" Ref="#PWR016"  Part="1" 
AR Path="/5AD7153D4B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/A4B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/403091264B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/403051264B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/4032F78D4B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/403251264B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/4032AAC04B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/4030D1264B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/4031778D4B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/3FEA24DD4B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/2600004B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/6FE934E34B3CE98F" Ref="#PWR03"  Part="1" 
AR Path="/773F65F14B3CE98F" Ref="#PWR07"  Part="1" 
AR Path="/773F8EB44B3CE98F" Ref="#PWR03"  Part="1" 
AR Path="/23C9F04B3CE98F" Ref="#PWR5"  Part="1" 
AR Path="/23BC884B3CE98F" Ref="#PWR07"  Part="1" 
AR Path="/DC0C124B3CE98F" Ref="#PWR017"  Part="1" 
AR Path="/6684D64B3CE98F" Ref="#PWR07"  Part="1" 
AR Path="/23C34C4B3CE98F" Ref="#PWR08"  Part="1" 
AR Path="/23CBC44B3CE98F" Ref="#PWR07"  Part="1" 
AR Path="/69549BC04B3CE98F" Ref="#PWR018"  Part="1" 
AR Path="/23C6504B3CE98F" Ref="#PWR02"  Part="1" 
AR Path="/39803EA4B3CE98F" Ref="#PWR022"  Part="1" 
AR Path="/FFFFFFFF4B3CE98F" Ref="#PWR08"  Part="1" 
AR Path="/D058A04B3CE98F" Ref="#PWR023"  Part="1" 
AR Path="/1607D44B3CE98F" Ref="#PWR023"  Part="1" 
AR Path="/D1C3804B3CE98F" Ref="#PWR023"  Part="1" 
AR Path="/24B3CE98F" Ref="#PWR023"  Part="1" 
AR Path="/23D70C4B3CE98F" Ref="#PWR023"  Part="1" 
AR Path="/2104B3CE98F" Ref="#PWR023"  Part="1" 
AR Path="/D1CA184B3CE98F" Ref="#PWR023"  Part="1" 
AR Path="/F4BF4B3CE98F" Ref="#PWR02"  Part="1" 
AR Path="/262F604B3CE98F" Ref="#PWR02"  Part="1" 
AR Path="/22C6E204B3CE98F" Ref="#PWR02"  Part="1" 
AR Path="/384B3CE98F" Ref="#PWR03"  Part="1" 
F 0 "#PWR05" H 4150 1350 30  0001 C CNN
F 1 "VCC" H 4150 1350 30  0000 C CNN
	1    4150 1250
	1    0    0    -1  
$EndComp
NoConn ~ 21250 13900
Text Notes 4200 1500 0    60   ~ 0
WAIT LED
$Comp
L R R?
U 1 1 4B7FFFAC
P 3500 1450
AR Path="/9F00384B7FFFAC" Ref="R?"  Part="1" 
AR Path="/4B7FFFAC" Ref="R9"  Part="1" 
AR Path="/464641434B7FFFAC" Ref="R?"  Part="1" 
AR Path="/773F65F14B7FFFAC" Ref="R9"  Part="1" 
AR Path="/773F8EB44B7FFFAC" Ref="R9"  Part="1" 
AR Path="/23C9F04B7FFFAC" Ref="R9"  Part="1" 
AR Path="/94B7FFFAC" Ref="R"  Part="1" 
AR Path="/23C6504B7FFFAC" Ref="R9"  Part="1" 
AR Path="/23C34C4B7FFFAC" Ref="R9"  Part="1" 
AR Path="/F4BF4B7FFFAC" Ref="R9"  Part="1" 
AR Path="/24B7FFFAC" Ref="R9"  Part="1" 
AR Path="/23BC884B7FFFAC" Ref="R9"  Part="1" 
AR Path="/14B7FFFAC" Ref="R9"  Part="1" 
AR Path="/FFFFFFFF4B7FFFAC" Ref="R9"  Part="1" 
AR Path="/262F604B7FFFAC" Ref="R9"  Part="1" 
AR Path="/22C6E204B7FFFAC" Ref="R9"  Part="1" 
AR Path="/23CBC44B7FFFAC" Ref="R9"  Part="1" 
AR Path="/384B7FFFAC" Ref="R9"  Part="1" 
AR Path="/6FE934E34B7FFFAC" Ref="R9"  Part="1" 
F 0 "R9" V 3580 1450 50  0000 C CNN
F 1 "470" V 3500 1450 50  0000 C CNN
F 2 "R3" H 3500 1450 60  0001 C CNN
	1    3500 1450
	0    1    1    0   
$EndComp
$Comp
L LED D?
U 1 1 4B7FFF9B
P 3950 1450
AR Path="/9F00384B7FFF9B" Ref="D?"  Part="1" 
AR Path="/4B7FFF9B" Ref="D10"  Part="1" 
AR Path="/773F65F14B7FFF9B" Ref="D10"  Part="1" 
AR Path="/773F8EB44B7FFF9B" Ref="D10"  Part="1" 
AR Path="/23C9F04B7FFF9B" Ref="D10"  Part="1" 
AR Path="/94B7FFF9B" Ref="D"  Part="1" 
AR Path="/23C6504B7FFF9B" Ref="D10"  Part="1" 
AR Path="/23C34C4B7FFF9B" Ref="D10"  Part="1" 
AR Path="/F4BF4B7FFF9B" Ref="D6"  Part="1" 
AR Path="/24B7FFF9B" Ref="D10"  Part="1" 
AR Path="/23BC884B7FFF9B" Ref="D10"  Part="1" 
AR Path="/14B7FFF9B" Ref="D10"  Part="1" 
AR Path="/FFFFFFFF4B7FFF9B" Ref="D10"  Part="1" 
AR Path="/262F604B7FFF9B" Ref="D6"  Part="1" 
AR Path="/22C6E204B7FFF9B" Ref="D6"  Part="1" 
AR Path="/23CBC44B7FFF9B" Ref="D10"  Part="1" 
AR Path="/B0084C4B7FFF9B" Ref="D"  Part="1" 
AR Path="/11809344B7FFF9B" Ref="D"  Part="1" 
AR Path="/384B7FFF9B" Ref="D10"  Part="1" 
AR Path="/7E428DAC4B7FFF9B" Ref="D10"  Part="1" 
AR Path="/7C9100004B7FFF9B" Ref="D2"  Part="1" 
AR Path="/16008AC4B7FFF9B" Ref="D"  Part="1" 
AR Path="/10A084E4B7FFF9B" Ref="D"  Part="1" 
AR Path="/112084E4B7FFF9B" Ref="D"  Part="1" 
AR Path="/C4080C4B7FFF9B" Ref="D"  Part="1" 
AR Path="/17B09424B7FFF9B" Ref="D"  Part="1" 
AR Path="/35312E304B7FFF9B" Ref="D2"  Part="1" 
AR Path="/6FE934E34B7FFF9B" Ref="D10"  Part="1" 
AR Path="/17008AC4B7FFF9B" Ref="D"  Part="1" 
F 0 "D10" H 3950 1550 50  0000 C CNN
F 1 "LED" H 3950 1350 50  0000 C CNN
F 2 "LEDV" H 3950 1450 60  0001 C CNN
	1    3950 1450
	-1   0    0    1   
$EndComp
Text Label 8400 2800 0    60   ~ 0
RAMCS3*
Text Label 8400 2600 0    60   ~ 0
RAMCS2*
Text Label 8400 2400 0    60   ~ 0
RAMCS1*
Text Label 8400 2200 0    60   ~ 0
RAMCS0*
$Comp
L JUMPER JP7
U 1 1 4B7FFF02
P 8050 2800
AR Path="/384B7FFF02" Ref="JP7"  Part="1" 
AR Path="/4B7FFF02" Ref="JP7"  Part="1" 
AR Path="/464630324B7FFF02" Ref="JP?"  Part="1" 
AR Path="/2606084B7FFF02" Ref="JP7"  Part="1" 
AR Path="/773F65F14B7FFF02" Ref="JP7"  Part="1" 
AR Path="/773F8EB44B7FFF02" Ref="JP7"  Part="1" 
AR Path="/94B7FFF02" Ref="JP"  Part="1" 
AR Path="/23C6504B7FFF02" Ref="JP7"  Part="1" 
AR Path="/23C34C4B7FFF02" Ref="JP7"  Part="1" 
AR Path="/F4BF4B7FFF02" Ref="JP7"  Part="1" 
AR Path="/24B7FFF02" Ref="JP7"  Part="1" 
AR Path="/23BC884B7FFF02" Ref="JP7"  Part="1" 
AR Path="/14B7FFF02" Ref="JP7"  Part="1" 
AR Path="/FFFFFFFF4B7FFF02" Ref="JP7"  Part="1" 
AR Path="/262F604B7FFF02" Ref="JP7"  Part="1" 
AR Path="/22C6E204B7FFF02" Ref="JP7"  Part="1" 
AR Path="/23C9F04B7FFF02" Ref="JP7"  Part="1" 
AR Path="/23CBC44B7FFF02" Ref="JP7"  Part="1" 
AR Path="/6FE934E34B7FFF02" Ref="JP7"  Part="1" 
F 0 "JP7" H 8050 2950 60  0000 C CNN
F 1 "JUMPER" H 8050 2720 40  0000 C CNN
F 2 "R3" H 8050 2800 60  0001 C CNN
	1    8050 2800
	1    0    0    -1  
$EndComp
$Comp
L JUMPER JP6
U 1 1 4B7FFEF9
P 8050 2600
AR Path="/384B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/4B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/464546394B7FFEF9" Ref="JP?"  Part="1" 
AR Path="/2606084B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/773F65F14B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/773F8EB44B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/94B7FFEF9" Ref="JP"  Part="1" 
AR Path="/23C6504B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/23C34C4B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/F4BF4B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/24B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/23BC884B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/14B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/FFFFFFFF4B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/262F604B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/22C6E204B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/23C9F04B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/23CBC44B7FFEF9" Ref="JP6"  Part="1" 
AR Path="/6FE934E34B7FFEF9" Ref="JP6"  Part="1" 
F 0 "JP6" H 8050 2750 60  0000 C CNN
F 1 "JUMPER" H 8050 2520 40  0000 C CNN
F 2 "R3" H 8050 2600 60  0001 C CNN
	1    8050 2600
	1    0    0    -1  
$EndComp
$Comp
L JUMPER JP5
U 1 1 4B7FFEEF
P 8050 2400
AR Path="/384B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/4B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/464545464B7FFEEF" Ref="JP?"  Part="1" 
AR Path="/2606084B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/773F65F14B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/773F8EB44B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/94B7FFEEF" Ref="JP"  Part="1" 
AR Path="/23C6504B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/23C34C4B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/F4BF4B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/24B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/23BC884B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/14B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/FFFFFFFF4B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/262F604B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/22C6E204B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/23C9F04B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/23CBC44B7FFEEF" Ref="JP5"  Part="1" 
AR Path="/6FE934E34B7FFEEF" Ref="JP5"  Part="1" 
F 0 "JP5" H 8050 2550 60  0000 C CNN
F 1 "JUMPER" H 8050 2320 40  0000 C CNN
F 2 "R3" H 8050 2400 60  0001 C CNN
	1    8050 2400
	1    0    0    -1  
$EndComp
$Comp
L JUMPER JP?
U 1 1 4B7FFEE0
P 8050 2200
AR Path="/9F00384B7FFEE0" Ref="JP?"  Part="1" 
AR Path="/4B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/464545304B7FFEE0" Ref="JP?"  Part="1" 
AR Path="/2606084B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/773F65F14B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/773F8EB44B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/23C9F04B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/94B7FFEE0" Ref="JP"  Part="1" 
AR Path="/23C6504B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/23C34C4B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/F4BF4B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/24B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/23BC884B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/14B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/FFFFFFFF4B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/262F604B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/22C6E204B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/23CBC44B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/384B7FFEE0" Ref="JP4"  Part="1" 
AR Path="/6FE934E34B7FFEE0" Ref="JP4"  Part="1" 
F 0 "JP4" H 8050 2350 60  0000 C CNN
F 1 "JUMPER" H 8050 2120 40  0000 C CNN
F 2 "R3" H 8050 2200 60  0001 C CNN
	1    8050 2200
	1    0    0    -1  
$EndComp
Text Label 9650 1400 0    60   ~ 0
4MG_RAM_SELECT
Text Notes 8550 1950 0    60   ~ 0
RAM CS3
Text Notes 8550 1750 0    60   ~ 0
RAM CS2
Text Notes 8550 1550 0    60   ~ 0
RAM CS1
Text Notes 8550 1350 0    60   ~ 0
RAM CS0
$Comp
L R R?
U 1 1 4B7FF5B9
P 7850 1300
AR Path="/D1CA184B7FF5B9" Ref="R?"  Part="1" 
AR Path="/FFFFFFFF4B7FF5B9" Ref="R10"  Part="1" 
AR Path="/4B7FF5B9" Ref="R10"  Part="1" 
AR Path="/773F65F14B7FF5B9" Ref="R10"  Part="1" 
AR Path="/773F8EB44B7FF5B9" Ref="R10"  Part="1" 
AR Path="/23C9F04B7FF5B9" Ref="R10"  Part="1" 
AR Path="/94B7FF5B9" Ref="R"  Part="1" 
AR Path="/23C6504B7FF5B9" Ref="R10"  Part="1" 
AR Path="/23C34C4B7FF5B9" Ref="R10"  Part="1" 
AR Path="/F4BF4B7FF5B9" Ref="R10"  Part="1" 
AR Path="/24B7FF5B9" Ref="R10"  Part="1" 
AR Path="/23BC884B7FF5B9" Ref="R10"  Part="1" 
AR Path="/14B7FF5B9" Ref="R10"  Part="1" 
AR Path="/262F604B7FF5B9" Ref="R10"  Part="1" 
AR Path="/22C6E204B7FF5B9" Ref="R10"  Part="1" 
AR Path="/23CBC44B7FF5B9" Ref="R10"  Part="1" 
AR Path="/384B7FF5B9" Ref="R10"  Part="1" 
AR Path="/6FE934E34B7FF5B9" Ref="R10"  Part="1" 
F 0 "R10" V 7930 1300 50  0000 C CNN
F 1 "470" V 7850 1300 50  0000 C CNN
F 2 "R3" H 7850 1300 60  0001 C CNN
	1    7850 1300
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 4B7FF5B8
P 7850 1500
AR Path="/D1CA184B7FF5B8" Ref="R?"  Part="1" 
AR Path="/FFFFFFFF4B7FF5B8" Ref="R11"  Part="1" 
AR Path="/4B7FF5B8" Ref="R11"  Part="1" 
AR Path="/773F65F14B7FF5B8" Ref="R11"  Part="1" 
AR Path="/773F8EB44B7FF5B8" Ref="R11"  Part="1" 
AR Path="/94B7FF5B8" Ref="R"  Part="1" 
AR Path="/23C6504B7FF5B8" Ref="R11"  Part="1" 
AR Path="/23C34C4B7FF5B8" Ref="R11"  Part="1" 
AR Path="/F4BF4B7FF5B8" Ref="R11"  Part="1" 
AR Path="/24B7FF5B8" Ref="R11"  Part="1" 
AR Path="/23BC884B7FF5B8" Ref="R11"  Part="1" 
AR Path="/14B7FF5B8" Ref="R11"  Part="1" 
AR Path="/262F604B7FF5B8" Ref="R11"  Part="1" 
AR Path="/22C6E204B7FF5B8" Ref="R11"  Part="1" 
AR Path="/23C9F04B7FF5B8" Ref="R11"  Part="1" 
AR Path="/23CBC44B7FF5B8" Ref="R11"  Part="1" 
AR Path="/384B7FF5B8" Ref="R11"  Part="1" 
AR Path="/6FE934E34B7FF5B8" Ref="R11"  Part="1" 
F 0 "R11" V 7930 1500 50  0000 C CNN
F 1 "470" V 7850 1500 50  0000 C CNN
F 2 "R3" H 7850 1500 60  0001 C CNN
	1    7850 1500
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 4B7FF5B7
P 7850 1700
AR Path="/D1CA184B7FF5B7" Ref="R?"  Part="1" 
AR Path="/FFFFFFFF4B7FF5B7" Ref="R12"  Part="1" 
AR Path="/4B7FF5B7" Ref="R12"  Part="1" 
AR Path="/773F65F14B7FF5B7" Ref="R12"  Part="1" 
AR Path="/773F8EB44B7FF5B7" Ref="R12"  Part="1" 
AR Path="/94B7FF5B7" Ref="R"  Part="1" 
AR Path="/23C6504B7FF5B7" Ref="R12"  Part="1" 
AR Path="/23C34C4B7FF5B7" Ref="R12"  Part="1" 
AR Path="/F4BF4B7FF5B7" Ref="R12"  Part="1" 
AR Path="/24B7FF5B7" Ref="R12"  Part="1" 
AR Path="/23BC884B7FF5B7" Ref="R12"  Part="1" 
AR Path="/14B7FF5B7" Ref="R12"  Part="1" 
AR Path="/262F604B7FF5B7" Ref="R12"  Part="1" 
AR Path="/22C6E204B7FF5B7" Ref="R12"  Part="1" 
AR Path="/23C9F04B7FF5B7" Ref="R12"  Part="1" 
AR Path="/23CBC44B7FF5B7" Ref="R12"  Part="1" 
AR Path="/384B7FF5B7" Ref="R12"  Part="1" 
AR Path="/6FE934E34B7FF5B7" Ref="R12"  Part="1" 
F 0 "R12" V 7930 1700 50  0000 C CNN
F 1 "470" V 7850 1700 50  0000 C CNN
F 2 "R3" H 7850 1700 60  0001 C CNN
	1    7850 1700
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 4B7FF5B6
P 7850 1900
AR Path="/D1CA184B7FF5B6" Ref="R?"  Part="1" 
AR Path="/FFFFFFFF4B7FF5B6" Ref="R13"  Part="1" 
AR Path="/4B7FF5B6" Ref="R13"  Part="1" 
AR Path="/773F65F14B7FF5B6" Ref="R13"  Part="1" 
AR Path="/773F8EB44B7FF5B6" Ref="R13"  Part="1" 
AR Path="/94B7FF5B6" Ref="R"  Part="1" 
AR Path="/23C6504B7FF5B6" Ref="R13"  Part="1" 
AR Path="/23C34C4B7FF5B6" Ref="R13"  Part="1" 
AR Path="/F4BF4B7FF5B6" Ref="R13"  Part="1" 
AR Path="/24B7FF5B6" Ref="R13"  Part="1" 
AR Path="/23BC884B7FF5B6" Ref="R13"  Part="1" 
AR Path="/14B7FF5B6" Ref="R13"  Part="1" 
AR Path="/262F604B7FF5B6" Ref="R13"  Part="1" 
AR Path="/22C6E204B7FF5B6" Ref="R13"  Part="1" 
AR Path="/23C9F04B7FF5B6" Ref="R13"  Part="1" 
AR Path="/23CBC44B7FF5B6" Ref="R13"  Part="1" 
AR Path="/384B7FF5B6" Ref="R13"  Part="1" 
AR Path="/6FE934E34B7FF5B6" Ref="R13"  Part="1" 
F 0 "R13" V 7930 1900 50  0000 C CNN
F 1 "470" V 7850 1900 50  0000 C CNN
F 2 "R3" H 7850 1900 60  0001 C CNN
	1    7850 1900
	0    1    1    0   
$EndComp
$Comp
L LED D?
U 1 1 4B7FF5B5
P 8300 1300
AR Path="/D1CA184B7FF5B5" Ref="D?"  Part="1" 
AR Path="/FFFFFFFF4B7FF5B5" Ref="D6"  Part="1" 
AR Path="/4B7FF5B5" Ref="D6"  Part="1" 
AR Path="/773F65F14B7FF5B5" Ref="D6"  Part="1" 
AR Path="/773F8EB44B7FF5B5" Ref="D6"  Part="1" 
AR Path="/23C9F04B7FF5B5" Ref="D6"  Part="1" 
AR Path="/94B7FF5B5" Ref="D"  Part="1" 
AR Path="/23C6504B7FF5B5" Ref="D6"  Part="1" 
AR Path="/23C34C4B7FF5B5" Ref="D6"  Part="1" 
AR Path="/F4BF4B7FF5B5" Ref="D8"  Part="1" 
AR Path="/24B7FF5B5" Ref="D6"  Part="1" 
AR Path="/23BC884B7FF5B5" Ref="D6"  Part="1" 
AR Path="/14B7FF5B5" Ref="D6"  Part="1" 
AR Path="/262F604B7FF5B5" Ref="D8"  Part="1" 
AR Path="/22C6E204B7FF5B5" Ref="D8"  Part="1" 
AR Path="/23CBC44B7FF5B5" Ref="D6"  Part="1" 
AR Path="/7E428DAC4B7FF5B5" Ref="D8"  Part="1" 
AR Path="/380086E4B7FF5B5" Ref="D"  Part="1" 
AR Path="/384B7FF5B5" Ref="D6"  Part="1" 
AR Path="/CF080C4B7FF5B5" Ref="D"  Part="1" 
AR Path="/114B7FF5B5" Ref="D6"  Part="1" 
AR Path="/E509384B7FF5B5" Ref="D"  Part="1" 
AR Path="/6FE934E34B7FF5B5" Ref="D6"  Part="1" 
F 0 "D6" H 8300 1400 50  0000 C CNN
F 1 "LED" H 8300 1200 50  0000 C CNN
F 2 "LEDV" H 8300 1300 60  0001 C CNN
	1    8300 1300
	-1   0    0    1   
$EndComp
$Comp
L LED D?
U 1 1 4B7FF5B4
P 8300 1500
AR Path="/D1CA184B7FF5B4" Ref="D?"  Part="1" 
AR Path="/FFFFFFFF4B7FF5B4" Ref="D7"  Part="1" 
AR Path="/4B7FF5B4" Ref="D7"  Part="1" 
AR Path="/773F65F14B7FF5B4" Ref="D7"  Part="1" 
AR Path="/773F8EB44B7FF5B4" Ref="D7"  Part="1" 
AR Path="/94B7FF5B4" Ref="D"  Part="1" 
AR Path="/23C6504B7FF5B4" Ref="D7"  Part="1" 
AR Path="/23C34C4B7FF5B4" Ref="D7"  Part="1" 
AR Path="/F4BF4B7FF5B4" Ref="D9"  Part="1" 
AR Path="/24B7FF5B4" Ref="D7"  Part="1" 
AR Path="/23BC884B7FF5B4" Ref="D7"  Part="1" 
AR Path="/14B7FF5B4" Ref="D7"  Part="1" 
AR Path="/262F604B7FF5B4" Ref="D9"  Part="1" 
AR Path="/22C6E204B7FF5B4" Ref="D9"  Part="1" 
AR Path="/23C9F04B7FF5B4" Ref="D7"  Part="1" 
AR Path="/23CBC44B7FF5B4" Ref="D7"  Part="1" 
AR Path="/7E428DAC4B7FF5B4" Ref="D7"  Part="1" 
AR Path="/11509244B7FF5B4" Ref="D"  Part="1" 
AR Path="/384B7FF5B4" Ref="D7"  Part="1" 
AR Path="/10B08304B7FF5B4" Ref="D"  Part="1" 
AR Path="/6FE934E34B7FF5B4" Ref="D7"  Part="1" 
F 0 "D7" H 8300 1600 50  0000 C CNN
F 1 "LED" H 8300 1400 50  0000 C CNN
F 2 "LEDV" H 8300 1500 60  0001 C CNN
	1    8300 1500
	-1   0    0    1   
$EndComp
$Comp
L LED D?
U 1 1 4B7FF5B2
P 3950 1550
AR Path="/D1CA184B7FF5B2" Ref="D?"  Part="1" 
AR Path="/FFFFFFFF4B7FF5B2" Ref="D2"  Part="1" 
AR Path="/4B7FF5B2" Ref="D2"  Part="1" 
AR Path="/773F65F14B7FF5B2" Ref="D2"  Part="1" 
AR Path="/773F8EB44B7FF5B2" Ref="D2"  Part="1" 
AR Path="/94B7FF5B2" Ref="D"  Part="1" 
AR Path="/23C6504B7FF5B2" Ref="D2"  Part="1" 
AR Path="/23C34C4B7FF5B2" Ref="D2"  Part="1" 
AR Path="/F4BF4B7FF5B2" Ref="D7"  Part="1" 
AR Path="/24B7FF5B2" Ref="D2"  Part="1" 
AR Path="/23BC884B7FF5B2" Ref="D2"  Part="1" 
AR Path="/14B7FF5B2" Ref="D7"  Part="1" 
AR Path="/262F604B7FF5B2" Ref="D7"  Part="1" 
AR Path="/22C6E204B7FF5B2" Ref="D7"  Part="1" 
AR Path="/23C9F04B7FF5B2" Ref="D2"  Part="1" 
AR Path="/23CBC44B7FF5B2" Ref="D2"  Part="1" 
AR Path="/7E428DAC4B7FF5B2" Ref="D3"  Part="1" 
AR Path="/384B7FF5B2" Ref="D7"  Part="1" 
AR Path="/7C9100004B7FF5B2" Ref="D3"  Part="1" 
AR Path="/35312E304B7FF5B2" Ref="D7"  Part="1" 
AR Path="/12208F04B7FF5B2" Ref="D"  Part="1" 
AR Path="/16808AC4B7FF5B2" Ref="D"  Part="1" 
AR Path="/7C91005D4B7FF5B2" Ref="D3"  Part="1" 
AR Path="/23D7004B7FF5B2" Ref="D3"  Part="1" 
AR Path="/77F16BF24B7FF5B2" Ref="D3"  Part="1" 
AR Path="/23D7F44B7FF5B2" Ref="D3"  Part="1" 
AR Path="/6FE934E34B7FF5B2" Ref="D2"  Part="1" 
AR Path="/184087A4B7FF5B2" Ref="D"  Part="1" 
AR Path="/2B6D7C4B7FF5B2" Ref="D2"  Part="1" 
F 0 "D2" H 3950 1650 50  0000 C CNN
F 1 "LED" H 3950 1450 50  0000 C CNN
F 2 "LEDV" H 3950 1550 60  0001 C CNN
	1    3950 1550
	-1   0    0    1   
$EndComp
NoConn ~ 17350 2850
Text Label 8100 15800 0    60   ~ 0
bA19
Text Label 8100 15400 0    60   ~ 0
bA15
Text Label 8100 16100 0    60   ~ 0
G*
Text Label 8000 16200 0    60   ~ 0
RAMCS3*
Text Label 8100 15500 0    60   ~ 0
bA16
Text Label 8100 15700 0    60   ~ 0
bA18
Text Label 8100 15600 0    60   ~ 0
bA17
Text Label 8100 16000 0    60   ~ 0
E*
Text Label 5750 16000 0    60   ~ 0
E*
Text Label 5750 15600 0    60   ~ 0
bA17
Text Label 5750 15700 0    60   ~ 0
bA18
Text Label 5750 15500 0    60   ~ 0
bA16
Text Label 5650 16200 0    60   ~ 0
RAMCS2*
Text Label 5750 16100 0    60   ~ 0
G*
Text Label 5750 15400 0    60   ~ 0
bA15
Text Label 5750 15800 0    60   ~ 0
bA19
Text Label 1050 16000 0    60   ~ 0
E*
Text Label 1050 15200 0    60   ~ 0
bA13
Text Label 1050 15300 0    60   ~ 0
bA14
Text Label 1050 15100 0    60   ~ 0
bA12
Text Label 1050 15600 0    60   ~ 0
bA17
Text Label 1050 15700 0    60   ~ 0
bA18
Text Label 1050 15500 0    60   ~ 0
bA16
Text Label 950  16200 0    60   ~ 0
RAMCS0*
Text Label 1050 16100 0    60   ~ 0
G*
Text Label 1050 15400 0    60   ~ 0
bA15
Text Label 1050 15800 0    60   ~ 0
bA19
Text Label 3400 15800 0    60   ~ 0
bA19
Text Label 3400 15400 0    60   ~ 0
bA15
Text Label 3400 16100 0    60   ~ 0
G*
Text Label 8100 12550 0    60   ~ 0
bA18
Text Label 8100 12350 0    60   ~ 0
bA16
Text Label 8100 12850 0    60   ~ 0
D*
Text Label 8000 13050 0    60   ~ 0
RAMCS3*
Text Label 8100 12250 0    60   ~ 0
bA15
Text Label 8100 12450 0    60   ~ 0
bA17
Text Label 8100 12650 0    60   ~ 0
bA19
Text Label 8100 12950 0    60   ~ 0
F*
Text Label 5750 12950 0    60   ~ 0
F*
Text Label 5750 12650 0    60   ~ 0
bA19
Text Label 5750 12450 0    60   ~ 0
bA17
Text Label 5750 12250 0    60   ~ 0
bA15
Text Label 5650 13050 0    60   ~ 0
RAMCS2*
Text Label 5750 12850 0    60   ~ 0
D*
Text Label 5750 12350 0    60   ~ 0
bA16
Text Label 5750 12550 0    60   ~ 0
bA18
Text Label 3400 12550 0    60   ~ 0
bA18
Text Label 3400 12350 0    60   ~ 0
bA16
Text Label 3400 12850 0    60   ~ 0
D*
Text Label 3300 13050 0    60   ~ 0
RAMCS1*
Text Label 3400 12250 0    60   ~ 0
bA15
Text Label 3400 12450 0    60   ~ 0
bA17
Text Label 3400 12650 0    60   ~ 0
bA19
Text Label 3400 12950 0    60   ~ 0
F*
Text Label 1050 12950 0    60   ~ 0
F*
Text Label 1050 12650 0    60   ~ 0
bA19
Text Label 1050 12450 0    60   ~ 0
bA17
Text Label 1050 12250 0    60   ~ 0
bA15
Text Label 1050 12150 0    60   ~ 0
bA14
Text Label 1050 11950 0    60   ~ 0
bA12
$Comp
L C C43
U 1 1 4B4A5AF2
P 15900 15600
AR Path="/773F8EB44B4A5AF2" Ref="C43"  Part="1" 
AR Path="/23C9F04B4A5AF2" Ref="C43"  Part="1" 
AR Path="/94B4A5AF2" Ref="C"  Part="1" 
AR Path="/4B4A5AF2" Ref="C43"  Part="1" 
AR Path="/773F65F14B4A5AF2" Ref="C43"  Part="1" 
AR Path="/7CAD4B4A5AF2" Ref="C43"  Part="1" 
AR Path="/24B4A5AF2" Ref="C43"  Part="1" 
AR Path="/23BC884B4A5AF2" Ref="C43"  Part="1" 
AR Path="/73254B4A5AF2" Ref="C43"  Part="1" 
AR Path="/D058A04B4A5AF2" Ref="C43"  Part="1" 
AR Path="/1607D44B4A5AF2" Ref="C43"  Part="1" 
AR Path="/FFFFFFFF4B4A5AF2" Ref="C43"  Part="1" 
AR Path="/D1C3804B4A5AF2" Ref="C43"  Part="1" 
AR Path="/23C6504B4A5AF2" Ref="C43"  Part="1" 
AR Path="/23C34C4B4A5AF2" Ref="C43"  Part="1" 
AR Path="/755D912A4B4A5AF2" Ref="C43"  Part="1" 
AR Path="/14B4A5AF2" Ref="C43"  Part="1" 
AR Path="/23D70C4B4A5AF2" Ref="C43"  Part="1" 
AR Path="/6FE934E34B4A5AF2" Ref="C43"  Part="1" 
AR Path="/2104B4A5AF2" Ref="C43"  Part="1" 
AR Path="/D1CA184B4A5AF2" Ref="C43"  Part="1" 
AR Path="/F4BF4B4A5AF2" Ref="C43"  Part="1" 
AR Path="/262F604B4A5AF2" Ref="C43"  Part="1" 
AR Path="/22C6E204B4A5AF2" Ref="C43"  Part="1" 
AR Path="/23CBC44B4A5AF2" Ref="C43"  Part="1" 
AR Path="/384B4A5AF2" Ref="C43"  Part="1" 
F 0 "C43" H 15950 15700 50  0000 L CNN
F 1 "0.1 uF" H 15950 15500 50  0000 L CNN
F 2 "C2" H 15900 15600 60  0001 C CNN
	1    15900 15600
	1    0    0    -1  
$EndComp
$Comp
L JUMPER JP?
U 1 1 4B48EEA8
P 16700 8700
AR Path="/9900384B48EEA8" Ref="JP?"  Part="1" 
AR Path="/4B48EEA8" Ref="JP8"  Part="1" 
AR Path="/773F8EB44B48EEA8" Ref="JP8"  Part="1" 
AR Path="/23C9F04B48EEA8" Ref="JP8"  Part="1" 
AR Path="/94B48EEA8" Ref="JP"  Part="1" 
AR Path="/773F65F14B48EEA8" Ref="JP8"  Part="1" 
AR Path="/24B48EEA8" Ref="JP8"  Part="1" 
AR Path="/23BC884B48EEA8" Ref="JP8"  Part="1" 
AR Path="/73254B48EEA8" Ref="JP8"  Part="1" 
AR Path="/D058A04B48EEA8" Ref="JP8"  Part="1" 
AR Path="/1607D44B48EEA8" Ref="JP8"  Part="1" 
AR Path="/FFFFFFFF4B48EEA8" Ref="JP8"  Part="1" 
AR Path="/D1C3804B48EEA8" Ref="JP8"  Part="1" 
AR Path="/23C6504B48EEA8" Ref="JP8"  Part="1" 
AR Path="/23C34C4B48EEA8" Ref="JP8"  Part="1" 
AR Path="/755D912A4B48EEA8" Ref="JP8"  Part="1" 
AR Path="/14B48EEA8" Ref="JP8"  Part="1" 
AR Path="/23D70C4B48EEA8" Ref="JP8"  Part="1" 
AR Path="/6FE934E34B48EEA8" Ref="JP8"  Part="1" 
AR Path="/2104B48EEA8" Ref="JP8"  Part="1" 
AR Path="/D1CA184B48EEA8" Ref="JP8"  Part="1" 
AR Path="/F4BF4B48EEA8" Ref="JP8"  Part="1" 
AR Path="/262F604B48EEA8" Ref="JP8"  Part="1" 
AR Path="/22C6E204B48EEA8" Ref="JP8"  Part="1" 
AR Path="/23CBC44B48EEA8" Ref="JP8"  Part="1" 
AR Path="/384B48EEA8" Ref="JP8"  Part="1" 
F 0 "JP8" H 16700 8850 60  0000 C CNN
F 1 "JUMPER" H 16700 8620 40  0000 C CNN
F 2 "PIN_ARRAY_2X1" H 16700 8700 60  0001 C CNN
	1    16700 8700
	1    0    0    -1  
$EndComp
NoConn ~ 4000 3850
Text Label 800  3850 0    60   ~ 0
bA21
Text Label 5300 2400 0    60   ~ 0
bA20
Text Label 5300 2250 0    60   ~ 0
bA21
$Comp
L GND #PWR?
U 1 1 4B489D5C
P 2600 5450
AR Path="/A800384B489D5C" Ref="#PWR?"  Part="1" 
AR Path="/4B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/69549BC04B489D5C" Ref="#PWR?"  Part="1" 
AR Path="/773F8EB44B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/23C9F04B489D5C" Ref="#PWR7"  Part="1" 
AR Path="/94B489D5C" Ref="#PWR"  Part="1" 
AR Path="/773F65F14B489D5C" Ref="#PWR010"  Part="1" 
AR Path="/6684D64B489D5C" Ref="#PWR010"  Part="1" 
AR Path="/23C6504B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/23CBC44B489D5C" Ref="#PWR010"  Part="1" 
AR Path="/23C34C4B489D5C" Ref="#PWR011"  Part="1" 
AR Path="/23BC884B489D5C" Ref="#PWR010"  Part="1" 
AR Path="/39803EA4B489D5C" Ref="#PWR05"  Part="1" 
AR Path="/FFFFFFFF4B489D5C" Ref="#PWR011"  Part="1" 
AR Path="/73254B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/D058A04B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/1607D44B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/D1C3804B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/755D912A4B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/24B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/23D70C4B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/6FE934E34B489D5C" Ref="#PWR07"  Part="1" 
AR Path="/14B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/2104B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/D1CA184B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/F4BF4B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/262F604B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/22C6E204B489D5C" Ref="#PWR06"  Part="1" 
AR Path="/384B489D5C" Ref="#PWR07"  Part="1" 
AR Path="/6FF0DD404B489D5C" Ref="#PWR07"  Part="1" 
AR Path="/333946414B489D5C" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 2600 5450 30  0001 C CNN
F 1 "GND" H 2600 5380 30  0001 C CNN
	1    2600 5450
	1    0    0    -1  
$EndComp
Text Label 800  4250 0    60   ~ 0
bA17
Text Label 800  4350 0    60   ~ 0
bA16
Text Label 800  4150 0    60   ~ 0
bA18
Text Label 800  4050 0    60   ~ 0
bA19
Text Label 800  3750 0    60   ~ 0
bA22
Text Label 800  3650 0    60   ~ 0
bA23
$Comp
L 74LS682N IC2
U 1 1 4B489BEB
P 3500 4450
AR Path="/384B489BEB" Ref="IC2"  Part="1" 
AR Path="/4B489BEB" Ref="IC2"  Part="1" 
AR Path="/284F4C4B489BEB" Ref="IC?"  Part="1" 
AR Path="/7E4188DA4B489BEB" Ref="IC2"  Part="1" 
AR Path="/69549BC04B489BEB" Ref="IC2"  Part="1" 
AR Path="/773F8EB44B489BEB" Ref="IC2"  Part="1" 
AR Path="/23C9F04B489BEB" Ref="IC2"  Part="1" 
AR Path="/94B489BEB" Ref="IC"  Part="1" 
AR Path="/773F65F14B489BEB" Ref="IC2"  Part="1" 
AR Path="/23C6504B489BEB" Ref="IC2"  Part="1" 
AR Path="/23CBC44B489BEB" Ref="IC2"  Part="1" 
AR Path="/23C34C4B489BEB" Ref="IC2"  Part="1" 
AR Path="/23BC884B489BEB" Ref="IC2"  Part="1" 
AR Path="/39803EA4B489BEB" Ref="IC2"  Part="1" 
AR Path="/FFFFFFFF4B489BEB" Ref="IC2"  Part="1" 
AR Path="/73254B489BEB" Ref="IC2"  Part="1" 
AR Path="/D058A04B489BEB" Ref="IC2"  Part="1" 
AR Path="/1607D44B489BEB" Ref="IC2"  Part="1" 
AR Path="/D1C3804B489BEB" Ref="IC2"  Part="1" 
AR Path="/24B489BEB" Ref="IC2"  Part="1" 
AR Path="/23D70C4B489BEB" Ref="IC2"  Part="1" 
AR Path="/6FE934E34B489BEB" Ref="IC2"  Part="1" 
AR Path="/14B489BEB" Ref="IC2"  Part="1" 
AR Path="/2104B489BEB" Ref="IC2"  Part="1" 
AR Path="/D1CA184B489BEB" Ref="IC2"  Part="1" 
AR Path="/F4BF4B489BEB" Ref="IC2"  Part="1" 
AR Path="/262F604B489BEB" Ref="IC2"  Part="1" 
AR Path="/22C6E204B489BEB" Ref="IC2"  Part="1" 
F 0 "IC2" H 3200 5375 50  0000 L BNN
F 1 "74LS682" H 3200 3450 50  0000 L BNN
F 2 "20dip300" H 3500 4600 50  0001 C CNN
	1    3500 4450
	1    0    0    -1  
$EndComp
Text Label 3700 8200 0    60   ~ 0
IO_REQUEST
$Comp
L VCC #PWR?
U 1 1 4B453713
P 11400 3150
AR Path="/9F00384B453713" Ref="#PWR?"  Part="1" 
AR Path="/4B453713" Ref="#PWR07"  Part="1" 
AR Path="/333731334B453713" Ref="#PWR?"  Part="1" 
AR Path="/773F8EB44B453713" Ref="#PWR07"  Part="1" 
AR Path="/23C9F04B453713" Ref="#PWR29"  Part="1" 
AR Path="/94B453713" Ref="#PWR"  Part="1" 
AR Path="/773F65F14B453713" Ref="#PWR011"  Part="1" 
AR Path="/6684D64B453713" Ref="#PWR011"  Part="1" 
AR Path="/23BC884B453713" Ref="#PWR011"  Part="1" 
AR Path="/23CBC44B453713" Ref="#PWR011"  Part="1" 
AR Path="/69549BC04B453713" Ref="#PWR01"  Part="1" 
AR Path="/23C6504B453713" Ref="#PWR07"  Part="1" 
AR Path="/23C34C4B453713" Ref="#PWR012"  Part="1" 
AR Path="/39803EA4B453713" Ref="#PWR07"  Part="1" 
AR Path="/FFFFFFFF4B453713" Ref="#PWR012"  Part="1" 
AR Path="/73254B453713" Ref="#PWR08"  Part="1" 
AR Path="/D058A04B453713" Ref="#PWR08"  Part="1" 
AR Path="/1607D44B453713" Ref="#PWR08"  Part="1" 
AR Path="/D1C3804B453713" Ref="#PWR08"  Part="1" 
AR Path="/24B453713" Ref="#PWR08"  Part="1" 
AR Path="/23D70C4B453713" Ref="#PWR08"  Part="1" 
AR Path="/6FE934E34B453713" Ref="#PWR08"  Part="1" 
AR Path="/14B453713" Ref="#PWR07"  Part="1" 
AR Path="/2104B453713" Ref="#PWR08"  Part="1" 
AR Path="/D1CA184B453713" Ref="#PWR08"  Part="1" 
AR Path="/F4BF4B453713" Ref="#PWR07"  Part="1" 
AR Path="/262F604B453713" Ref="#PWR07"  Part="1" 
AR Path="/22C6E204B453713" Ref="#PWR07"  Part="1" 
AR Path="/384B453713" Ref="#PWR08"  Part="1" 
AR Path="/6FF0DD404B453713" Ref="#PWR08"  Part="1" 
F 0 "#PWR07" H 11400 3250 30  0001 C CNN
F 1 "VCC" H 11400 3250 30  0000 C CNN
	1    11400 3150
	1    0    0    -1  
$EndComp
$Comp
L DIPS_08 SW?
U 1 1 4B452E67
P 11150 2450
AR Path="/2300384B452E67" Ref="SW?"  Part="1" 
AR Path="/4B452E67" Ref="SW2"  Part="1" 
AR Path="/773F8EB44B452E67" Ref="SW2"  Part="1" 
AR Path="/94B452E67" Ref="SW"  Part="1" 
AR Path="/773F65F14B452E67" Ref="SW2"  Part="1" 
AR Path="/23C9F04B452E67" Ref="SW2"  Part="1" 
AR Path="/FFFFFFF04B452E67" Ref="SW2"  Part="1" 
AR Path="/42EB4B452E67" Ref="SW2"  Part="1" 
AR Path="/24B452E67" Ref="SW2"  Part="1" 
AR Path="/23BC884B452E67" Ref="SW2"  Part="1" 
AR Path="/DC0C124B452E67" Ref="SW2"  Part="1" 
AR Path="/23C34C4B452E67" Ref="SW2"  Part="1" 
AR Path="/6FE934E34B452E67" Ref="SW2"  Part="1" 
AR Path="/23CBC44B452E67" Ref="SW2"  Part="1" 
AR Path="/69549BC04B452E67" Ref="SW2"  Part="1" 
AR Path="/23C6504B452E67" Ref="SW2"  Part="1" 
AR Path="/39803EA4B452E67" Ref="SW2"  Part="1" 
AR Path="/FFFFFFFF4B452E67" Ref="SW2"  Part="1" 
AR Path="/73254B452E67" Ref="SW2"  Part="1" 
AR Path="/D058A04B452E67" Ref="SW2"  Part="1" 
AR Path="/1607D44B452E67" Ref="SW2"  Part="1" 
AR Path="/D1C3804B452E67" Ref="SW2"  Part="1" 
AR Path="/23D70C4B452E67" Ref="SW2"  Part="1" 
AR Path="/14B452E67" Ref="SW2"  Part="1" 
AR Path="/2104B452E67" Ref="SW2"  Part="1" 
AR Path="/D1CA184B452E67" Ref="SW2"  Part="1" 
AR Path="/F4BF4B452E67" Ref="SW2"  Part="1" 
AR Path="/262F604B452E67" Ref="SW2"  Part="1" 
AR Path="/22C6E204B452E67" Ref="SW2"  Part="1" 
AR Path="/384B452E67" Ref="SW2"  Part="1" 
F 0 "SW2" V 10700 2450 60  0000 C CNN
F 1 "DIPS_08" V 11600 2450 60  0000 C CNN
F 2 "SWDIP8" H 11150 2450 60  0001 C CNN
	1    11150 2450
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR?
U 1 1 4B452BD4
P 12550 2000
AR Path="/A300384B452BD4" Ref="#PWR?"  Part="1" 
AR Path="/4B452BD4" Ref="#PWR08"  Part="1" 
AR Path="/773F65F14B452BD4" Ref="#PWR012"  Part="1" 
AR Path="/773F8EB44B452BD4" Ref="#PWR08"  Part="1" 
AR Path="/23C9F04B452BD4" Ref="#PWR36"  Part="1" 
AR Path="/94B452BD4" Ref="#PWR"  Part="1" 
AR Path="/FFFFFFF04B452BD4" Ref="#PWR30"  Part="1" 
AR Path="/23BC884B452BD4" Ref="#PWR012"  Part="1" 
AR Path="/DC0C124B452BD4" Ref="#PWR01"  Part="1" 
AR Path="/6684D64B452BD4" Ref="#PWR012"  Part="1" 
AR Path="/23C34C4B452BD4" Ref="#PWR013"  Part="1" 
AR Path="/23CBC44B452BD4" Ref="#PWR012"  Part="1" 
AR Path="/69549BC04B452BD4" Ref="#PWR02"  Part="1" 
AR Path="/23C6504B452BD4" Ref="#PWR08"  Part="1" 
AR Path="/39803EA4B452BD4" Ref="#PWR08"  Part="1" 
AR Path="/FFFFFFFF4B452BD4" Ref="#PWR013"  Part="1" 
AR Path="/73254B452BD4" Ref="#PWR09"  Part="1" 
AR Path="/D058A04B452BD4" Ref="#PWR09"  Part="1" 
AR Path="/1607D44B452BD4" Ref="#PWR09"  Part="1" 
AR Path="/D1C3804B452BD4" Ref="#PWR09"  Part="1" 
AR Path="/24B452BD4" Ref="#PWR09"  Part="1" 
AR Path="/23D70C4B452BD4" Ref="#PWR09"  Part="1" 
AR Path="/6FE934E34B452BD4" Ref="#PWR09"  Part="1" 
AR Path="/14B452BD4" Ref="#PWR08"  Part="1" 
AR Path="/2104B452BD4" Ref="#PWR09"  Part="1" 
AR Path="/D1CA184B452BD4" Ref="#PWR09"  Part="1" 
AR Path="/F4BF4B452BD4" Ref="#PWR08"  Part="1" 
AR Path="/262F604B452BD4" Ref="#PWR08"  Part="1" 
AR Path="/22C6E204B452BD4" Ref="#PWR08"  Part="1" 
AR Path="/384B452BD4" Ref="#PWR09"  Part="1" 
AR Path="/6FF0DD404B452BD4" Ref="#PWR09"  Part="1" 
F 0 "#PWR08" H 12550 2100 30  0001 C CNN
F 1 "VCC" H 12550 2100 30  0000 C CNN
	1    12550 2000
	1    0    0    -1  
$EndComp
NoConn ~ 19400 7400
Text Notes 6250 850  0    60   ~ 0
Note the LS139 selects each RAM pair within the BS, 1MG board selection.
$Comp
L GND #PWR?
U 1 1 4B3E0AE6
P 6750 13200
AR Path="/23D9D84B3E0AE6" Ref="#PWR?"  Part="1" 
AR Path="/394433324B3E0AE6" Ref="#PWR?"  Part="1" 
AR Path="/4B3E0AE6" Ref="#PWR09"  Part="1" 
AR Path="/94B3E0AE6" Ref="#PWR8"  Part="1" 
AR Path="/25E4B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/DCBAABCD4B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/5AD7153D4B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/A4B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/6FE901F74B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/3FEFFFFF4B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/403091264B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/403051264B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/4032F78D4B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/403251264B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/4032AAC04B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/4030D1264B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/4031778D4B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/3FEA24DD4B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/23D8D44B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/2600004B3E0AE6" Ref="#PWR01"  Part="1" 
AR Path="/6FF0DD404B3E0AE6" Ref="#PWR010"  Part="1" 
AR Path="/6FE934E34B3E0AE6" Ref="#PWR010"  Part="1" 
AR Path="/773F65F14B3E0AE6" Ref="#PWR013"  Part="1" 
AR Path="/773F8EB44B3E0AE6" Ref="#PWR09"  Part="1" 
AR Path="/23C9F04B3E0AE6" Ref="#PWR20"  Part="1" 
AR Path="/FFFFFFF04B3E0AE6" Ref="#PWR8"  Part="1" 
AR Path="/23BC884B3E0AE6" Ref="#PWR013"  Part="1" 
AR Path="/DC0C124B3E0AE6" Ref="#PWR02"  Part="1" 
AR Path="/6684D64B3E0AE6" Ref="#PWR013"  Part="1" 
AR Path="/23C34C4B3E0AE6" Ref="#PWR014"  Part="1" 
AR Path="/23CBC44B3E0AE6" Ref="#PWR013"  Part="1" 
AR Path="/69549BC04B3E0AE6" Ref="#PWR03"  Part="1" 
AR Path="/23C6504B3E0AE6" Ref="#PWR09"  Part="1" 
AR Path="/39803EA4B3E0AE6" Ref="#PWR09"  Part="1" 
AR Path="/FFFFFFFF4B3E0AE6" Ref="#PWR014"  Part="1" 
AR Path="/73254B3E0AE6" Ref="#PWR010"  Part="1" 
AR Path="/D058A04B3E0AE6" Ref="#PWR010"  Part="1" 
AR Path="/1607D44B3E0AE6" Ref="#PWR010"  Part="1" 
AR Path="/D1C3804B3E0AE6" Ref="#PWR010"  Part="1" 
AR Path="/24B3E0AE6" Ref="#PWR010"  Part="1" 
AR Path="/23D70C4B3E0AE6" Ref="#PWR010"  Part="1" 
AR Path="/14B3E0AE6" Ref="#PWR09"  Part="1" 
AR Path="/2104B3E0AE6" Ref="#PWR010"  Part="1" 
AR Path="/D1CA184B3E0AE6" Ref="#PWR010"  Part="1" 
AR Path="/F4BF4B3E0AE6" Ref="#PWR09"  Part="1" 
AR Path="/262F604B3E0AE6" Ref="#PWR09"  Part="1" 
AR Path="/22C6E204B3E0AE6" Ref="#PWR09"  Part="1" 
AR Path="/384B3E0AE6" Ref="#PWR010"  Part="1" 
F 0 "#PWR09" H 6750 13200 30  0001 C CNN
F 1 "GND" H 6750 13130 30  0001 C CNN
	1    6750 13200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4B3E0AD1
P 6750 16350
AR Path="/23D9D84B3E0AD1" Ref="#PWR?"  Part="1" 
AR Path="/394433324B3E0AD1" Ref="#PWR?"  Part="1" 
AR Path="/25E4B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/FFFFFFFF4B3E0AD1" Ref="#PWR015"  Part="1" 
AR Path="/363030314B3E0AD1" Ref="#PWR01"  Part="1" 
AR Path="/23C5884B3E0AD1" Ref="#PWR01"  Part="1" 
AR Path="/4B3E0AD1" Ref="#PWR010"  Part="1" 
AR Path="/94B3E0AD1" Ref="#PWR32"  Part="1" 
AR Path="/DCBAABCD4B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/5AD7153D4B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/A4B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/6FE901F74B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/3FEFFFFF4B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/403091264B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/403051264B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/4032F78D4B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/403251264B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/4032AAC04B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/4030D1264B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/4031778D4B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/3FEA24DD4B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/23D8D44B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/2600004B3E0AD1" Ref="#PWR02"  Part="1" 
AR Path="/6FF0DD404B3E0AD1" Ref="#PWR011"  Part="1" 
AR Path="/6FE934E34B3E0AD1" Ref="#PWR011"  Part="1" 
AR Path="/773F65F14B3E0AD1" Ref="#PWR014"  Part="1" 
AR Path="/773F8EB44B3E0AD1" Ref="#PWR010"  Part="1" 
AR Path="/23C9F04B3E0AD1" Ref="#PWR22"  Part="1" 
AR Path="/FFFFFFF04B3E0AD1" Ref="#PWR34"  Part="1" 
AR Path="/23BC884B3E0AD1" Ref="#PWR014"  Part="1" 
AR Path="/DC0C124B3E0AD1" Ref="#PWR03"  Part="1" 
AR Path="/6684D64B3E0AD1" Ref="#PWR014"  Part="1" 
AR Path="/23C34C4B3E0AD1" Ref="#PWR015"  Part="1" 
AR Path="/23CBC44B3E0AD1" Ref="#PWR014"  Part="1" 
AR Path="/69549BC04B3E0AD1" Ref="#PWR04"  Part="1" 
AR Path="/23C6504B3E0AD1" Ref="#PWR010"  Part="1" 
AR Path="/39803EA4B3E0AD1" Ref="#PWR010"  Part="1" 
AR Path="/73254B3E0AD1" Ref="#PWR011"  Part="1" 
AR Path="/D058A04B3E0AD1" Ref="#PWR011"  Part="1" 
AR Path="/1607D44B3E0AD1" Ref="#PWR011"  Part="1" 
AR Path="/D1C3804B3E0AD1" Ref="#PWR011"  Part="1" 
AR Path="/24B3E0AD1" Ref="#PWR011"  Part="1" 
AR Path="/23D70C4B3E0AD1" Ref="#PWR011"  Part="1" 
AR Path="/14B3E0AD1" Ref="#PWR010"  Part="1" 
AR Path="/2104B3E0AD1" Ref="#PWR011"  Part="1" 
AR Path="/D1CA184B3E0AD1" Ref="#PWR011"  Part="1" 
AR Path="/F4BF4B3E0AD1" Ref="#PWR010"  Part="1" 
AR Path="/262F604B3E0AD1" Ref="#PWR010"  Part="1" 
AR Path="/22C6E204B3E0AD1" Ref="#PWR010"  Part="1" 
AR Path="/384B3E0AD1" Ref="#PWR011"  Part="1" 
F 0 "#PWR010" H 6750 16350 30  0001 C CNN
F 1 "GND" H 6750 16280 30  0001 C CNN
	1    6750 16350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4B3E0ACC
P 4400 16350
AR Path="/23D9D84B3E0ACC" Ref="#PWR?"  Part="1" 
AR Path="/394433324B3E0ACC" Ref="#PWR?"  Part="1" 
AR Path="/25E4B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/FFFFFFFF4B3E0ACC" Ref="#PWR016"  Part="1" 
AR Path="/363030314B3E0ACC" Ref="#PWR02"  Part="1" 
AR Path="/23C5884B3E0ACC" Ref="#PWR02"  Part="1" 
AR Path="/4B3E0ACC" Ref="#PWR011"  Part="1" 
AR Path="/94B3E0ACC" Ref="#PWR24"  Part="1" 
AR Path="/DCBAABCD4B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/5AD7153D4B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/A4B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/6FE901F74B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/3FEFFFFF4B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/403091264B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/403051264B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/4032F78D4B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/403251264B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/4032AAC04B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/4030D1264B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/4031778D4B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/3FEA24DD4B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/23D8D44B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/2600004B3E0ACC" Ref="#PWR03"  Part="1" 
AR Path="/6FF0DD404B3E0ACC" Ref="#PWR012"  Part="1" 
AR Path="/6FE934E34B3E0ACC" Ref="#PWR012"  Part="1" 
AR Path="/773F65F14B3E0ACC" Ref="#PWR015"  Part="1" 
AR Path="/773F8EB44B3E0ACC" Ref="#PWR011"  Part="1" 
AR Path="/23C9F04B3E0ACC" Ref="#PWR18"  Part="1" 
AR Path="/FFFFFFF04B3E0ACC" Ref="#PWR32"  Part="1" 
AR Path="/23BC884B3E0ACC" Ref="#PWR015"  Part="1" 
AR Path="/DC0C124B3E0ACC" Ref="#PWR04"  Part="1" 
AR Path="/6684D64B3E0ACC" Ref="#PWR015"  Part="1" 
AR Path="/23C34C4B3E0ACC" Ref="#PWR016"  Part="1" 
AR Path="/23CBC44B3E0ACC" Ref="#PWR015"  Part="1" 
AR Path="/69549BC04B3E0ACC" Ref="#PWR05"  Part="1" 
AR Path="/23C6504B3E0ACC" Ref="#PWR011"  Part="1" 
AR Path="/39803EA4B3E0ACC" Ref="#PWR011"  Part="1" 
AR Path="/73254B3E0ACC" Ref="#PWR012"  Part="1" 
AR Path="/D058A04B3E0ACC" Ref="#PWR012"  Part="1" 
AR Path="/1607D44B3E0ACC" Ref="#PWR012"  Part="1" 
AR Path="/D1C3804B3E0ACC" Ref="#PWR012"  Part="1" 
AR Path="/24B3E0ACC" Ref="#PWR012"  Part="1" 
AR Path="/23D70C4B3E0ACC" Ref="#PWR012"  Part="1" 
AR Path="/14B3E0ACC" Ref="#PWR011"  Part="1" 
AR Path="/2104B3E0ACC" Ref="#PWR012"  Part="1" 
AR Path="/D1CA184B3E0ACC" Ref="#PWR012"  Part="1" 
AR Path="/F4BF4B3E0ACC" Ref="#PWR011"  Part="1" 
AR Path="/262F604B3E0ACC" Ref="#PWR011"  Part="1" 
AR Path="/22C6E204B3E0ACC" Ref="#PWR011"  Part="1" 
AR Path="/384B3E0ACC" Ref="#PWR012"  Part="1" 
F 0 "#PWR011" H 4400 16350 30  0001 C CNN
F 1 "GND" H 4400 16280 30  0001 C CNN
	1    4400 16350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4B3E0AC2
P 2050 16350
AR Path="/23D9D84B3E0AC2" Ref="#PWR?"  Part="1" 
AR Path="/394433324B3E0AC2" Ref="#PWR?"  Part="1" 
AR Path="/25E4B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/FFFFFFFF4B3E0AC2" Ref="#PWR017"  Part="1" 
AR Path="/363030314B3E0AC2" Ref="#PWR03"  Part="1" 
AR Path="/23C5884B3E0AC2" Ref="#PWR03"  Part="1" 
AR Path="/4B3E0AC2" Ref="#PWR012"  Part="1" 
AR Path="/94B3E0AC2" Ref="#PWR22"  Part="1" 
AR Path="/DCBAABCD4B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/5AD7153D4B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/A4B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/6FE901F74B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/3FEFFFFF4B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/403091264B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/403051264B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/4032F78D4B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/403251264B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/4032AAC04B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/4030D1264B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/4031778D4B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/3FEA24DD4B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/23D8D44B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/2600004B3E0AC2" Ref="#PWR04"  Part="1" 
AR Path="/6FF0DD404B3E0AC2" Ref="#PWR013"  Part="1" 
AR Path="/6FE934E34B3E0AC2" Ref="#PWR013"  Part="1" 
AR Path="/773F65F14B3E0AC2" Ref="#PWR016"  Part="1" 
AR Path="/773F8EB44B3E0AC2" Ref="#PWR012"  Part="1" 
AR Path="/23C9F04B3E0AC2" Ref="#PWR6"  Part="1" 
AR Path="/FFFFFFF04B3E0AC2" Ref="#PWR29"  Part="1" 
AR Path="/23BC884B3E0AC2" Ref="#PWR016"  Part="1" 
AR Path="/DC0C124B3E0AC2" Ref="#PWR05"  Part="1" 
AR Path="/6684D64B3E0AC2" Ref="#PWR016"  Part="1" 
AR Path="/23C34C4B3E0AC2" Ref="#PWR017"  Part="1" 
AR Path="/23CBC44B3E0AC2" Ref="#PWR016"  Part="1" 
AR Path="/69549BC04B3E0AC2" Ref="#PWR06"  Part="1" 
AR Path="/23C6504B3E0AC2" Ref="#PWR012"  Part="1" 
AR Path="/39803EA4B3E0AC2" Ref="#PWR012"  Part="1" 
AR Path="/73254B3E0AC2" Ref="#PWR013"  Part="1" 
AR Path="/D058A04B3E0AC2" Ref="#PWR013"  Part="1" 
AR Path="/1607D44B3E0AC2" Ref="#PWR013"  Part="1" 
AR Path="/D1C3804B3E0AC2" Ref="#PWR013"  Part="1" 
AR Path="/24B3E0AC2" Ref="#PWR013"  Part="1" 
AR Path="/23D70C4B3E0AC2" Ref="#PWR013"  Part="1" 
AR Path="/14B3E0AC2" Ref="#PWR012"  Part="1" 
AR Path="/2104B3E0AC2" Ref="#PWR013"  Part="1" 
AR Path="/D1CA184B3E0AC2" Ref="#PWR013"  Part="1" 
AR Path="/F4BF4B3E0AC2" Ref="#PWR012"  Part="1" 
AR Path="/262F604B3E0AC2" Ref="#PWR012"  Part="1" 
AR Path="/22C6E204B3E0AC2" Ref="#PWR012"  Part="1" 
AR Path="/384B3E0AC2" Ref="#PWR013"  Part="1" 
F 0 "#PWR012" H 2050 16350 30  0001 C CNN
F 1 "GND" H 2050 16280 30  0001 C CNN
	1    2050 16350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4B3E0ABD
P 4400 13200
AR Path="/23D9D84B3E0ABD" Ref="#PWR?"  Part="1" 
AR Path="/394433324B3E0ABD" Ref="#PWR?"  Part="1" 
AR Path="/25E4B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/FFFFFFFF4B3E0ABD" Ref="#PWR018"  Part="1" 
AR Path="/363030314B3E0ABD" Ref="#PWR04"  Part="1" 
AR Path="/23C5884B3E0ABD" Ref="#PWR04"  Part="1" 
AR Path="/4B3E0ABD" Ref="#PWR013"  Part="1" 
AR Path="/94B3E0ABD" Ref="#PWR5"  Part="1" 
AR Path="/DCBAABCD4B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/5AD7153D4B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/A4B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/6FE901F74B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/3FEFFFFF4B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/403091264B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/403051264B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/4032F78D4B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/403251264B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/4032AAC04B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/4030D1264B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/4031778D4B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/3FEA24DD4B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/23D8D44B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/2600004B3E0ABD" Ref="#PWR05"  Part="1" 
AR Path="/6FF0DD404B3E0ABD" Ref="#PWR014"  Part="1" 
AR Path="/6FE934E34B3E0ABD" Ref="#PWR014"  Part="1" 
AR Path="/773F65F14B3E0ABD" Ref="#PWR017"  Part="1" 
AR Path="/773F8EB44B3E0ABD" Ref="#PWR013"  Part="1" 
AR Path="/23C9F04B3E0ABD" Ref="#PWR16"  Part="1" 
AR Path="/FFFFFFF04B3E0ABD" Ref="#PWR5"  Part="1" 
AR Path="/23BC884B3E0ABD" Ref="#PWR017"  Part="1" 
AR Path="/DC0C124B3E0ABD" Ref="#PWR06"  Part="1" 
AR Path="/6684D64B3E0ABD" Ref="#PWR017"  Part="1" 
AR Path="/23C34C4B3E0ABD" Ref="#PWR018"  Part="1" 
AR Path="/23CBC44B3E0ABD" Ref="#PWR017"  Part="1" 
AR Path="/69549BC04B3E0ABD" Ref="#PWR07"  Part="1" 
AR Path="/23C6504B3E0ABD" Ref="#PWR013"  Part="1" 
AR Path="/39803EA4B3E0ABD" Ref="#PWR013"  Part="1" 
AR Path="/73254B3E0ABD" Ref="#PWR014"  Part="1" 
AR Path="/D058A04B3E0ABD" Ref="#PWR014"  Part="1" 
AR Path="/1607D44B3E0ABD" Ref="#PWR014"  Part="1" 
AR Path="/D1C3804B3E0ABD" Ref="#PWR014"  Part="1" 
AR Path="/24B3E0ABD" Ref="#PWR014"  Part="1" 
AR Path="/23D70C4B3E0ABD" Ref="#PWR014"  Part="1" 
AR Path="/14B3E0ABD" Ref="#PWR013"  Part="1" 
AR Path="/2104B3E0ABD" Ref="#PWR014"  Part="1" 
AR Path="/D1CA184B3E0ABD" Ref="#PWR014"  Part="1" 
AR Path="/F4BF4B3E0ABD" Ref="#PWR013"  Part="1" 
AR Path="/262F604B3E0ABD" Ref="#PWR013"  Part="1" 
AR Path="/22C6E204B3E0ABD" Ref="#PWR013"  Part="1" 
AR Path="/384B3E0ABD" Ref="#PWR014"  Part="1" 
F 0 "#PWR013" H 4400 13200 30  0001 C CNN
F 1 "GND" H 4400 13130 30  0001 C CNN
	1    4400 13200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4B3E0ABA
P 2050 13200
AR Path="/23D9D84B3E0ABA" Ref="#PWR?"  Part="1" 
AR Path="/394433324B3E0ABA" Ref="#PWR?"  Part="1" 
AR Path="/25E4B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/FFFFFFFF4B3E0ABA" Ref="#PWR019"  Part="1" 
AR Path="/363030314B3E0ABA" Ref="#PWR05"  Part="1" 
AR Path="/23C5884B3E0ABA" Ref="#PWR05"  Part="1" 
AR Path="/4B3E0ABA" Ref="#PWR014"  Part="1" 
AR Path="/94B3E0ABA" Ref="#PWR3"  Part="1" 
AR Path="/DCBAABCD4B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/5AD7153D4B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/A4B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/6FE901F74B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/3FEFFFFF4B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/403091264B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/403051264B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/4032F78D4B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/403251264B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/4032AAC04B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/4030D1264B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/4031778D4B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/3FEA24DD4B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/23D8D44B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/2600004B3E0ABA" Ref="#PWR06"  Part="1" 
AR Path="/6FF0DD404B3E0ABA" Ref="#PWR015"  Part="1" 
AR Path="/6FE934E34B3E0ABA" Ref="#PWR015"  Part="1" 
AR Path="/773F65F14B3E0ABA" Ref="#PWR018"  Part="1" 
AR Path="/773F8EB44B3E0ABA" Ref="#PWR014"  Part="1" 
AR Path="/23C9F04B3E0ABA" Ref="#PWR4"  Part="1" 
AR Path="/FFFFFFF04B3E0ABA" Ref="#PWR3"  Part="1" 
AR Path="/23BC884B3E0ABA" Ref="#PWR018"  Part="1" 
AR Path="/DC0C124B3E0ABA" Ref="#PWR07"  Part="1" 
AR Path="/6684D64B3E0ABA" Ref="#PWR018"  Part="1" 
AR Path="/23C34C4B3E0ABA" Ref="#PWR019"  Part="1" 
AR Path="/23CBC44B3E0ABA" Ref="#PWR018"  Part="1" 
AR Path="/69549BC04B3E0ABA" Ref="#PWR08"  Part="1" 
AR Path="/23C6504B3E0ABA" Ref="#PWR014"  Part="1" 
AR Path="/39803EA4B3E0ABA" Ref="#PWR014"  Part="1" 
AR Path="/73254B3E0ABA" Ref="#PWR015"  Part="1" 
AR Path="/D058A04B3E0ABA" Ref="#PWR015"  Part="1" 
AR Path="/1607D44B3E0ABA" Ref="#PWR015"  Part="1" 
AR Path="/D1C3804B3E0ABA" Ref="#PWR015"  Part="1" 
AR Path="/24B3E0ABA" Ref="#PWR015"  Part="1" 
AR Path="/23D70C4B3E0ABA" Ref="#PWR015"  Part="1" 
AR Path="/14B3E0ABA" Ref="#PWR014"  Part="1" 
AR Path="/2104B3E0ABA" Ref="#PWR015"  Part="1" 
AR Path="/D1CA184B3E0ABA" Ref="#PWR015"  Part="1" 
AR Path="/F4BF4B3E0ABA" Ref="#PWR014"  Part="1" 
AR Path="/262F604B3E0ABA" Ref="#PWR014"  Part="1" 
AR Path="/22C6E204B3E0ABA" Ref="#PWR014"  Part="1" 
AR Path="/384B3E0ABA" Ref="#PWR015"  Part="1" 
F 0 "#PWR014" H 2050 13200 30  0001 C CNN
F 1 "GND" H 2050 13130 30  0001 C CNN
	1    2050 13200
	1    0    0    -1  
$EndComp
Text Label 10350 8750 0    60   ~ 0
sMEMR*
Text Label 8450 8750 0    60   ~ 0
sMEMR
$Comp
L GND #PWR015
U 1 1 4B3D3479
P 12300 15850
AR Path="/4B3D3479" Ref="#PWR015"  Part="1" 
AR Path="/94B3D3479" Ref="#PWR30"  Part="1" 
AR Path="/25E4B3D3479" Ref="#PWR09"  Part="1" 
AR Path="/FFFFFFF04B3D3479" Ref="#PWR26"  Part="1" 
AR Path="/DCBAABCD4B3D3479" Ref="#PWR01"  Part="1" 
AR Path="/6FE901F74B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/4032778D4B3D3479" Ref="#PWR01"  Part="1" 
AR Path="/A84B3D3479" Ref="#PWR29"  Part="1" 
AR Path="/363030314B3D3479" Ref="#PWR09"  Part="1" 
AR Path="/5AD7153D4B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/A4B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/3FEFFFFF4B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/403091264B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/403051264B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/4032F78D4B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/403251264B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/4032AAC04B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/4030D1264B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/4031778D4B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/3FEA24DD4B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/23D8D44B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/2600004B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/6FF0DD404B3D3479" Ref="#PWR016"  Part="1" 
AR Path="/6FE934E34B3D3479" Ref="#PWR016"  Part="1" 
AR Path="/773F65F14B3D3479" Ref="#PWR019"  Part="1" 
AR Path="/773F8EB44B3D3479" Ref="#PWR015"  Part="1" 
AR Path="/23C9F04B3D3479" Ref="#PWR35"  Part="1" 
AR Path="/23BC884B3D3479" Ref="#PWR019"  Part="1" 
AR Path="/DC0C124B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/6684D64B3D3479" Ref="#PWR019"  Part="1" 
AR Path="/23C34C4B3D3479" Ref="#PWR020"  Part="1" 
AR Path="/23CBC44B3D3479" Ref="#PWR019"  Part="1" 
AR Path="/69549BC04B3D3479" Ref="#PWR011"  Part="1" 
AR Path="/23C6504B3D3479" Ref="#PWR015"  Part="1" 
AR Path="/39803EA4B3D3479" Ref="#PWR015"  Part="1" 
AR Path="/FFFFFFFF4B3D3479" Ref="#PWR020"  Part="1" 
AR Path="/73254B3D3479" Ref="#PWR016"  Part="1" 
AR Path="/D058A04B3D3479" Ref="#PWR016"  Part="1" 
AR Path="/1607D44B3D3479" Ref="#PWR016"  Part="1" 
AR Path="/D1C3804B3D3479" Ref="#PWR016"  Part="1" 
AR Path="/24B3D3479" Ref="#PWR016"  Part="1" 
AR Path="/23D70C4B3D3479" Ref="#PWR016"  Part="1" 
AR Path="/14B3D3479" Ref="#PWR015"  Part="1" 
AR Path="/2104B3D3479" Ref="#PWR016"  Part="1" 
AR Path="/D1CA184B3D3479" Ref="#PWR016"  Part="1" 
AR Path="/F4BF4B3D3479" Ref="#PWR015"  Part="1" 
AR Path="/262F604B3D3479" Ref="#PWR015"  Part="1" 
AR Path="/22C6E204B3D3479" Ref="#PWR015"  Part="1" 
AR Path="/384B3D3479" Ref="#PWR016"  Part="1" 
F 0 "#PWR015" H 12300 15850 30  0001 C CNN
F 1 "GND" H 12300 15780 30  0001 C CNN
	1    12300 15850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 4B3D3475
P 12300 15200
AR Path="/4B3D3475" Ref="#PWR016"  Part="1" 
AR Path="/94B3D3475" Ref="#PWR29"  Part="1" 
AR Path="/25E4B3D3475" Ref="#PWR010"  Part="1" 
AR Path="/FFFFFFF04B3D3475" Ref="#PWR25"  Part="1" 
AR Path="/DCBAABCD4B3D3475" Ref="#PWR02"  Part="1" 
AR Path="/6FE901F74B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/4032778D4B3D3475" Ref="#PWR02"  Part="1" 
AR Path="/A84B3D3475" Ref="#PWR28"  Part="1" 
AR Path="/363030314B3D3475" Ref="#PWR010"  Part="1" 
AR Path="/5AD7153D4B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/A4B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/3FEFFFFF4B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/403091264B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/403051264B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/4032F78D4B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/403251264B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/4032AAC04B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/4030D1264B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/4031778D4B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/3FEA24DD4B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/23D8D44B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/2600004B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/6FF0DD404B3D3475" Ref="#PWR017"  Part="1" 
AR Path="/6FE934E34B3D3475" Ref="#PWR017"  Part="1" 
AR Path="/773F65F14B3D3475" Ref="#PWR020"  Part="1" 
AR Path="/773F8EB44B3D3475" Ref="#PWR016"  Part="1" 
AR Path="/23C9F04B3D3475" Ref="#PWR34"  Part="1" 
AR Path="/23BC884B3D3475" Ref="#PWR020"  Part="1" 
AR Path="/DC0C124B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/6684D64B3D3475" Ref="#PWR020"  Part="1" 
AR Path="/23C34C4B3D3475" Ref="#PWR021"  Part="1" 
AR Path="/23CBC44B3D3475" Ref="#PWR020"  Part="1" 
AR Path="/69549BC04B3D3475" Ref="#PWR012"  Part="1" 
AR Path="/23C6504B3D3475" Ref="#PWR016"  Part="1" 
AR Path="/39803EA4B3D3475" Ref="#PWR016"  Part="1" 
AR Path="/FFFFFFFF4B3D3475" Ref="#PWR021"  Part="1" 
AR Path="/73254B3D3475" Ref="#PWR017"  Part="1" 
AR Path="/D058A04B3D3475" Ref="#PWR017"  Part="1" 
AR Path="/1607D44B3D3475" Ref="#PWR017"  Part="1" 
AR Path="/D1C3804B3D3475" Ref="#PWR017"  Part="1" 
AR Path="/24B3D3475" Ref="#PWR017"  Part="1" 
AR Path="/23D70C4B3D3475" Ref="#PWR017"  Part="1" 
AR Path="/14B3D3475" Ref="#PWR016"  Part="1" 
AR Path="/2104B3D3475" Ref="#PWR017"  Part="1" 
AR Path="/D1CA184B3D3475" Ref="#PWR017"  Part="1" 
AR Path="/F4BF4B3D3475" Ref="#PWR016"  Part="1" 
AR Path="/262F604B3D3475" Ref="#PWR016"  Part="1" 
AR Path="/22C6E204B3D3475" Ref="#PWR016"  Part="1" 
AR Path="/384B3D3475" Ref="#PWR017"  Part="1" 
F 0 "#PWR016" H 12300 15200 30  0001 C CNN
F 1 "GND" H 12300 15130 30  0001 C CNN
	1    12300 15200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4B3D3469
P 11850 15400
AR Path="/23D9D84B3D3469" Ref="#PWR?"  Part="1" 
AR Path="/394433324B3D3469" Ref="#PWR?"  Part="1" 
AR Path="/4B3D3469" Ref="#PWR017"  Part="1" 
AR Path="/94B3D3469" Ref="#PWR27"  Part="1" 
AR Path="/25E4B3D3469" Ref="#PWR011"  Part="1" 
AR Path="/FFFFFFF04B3D3469" Ref="#PWR23"  Part="1" 
AR Path="/DCBAABCD4B3D3469" Ref="#PWR03"  Part="1" 
AR Path="/6FE901F74B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/4032778D4B3D3469" Ref="#PWR03"  Part="1" 
AR Path="/A84B3D3469" Ref="#PWR26"  Part="1" 
AR Path="/363030314B3D3469" Ref="#PWR011"  Part="1" 
AR Path="/5AD7153D4B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/A4B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/3FEFFFFF4B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/403091264B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/403051264B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/4032F78D4B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/403251264B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/4032AAC04B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/4030D1264B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/4031778D4B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/3FEA24DD4B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/23D8D44B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/2600004B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/6FF0DD404B3D3469" Ref="#PWR018"  Part="1" 
AR Path="/6FE934E34B3D3469" Ref="#PWR018"  Part="1" 
AR Path="/773F65F14B3D3469" Ref="#PWR021"  Part="1" 
AR Path="/773F8EB44B3D3469" Ref="#PWR017"  Part="1" 
AR Path="/23C9F04B3D3469" Ref="#PWR32"  Part="1" 
AR Path="/23BC884B3D3469" Ref="#PWR021"  Part="1" 
AR Path="/DC0C124B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/6684D64B3D3469" Ref="#PWR021"  Part="1" 
AR Path="/23C34C4B3D3469" Ref="#PWR022"  Part="1" 
AR Path="/23CBC44B3D3469" Ref="#PWR021"  Part="1" 
AR Path="/69549BC04B3D3469" Ref="#PWR013"  Part="1" 
AR Path="/23C6504B3D3469" Ref="#PWR017"  Part="1" 
AR Path="/39803EA4B3D3469" Ref="#PWR017"  Part="1" 
AR Path="/FFFFFFFF4B3D3469" Ref="#PWR022"  Part="1" 
AR Path="/73254B3D3469" Ref="#PWR018"  Part="1" 
AR Path="/D058A04B3D3469" Ref="#PWR018"  Part="1" 
AR Path="/1607D44B3D3469" Ref="#PWR018"  Part="1" 
AR Path="/D1C3804B3D3469" Ref="#PWR018"  Part="1" 
AR Path="/24B3D3469" Ref="#PWR018"  Part="1" 
AR Path="/23D70C4B3D3469" Ref="#PWR018"  Part="1" 
AR Path="/14B3D3469" Ref="#PWR017"  Part="1" 
AR Path="/2104B3D3469" Ref="#PWR018"  Part="1" 
AR Path="/D1CA184B3D3469" Ref="#PWR018"  Part="1" 
AR Path="/F4BF4B3D3469" Ref="#PWR017"  Part="1" 
AR Path="/262F604B3D3469" Ref="#PWR017"  Part="1" 
AR Path="/22C6E204B3D3469" Ref="#PWR017"  Part="1" 
AR Path="/384B3D3469" Ref="#PWR018"  Part="1" 
F 0 "#PWR017" H 11850 15500 30  0001 C CNN
F 1 "VCC" H 11850 15500 30  0000 C CNN
	1    11850 15400
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4B3D344D
P 11850 14750
AR Path="/23D9D84B3D344D" Ref="#PWR?"  Part="1" 
AR Path="/394433324B3D344D" Ref="#PWR?"  Part="1" 
AR Path="/4B3D344D" Ref="#PWR018"  Part="1" 
AR Path="/94B3D344D" Ref="#PWR26"  Part="1" 
AR Path="/25E4B3D344D" Ref="#PWR012"  Part="1" 
AR Path="/FFFFFFF04B3D344D" Ref="#PWR22"  Part="1" 
AR Path="/DCBAABCD4B3D344D" Ref="#PWR04"  Part="1" 
AR Path="/6FE901F74B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/4032778D4B3D344D" Ref="#PWR04"  Part="1" 
AR Path="/A84B3D344D" Ref="#PWR25"  Part="1" 
AR Path="/363030314B3D344D" Ref="#PWR012"  Part="1" 
AR Path="/5AD7153D4B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/A4B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/3FEFFFFF4B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/403091264B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/403051264B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/4032F78D4B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/403251264B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/4032AAC04B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/4030D1264B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/4031778D4B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/3FEA24DD4B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/23D8D44B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/2600004B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/6FF0DD404B3D344D" Ref="#PWR019"  Part="1" 
AR Path="/6FE934E34B3D344D" Ref="#PWR019"  Part="1" 
AR Path="/773F65F14B3D344D" Ref="#PWR022"  Part="1" 
AR Path="/773F8EB44B3D344D" Ref="#PWR018"  Part="1" 
AR Path="/23C9F04B3D344D" Ref="#PWR31"  Part="1" 
AR Path="/23BC884B3D344D" Ref="#PWR022"  Part="1" 
AR Path="/DC0C124B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/6684D64B3D344D" Ref="#PWR022"  Part="1" 
AR Path="/23C34C4B3D344D" Ref="#PWR023"  Part="1" 
AR Path="/23CBC44B3D344D" Ref="#PWR022"  Part="1" 
AR Path="/69549BC04B3D344D" Ref="#PWR014"  Part="1" 
AR Path="/23C6504B3D344D" Ref="#PWR018"  Part="1" 
AR Path="/39803EA4B3D344D" Ref="#PWR018"  Part="1" 
AR Path="/FFFFFFFF4B3D344D" Ref="#PWR023"  Part="1" 
AR Path="/73254B3D344D" Ref="#PWR019"  Part="1" 
AR Path="/D058A04B3D344D" Ref="#PWR019"  Part="1" 
AR Path="/1607D44B3D344D" Ref="#PWR019"  Part="1" 
AR Path="/D1C3804B3D344D" Ref="#PWR019"  Part="1" 
AR Path="/24B3D344D" Ref="#PWR019"  Part="1" 
AR Path="/23D70C4B3D344D" Ref="#PWR019"  Part="1" 
AR Path="/14B3D344D" Ref="#PWR018"  Part="1" 
AR Path="/2104B3D344D" Ref="#PWR019"  Part="1" 
AR Path="/D1CA184B3D344D" Ref="#PWR019"  Part="1" 
AR Path="/F4BF4B3D344D" Ref="#PWR018"  Part="1" 
AR Path="/262F604B3D344D" Ref="#PWR018"  Part="1" 
AR Path="/22C6E204B3D344D" Ref="#PWR018"  Part="1" 
AR Path="/384B3D344D" Ref="#PWR019"  Part="1" 
F 0 "#PWR018" H 11850 14850 30  0001 C CNN
F 1 "VCC" H 11850 14850 30  0000 C CNN
	1    11850 14750
	1    0    0    -1  
$EndComp
$Comp
L C C42
U 1 1 4B3D33B6
P 16350 14950
AR Path="/4B3D33B6" Ref="C42"  Part="1" 
AR Path="/94B3D33B6" Ref="C42"  Part="1" 
AR Path="/FFFFFFF04B3D33B6" Ref="C42"  Part="1" 
AR Path="/DCBAABCD4B3D33B6" Ref="C42"  Part="1" 
AR Path="/6FE901F74B3D33B6" Ref="C42"  Part="1" 
AR Path="/4032778D4B3D33B6" Ref="C42"  Part="1" 
AR Path="/A84B3D33B6" Ref="C42"  Part="1" 
AR Path="/5AD7153D4B3D33B6" Ref="C42"  Part="1" 
AR Path="/A4B3D33B6" Ref="C42"  Part="1" 
AR Path="/3FEFFFFF4B3D33B6" Ref="C42"  Part="1" 
AR Path="/403091264B3D33B6" Ref="C42"  Part="1" 
AR Path="/403051264B3D33B6" Ref="C42"  Part="1" 
AR Path="/4032F78D4B3D33B6" Ref="C42"  Part="1" 
AR Path="/403251264B3D33B6" Ref="C42"  Part="1" 
AR Path="/4032AAC04B3D33B6" Ref="C42"  Part="1" 
AR Path="/4030D1264B3D33B6" Ref="C42"  Part="1" 
AR Path="/4031778D4B3D33B6" Ref="C42"  Part="1" 
AR Path="/3FEA24DD4B3D33B6" Ref="C42"  Part="1" 
AR Path="/23D8D44B3D33B6" Ref="C42"  Part="1" 
AR Path="/2600004B3D33B6" Ref="C42"  Part="1" 
AR Path="/14B3D33B6" Ref="C42"  Part="1" 
AR Path="/6FF0DD404B3D33B6" Ref="C42"  Part="1" 
AR Path="/6FE934E34B3D33B6" Ref="C42"  Part="1" 
AR Path="/773F65F14B3D33B6" Ref="C42"  Part="1" 
AR Path="/773F8EB44B3D33B6" Ref="C42"  Part="1" 
AR Path="/23C9F04B3D33B6" Ref="C42"  Part="1" 
AR Path="/24B3D33B6" Ref="C42"  Part="1" 
AR Path="/23BC884B3D33B6" Ref="C42"  Part="1" 
AR Path="/DC0C124B3D33B6" Ref="C42"  Part="1" 
AR Path="/23C34C4B3D33B6" Ref="C42"  Part="1" 
AR Path="/23CBC44B3D33B6" Ref="C42"  Part="1" 
AR Path="/69549BC04B3D33B6" Ref="C42"  Part="1" 
AR Path="/23C6504B3D33B6" Ref="C42"  Part="1" 
AR Path="/39803EA4B3D33B6" Ref="C42"  Part="1" 
AR Path="/FFFFFFFF4B3D33B6" Ref="C42"  Part="1" 
AR Path="/73254B3D33B6" Ref="C42"  Part="1" 
AR Path="/D058A04B3D33B6" Ref="C42"  Part="1" 
AR Path="/1607D44B3D33B6" Ref="C42"  Part="1" 
AR Path="/D1C3804B3D33B6" Ref="C42"  Part="1" 
AR Path="/23D70C4B3D33B6" Ref="C42"  Part="1" 
AR Path="/2104B3D33B6" Ref="C42"  Part="1" 
AR Path="/D1CA184B3D33B6" Ref="C42"  Part="1" 
AR Path="/F4BF4B3D33B6" Ref="C42"  Part="1" 
AR Path="/262F604B3D33B6" Ref="C42"  Part="1" 
AR Path="/22C6E204B3D33B6" Ref="C42"  Part="1" 
AR Path="/384B3D33B6" Ref="C42"  Part="1" 
F 0 "C42" H 16400 15050 50  0000 L CNN
F 1 "0.1 uF" H 16400 14850 50  0000 L CNN
F 2 "C2" H 16350 14950 60  0001 C CNN
	1    16350 14950
	1    0    0    -1  
$EndComp
$Comp
L C C41
U 1 1 4B3D33B5
P 15900 14950
AR Path="/4B3D33B5" Ref="C41"  Part="1" 
AR Path="/94B3D33B5" Ref="C41"  Part="1" 
AR Path="/FFFFFFF04B3D33B5" Ref="C41"  Part="1" 
AR Path="/4032778D4B3D33B5" Ref="C41"  Part="1" 
AR Path="/A84B3D33B5" Ref="C41"  Part="1" 
AR Path="/5AD7153D4B3D33B5" Ref="C41"  Part="1" 
AR Path="/A4B3D33B5" Ref="C41"  Part="1" 
AR Path="/6FE901F74B3D33B5" Ref="C41"  Part="1" 
AR Path="/3FEFFFFF4B3D33B5" Ref="C41"  Part="1" 
AR Path="/403091264B3D33B5" Ref="C41"  Part="1" 
AR Path="/403051264B3D33B5" Ref="C41"  Part="1" 
AR Path="/4032F78D4B3D33B5" Ref="C41"  Part="1" 
AR Path="/403251264B3D33B5" Ref="C41"  Part="1" 
AR Path="/4032AAC04B3D33B5" Ref="C41"  Part="1" 
AR Path="/4030D1264B3D33B5" Ref="C41"  Part="1" 
AR Path="/4031778D4B3D33B5" Ref="C41"  Part="1" 
AR Path="/3FEA24DD4B3D33B5" Ref="C41"  Part="1" 
AR Path="/23D8D44B3D33B5" Ref="C41"  Part="1" 
AR Path="/2600004B3D33B5" Ref="C41"  Part="1" 
AR Path="/14B3D33B5" Ref="C41"  Part="1" 
AR Path="/6FF0DD404B3D33B5" Ref="C41"  Part="1" 
AR Path="/6FE934E34B3D33B5" Ref="C41"  Part="1" 
AR Path="/773F65F14B3D33B5" Ref="C41"  Part="1" 
AR Path="/773F8EB44B3D33B5" Ref="C41"  Part="1" 
AR Path="/23C9F04B3D33B5" Ref="C41"  Part="1" 
AR Path="/24B3D33B5" Ref="C41"  Part="1" 
AR Path="/23BC884B3D33B5" Ref="C41"  Part="1" 
AR Path="/DC0C124B3D33B5" Ref="C41"  Part="1" 
AR Path="/23C34C4B3D33B5" Ref="C41"  Part="1" 
AR Path="/23CBC44B3D33B5" Ref="C41"  Part="1" 
AR Path="/69549BC04B3D33B5" Ref="C41"  Part="1" 
AR Path="/23C6504B3D33B5" Ref="C41"  Part="1" 
AR Path="/39803EA4B3D33B5" Ref="C41"  Part="1" 
AR Path="/FFFFFFFF4B3D33B5" Ref="C41"  Part="1" 
AR Path="/73254B3D33B5" Ref="C41"  Part="1" 
AR Path="/D058A04B3D33B5" Ref="C41"  Part="1" 
AR Path="/1607D44B3D33B5" Ref="C41"  Part="1" 
AR Path="/D1C3804B3D33B5" Ref="C41"  Part="1" 
AR Path="/23D70C4B3D33B5" Ref="C41"  Part="1" 
AR Path="/2104B3D33B5" Ref="C41"  Part="1" 
AR Path="/D1CA184B3D33B5" Ref="C41"  Part="1" 
AR Path="/F4BF4B3D33B5" Ref="C41"  Part="1" 
AR Path="/262F604B3D33B5" Ref="C41"  Part="1" 
AR Path="/22C6E204B3D33B5" Ref="C41"  Part="1" 
AR Path="/384B3D33B5" Ref="C41"  Part="1" 
F 0 "C41" H 15950 15050 50  0000 L CNN
F 1 "0.1 uF" H 15950 14850 50  0000 L CNN
F 2 "C2" H 15900 14950 60  0001 C CNN
	1    15900 14950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4B3D1569
P 10950 3000
AR Path="/69698AFC4B3D1569" Ref="#PWR?"  Part="1" 
AR Path="/393639364B3D1569" Ref="#PWR?"  Part="1" 
AR Path="/4B3D1569" Ref="#PWR019"  Part="1" 
AR Path="/94B3D1569" Ref="#PWR20"  Part="1" 
AR Path="/25E4B3D1569" Ref="#PWR015"  Part="1" 
AR Path="/14B3D1569" Ref="#PWR019"  Part="1" 
AR Path="/DCBAABCD4B3D1569" Ref="#PWR01"  Part="1" 
AR Path="/23D9304B3D1569" Ref="#PWR01"  Part="1" 
AR Path="/23D8D44B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/4031DDF34B3D1569" Ref="#PWR01"  Part="1" 
AR Path="/3FEFFFFF4B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/3FE88B434B3D1569" Ref="#PWR01"  Part="1" 
AR Path="/FFFFFFF04B3D1569" Ref="#PWR20"  Part="1" 
AR Path="/4032778D4B3D1569" Ref="#PWR07"  Part="1" 
AR Path="/A84B3D1569" Ref="#PWR19"  Part="1" 
AR Path="/363030314B3D1569" Ref="#PWR015"  Part="1" 
AR Path="/5AD7153D4B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/A4B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/6FE901F74B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/403091264B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/403051264B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/4032F78D4B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/403251264B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/4032AAC04B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/4030D1264B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/4031778D4B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/3FEA24DD4B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/2600004B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/6FF0DD404B3D1569" Ref="#PWR020"  Part="1" 
AR Path="/6FE934E34B3D1569" Ref="#PWR020"  Part="1" 
AR Path="/773F65F14B3D1569" Ref="#PWR023"  Part="1" 
AR Path="/773F8EB44B3D1569" Ref="#PWR019"  Part="1" 
AR Path="/23C9F04B3D1569" Ref="#PWR28"  Part="1" 
AR Path="/23BC884B3D1569" Ref="#PWR023"  Part="1" 
AR Path="/DC0C124B3D1569" Ref="#PWR016"  Part="1" 
AR Path="/6684D64B3D1569" Ref="#PWR023"  Part="1" 
AR Path="/23C34C4B3D1569" Ref="#PWR024"  Part="1" 
AR Path="/23CBC44B3D1569" Ref="#PWR023"  Part="1" 
AR Path="/69549BC04B3D1569" Ref="#PWR017"  Part="1" 
AR Path="/23C6504B3D1569" Ref="#PWR019"  Part="1" 
AR Path="/39803EA4B3D1569" Ref="#PWR021"  Part="1" 
AR Path="/FFFFFFFF4B3D1569" Ref="#PWR024"  Part="1" 
AR Path="/73254B3D1569" Ref="#PWR022"  Part="1" 
AR Path="/D058A04B3D1569" Ref="#PWR022"  Part="1" 
AR Path="/1607D44B3D1569" Ref="#PWR022"  Part="1" 
AR Path="/D1C3804B3D1569" Ref="#PWR022"  Part="1" 
AR Path="/24B3D1569" Ref="#PWR022"  Part="1" 
AR Path="/23D70C4B3D1569" Ref="#PWR022"  Part="1" 
AR Path="/2104B3D1569" Ref="#PWR022"  Part="1" 
AR Path="/D1CA184B3D1569" Ref="#PWR022"  Part="1" 
AR Path="/F4BF4B3D1569" Ref="#PWR019"  Part="1" 
AR Path="/262F604B3D1569" Ref="#PWR019"  Part="1" 
AR Path="/22C6E204B3D1569" Ref="#PWR019"  Part="1" 
AR Path="/384B3D1569" Ref="#PWR020"  Part="1" 
F 0 "#PWR019" H 10950 3000 30  0001 C CNN
F 1 "GND" H 10950 2930 30  0001 C CNN
	1    10950 3000
	1    0    0    -1  
$EndComp
Text Label 950  13050 0    60   ~ 0
RAMCS0*
Text Label 3300 16200 0    60   ~ 0
RAMCS1*
Text Label 9850 10850 0    60   ~ 0
OD0
Text Label 9850 10950 0    60   ~ 0
OD1
Text Label 9850 11050 0    60   ~ 0
OD2
Text Label 9850 11150 0    60   ~ 0
OD3
Text Label 9850 11250 0    60   ~ 0
OD4
Text Label 9850 11350 0    60   ~ 0
OD5
Text Label 9850 11450 0    60   ~ 0
OD6
Text Label 9850 11550 0    60   ~ 0
OD7
Text Label 7500 11550 0    60   ~ 0
OD7
Text Label 7500 11450 0    60   ~ 0
OD6
Text Label 7500 11350 0    60   ~ 0
OD5
Text Label 7500 11250 0    60   ~ 0
OD4
Text Label 7500 11150 0    60   ~ 0
OD3
Text Label 7500 11050 0    60   ~ 0
OD2
Text Label 7500 10950 0    60   ~ 0
OD1
Text Label 7500 10850 0    60   ~ 0
OD0
Text Label 5150 10850 0    60   ~ 0
OD0
Text Label 5150 10950 0    60   ~ 0
OD1
Text Label 5150 11050 0    60   ~ 0
OD2
Text Label 5150 11150 0    60   ~ 0
OD3
Text Label 5150 11250 0    60   ~ 0
OD4
Text Label 5150 11350 0    60   ~ 0
OD5
Text Label 5150 11450 0    60   ~ 0
OD6
Text Label 5150 11550 0    60   ~ 0
OD7
Text Label 2800 11550 0    60   ~ 0
OD7
Text Label 2800 11450 0    60   ~ 0
OD6
Text Label 2800 11350 0    60   ~ 0
OD5
Text Label 2800 11250 0    60   ~ 0
OD4
Text Label 2800 11150 0    60   ~ 0
OD3
Text Label 2800 11050 0    60   ~ 0
OD2
Text Label 2800 10950 0    60   ~ 0
OD1
Text Label 2800 10850 0    60   ~ 0
OD0
Text Label 2800 14700 0    60   ~ 0
ED7
Text Label 2800 14600 0    60   ~ 0
ED6
Text Label 2800 14500 0    60   ~ 0
ED5
Text Label 2800 14400 0    60   ~ 0
ED4
Text Label 2800 14300 0    60   ~ 0
ED3
Text Label 2800 14200 0    60   ~ 0
ED2
Text Label 2800 14100 0    60   ~ 0
ED1
Text Label 2800 14000 0    60   ~ 0
ED0
Text Label 5150 14000 0    60   ~ 0
ED0
Text Label 5150 14100 0    60   ~ 0
ED1
Text Label 5150 14200 0    60   ~ 0
ED2
Text Label 5150 14300 0    60   ~ 0
ED3
Text Label 5150 14400 0    60   ~ 0
ED4
Text Label 5150 14500 0    60   ~ 0
ED5
Text Label 5150 14600 0    60   ~ 0
ED6
Text Label 5150 14700 0    60   ~ 0
ED7
Text Label 7500 14700 0    60   ~ 0
ED7
Text Label 7500 14600 0    60   ~ 0
ED6
Text Label 7500 14500 0    60   ~ 0
ED5
Text Label 7500 14400 0    60   ~ 0
ED4
Text Label 7500 14300 0    60   ~ 0
ED3
Text Label 7500 14200 0    60   ~ 0
ED2
Text Label 7500 14100 0    60   ~ 0
ED1
Text Label 7500 14000 0    60   ~ 0
ED0
Text Label 9850 14000 0    60   ~ 0
ED0
Text Label 9850 14100 0    60   ~ 0
ED1
Text Label 9850 14200 0    60   ~ 0
ED2
Text Label 9850 14300 0    60   ~ 0
ED3
Text Label 9850 14400 0    60   ~ 0
ED4
Text Label 9850 14500 0    60   ~ 0
ED5
Text Label 9850 14600 0    60   ~ 0
ED6
Text Label 9850 14700 0    60   ~ 0
ED7
Text Label 17400 6750 0    60   ~ 0
ED7
Text Label 17400 6650 0    60   ~ 0
ED6
Text Label 17400 6550 0    60   ~ 0
ED5
Text Label 17400 6450 0    60   ~ 0
ED4
Text Label 17400 6350 0    60   ~ 0
ED3
Text Label 17400 6250 0    60   ~ 0
ED2
Text Label 17400 6150 0    60   ~ 0
ED1
Text Label 17400 6050 0    60   ~ 0
ED0
Text Label 15650 5450 0    60   ~ 0
OD7
Text Label 15650 5350 0    60   ~ 0
OD6
Text Label 15650 5250 0    60   ~ 0
OD5
Text Label 15650 5150 0    60   ~ 0
OD4
Text Label 15650 5050 0    60   ~ 0
OD3
Text Label 15650 4950 0    60   ~ 0
OD2
Text Label 15650 4850 0    60   ~ 0
OD1
Text Label 15650 4750 0    60   ~ 0
OD0
Text Label 15650 5650 0    60   ~ 0
GND
$Comp
L 74LS245 U?
U 1 1 4B3CF4C1
P 16650 5250
AR Path="/69698AFC4B3CF4C1" Ref="U?"  Part="1" 
AR Path="/393639364B3CF4C1" Ref="U?"  Part="1" 
AR Path="/23D88C4B3CF4C1" Ref="U?"  Part="1" 
AR Path="/4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/7E44048F4B3CF4C1" Ref="U?"  Part="1" 
AR Path="/2E309E04B3CF4C1" Ref="U"  Part="1" 
AR Path="/A84B3CF4C1" Ref="U31"  Part="1" 
AR Path="/94B3CF4C1" Ref="U31"  Part="1" 
AR Path="/FFFFFFF04B3CF4C1" Ref="U31"  Part="1" 
AR Path="/14B3CF4C1" Ref="U31"  Part="1" 
AR Path="/6FF0DD404B3CF4C1" Ref="U31"  Part="1" 
AR Path="/23D9304B3CF4C1" Ref="U31"  Part="1" 
AR Path="/23D8D44B3CF4C1" Ref="U31"  Part="1" 
AR Path="/4031DDF34B3CF4C1" Ref="U31"  Part="1" 
AR Path="/3FEFFFFF4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/3FE88B434B3CF4C1" Ref="U31"  Part="1" 
AR Path="/4032778D4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/5AD7153D4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/A4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/403091264B3CF4C1" Ref="U31"  Part="1" 
AR Path="/403051264B3CF4C1" Ref="U31"  Part="1" 
AR Path="/4032F78D4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/403251264B3CF4C1" Ref="U31"  Part="1" 
AR Path="/4032AAC04B3CF4C1" Ref="U31"  Part="1" 
AR Path="/4030D1264B3CF4C1" Ref="U31"  Part="1" 
AR Path="/4031778D4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/3FEA24DD4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/2600004B3CF4C1" Ref="U31"  Part="1" 
AR Path="/6FE934E34B3CF4C1" Ref="U31"  Part="1" 
AR Path="/773F8EB44B3CF4C1" Ref="U31"  Part="1" 
AR Path="/23C9F04B3CF4C1" Ref="U31"  Part="1" 
AR Path="/24B3CF4C1" Ref="U31"  Part="1" 
AR Path="/23BC884B3CF4C1" Ref="U31"  Part="1" 
AR Path="/DC0C124B3CF4C1" Ref="U31"  Part="1" 
AR Path="/23C34C4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/23CBC44B3CF4C1" Ref="U31"  Part="1" 
AR Path="/69549BC04B3CF4C1" Ref="U31"  Part="1" 
AR Path="/23C6504B3CF4C1" Ref="U31"  Part="1" 
AR Path="/39803EA4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/FFFFFFFF4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/73254B3CF4C1" Ref="U31"  Part="1" 
AR Path="/D058A04B3CF4C1" Ref="U31"  Part="1" 
AR Path="/1607D44B3CF4C1" Ref="U31"  Part="1" 
AR Path="/D1C3804B3CF4C1" Ref="U31"  Part="1" 
AR Path="/23D70C4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/2104B3CF4C1" Ref="U31"  Part="1" 
AR Path="/D1CA184B3CF4C1" Ref="U31"  Part="1" 
AR Path="/F4BF4B3CF4C1" Ref="U31"  Part="1" 
AR Path="/262F604B3CF4C1" Ref="U31"  Part="1" 
AR Path="/22C6E204B3CF4C1" Ref="U31"  Part="1" 
AR Path="/384B3CF4C1" Ref="U31"  Part="1" 
F 0 "U31" H 16750 5850 60  0000 L CNN
F 1 "74LS245" H 16750 4650 60  0000 L CNN
F 2 "20dip300" H 16650 5250 60  0001 C CNN
	1    16650 5250
	1    0    0    -1  
$EndComp
Text Label 17400 850  0    60   ~ 0
bA1
Text Label 15750 2850 0    60   ~ 0
A16
Text Label 16450 9250 0    60   ~ 0
C*
Text Label 16500 7900 0    60   ~ 0
A*
Text Label 17700 12850 0    60   ~ 0
G*
Text Label 17700 12150 0    60   ~ 0
F*
Text Label 17700 11550 0    60   ~ 0
E*
Text Label 17700 11000 0    60   ~ 0
D*
Text Label 13650 11000 0    60   ~ 0
buffered_A0
Text Label 14050 10750 0    60   ~ 0
A0*
Text Label 17700 10100 0    60   ~ 0
B*
Text Label 16200 10100 0    60   ~ 0
GND
Text Notes 4200 1600 0    60   ~ 0
8rd\n16rd\n8wr\n16wr
$Comp
L LED D?
U 1 1 4B3CE961
P 3950 1850
AR Path="/23D9D84B3CE961" Ref="D?"  Part="1" 
AR Path="/394433324B3CE961" Ref="D?"  Part="1" 
AR Path="/4B3CE961" Ref="D5"  Part="1" 
AR Path="/A84B3CE961" Ref="D5"  Part="1" 
AR Path="/94B3CE961" Ref="D5"  Part="1" 
AR Path="/FFFFFFF04B3CE961" Ref="D5"  Part="1" 
AR Path="/14B3CE961" Ref="D5"  Part="1" 
AR Path="/6FF0DD404B3CE961" Ref="D5"  Part="1" 
AR Path="/23D9304B3CE961" Ref="D5"  Part="1" 
AR Path="/23D8D44B3CE961" Ref="D5"  Part="1" 
AR Path="/4031DDF34B3CE961" Ref="D5"  Part="1" 
AR Path="/3FEFFFFF4B3CE961" Ref="D5"  Part="1" 
AR Path="/3FE88B434B3CE961" Ref="D5"  Part="1" 
AR Path="/4032778D4B3CE961" Ref="D5"  Part="1" 
AR Path="/5AD7153D4B3CE961" Ref="D5"  Part="1" 
AR Path="/A4B3CE961" Ref="D5"  Part="1" 
AR Path="/403091264B3CE961" Ref="D5"  Part="1" 
AR Path="/403051264B3CE961" Ref="D5"  Part="1" 
AR Path="/4032F78D4B3CE961" Ref="D5"  Part="1" 
AR Path="/403251264B3CE961" Ref="D5"  Part="1" 
AR Path="/4032AAC04B3CE961" Ref="D5"  Part="1" 
AR Path="/4030D1264B3CE961" Ref="D5"  Part="1" 
AR Path="/4031778D4B3CE961" Ref="D5"  Part="1" 
AR Path="/3FEA24DD4B3CE961" Ref="D5"  Part="1" 
AR Path="/2600004B3CE961" Ref="D5"  Part="1" 
AR Path="/6FE934E34B3CE961" Ref="D5"  Part="1" 
AR Path="/773F65F14B3CE961" Ref="D5"  Part="1" 
AR Path="/773F8EB44B3CE961" Ref="D5"  Part="1" 
AR Path="/23C9F04B3CE961" Ref="D5"  Part="1" 
AR Path="/24B3CE961" Ref="D5"  Part="1" 
AR Path="/23BC884B3CE961" Ref="D5"  Part="1" 
AR Path="/DC0C124B3CE961" Ref="D5"  Part="1" 
AR Path="/23C34C4B3CE961" Ref="D5"  Part="1" 
AR Path="/23CBC44B3CE961" Ref="D5"  Part="1" 
AR Path="/69549BC04B3CE961" Ref="D5"  Part="1" 
AR Path="/23C6504B3CE961" Ref="D5"  Part="1" 
AR Path="/39803EA4B3CE961" Ref="D5"  Part="1" 
AR Path="/FFFFFFFF4B3CE961" Ref="D5"  Part="1" 
AR Path="/D058A04B3CE961" Ref="D5"  Part="1" 
AR Path="/1607D44B3CE961" Ref="D5"  Part="1" 
AR Path="/D1C3804B3CE961" Ref="D5"  Part="1" 
AR Path="/23D70C4B3CE961" Ref="D5"  Part="1" 
AR Path="/2104B3CE961" Ref="D5"  Part="1" 
AR Path="/D1CA184B3CE961" Ref="D5"  Part="1" 
AR Path="/F4BF4B3CE961" Ref="D5"  Part="1" 
AR Path="/262F604B3CE961" Ref="D5"  Part="1" 
AR Path="/22C6E204B3CE961" Ref="D5"  Part="1" 
AR Path="/384B3CE961" Ref="D5"  Part="1" 
AR Path="/7E428DAC4B3CE961" Ref="D4"  Part="1" 
AR Path="/35312E304B3CE961" Ref="D5"  Part="1" 
AR Path="/10C08904B3CE961" Ref="D"  Part="1" 
AR Path="/390086E4B3CE961" Ref="D"  Part="1" 
AR Path="/17A087A4B3CE961" Ref="D"  Part="1" 
AR Path="/12C08F04B3CE961" Ref="D"  Part="1" 
F 0 "D5" H 3950 1950 50  0000 C CNN
F 1 "LED" H 3950 1750 50  0000 C CNN
F 2 "LEDV" H 3950 1850 60  0001 C CNN
	1    3950 1850
	-1   0    0    1   
$EndComp
$Comp
L LED D?
U 1 1 4B3CE95A
P 3950 1750
AR Path="/23D9D84B3CE95A" Ref="D?"  Part="1" 
AR Path="/394433324B3CE95A" Ref="D?"  Part="1" 
AR Path="/4B3CE95A" Ref="D4"  Part="1" 
AR Path="/A84B3CE95A" Ref="D4"  Part="1" 
AR Path="/94B3CE95A" Ref="D4"  Part="1" 
AR Path="/FFFFFFF04B3CE95A" Ref="D4"  Part="1" 
AR Path="/14B3CE95A" Ref="D4"  Part="1" 
AR Path="/6FF0DD404B3CE95A" Ref="D4"  Part="1" 
AR Path="/23D9304B3CE95A" Ref="D4"  Part="1" 
AR Path="/23D8D44B3CE95A" Ref="D4"  Part="1" 
AR Path="/4031DDF34B3CE95A" Ref="D4"  Part="1" 
AR Path="/3FEFFFFF4B3CE95A" Ref="D4"  Part="1" 
AR Path="/3FE88B434B3CE95A" Ref="D4"  Part="1" 
AR Path="/4032778D4B3CE95A" Ref="D4"  Part="1" 
AR Path="/5AD7153D4B3CE95A" Ref="D4"  Part="1" 
AR Path="/A4B3CE95A" Ref="D4"  Part="1" 
AR Path="/403091264B3CE95A" Ref="D4"  Part="1" 
AR Path="/403051264B3CE95A" Ref="D4"  Part="1" 
AR Path="/4032F78D4B3CE95A" Ref="D4"  Part="1" 
AR Path="/403251264B3CE95A" Ref="D4"  Part="1" 
AR Path="/4032AAC04B3CE95A" Ref="D4"  Part="1" 
AR Path="/4030D1264B3CE95A" Ref="D4"  Part="1" 
AR Path="/4031778D4B3CE95A" Ref="D4"  Part="1" 
AR Path="/3FEA24DD4B3CE95A" Ref="D4"  Part="1" 
AR Path="/2600004B3CE95A" Ref="D4"  Part="1" 
AR Path="/6FE934E34B3CE95A" Ref="D4"  Part="1" 
AR Path="/773F65F14B3CE95A" Ref="D4"  Part="1" 
AR Path="/773F8EB44B3CE95A" Ref="D4"  Part="1" 
AR Path="/23C9F04B3CE95A" Ref="D4"  Part="1" 
AR Path="/24B3CE95A" Ref="D4"  Part="1" 
AR Path="/23BC884B3CE95A" Ref="D4"  Part="1" 
AR Path="/DC0C124B3CE95A" Ref="D4"  Part="1" 
AR Path="/23C34C4B3CE95A" Ref="D4"  Part="1" 
AR Path="/23CBC44B3CE95A" Ref="D4"  Part="1" 
AR Path="/69549BC04B3CE95A" Ref="D4"  Part="1" 
AR Path="/23C6504B3CE95A" Ref="D4"  Part="1" 
AR Path="/39803EA4B3CE95A" Ref="D4"  Part="1" 
AR Path="/FFFFFFFF4B3CE95A" Ref="D4"  Part="1" 
AR Path="/D058A04B3CE95A" Ref="D4"  Part="1" 
AR Path="/1607D44B3CE95A" Ref="D4"  Part="1" 
AR Path="/D1C3804B3CE95A" Ref="D4"  Part="1" 
AR Path="/23D70C4B3CE95A" Ref="D4"  Part="1" 
AR Path="/2104B3CE95A" Ref="D4"  Part="1" 
AR Path="/D1CA184B3CE95A" Ref="D4"  Part="1" 
AR Path="/F4BF4B3CE95A" Ref="D4"  Part="1" 
AR Path="/262F604B3CE95A" Ref="D4"  Part="1" 
AR Path="/22C6E204B3CE95A" Ref="D4"  Part="1" 
AR Path="/384B3CE95A" Ref="D4"  Part="1" 
F 0 "D4" H 3950 1850 50  0000 C CNN
F 1 "LED" H 3950 1650 50  0000 C CNN
F 2 "LEDV" H 3950 1750 60  0001 C CNN
	1    3950 1750
	-1   0    0    1   
$EndComp
$Comp
L LED D?
U 1 1 4B3CE94F
P 3950 1650
AR Path="/23D9D84B3CE94F" Ref="D?"  Part="1" 
AR Path="/394433324B3CE94F" Ref="D?"  Part="1" 
AR Path="/4B3CE94F" Ref="D3"  Part="1" 
AR Path="/A84B3CE94F" Ref="D3"  Part="1" 
AR Path="/94B3CE94F" Ref="D3"  Part="1" 
AR Path="/FFFFFFF04B3CE94F" Ref="D3"  Part="1" 
AR Path="/14B3CE94F" Ref="D3"  Part="1" 
AR Path="/6FF0DD404B3CE94F" Ref="D3"  Part="1" 
AR Path="/23D9304B3CE94F" Ref="D3"  Part="1" 
AR Path="/23D8D44B3CE94F" Ref="D3"  Part="1" 
AR Path="/4031DDF34B3CE94F" Ref="D3"  Part="1" 
AR Path="/3FEFFFFF4B3CE94F" Ref="D3"  Part="1" 
AR Path="/3FE88B434B3CE94F" Ref="D3"  Part="1" 
AR Path="/4032778D4B3CE94F" Ref="D3"  Part="1" 
AR Path="/5AD7153D4B3CE94F" Ref="D3"  Part="1" 
AR Path="/A4B3CE94F" Ref="D3"  Part="1" 
AR Path="/403091264B3CE94F" Ref="D3"  Part="1" 
AR Path="/403051264B3CE94F" Ref="D3"  Part="1" 
AR Path="/4032F78D4B3CE94F" Ref="D3"  Part="1" 
AR Path="/403251264B3CE94F" Ref="D3"  Part="1" 
AR Path="/4032AAC04B3CE94F" Ref="D3"  Part="1" 
AR Path="/4030D1264B3CE94F" Ref="D3"  Part="1" 
AR Path="/4031778D4B3CE94F" Ref="D3"  Part="1" 
AR Path="/3FEA24DD4B3CE94F" Ref="D3"  Part="1" 
AR Path="/2600004B3CE94F" Ref="D3"  Part="1" 
AR Path="/6FE934E34B3CE94F" Ref="D3"  Part="1" 
AR Path="/773F65F14B3CE94F" Ref="D3"  Part="1" 
AR Path="/773F8EB44B3CE94F" Ref="D3"  Part="1" 
AR Path="/23C9F04B3CE94F" Ref="D3"  Part="1" 
AR Path="/24B3CE94F" Ref="D3"  Part="1" 
AR Path="/23BC884B3CE94F" Ref="D3"  Part="1" 
AR Path="/DC0C124B3CE94F" Ref="D3"  Part="1" 
AR Path="/23C34C4B3CE94F" Ref="D3"  Part="1" 
AR Path="/23CBC44B3CE94F" Ref="D3"  Part="1" 
AR Path="/69549BC04B3CE94F" Ref="D3"  Part="1" 
AR Path="/23C6504B3CE94F" Ref="D3"  Part="1" 
AR Path="/39803EA4B3CE94F" Ref="D3"  Part="1" 
AR Path="/FFFFFFFF4B3CE94F" Ref="D3"  Part="1" 
AR Path="/D058A04B3CE94F" Ref="D3"  Part="1" 
AR Path="/1607D44B3CE94F" Ref="D3"  Part="1" 
AR Path="/D1C3804B3CE94F" Ref="D3"  Part="1" 
AR Path="/23D70C4B3CE94F" Ref="D3"  Part="1" 
AR Path="/2104B3CE94F" Ref="D3"  Part="1" 
AR Path="/D1CA184B3CE94F" Ref="D3"  Part="1" 
AR Path="/F4BF4B3CE94F" Ref="D3"  Part="1" 
AR Path="/262F604B3CE94F" Ref="D3"  Part="1" 
AR Path="/22C6E204B3CE94F" Ref="D3"  Part="1" 
AR Path="/384B3CE94F" Ref="D3"  Part="1" 
F 0 "D3" H 3950 1750 50  0000 C CNN
F 1 "LED" H 3950 1550 50  0000 C CNN
F 2 "LEDV" H 3950 1650 60  0001 C CNN
	1    3950 1650
	-1   0    0    1   
$EndComp
$Comp
L LED D?
U 1 1 4B3CE93D
P 8300 1900
AR Path="/69698AFC4B3CE93D" Ref="D?"  Part="1" 
AR Path="/393639364B3CE93D" Ref="D?"  Part="1" 
AR Path="/4B3CE93D" Ref="D9"  Part="1" 
AR Path="/A84B3CE93D" Ref="D2"  Part="1" 
AR Path="/94B3CE93D" Ref="D2"  Part="1" 
AR Path="/FFFFFFF04B3CE93D" Ref="D2"  Part="1" 
AR Path="/14B3CE93D" Ref="D2"  Part="1" 
AR Path="/6FF0DD404B3CE93D" Ref="D2"  Part="1" 
AR Path="/23D9304B3CE93D" Ref="D2"  Part="1" 
AR Path="/23D8D44B3CE93D" Ref="D2"  Part="1" 
AR Path="/4031DDF34B3CE93D" Ref="D2"  Part="1" 
AR Path="/3FEFFFFF4B3CE93D" Ref="D2"  Part="1" 
AR Path="/3FE88B434B3CE93D" Ref="D2"  Part="1" 
AR Path="/4032778D4B3CE93D" Ref="D2"  Part="1" 
AR Path="/5AD7153D4B3CE93D" Ref="D2"  Part="1" 
AR Path="/A4B3CE93D" Ref="D2"  Part="1" 
AR Path="/403091264B3CE93D" Ref="D2"  Part="1" 
AR Path="/403051264B3CE93D" Ref="D2"  Part="1" 
AR Path="/4032F78D4B3CE93D" Ref="D2"  Part="1" 
AR Path="/403251264B3CE93D" Ref="D2"  Part="1" 
AR Path="/4032AAC04B3CE93D" Ref="D2"  Part="1" 
AR Path="/4030D1264B3CE93D" Ref="D2"  Part="1" 
AR Path="/4031778D4B3CE93D" Ref="D2"  Part="1" 
AR Path="/3FEA24DD4B3CE93D" Ref="D2"  Part="1" 
AR Path="/2600004B3CE93D" Ref="D2"  Part="1" 
AR Path="/6FE934E34B3CE93D" Ref="D2"  Part="1" 
AR Path="/773F65F14B3CE93D" Ref="D9"  Part="1" 
AR Path="/773F8EB44B3CE93D" Ref="D9"  Part="1" 
AR Path="/23C9F04B3CE93D" Ref="D9"  Part="1" 
AR Path="/24B3CE93D" Ref="D9"  Part="1" 
AR Path="/23BC884B3CE93D" Ref="D9"  Part="1" 
AR Path="/DC0C124B3CE93D" Ref="D2"  Part="1" 
AR Path="/23C34C4B3CE93D" Ref="D9"  Part="1" 
AR Path="/23CBC44B3CE93D" Ref="D9"  Part="1" 
AR Path="/69549BC04B3CE93D" Ref="D2"  Part="1" 
AR Path="/23C6504B3CE93D" Ref="D9"  Part="1" 
AR Path="/39803EA4B3CE93D" Ref="D2"  Part="1" 
AR Path="/FFFFFFFF4B3CE93D" Ref="D9"  Part="1" 
AR Path="/D058A04B3CE93D" Ref="D2"  Part="1" 
AR Path="/1607D44B3CE93D" Ref="D2"  Part="1" 
AR Path="/D1C3804B3CE93D" Ref="D2"  Part="1" 
AR Path="/23D70C4B3CE93D" Ref="D2"  Part="1" 
AR Path="/2104B3CE93D" Ref="D2"  Part="1" 
AR Path="/D1CA184B3CE93D" Ref="D2"  Part="1" 
AR Path="/F4BF4B3CE93D" Ref="D2"  Part="1" 
AR Path="/262F604B3CE93D" Ref="D2"  Part="1" 
AR Path="/22C6E204B3CE93D" Ref="D2"  Part="1" 
AR Path="/7E42B4154B3CE93D" Ref="D6"  Part="1" 
AR Path="/7E428DAC4B3CE93D" Ref="D9"  Part="1" 
AR Path="/131083A4B3CE93D" Ref="D"  Part="1" 
AR Path="/384B3CE93D" Ref="D9"  Part="1" 
AR Path="/D108084B3CE93D" Ref="D"  Part="1" 
F 0 "D9" H 8300 2000 50  0000 C CNN
F 1 "LED" H 8300 1800 50  0000 C CNN
F 2 "LEDV" H 8300 1900 60  0001 C CNN
	1    8300 1900
	-1   0    0    1   
$EndComp
$Comp
L R R?
U 1 1 4B3CE92D
P 3500 1850
AR Path="/23D9D84B3CE92D" Ref="R?"  Part="1" 
AR Path="/394433324B3CE92D" Ref="R?"  Part="1" 
AR Path="/74B3CE92D" Ref="R?"  Part="1" 
AR Path="/4B3CE92D" Ref="R5"  Part="1" 
AR Path="/A84B3CE92D" Ref="R5"  Part="1" 
AR Path="/94B3CE92D" Ref="R5"  Part="1" 
AR Path="/FFFFFFF04B3CE92D" Ref="R5"  Part="1" 
AR Path="/6FF0DD404B3CE92D" Ref="R5"  Part="1" 
AR Path="/23D9304B3CE92D" Ref="R5"  Part="1" 
AR Path="/6FE901F74B3CE92D" Ref="R5"  Part="1" 
AR Path="/23D8D44B3CE92D" Ref="R5"  Part="1" 
AR Path="/4031DDF34B3CE92D" Ref="R5"  Part="1" 
AR Path="/3FEFFFFF4B3CE92D" Ref="R5"  Part="1" 
AR Path="/3FE88B434B3CE92D" Ref="R5"  Part="1" 
AR Path="/4032778D4B3CE92D" Ref="R5"  Part="1" 
AR Path="/5AD7153D4B3CE92D" Ref="R5"  Part="1" 
AR Path="/A4B3CE92D" Ref="R5"  Part="1" 
AR Path="/403091264B3CE92D" Ref="R5"  Part="1" 
AR Path="/403051264B3CE92D" Ref="R5"  Part="1" 
AR Path="/4032F78D4B3CE92D" Ref="R5"  Part="1" 
AR Path="/403251264B3CE92D" Ref="R5"  Part="1" 
AR Path="/4032AAC04B3CE92D" Ref="R5"  Part="1" 
AR Path="/4030D1264B3CE92D" Ref="R5"  Part="1" 
AR Path="/4031778D4B3CE92D" Ref="R5"  Part="1" 
AR Path="/3FEA24DD4B3CE92D" Ref="R5"  Part="1" 
AR Path="/2600004B3CE92D" Ref="R5"  Part="1" 
AR Path="/6FE934E34B3CE92D" Ref="R5"  Part="1" 
AR Path="/773F65F14B3CE92D" Ref="R5"  Part="1" 
AR Path="/773F8EB44B3CE92D" Ref="R5"  Part="1" 
AR Path="/23C9F04B3CE92D" Ref="R5"  Part="1" 
AR Path="/24B3CE92D" Ref="R5"  Part="1" 
AR Path="/23BC884B3CE92D" Ref="R5"  Part="1" 
AR Path="/DC0C124B3CE92D" Ref="R5"  Part="1" 
AR Path="/23C34C4B3CE92D" Ref="R5"  Part="1" 
AR Path="/23CBC44B3CE92D" Ref="R5"  Part="1" 
AR Path="/69549BC04B3CE92D" Ref="R5"  Part="1" 
AR Path="/23C6504B3CE92D" Ref="R5"  Part="1" 
AR Path="/39803EA4B3CE92D" Ref="R5"  Part="1" 
AR Path="/FFFFFFFF4B3CE92D" Ref="R5"  Part="1" 
AR Path="/D058A04B3CE92D" Ref="R5"  Part="1" 
AR Path="/1607D44B3CE92D" Ref="R5"  Part="1" 
AR Path="/D1C3804B3CE92D" Ref="R5"  Part="1" 
AR Path="/23D70C4B3CE92D" Ref="R5"  Part="1" 
AR Path="/14B3CE92D" Ref="R5"  Part="1" 
AR Path="/2104B3CE92D" Ref="R5"  Part="1" 
AR Path="/D1CA184B3CE92D" Ref="R5"  Part="1" 
AR Path="/F4BF4B3CE92D" Ref="R5"  Part="1" 
AR Path="/262F604B3CE92D" Ref="R5"  Part="1" 
AR Path="/22C6E204B3CE92D" Ref="R5"  Part="1" 
AR Path="/384B3CE92D" Ref="R5"  Part="1" 
F 0 "R5" V 3580 1850 50  0000 C CNN
F 1 "470" V 3500 1850 50  0000 C CNN
F 2 "R3" H 3500 1850 60  0001 C CNN
	1    3500 1850
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 4B3CE91F
P 3500 1750
AR Path="/23D9D84B3CE91F" Ref="R?"  Part="1" 
AR Path="/394433324B3CE91F" Ref="R?"  Part="1" 
AR Path="/74B3CE91F" Ref="R?"  Part="1" 
AR Path="/4B3CE91F" Ref="R4"  Part="1" 
AR Path="/A84B3CE91F" Ref="R4"  Part="1" 
AR Path="/94B3CE91F" Ref="R4"  Part="1" 
AR Path="/FFFFFFF04B3CE91F" Ref="R4"  Part="1" 
AR Path="/6FF0DD404B3CE91F" Ref="R4"  Part="1" 
AR Path="/23D9304B3CE91F" Ref="R4"  Part="1" 
AR Path="/6FE901F74B3CE91F" Ref="R4"  Part="1" 
AR Path="/23D8D44B3CE91F" Ref="R4"  Part="1" 
AR Path="/4031DDF34B3CE91F" Ref="R4"  Part="1" 
AR Path="/3FEFFFFF4B3CE91F" Ref="R4"  Part="1" 
AR Path="/3FE88B434B3CE91F" Ref="R4"  Part="1" 
AR Path="/4032778D4B3CE91F" Ref="R4"  Part="1" 
AR Path="/5AD7153D4B3CE91F" Ref="R4"  Part="1" 
AR Path="/A4B3CE91F" Ref="R4"  Part="1" 
AR Path="/403091264B3CE91F" Ref="R4"  Part="1" 
AR Path="/403051264B3CE91F" Ref="R4"  Part="1" 
AR Path="/4032F78D4B3CE91F" Ref="R4"  Part="1" 
AR Path="/403251264B3CE91F" Ref="R4"  Part="1" 
AR Path="/4032AAC04B3CE91F" Ref="R4"  Part="1" 
AR Path="/4030D1264B3CE91F" Ref="R4"  Part="1" 
AR Path="/4031778D4B3CE91F" Ref="R4"  Part="1" 
AR Path="/3FEA24DD4B3CE91F" Ref="R4"  Part="1" 
AR Path="/2600004B3CE91F" Ref="R4"  Part="1" 
AR Path="/6FE934E34B3CE91F" Ref="R4"  Part="1" 
AR Path="/773F65F14B3CE91F" Ref="R4"  Part="1" 
AR Path="/773F8EB44B3CE91F" Ref="R4"  Part="1" 
AR Path="/23C9F04B3CE91F" Ref="R4"  Part="1" 
AR Path="/24B3CE91F" Ref="R4"  Part="1" 
AR Path="/23BC884B3CE91F" Ref="R4"  Part="1" 
AR Path="/DC0C124B3CE91F" Ref="R4"  Part="1" 
AR Path="/23C34C4B3CE91F" Ref="R4"  Part="1" 
AR Path="/23CBC44B3CE91F" Ref="R4"  Part="1" 
AR Path="/69549BC04B3CE91F" Ref="R4"  Part="1" 
AR Path="/23C6504B3CE91F" Ref="R4"  Part="1" 
AR Path="/39803EA4B3CE91F" Ref="R4"  Part="1" 
AR Path="/FFFFFFFF4B3CE91F" Ref="R4"  Part="1" 
AR Path="/D058A04B3CE91F" Ref="R4"  Part="1" 
AR Path="/1607D44B3CE91F" Ref="R4"  Part="1" 
AR Path="/D1C3804B3CE91F" Ref="R4"  Part="1" 
AR Path="/23D70C4B3CE91F" Ref="R4"  Part="1" 
AR Path="/14B3CE91F" Ref="R4"  Part="1" 
AR Path="/2104B3CE91F" Ref="R4"  Part="1" 
AR Path="/D1CA184B3CE91F" Ref="R4"  Part="1" 
AR Path="/F4BF4B3CE91F" Ref="R4"  Part="1" 
AR Path="/262F604B3CE91F" Ref="R4"  Part="1" 
AR Path="/22C6E204B3CE91F" Ref="R4"  Part="1" 
AR Path="/384B3CE91F" Ref="R4"  Part="1" 
F 0 "R4" V 3580 1750 50  0000 C CNN
F 1 "470" V 3500 1750 50  0000 C CNN
F 2 "R3" H 3500 1750 60  0001 C CNN
	1    3500 1750
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 4B3CE90E
P 3500 1650
AR Path="/23D9D84B3CE90E" Ref="R?"  Part="1" 
AR Path="/394433324B3CE90E" Ref="R?"  Part="1" 
AR Path="/74B3CE90E" Ref="R?"  Part="1" 
AR Path="/4B3CE90E" Ref="R3"  Part="1" 
AR Path="/A84B3CE90E" Ref="R3"  Part="1" 
AR Path="/94B3CE90E" Ref="R3"  Part="1" 
AR Path="/FFFFFFF04B3CE90E" Ref="R3"  Part="1" 
AR Path="/6FF0DD404B3CE90E" Ref="R3"  Part="1" 
AR Path="/23D9304B3CE90E" Ref="R3"  Part="1" 
AR Path="/6FE901F74B3CE90E" Ref="R3"  Part="1" 
AR Path="/23D8D44B3CE90E" Ref="R3"  Part="1" 
AR Path="/4031DDF34B3CE90E" Ref="R3"  Part="1" 
AR Path="/3FEFFFFF4B3CE90E" Ref="R3"  Part="1" 
AR Path="/3FE88B434B3CE90E" Ref="R3"  Part="1" 
AR Path="/4032778D4B3CE90E" Ref="R3"  Part="1" 
AR Path="/5AD7153D4B3CE90E" Ref="R3"  Part="1" 
AR Path="/A4B3CE90E" Ref="R3"  Part="1" 
AR Path="/403091264B3CE90E" Ref="R3"  Part="1" 
AR Path="/403051264B3CE90E" Ref="R3"  Part="1" 
AR Path="/4032F78D4B3CE90E" Ref="R3"  Part="1" 
AR Path="/403251264B3CE90E" Ref="R3"  Part="1" 
AR Path="/4032AAC04B3CE90E" Ref="R3"  Part="1" 
AR Path="/4030D1264B3CE90E" Ref="R3"  Part="1" 
AR Path="/4031778D4B3CE90E" Ref="R3"  Part="1" 
AR Path="/3FEA24DD4B3CE90E" Ref="R3"  Part="1" 
AR Path="/2600004B3CE90E" Ref="R3"  Part="1" 
AR Path="/6FE934E34B3CE90E" Ref="R3"  Part="1" 
AR Path="/773F65F14B3CE90E" Ref="R3"  Part="1" 
AR Path="/773F8EB44B3CE90E" Ref="R3"  Part="1" 
AR Path="/23C9F04B3CE90E" Ref="R3"  Part="1" 
AR Path="/24B3CE90E" Ref="R3"  Part="1" 
AR Path="/23BC884B3CE90E" Ref="R3"  Part="1" 
AR Path="/DC0C124B3CE90E" Ref="R3"  Part="1" 
AR Path="/23C34C4B3CE90E" Ref="R3"  Part="1" 
AR Path="/23CBC44B3CE90E" Ref="R3"  Part="1" 
AR Path="/69549BC04B3CE90E" Ref="R3"  Part="1" 
AR Path="/23C6504B3CE90E" Ref="R3"  Part="1" 
AR Path="/39803EA4B3CE90E" Ref="R3"  Part="1" 
AR Path="/FFFFFFFF4B3CE90E" Ref="R3"  Part="1" 
AR Path="/D058A04B3CE90E" Ref="R3"  Part="1" 
AR Path="/1607D44B3CE90E" Ref="R3"  Part="1" 
AR Path="/D1C3804B3CE90E" Ref="R3"  Part="1" 
AR Path="/23D70C4B3CE90E" Ref="R3"  Part="1" 
AR Path="/14B3CE90E" Ref="R3"  Part="1" 
AR Path="/2104B3CE90E" Ref="R3"  Part="1" 
AR Path="/D1CA184B3CE90E" Ref="R3"  Part="1" 
AR Path="/F4BF4B3CE90E" Ref="R3"  Part="1" 
AR Path="/262F604B3CE90E" Ref="R3"  Part="1" 
AR Path="/22C6E204B3CE90E" Ref="R3"  Part="1" 
AR Path="/384B3CE90E" Ref="R3"  Part="1" 
F 0 "R3" V 3580 1650 50  0000 C CNN
F 1 "470" V 3500 1650 50  0000 C CNN
F 2 "R3" H 3500 1650 60  0001 C CNN
	1    3500 1650
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 4B3CE901
P 3500 1550
AR Path="/69698AFC4B3CE901" Ref="R?"  Part="1" 
AR Path="/393639364B3CE901" Ref="R?"  Part="1" 
AR Path="/23D6B44B3CE901" Ref="R?"  Part="1" 
AR Path="/4B3CE901" Ref="R2"  Part="1" 
AR Path="/A84B3CE901" Ref="R2"  Part="1" 
AR Path="/94B3CE901" Ref="R2"  Part="1" 
AR Path="/FFFFFFF04B3CE901" Ref="R2"  Part="1" 
AR Path="/14B3CE901" Ref="R2"  Part="1" 
AR Path="/6FF0DD404B3CE901" Ref="R2"  Part="1" 
AR Path="/23D9304B3CE901" Ref="R2"  Part="1" 
AR Path="/23D8D44B3CE901" Ref="R2"  Part="1" 
AR Path="/4031DDF34B3CE901" Ref="R2"  Part="1" 
AR Path="/3FEFFFFF4B3CE901" Ref="R2"  Part="1" 
AR Path="/3FE88B434B3CE901" Ref="R2"  Part="1" 
AR Path="/4032778D4B3CE901" Ref="R2"  Part="1" 
AR Path="/5AD7153D4B3CE901" Ref="R2"  Part="1" 
AR Path="/A4B3CE901" Ref="R2"  Part="1" 
AR Path="/403091264B3CE901" Ref="R2"  Part="1" 
AR Path="/403051264B3CE901" Ref="R2"  Part="1" 
AR Path="/4032F78D4B3CE901" Ref="R2"  Part="1" 
AR Path="/403251264B3CE901" Ref="R2"  Part="1" 
AR Path="/4032AAC04B3CE901" Ref="R2"  Part="1" 
AR Path="/4030D1264B3CE901" Ref="R2"  Part="1" 
AR Path="/4031778D4B3CE901" Ref="R2"  Part="1" 
AR Path="/3FEA24DD4B3CE901" Ref="R2"  Part="1" 
AR Path="/2600004B3CE901" Ref="R2"  Part="1" 
AR Path="/6FE934E34B3CE901" Ref="R2"  Part="1" 
AR Path="/773F65F14B3CE901" Ref="R2"  Part="1" 
AR Path="/773F8EB44B3CE901" Ref="R2"  Part="1" 
AR Path="/23C9F04B3CE901" Ref="R2"  Part="1" 
AR Path="/24B3CE901" Ref="R2"  Part="1" 
AR Path="/23BC884B3CE901" Ref="R2"  Part="1" 
AR Path="/DC0C124B3CE901" Ref="R2"  Part="1" 
AR Path="/23C34C4B3CE901" Ref="R2"  Part="1" 
AR Path="/23CBC44B3CE901" Ref="R2"  Part="1" 
AR Path="/69549BC04B3CE901" Ref="R2"  Part="1" 
AR Path="/23C6504B3CE901" Ref="R2"  Part="1" 
AR Path="/39803EA4B3CE901" Ref="R2"  Part="1" 
AR Path="/FFFFFFFF4B3CE901" Ref="R2"  Part="1" 
AR Path="/D058A04B3CE901" Ref="R2"  Part="1" 
AR Path="/1607D44B3CE901" Ref="R2"  Part="1" 
AR Path="/D1C3804B3CE901" Ref="R2"  Part="1" 
AR Path="/23D70C4B3CE901" Ref="R2"  Part="1" 
AR Path="/2104B3CE901" Ref="R2"  Part="1" 
AR Path="/D1CA184B3CE901" Ref="R2"  Part="1" 
AR Path="/F4BF4B3CE901" Ref="R2"  Part="1" 
AR Path="/262F604B3CE901" Ref="R2"  Part="1" 
AR Path="/22C6E204B3CE901" Ref="R2"  Part="1" 
AR Path="/384B3CE901" Ref="R2"  Part="1" 
F 0 "R2" V 3580 1550 50  0000 C CNN
F 1 "470" V 3500 1550 50  0000 C CNN
F 2 "R3" H 3500 1550 60  0001 C CNN
	1    3500 1550
	0    1    1    0   
$EndComp
$Comp
L 74LS11 U?
U 2 1 4B3CE79D
P 20650 12900
AR Path="/23D9D84B3CE79D" Ref="U?"  Part="1" 
AR Path="/394433324B3CE79D" Ref="U?"  Part="1" 
AR Path="/74B3CE79D" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3CE79D" Ref="U?"  Part="1" 
AR Path="/1FD0A964B3CE79D" Ref="U"  Part="2" 
AR Path="/4B3CE79D" Ref="U13"  Part="2" 
AR Path="/A84B3CE79D" Ref="U13"  Part="2" 
AR Path="/94B3CE79D" Ref="U13"  Part="2" 
AR Path="/FFFFFFF04B3CE79D" Ref="U13"  Part="2" 
AR Path="/6FF0DD404B3CE79D" Ref="U13"  Part="2" 
AR Path="/23D9304B3CE79D" Ref="U13"  Part="2" 
AR Path="/6FE901F74B3CE79D" Ref="U13"  Part="2" 
AR Path="/23D8D44B3CE79D" Ref="U13"  Part="2" 
AR Path="/4031DDF34B3CE79D" Ref="U13"  Part="2" 
AR Path="/3FEFFFFF4B3CE79D" Ref="U13"  Part="2" 
AR Path="/3FE88B434B3CE79D" Ref="U13"  Part="2" 
AR Path="/4032778D4B3CE79D" Ref="U13"  Part="2" 
AR Path="/5AD7153D4B3CE79D" Ref="U13"  Part="2" 
AR Path="/A4B3CE79D" Ref="U13"  Part="2" 
AR Path="/403091264B3CE79D" Ref="U13"  Part="2" 
AR Path="/403051264B3CE79D" Ref="U13"  Part="2" 
AR Path="/4032F78D4B3CE79D" Ref="U13"  Part="2" 
AR Path="/403251264B3CE79D" Ref="U13"  Part="2" 
AR Path="/4032AAC04B3CE79D" Ref="U13"  Part="2" 
AR Path="/4030D1264B3CE79D" Ref="U13"  Part="2" 
AR Path="/4031778D4B3CE79D" Ref="U13"  Part="2" 
AR Path="/3FEA24DD4B3CE79D" Ref="U13"  Part="2" 
AR Path="/2600004B3CE79D" Ref="U13"  Part="2" 
AR Path="/6FE934E34B3CE79D" Ref="U13"  Part="2" 
AR Path="/773F8EB44B3CE79D" Ref="U13"  Part="2" 
AR Path="/23C9F04B3CE79D" Ref="U13"  Part="2" 
AR Path="/24B3CE79D" Ref="U13"  Part="2" 
AR Path="/23BC884B3CE79D" Ref="U13"  Part="2" 
AR Path="/DC0C124B3CE79D" Ref="U13"  Part="2" 
AR Path="/23C34C4B3CE79D" Ref="U13"  Part="2" 
AR Path="/23CBC44B3CE79D" Ref="U13"  Part="2" 
AR Path="/69549BC04B3CE79D" Ref="U13"  Part="2" 
AR Path="/23C6504B3CE79D" Ref="U13"  Part="2" 
AR Path="/39803EA4B3CE79D" Ref="U13"  Part="2" 
AR Path="/FFFFFFFF4B3CE79D" Ref="U13"  Part="2" 
AR Path="/D058A04B3CE79D" Ref="U13"  Part="2" 
AR Path="/1607D44B3CE79D" Ref="U13"  Part="2" 
AR Path="/D1C3804B3CE79D" Ref="U13"  Part="2" 
AR Path="/23D70C4B3CE79D" Ref="U13"  Part="2" 
AR Path="/14B3CE79D" Ref="U13"  Part="2" 
AR Path="/2104B3CE79D" Ref="U13"  Part="2" 
AR Path="/D1CA184B3CE79D" Ref="U13"  Part="2" 
AR Path="/F4BF4B3CE79D" Ref="U13"  Part="2" 
AR Path="/262F604B3CE79D" Ref="U13"  Part="2" 
AR Path="/22C6E204B3CE79D" Ref="U13"  Part="2" 
AR Path="/384B3CE79D" Ref="U13"  Part="2" 
F 0 "U13" H 20650 12950 60  0000 C CNN
F 1 "74LS11" H 20650 12850 60  0000 C CNN
F 2 "14dip300" H 20650 12900 60  0001 C CNN
	2    20650 12900
	1    0    0    -1  
$EndComp
$Comp
L 74LS11 U?
U 1 1 4B3CE53E
P 12950 10450
AR Path="/23D9D84B3CE53E" Ref="U?"  Part="1" 
AR Path="/394433324B3CE53E" Ref="U?"  Part="1" 
AR Path="/74B3CE53E" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3CE53E" Ref="U?"  Part="1" 
AR Path="/2F109944B3CE53E" Ref="U"  Part="1" 
AR Path="/4B3CE53E" Ref="U13"  Part="1" 
AR Path="/A84B3CE53E" Ref="U13"  Part="1" 
AR Path="/94B3CE53E" Ref="U13"  Part="1" 
AR Path="/FFFFFFF04B3CE53E" Ref="U13"  Part="1" 
AR Path="/6FF0DD404B3CE53E" Ref="U13"  Part="1" 
AR Path="/23D9304B3CE53E" Ref="U13"  Part="1" 
AR Path="/6FE901F74B3CE53E" Ref="U13"  Part="1" 
AR Path="/23D8D44B3CE53E" Ref="U13"  Part="1" 
AR Path="/4031DDF34B3CE53E" Ref="U13"  Part="1" 
AR Path="/3FEFFFFF4B3CE53E" Ref="U13"  Part="1" 
AR Path="/3FE88B434B3CE53E" Ref="U13"  Part="1" 
AR Path="/4032778D4B3CE53E" Ref="U13"  Part="1" 
AR Path="/5AD7153D4B3CE53E" Ref="U13"  Part="1" 
AR Path="/A4B3CE53E" Ref="U13"  Part="1" 
AR Path="/403091264B3CE53E" Ref="U13"  Part="1" 
AR Path="/403051264B3CE53E" Ref="U13"  Part="1" 
AR Path="/4032F78D4B3CE53E" Ref="U13"  Part="1" 
AR Path="/403251264B3CE53E" Ref="U13"  Part="1" 
AR Path="/4032AAC04B3CE53E" Ref="U13"  Part="1" 
AR Path="/4030D1264B3CE53E" Ref="U13"  Part="1" 
AR Path="/4031778D4B3CE53E" Ref="U13"  Part="1" 
AR Path="/3FEA24DD4B3CE53E" Ref="U13"  Part="1" 
AR Path="/2600004B3CE53E" Ref="U13"  Part="1" 
AR Path="/6FE934E34B3CE53E" Ref="U13"  Part="1" 
AR Path="/773F8EB44B3CE53E" Ref="U13"  Part="1" 
AR Path="/23C9F04B3CE53E" Ref="U13"  Part="1" 
AR Path="/23BC884B3CE53E" Ref="U13"  Part="1" 
AR Path="/DC0C124B3CE53E" Ref="U13"  Part="1" 
AR Path="/23C34C4B3CE53E" Ref="U13"  Part="1" 
AR Path="/23CBC44B3CE53E" Ref="U13"  Part="1" 
AR Path="/69549BC04B3CE53E" Ref="U13"  Part="1" 
AR Path="/23C6504B3CE53E" Ref="U13"  Part="1" 
AR Path="/39803EA4B3CE53E" Ref="U13"  Part="1" 
AR Path="/FFFFFFFF4B3CE53E" Ref="U13"  Part="1" 
AR Path="/D058A04B3CE53E" Ref="U13"  Part="1" 
AR Path="/1607D44B3CE53E" Ref="U13"  Part="1" 
AR Path="/D1C3804B3CE53E" Ref="U13"  Part="1" 
AR Path="/24B3CE53E" Ref="U13"  Part="1" 
AR Path="/23D70C4B3CE53E" Ref="U13"  Part="1" 
AR Path="/14B3CE53E" Ref="U13"  Part="1" 
AR Path="/2104B3CE53E" Ref="U13"  Part="1" 
AR Path="/D1CA184B3CE53E" Ref="U13"  Part="1" 
AR Path="/F4BF4B3CE53E" Ref="U13"  Part="1" 
AR Path="/262F604B3CE53E" Ref="U13"  Part="1" 
AR Path="/22C6E204B3CE53E" Ref="U13"  Part="1" 
AR Path="/384B3CE53E" Ref="U13"  Part="1" 
F 0 "U13" H 12950 10500 60  0000 C CNN
F 1 "74LS11" H 12950 10400 60  0000 C CNN
F 2 "14dip300" H 12950 10450 60  0001 C CNN
	1    12950 10450
	1    0    0    -1  
$EndComp
$Comp
L 74LS11 U?
U 3 1 4B3CE4B2
P 12950 9650
AR Path="/23D9B04B3CE4B2" Ref="U?"  Part="1" 
AR Path="/394433324B3CE4B2" Ref="U?"  Part="1" 
AR Path="/23D6544B3CE4B2" Ref="U12"  Part="1" 
AR Path="/7E44048F4B3CE4B2" Ref="U12"  Part="1" 
AR Path="/25D09E64B3CE4B2" Ref="U"  Part="3" 
AR Path="/23309DC4B3CE4B2" Ref="U"  Part="3" 
AR Path="/4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/A84B3CE4B2" Ref="U12"  Part="3" 
AR Path="/94B3CE4B2" Ref="U12"  Part="3" 
AR Path="/FFFFFFF04B3CE4B2" Ref="U12"  Part="3" 
AR Path="/6FF0DD404B3CE4B2" Ref="U12"  Part="3" 
AR Path="/14B3CE4B2" Ref="U12"  Part="3" 
AR Path="/23D9304B3CE4B2" Ref="U12"  Part="3" 
AR Path="/23D8D44B3CE4B2" Ref="U12"  Part="3" 
AR Path="/4031DDF34B3CE4B2" Ref="U12"  Part="3" 
AR Path="/3FEFFFFF4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/3FE88B434B3CE4B2" Ref="U12"  Part="3" 
AR Path="/4032778D4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/5AD7153D4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/A4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/403091264B3CE4B2" Ref="U12"  Part="3" 
AR Path="/403051264B3CE4B2" Ref="U12"  Part="3" 
AR Path="/4032F78D4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/403251264B3CE4B2" Ref="U12"  Part="3" 
AR Path="/4032AAC04B3CE4B2" Ref="U12"  Part="3" 
AR Path="/4030D1264B3CE4B2" Ref="U12"  Part="3" 
AR Path="/4031778D4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/3FEA24DD4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/2600004B3CE4B2" Ref="U12"  Part="3" 
AR Path="/6FE934E34B3CE4B2" Ref="U12"  Part="3" 
AR Path="/773F8EB44B3CE4B2" Ref="U12"  Part="3" 
AR Path="/23C9F04B3CE4B2" Ref="U12"  Part="3" 
AR Path="/24B3CE4B2" Ref="U12"  Part="3" 
AR Path="/23BC884B3CE4B2" Ref="U12"  Part="3" 
AR Path="/DC0C124B3CE4B2" Ref="U12"  Part="3" 
AR Path="/23C34C4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/23CBC44B3CE4B2" Ref="U12"  Part="3" 
AR Path="/69549BC04B3CE4B2" Ref="U12"  Part="3" 
AR Path="/23C6504B3CE4B2" Ref="U12"  Part="3" 
AR Path="/39803EA4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/FFFFFFFF4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/D058A04B3CE4B2" Ref="U12"  Part="3" 
AR Path="/1607D44B3CE4B2" Ref="U12"  Part="3" 
AR Path="/D1C3804B3CE4B2" Ref="U12"  Part="3" 
AR Path="/23D70C4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/2104B3CE4B2" Ref="U12"  Part="3" 
AR Path="/D1CA184B3CE4B2" Ref="U12"  Part="3" 
AR Path="/F4BF4B3CE4B2" Ref="U12"  Part="3" 
AR Path="/262F604B3CE4B2" Ref="U12"  Part="3" 
AR Path="/22C6E204B3CE4B2" Ref="U12"  Part="3" 
AR Path="/384B3CE4B2" Ref="U12"  Part="3" 
F 0 "U12" H 12950 9700 60  0000 C CNN
F 1 "74LS11" H 12950 9600 60  0000 C CNN
F 2 "14dip300" H 12950 9650 60  0001 C CNN
	3    12950 9650
	1    0    0    -1  
$EndComp
$Comp
L 74LS11 U?
U 2 1 4B3CE4AA
P 12900 8900
AR Path="/23D9D84B3CE4AA" Ref="U?"  Part="1" 
AR Path="/394433324B3CE4AA" Ref="U?"  Part="1" 
AR Path="/74B3CE4AA" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3CE4AA" Ref="U?"  Part="1" 
AR Path="/38209BA4B3CE4AA" Ref="U"  Part="2" 
AR Path="/4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/A84B3CE4AA" Ref="U12"  Part="2" 
AR Path="/94B3CE4AA" Ref="U12"  Part="2" 
AR Path="/FFFFFFF04B3CE4AA" Ref="U12"  Part="2" 
AR Path="/6FF0DD404B3CE4AA" Ref="U12"  Part="2" 
AR Path="/23D9304B3CE4AA" Ref="U12"  Part="2" 
AR Path="/6FE901F74B3CE4AA" Ref="U12"  Part="2" 
AR Path="/23D8D44B3CE4AA" Ref="U12"  Part="2" 
AR Path="/4031DDF34B3CE4AA" Ref="U12"  Part="2" 
AR Path="/3FEFFFFF4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/3FE88B434B3CE4AA" Ref="U12"  Part="2" 
AR Path="/4032778D4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/5AD7153D4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/A4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/403091264B3CE4AA" Ref="U12"  Part="2" 
AR Path="/403051264B3CE4AA" Ref="U12"  Part="2" 
AR Path="/4032F78D4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/403251264B3CE4AA" Ref="U12"  Part="2" 
AR Path="/4032AAC04B3CE4AA" Ref="U12"  Part="2" 
AR Path="/4030D1264B3CE4AA" Ref="U12"  Part="2" 
AR Path="/4031778D4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/3FEA24DD4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/2600004B3CE4AA" Ref="U12"  Part="2" 
AR Path="/6FE934E34B3CE4AA" Ref="U12"  Part="2" 
AR Path="/773F8EB44B3CE4AA" Ref="U12"  Part="2" 
AR Path="/23C9F04B3CE4AA" Ref="U12"  Part="2" 
AR Path="/23BC884B3CE4AA" Ref="U12"  Part="2" 
AR Path="/DC0C124B3CE4AA" Ref="U12"  Part="2" 
AR Path="/23C34C4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/23CBC44B3CE4AA" Ref="U12"  Part="2" 
AR Path="/69549BC04B3CE4AA" Ref="U12"  Part="2" 
AR Path="/23C6504B3CE4AA" Ref="U12"  Part="2" 
AR Path="/39803EA4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/FFFFFFFF4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/D058A04B3CE4AA" Ref="U12"  Part="2" 
AR Path="/1607D44B3CE4AA" Ref="U12"  Part="2" 
AR Path="/D1C3804B3CE4AA" Ref="U12"  Part="2" 
AR Path="/24B3CE4AA" Ref="U12"  Part="2" 
AR Path="/23D70C4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/14B3CE4AA" Ref="U12"  Part="2" 
AR Path="/2104B3CE4AA" Ref="U12"  Part="2" 
AR Path="/D1CA184B3CE4AA" Ref="U12"  Part="2" 
AR Path="/F4BF4B3CE4AA" Ref="U12"  Part="2" 
AR Path="/262F604B3CE4AA" Ref="U12"  Part="2" 
AR Path="/22C6E204B3CE4AA" Ref="U12"  Part="2" 
AR Path="/384B3CE4AA" Ref="U12"  Part="2" 
F 0 "U12" H 12900 8950 60  0000 C CNN
F 1 "74LS11" H 12900 8850 60  0000 C CNN
F 2 "14dip300" H 12900 8900 60  0001 C CNN
	2    12900 8900
	1    0    0    -1  
$EndComp
$Comp
L 74LS11 U?
U 1 1 4B3CE494
P 12900 7900
AR Path="/69698AFC4B3CE494" Ref="U?"  Part="1" 
AR Path="/393639364B3CE494" Ref="U?"  Part="1" 
AR Path="/23D6B44B3CE494" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3CE494" Ref="U?"  Part="1" 
AR Path="/33B09984B3CE494" Ref="U"  Part="1" 
AR Path="/4B3CE494" Ref="U12"  Part="1" 
AR Path="/A84B3CE494" Ref="U12"  Part="1" 
AR Path="/94B3CE494" Ref="U12"  Part="1" 
AR Path="/FFFFFFF04B3CE494" Ref="U12"  Part="1" 
AR Path="/14B3CE494" Ref="U12"  Part="1" 
AR Path="/6FF0DD404B3CE494" Ref="U12"  Part="1" 
AR Path="/23D9304B3CE494" Ref="U12"  Part="1" 
AR Path="/23D8D44B3CE494" Ref="U12"  Part="1" 
AR Path="/4031DDF34B3CE494" Ref="U12"  Part="1" 
AR Path="/3FEFFFFF4B3CE494" Ref="U12"  Part="1" 
AR Path="/3FE88B434B3CE494" Ref="U12"  Part="1" 
AR Path="/4032778D4B3CE494" Ref="U12"  Part="1" 
AR Path="/5AD7153D4B3CE494" Ref="U12"  Part="1" 
AR Path="/A4B3CE494" Ref="U12"  Part="1" 
AR Path="/403091264B3CE494" Ref="U12"  Part="1" 
AR Path="/403051264B3CE494" Ref="U12"  Part="1" 
AR Path="/4032F78D4B3CE494" Ref="U12"  Part="1" 
AR Path="/403251264B3CE494" Ref="U12"  Part="1" 
AR Path="/4032AAC04B3CE494" Ref="U12"  Part="1" 
AR Path="/4030D1264B3CE494" Ref="U12"  Part="1" 
AR Path="/4031778D4B3CE494" Ref="U12"  Part="1" 
AR Path="/3FEA24DD4B3CE494" Ref="U12"  Part="1" 
AR Path="/2600004B3CE494" Ref="U12"  Part="1" 
AR Path="/6FE934E34B3CE494" Ref="U12"  Part="1" 
AR Path="/773F8EB44B3CE494" Ref="U12"  Part="1" 
AR Path="/23C9F04B3CE494" Ref="U12"  Part="1" 
AR Path="/23BC884B3CE494" Ref="U12"  Part="1" 
AR Path="/DC0C124B3CE494" Ref="U12"  Part="1" 
AR Path="/23C34C4B3CE494" Ref="U12"  Part="1" 
AR Path="/23CBC44B3CE494" Ref="U12"  Part="1" 
AR Path="/69549BC04B3CE494" Ref="U12"  Part="1" 
AR Path="/23C6504B3CE494" Ref="U12"  Part="1" 
AR Path="/39803EA4B3CE494" Ref="U12"  Part="1" 
AR Path="/FFFFFFFF4B3CE494" Ref="U12"  Part="1" 
AR Path="/D058A04B3CE494" Ref="U12"  Part="1" 
AR Path="/1607D44B3CE494" Ref="U12"  Part="1" 
AR Path="/D1C3804B3CE494" Ref="U12"  Part="1" 
AR Path="/24B3CE494" Ref="U12"  Part="1" 
AR Path="/23D70C4B3CE494" Ref="U12"  Part="1" 
AR Path="/2104B3CE494" Ref="U12"  Part="1" 
AR Path="/D1CA184B3CE494" Ref="U12"  Part="1" 
AR Path="/F4BF4B3CE494" Ref="U12"  Part="1" 
AR Path="/262F604B3CE494" Ref="U12"  Part="1" 
AR Path="/22C6E204B3CE494" Ref="U12"  Part="1" 
AR Path="/384B3CE494" Ref="U12"  Part="1" 
F 0 "U12" H 12900 7950 60  0000 C CNN
F 1 "74LS11" H 12900 7850 60  0000 C CNN
F 2 "14dip300" H 12900 7900 60  0001 C CNN
	1    12900 7900
	1    0    0    -1  
$EndComp
Text Label 8400 9600 0    60   ~ 0
4MG_RAM_SELECT
$Comp
L 74LS27 U?
U 3 1 4B3CE2C7
P 17050 10100
AR Path="/23D9D84B3CE2C7" Ref="U?"  Part="1" 
AR Path="/394433324B3CE2C7" Ref="U?"  Part="1" 
AR Path="/6FF405304B3CE2C7" Ref="U?"  Part="1" 
AR Path="/23D6544B3CE2C7" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3CE2C7" Ref="U?"  Part="1" 
AR Path="/2A809C44B3CE2C7" Ref="U"  Part="3" 
AR Path="/4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/A84B3CE2C7" Ref="U30"  Part="3" 
AR Path="/94B3CE2C7" Ref="U30"  Part="3" 
AR Path="/FFFFFFF04B3CE2C7" Ref="U30"  Part="3" 
AR Path="/6FF0DD404B3CE2C7" Ref="U30"  Part="3" 
AR Path="/14B3CE2C7" Ref="U30"  Part="3" 
AR Path="/23D9304B3CE2C7" Ref="U30"  Part="3" 
AR Path="/23D8D44B3CE2C7" Ref="U30"  Part="3" 
AR Path="/4031DDF34B3CE2C7" Ref="U30"  Part="3" 
AR Path="/3FEFFFFF4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/3FE88B434B3CE2C7" Ref="U30"  Part="3" 
AR Path="/4032778D4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/5AD7153D4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/A4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/403091264B3CE2C7" Ref="U30"  Part="3" 
AR Path="/403051264B3CE2C7" Ref="U30"  Part="3" 
AR Path="/4032F78D4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/403251264B3CE2C7" Ref="U30"  Part="3" 
AR Path="/4032AAC04B3CE2C7" Ref="U30"  Part="3" 
AR Path="/4030D1264B3CE2C7" Ref="U30"  Part="3" 
AR Path="/4031778D4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/3FEA24DD4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/2600004B3CE2C7" Ref="U30"  Part="3" 
AR Path="/6FE934E34B3CE2C7" Ref="U30"  Part="3" 
AR Path="/773F8EB44B3CE2C7" Ref="U30"  Part="3" 
AR Path="/23C9F04B3CE2C7" Ref="U30"  Part="3" 
AR Path="/24B3CE2C7" Ref="U30"  Part="3" 
AR Path="/23BC884B3CE2C7" Ref="U30"  Part="3" 
AR Path="/DC0C124B3CE2C7" Ref="U30"  Part="3" 
AR Path="/23C34C4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/23CBC44B3CE2C7" Ref="U30"  Part="3" 
AR Path="/69549BC04B3CE2C7" Ref="U30"  Part="3" 
AR Path="/23C6504B3CE2C7" Ref="U30"  Part="3" 
AR Path="/39803EA4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/FFFFFFFF4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/D058A04B3CE2C7" Ref="U30"  Part="3" 
AR Path="/1607D44B3CE2C7" Ref="U30"  Part="3" 
AR Path="/D1C3804B3CE2C7" Ref="U30"  Part="3" 
AR Path="/23D70C4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/2104B3CE2C7" Ref="U30"  Part="3" 
AR Path="/D1CA184B3CE2C7" Ref="U30"  Part="3" 
AR Path="/F4BF4B3CE2C7" Ref="U30"  Part="3" 
AR Path="/262F604B3CE2C7" Ref="U30"  Part="3" 
AR Path="/22C6E204B3CE2C7" Ref="U30"  Part="3" 
AR Path="/384B3CE2C7" Ref="U30"  Part="3" 
F 0 "U30" H 17050 10150 60  0000 C CNN
F 1 "74LS27" H 17050 10050 60  0000 C CNN
F 2 "14dip300" H 17050 10100 60  0001 C CNN
	3    17050 10100
	1    0    0    -1  
$EndComp
$Comp
L 74LS27 U?
U 2 1 4B3CE2C4
P 15800 9250
AR Path="/23D9D84B3CE2C4" Ref="U?"  Part="1" 
AR Path="/394433324B3CE2C4" Ref="U?"  Part="1" 
AR Path="/23D6544B3CE2C4" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3CE2C4" Ref="U?"  Part="1" 
AR Path="/2C40A3E4B3CE2C4" Ref="U"  Part="2" 
AR Path="/4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/A84B3CE2C4" Ref="U30"  Part="2" 
AR Path="/94B3CE2C4" Ref="U30"  Part="2" 
AR Path="/FFFFFFF04B3CE2C4" Ref="U30"  Part="2" 
AR Path="/6FF0DD404B3CE2C4" Ref="U30"  Part="2" 
AR Path="/14B3CE2C4" Ref="U30"  Part="2" 
AR Path="/23D9304B3CE2C4" Ref="U30"  Part="2" 
AR Path="/23D8D44B3CE2C4" Ref="U30"  Part="2" 
AR Path="/4031DDF34B3CE2C4" Ref="U30"  Part="2" 
AR Path="/3FEFFFFF4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/3FE88B434B3CE2C4" Ref="U30"  Part="2" 
AR Path="/4032778D4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/5AD7153D4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/A4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/403091264B3CE2C4" Ref="U30"  Part="2" 
AR Path="/403051264B3CE2C4" Ref="U30"  Part="2" 
AR Path="/4032F78D4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/403251264B3CE2C4" Ref="U30"  Part="2" 
AR Path="/4032AAC04B3CE2C4" Ref="U30"  Part="2" 
AR Path="/4030D1264B3CE2C4" Ref="U30"  Part="2" 
AR Path="/4031778D4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/3FEA24DD4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/2600004B3CE2C4" Ref="U30"  Part="2" 
AR Path="/6FE934E34B3CE2C4" Ref="U30"  Part="2" 
AR Path="/773F8EB44B3CE2C4" Ref="U30"  Part="2" 
AR Path="/23C9F04B3CE2C4" Ref="U30"  Part="2" 
AR Path="/23BC884B3CE2C4" Ref="U30"  Part="2" 
AR Path="/DC0C124B3CE2C4" Ref="U30"  Part="2" 
AR Path="/23C34C4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/23CBC44B3CE2C4" Ref="U30"  Part="2" 
AR Path="/69549BC04B3CE2C4" Ref="U30"  Part="2" 
AR Path="/23C6504B3CE2C4" Ref="U30"  Part="2" 
AR Path="/39803EA4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/FFFFFFFF4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/D058A04B3CE2C4" Ref="U30"  Part="2" 
AR Path="/1607D44B3CE2C4" Ref="U30"  Part="2" 
AR Path="/D1C3804B3CE2C4" Ref="U30"  Part="2" 
AR Path="/24B3CE2C4" Ref="U30"  Part="2" 
AR Path="/23D70C4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/2104B3CE2C4" Ref="U30"  Part="2" 
AR Path="/D1CA184B3CE2C4" Ref="U30"  Part="2" 
AR Path="/F4BF4B3CE2C4" Ref="U30"  Part="2" 
AR Path="/262F604B3CE2C4" Ref="U30"  Part="2" 
AR Path="/22C6E204B3CE2C4" Ref="U30"  Part="2" 
AR Path="/384B3CE2C4" Ref="U30"  Part="2" 
F 0 "U30" H 15800 9300 60  0000 C CNN
F 1 "74LS27" H 15800 9200 60  0000 C CNN
F 2 "14dip300" H 15800 9250 60  0001 C CNN
	2    15800 9250
	1    0    0    -1  
$EndComp
$Comp
L 74LS27 U?
U 1 1 4B3CE2C1
P 15850 7900
AR Path="/69698AFC4B3CE2C1" Ref="U?"  Part="1" 
AR Path="/393639364B3CE2C1" Ref="U?"  Part="1" 
AR Path="/6FF405304B3CE2C1" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3CE2C1" Ref="U?"  Part="1" 
AR Path="/24D099E4B3CE2C1" Ref="U"  Part="1" 
AR Path="/4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/A84B3CE2C1" Ref="U30"  Part="1" 
AR Path="/94B3CE2C1" Ref="U30"  Part="1" 
AR Path="/FFFFFFF04B3CE2C1" Ref="U30"  Part="1" 
AR Path="/14B3CE2C1" Ref="U30"  Part="1" 
AR Path="/6FF0DD404B3CE2C1" Ref="U30"  Part="1" 
AR Path="/23D9304B3CE2C1" Ref="U30"  Part="1" 
AR Path="/23D8D44B3CE2C1" Ref="U30"  Part="1" 
AR Path="/4031DDF34B3CE2C1" Ref="U30"  Part="1" 
AR Path="/3FEFFFFF4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/3FE88B434B3CE2C1" Ref="U30"  Part="1" 
AR Path="/4032778D4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/5AD7153D4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/A4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/403091264B3CE2C1" Ref="U30"  Part="1" 
AR Path="/403051264B3CE2C1" Ref="U30"  Part="1" 
AR Path="/4032F78D4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/403251264B3CE2C1" Ref="U30"  Part="1" 
AR Path="/4032AAC04B3CE2C1" Ref="U30"  Part="1" 
AR Path="/4030D1264B3CE2C1" Ref="U30"  Part="1" 
AR Path="/4031778D4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/3FEA24DD4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/2600004B3CE2C1" Ref="U30"  Part="1" 
AR Path="/6FE934E34B3CE2C1" Ref="U30"  Part="1" 
AR Path="/773F8EB44B3CE2C1" Ref="U30"  Part="1" 
AR Path="/23C9F04B3CE2C1" Ref="U30"  Part="1" 
AR Path="/23BC884B3CE2C1" Ref="U30"  Part="1" 
AR Path="/DC0C124B3CE2C1" Ref="U30"  Part="1" 
AR Path="/23C34C4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/23CBC44B3CE2C1" Ref="U30"  Part="1" 
AR Path="/69549BC04B3CE2C1" Ref="U30"  Part="1" 
AR Path="/23C6504B3CE2C1" Ref="U30"  Part="1" 
AR Path="/39803EA4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/FFFFFFFF4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/D058A04B3CE2C1" Ref="U30"  Part="1" 
AR Path="/1607D44B3CE2C1" Ref="U30"  Part="1" 
AR Path="/D1C3804B3CE2C1" Ref="U30"  Part="1" 
AR Path="/24B3CE2C1" Ref="U30"  Part="1" 
AR Path="/23D70C4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/2104B3CE2C1" Ref="U30"  Part="1" 
AR Path="/D1CA184B3CE2C1" Ref="U30"  Part="1" 
AR Path="/F4BF4B3CE2C1" Ref="U30"  Part="1" 
AR Path="/262F604B3CE2C1" Ref="U30"  Part="1" 
AR Path="/22C6E204B3CE2C1" Ref="U30"  Part="1" 
AR Path="/384B3CE2C1" Ref="U30"  Part="1" 
F 0 "U30" H 15850 7950 60  0000 C CNN
F 1 "74LS27" H 15850 7850 60  0000 C CNN
F 2 "14dip300" H 15850 7900 60  0001 C CNN
	1    15850 7900
	1    0    0    -1  
$EndComp
$Comp
L 74LS240 U?
U 1 1 4B3CE168
P 9600 8550
AR Path="/23D9D84B3CE168" Ref="U?"  Part="1" 
AR Path="/394433324B3CE168" Ref="U?"  Part="1" 
AR Path="/77F184884B3CE168" Ref="U?"  Part="1" 
AR Path="/6FF405304B3CE168" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3CE168" Ref="U?"  Part="1" 
AR Path="/2A40A264B3CE168" Ref="U"  Part="1" 
AR Path="/4B3CE168" Ref="U9"  Part="1" 
AR Path="/A84B3CE168" Ref="U9"  Part="1" 
AR Path="/94B3CE168" Ref="U9"  Part="1" 
AR Path="/FFFFFFF04B3CE168" Ref="U9"  Part="1" 
AR Path="/14B3CE168" Ref="U9"  Part="1" 
AR Path="/6FF0DD404B3CE168" Ref="U9"  Part="1" 
AR Path="/23D9304B3CE168" Ref="U9"  Part="1" 
AR Path="/23D8D44B3CE168" Ref="U9"  Part="1" 
AR Path="/4031DDF34B3CE168" Ref="U9"  Part="1" 
AR Path="/3FEFFFFF4B3CE168" Ref="U9"  Part="1" 
AR Path="/3FE88B434B3CE168" Ref="U9"  Part="1" 
AR Path="/4032778D4B3CE168" Ref="U9"  Part="1" 
AR Path="/5AD7153D4B3CE168" Ref="U9"  Part="1" 
AR Path="/A4B3CE168" Ref="U9"  Part="1" 
AR Path="/403091264B3CE168" Ref="U9"  Part="1" 
AR Path="/403051264B3CE168" Ref="U9"  Part="1" 
AR Path="/4032F78D4B3CE168" Ref="U9"  Part="1" 
AR Path="/403251264B3CE168" Ref="U9"  Part="1" 
AR Path="/4032AAC04B3CE168" Ref="U9"  Part="1" 
AR Path="/4030D1264B3CE168" Ref="U9"  Part="1" 
AR Path="/4031778D4B3CE168" Ref="U9"  Part="1" 
AR Path="/3FEA24DD4B3CE168" Ref="U9"  Part="1" 
AR Path="/2600004B3CE168" Ref="U9"  Part="1" 
AR Path="/6FE934E34B3CE168" Ref="U9"  Part="1" 
AR Path="/773F8EB44B3CE168" Ref="U9"  Part="1" 
AR Path="/23C9F04B3CE168" Ref="U9"  Part="1" 
AR Path="/24B3CE168" Ref="U9"  Part="1" 
AR Path="/23BC884B3CE168" Ref="U9"  Part="1" 
AR Path="/DC0C124B3CE168" Ref="U9"  Part="1" 
AR Path="/23C34C4B3CE168" Ref="U9"  Part="1" 
AR Path="/23CBC44B3CE168" Ref="U9"  Part="1" 
AR Path="/69549BC04B3CE168" Ref="U9"  Part="1" 
AR Path="/23C6504B3CE168" Ref="U9"  Part="1" 
AR Path="/39803EA4B3CE168" Ref="U9"  Part="1" 
AR Path="/FFFFFFFF4B3CE168" Ref="U9"  Part="1" 
AR Path="/D058A04B3CE168" Ref="U9"  Part="1" 
AR Path="/1607D44B3CE168" Ref="U9"  Part="1" 
AR Path="/D1C3804B3CE168" Ref="U9"  Part="1" 
AR Path="/23D70C4B3CE168" Ref="U9"  Part="1" 
AR Path="/2104B3CE168" Ref="U9"  Part="1" 
AR Path="/D1CA184B3CE168" Ref="U9"  Part="1" 
AR Path="/F4BF4B3CE168" Ref="U9"  Part="1" 
AR Path="/262F604B3CE168" Ref="U9"  Part="1" 
AR Path="/22C6E204B3CE168" Ref="U9"  Part="1" 
AR Path="/384B3CE168" Ref="U9"  Part="1" 
F 0 "U9" H 9650 8350 60  0000 C CNN
F 1 "74LS240" H 9700 8150 60  0000 C CNN
F 2 "20dip300" H 9600 8550 60  0001 C CNN
	1    9600 8550
	1    0    0    -1  
$EndComp
Text Label 13050 1200 0    60   ~ 0
BOARD_SYNC
Text Label 12500 3900 0    60   ~ 0
PHI*
Text Label 1250 2150 0    60   ~ 0
GND
Text Label 1250 2050 0    60   ~ 0
GND
Text Label 1250 1850 0    60   ~ 0
16wr
Text Label 1250 1750 0    60   ~ 0
8wr
Text Label 1250 1650 0    60   ~ 0
16rd
Text Label 1250 1550 0    60   ~ 0
8rd
Text Label 5600 6150 0    60   ~ 0
4MG_RAM_SELECT*
Text Label 4650 2750 0    60   ~ 0
4MG_RAM_SELECT*
Text Label 2150 8300 0    60   ~ 0
sINTA
$Comp
L 74LS32 U?
U 2 1 4B3C0DFE
P 3050 8200
AR Path="/23D9D84B3C0DFE" Ref="U?"  Part="1" 
AR Path="/394433324B3C0DFE" Ref="U?"  Part="1" 
AR Path="/23D6544B3C0DFE" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3C0DFE" Ref="U?"  Part="1" 
AR Path="/4C0074C4B3C0DFE" Ref="U?"  Part="2" 
AR Path="/4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/A84B3C0DFE" Ref="U25"  Part="2" 
AR Path="/94B3C0DFE" Ref="U25"  Part="2" 
AR Path="/FFFFFFF04B3C0DFE" Ref="U25"  Part="2" 
AR Path="/6FF0DD404B3C0DFE" Ref="U25"  Part="2" 
AR Path="/14B3C0DFE" Ref="U25"  Part="2" 
AR Path="/23D9304B3C0DFE" Ref="U25"  Part="2" 
AR Path="/23D8D44B3C0DFE" Ref="U25"  Part="2" 
AR Path="/4031DDF34B3C0DFE" Ref="U25"  Part="2" 
AR Path="/3FEFFFFF4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/3FE88B434B3C0DFE" Ref="U25"  Part="2" 
AR Path="/4032778D4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/5AD7153D4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/A4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/403091264B3C0DFE" Ref="U25"  Part="2" 
AR Path="/403051264B3C0DFE" Ref="U25"  Part="2" 
AR Path="/4032F78D4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/403251264B3C0DFE" Ref="U25"  Part="2" 
AR Path="/4032AAC04B3C0DFE" Ref="U25"  Part="2" 
AR Path="/4030D1264B3C0DFE" Ref="U25"  Part="2" 
AR Path="/4031778D4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/3FEA24DD4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/2600004B3C0DFE" Ref="U25"  Part="2" 
AR Path="/6FE934E34B3C0DFE" Ref="U25"  Part="2" 
AR Path="/773F8EB44B3C0DFE" Ref="U25"  Part="2" 
AR Path="/23C9F04B3C0DFE" Ref="U25"  Part="2" 
AR Path="/23BC884B3C0DFE" Ref="U25"  Part="2" 
AR Path="/DC0C124B3C0DFE" Ref="U25"  Part="2" 
AR Path="/23C34C4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/23CBC44B3C0DFE" Ref="U25"  Part="2" 
AR Path="/69549BC04B3C0DFE" Ref="U25"  Part="2" 
AR Path="/23C6504B3C0DFE" Ref="U25"  Part="2" 
AR Path="/39803EA4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/FFFFFFFF4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/D058A04B3C0DFE" Ref="U25"  Part="2" 
AR Path="/1607D44B3C0DFE" Ref="U25"  Part="2" 
AR Path="/D1C3804B3C0DFE" Ref="U25"  Part="2" 
AR Path="/24B3C0DFE" Ref="U25"  Part="2" 
AR Path="/23D70C4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/2104B3C0DFE" Ref="U25"  Part="2" 
AR Path="/D1CA184B3C0DFE" Ref="U25"  Part="2" 
AR Path="/F4BF4B3C0DFE" Ref="U25"  Part="2" 
AR Path="/262F604B3C0DFE" Ref="U25"  Part="2" 
AR Path="/22C6E204B3C0DFE" Ref="U25"  Part="2" 
AR Path="/384B3C0DFE" Ref="U25"  Part="2" 
F 0 "U25" H 3050 8250 60  0000 C CNN
F 1 "74LS32" H 3050 8150 60  0000 C CNN
F 2 "14dip300" H 3050 8200 60  0001 C CNN
	2    3050 8200
	1    0    0    -1  
$EndComp
Text Label 650  8200 0    60   ~ 0
sINP
Text Label 650  8000 0    60   ~ 0
sOUT
$Comp
L 74LS32 U?
U 1 1 4B3C0DDB
P 1600 8100
AR Path="/23D9D84B3C0DDB" Ref="U?"  Part="1" 
AR Path="/394433324B3C0DDB" Ref="U?"  Part="1" 
AR Path="/6FF405304B3C0DDB" Ref="U?"  Part="1" 
AR Path="/7C9114604B3C0DDB" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3C0DDB" Ref="U?"  Part="1" 
AR Path="/2FB078C4B3C0DDB" Ref="U?"  Part="1" 
AR Path="/4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/A84B3C0DDB" Ref="U25"  Part="1" 
AR Path="/94B3C0DDB" Ref="U25"  Part="1" 
AR Path="/FFFFFFF04B3C0DDB" Ref="U25"  Part="1" 
AR Path="/14B3C0DDB" Ref="U25"  Part="1" 
AR Path="/6FF0DD404B3C0DDB" Ref="U25"  Part="1" 
AR Path="/23D9304B3C0DDB" Ref="U25"  Part="1" 
AR Path="/23D8D44B3C0DDB" Ref="U25"  Part="1" 
AR Path="/4031DDF34B3C0DDB" Ref="U25"  Part="1" 
AR Path="/3FEFFFFF4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/3FE88B434B3C0DDB" Ref="U25"  Part="1" 
AR Path="/4032778D4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/5AD7153D4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/A4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/403091264B3C0DDB" Ref="U25"  Part="1" 
AR Path="/403051264B3C0DDB" Ref="U25"  Part="1" 
AR Path="/4032F78D4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/403251264B3C0DDB" Ref="U25"  Part="1" 
AR Path="/4032AAC04B3C0DDB" Ref="U25"  Part="1" 
AR Path="/4030D1264B3C0DDB" Ref="U25"  Part="1" 
AR Path="/4031778D4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/3FEA24DD4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/2600004B3C0DDB" Ref="U25"  Part="1" 
AR Path="/6FE934E34B3C0DDB" Ref="U25"  Part="1" 
AR Path="/773F8EB44B3C0DDB" Ref="U25"  Part="1" 
AR Path="/23C9F04B3C0DDB" Ref="U25"  Part="1" 
AR Path="/23BC884B3C0DDB" Ref="U25"  Part="1" 
AR Path="/DC0C124B3C0DDB" Ref="U25"  Part="1" 
AR Path="/23C34C4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/23CBC44B3C0DDB" Ref="U25"  Part="1" 
AR Path="/69549BC04B3C0DDB" Ref="U25"  Part="1" 
AR Path="/23C6504B3C0DDB" Ref="U25"  Part="1" 
AR Path="/39803EA4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/FFFFFFFF4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/D058A04B3C0DDB" Ref="U25"  Part="1" 
AR Path="/1607D44B3C0DDB" Ref="U25"  Part="1" 
AR Path="/D1C3804B3C0DDB" Ref="U25"  Part="1" 
AR Path="/24B3C0DDB" Ref="U25"  Part="1" 
AR Path="/23D70C4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/2104B3C0DDB" Ref="U25"  Part="1" 
AR Path="/D1CA184B3C0DDB" Ref="U25"  Part="1" 
AR Path="/F4BF4B3C0DDB" Ref="U25"  Part="1" 
AR Path="/262F604B3C0DDB" Ref="U25"  Part="1" 
AR Path="/22C6E204B3C0DDB" Ref="U25"  Part="1" 
AR Path="/384B3C0DDB" Ref="U25"  Part="1" 
F 0 "U25" H 1600 8150 60  0000 C CNN
F 1 "74LS32" H 1600 8050 60  0000 C CNN
F 2 "14dip300" H 1600 8100 60  0001 C CNN
	1    1600 8100
	1    0    0    -1  
$EndComp
Text Label 7400 2800 0    60   ~ 0
RCS3*
Text Label 7400 2600 0    60   ~ 0
RCS2*
Text Label 7400 2400 0    60   ~ 0
RCS1*
Text Label 7400 2200 0    60   ~ 0
RCS0*
Text Label 15200 2300 0    60   ~ 0
RDY
Text Label 12600 3450 0    60   ~ 0
GND
$Comp
L 74LS165 U?
U 1 1 4B3C0828
P 13250 2600
AR Path="/23D9B04B3C0828" Ref="U?"  Part="1" 
AR Path="/394433324B3C0828" Ref="U?"  Part="1" 
AR Path="/23D6544B3C0828" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3C0828" Ref="U?"  Part="1" 
AR Path="/35505CE4B3C0828" Ref="U?"  Part="1" 
AR Path="/4B3C0828" Ref="U27"  Part="1" 
AR Path="/A84B3C0828" Ref="U27"  Part="1" 
AR Path="/94B3C0828" Ref="U27"  Part="1" 
AR Path="/FFFFFFF04B3C0828" Ref="U27"  Part="1" 
AR Path="/14B3C0828" Ref="U27"  Part="1" 
AR Path="/6FF0DD404B3C0828" Ref="U27"  Part="1" 
AR Path="/23D9304B3C0828" Ref="U27"  Part="1" 
AR Path="/23D8D44B3C0828" Ref="U27"  Part="1" 
AR Path="/4031DDF34B3C0828" Ref="U27"  Part="1" 
AR Path="/3FEFFFFF4B3C0828" Ref="U27"  Part="1" 
AR Path="/3FE88B434B3C0828" Ref="U27"  Part="1" 
AR Path="/4032778D4B3C0828" Ref="U27"  Part="1" 
AR Path="/5AD7153D4B3C0828" Ref="U27"  Part="1" 
AR Path="/A4B3C0828" Ref="U27"  Part="1" 
AR Path="/403091264B3C0828" Ref="U27"  Part="1" 
AR Path="/403051264B3C0828" Ref="U27"  Part="1" 
AR Path="/4032F78D4B3C0828" Ref="U27"  Part="1" 
AR Path="/403251264B3C0828" Ref="U27"  Part="1" 
AR Path="/4032AAC04B3C0828" Ref="U27"  Part="1" 
AR Path="/4030D1264B3C0828" Ref="U27"  Part="1" 
AR Path="/4031778D4B3C0828" Ref="U27"  Part="1" 
AR Path="/3FEA24DD4B3C0828" Ref="U27"  Part="1" 
AR Path="/2600004B3C0828" Ref="U27"  Part="1" 
AR Path="/6FE934E34B3C0828" Ref="U27"  Part="1" 
AR Path="/773F8EB44B3C0828" Ref="U27"  Part="1" 
AR Path="/23C9F04B3C0828" Ref="U27"  Part="1" 
AR Path="/24B3C0828" Ref="U27"  Part="1" 
AR Path="/23BC884B3C0828" Ref="U27"  Part="1" 
AR Path="/DC0C124B3C0828" Ref="U27"  Part="1" 
AR Path="/23C34C4B3C0828" Ref="U27"  Part="1" 
AR Path="/23CBC44B3C0828" Ref="U27"  Part="1" 
AR Path="/23C6504B3C0828" Ref="U27"  Part="1" 
AR Path="/39803EA4B3C0828" Ref="U27"  Part="1" 
AR Path="/FFFFFFFF4B3C0828" Ref="U27"  Part="1" 
AR Path="/D058A04B3C0828" Ref="U27"  Part="1" 
AR Path="/1607D44B3C0828" Ref="U27"  Part="1" 
AR Path="/D1C3804B3C0828" Ref="U27"  Part="1" 
AR Path="/23D70C4B3C0828" Ref="U27"  Part="1" 
AR Path="/2104B3C0828" Ref="U27"  Part="1" 
AR Path="/D1CA184B3C0828" Ref="U27"  Part="1" 
AR Path="/F4BF4B3C0828" Ref="U27"  Part="1" 
AR Path="/262F604B3C0828" Ref="U27"  Part="1" 
AR Path="/22C6E204B3C0828" Ref="U27"  Part="1" 
AR Path="/384B3C0828" Ref="U27"  Part="1" 
F 0 "U27" H 13400 2550 60  0000 C CNN
F 1 "74LS165" H 13400 2350 60  0000 C CNN
F 2 "16dip300" H 13250 2600 60  0001 C CNN
	1    13250 2600
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR?
U 1 1 4B3C079B
P 11900 3500
AR Path="/69698AFC4B3C079B" Ref="RR?"  Part="1" 
AR Path="/393639364B3C079B" Ref="RR?"  Part="1" 
AR Path="/44B3C079B" Ref="RR?"  Part="1" 
AR Path="/94B3C079B" Ref="RR1"  Part="1" 
AR Path="/4B3C079B" Ref="RR1"  Part="1" 
AR Path="/A84B3C079B" Ref="RR1"  Part="1" 
AR Path="/FFFFFFF04B3C079B" Ref="RR1"  Part="1" 
AR Path="/14B3C079B" Ref="RR1"  Part="1" 
AR Path="/6FF0DD404B3C079B" Ref="RR1"  Part="1" 
AR Path="/23D9304B3C079B" Ref="RR1"  Part="1" 
AR Path="/23D8D44B3C079B" Ref="RR1"  Part="1" 
AR Path="/4031DDF34B3C079B" Ref="RR1"  Part="1" 
AR Path="/3FEFFFFF4B3C079B" Ref="RR1"  Part="1" 
AR Path="/3FE88B434B3C079B" Ref="RR1"  Part="1" 
AR Path="/4032778D4B3C079B" Ref="RR1"  Part="1" 
AR Path="/5AD7153D4B3C079B" Ref="RR1"  Part="1" 
AR Path="/A4B3C079B" Ref="RR1"  Part="1" 
AR Path="/403091264B3C079B" Ref="RR1"  Part="1" 
AR Path="/403051264B3C079B" Ref="RR1"  Part="1" 
AR Path="/4032F78D4B3C079B" Ref="RR1"  Part="1" 
AR Path="/403251264B3C079B" Ref="RR1"  Part="1" 
AR Path="/4032AAC04B3C079B" Ref="RR1"  Part="1" 
AR Path="/4030D1264B3C079B" Ref="RR1"  Part="1" 
AR Path="/4031778D4B3C079B" Ref="RR1"  Part="1" 
AR Path="/3FEA24DD4B3C079B" Ref="RR1"  Part="1" 
AR Path="/2600004B3C079B" Ref="RR1"  Part="1" 
AR Path="/6FE934E34B3C079B" Ref="RR1"  Part="1" 
AR Path="/773F65F14B3C079B" Ref="RR1"  Part="1" 
AR Path="/773F8EB44B3C079B" Ref="RR1"  Part="1" 
AR Path="/23C9F04B3C079B" Ref="RR1"  Part="1" 
AR Path="/24B3C079B" Ref="RR1"  Part="1" 
AR Path="/23BC884B3C079B" Ref="RR1"  Part="1" 
AR Path="/DC0C124B3C079B" Ref="RR1"  Part="1" 
AR Path="/23C34C4B3C079B" Ref="RR1"  Part="1" 
AR Path="/23CBC44B3C079B" Ref="RR1"  Part="1" 
AR Path="/23C6504B3C079B" Ref="RR1"  Part="1" 
AR Path="/39803EA4B3C079B" Ref="RR1"  Part="1" 
AR Path="/FFFFFFFF4B3C079B" Ref="RR1"  Part="1" 
AR Path="/D058A04B3C079B" Ref="RR1"  Part="1" 
AR Path="/1607D44B3C079B" Ref="RR1"  Part="1" 
AR Path="/D1C3804B3C079B" Ref="RR1"  Part="1" 
AR Path="/23D70C4B3C079B" Ref="RR1"  Part="1" 
AR Path="/2104B3C079B" Ref="RR1"  Part="1" 
AR Path="/D1CA184B3C079B" Ref="RR1"  Part="1" 
AR Path="/F4BF4B3C079B" Ref="RR1"  Part="1" 
AR Path="/262F604B3C079B" Ref="RR1"  Part="1" 
AR Path="/22C6E204B3C079B" Ref="RR1"  Part="1" 
AR Path="/384B3C079B" Ref="RR1"  Part="1" 
F 0 "RR1" H 11950 4100 70  0000 C CNN
F 1 "1100" V 11930 3500 70  0000 C CNN
F 2 "r_pack9" H 11900 3500 60  0001 C CNN
	1    11900 3500
	0    -1   1    0   
$EndComp
Text Label 10150 1200 0    60   ~ 0
pSYNC
$Comp
L C C37
U 1 1 4B37DA0C
P 11850 14950
AR Path="/FFFFFFFF4B37DA0C" Ref="C37"  Part="1" 
AR Path="/4B37DA0C" Ref="C37"  Part="1" 
AR Path="/94B37DA0C" Ref="C37"  Part="1" 
AR Path="/DCBAABCD4B37DA0C" Ref="C37"  Part="1" 
AR Path="/A4B37DA0C" Ref="C37"  Part="1" 
AR Path="/6FE901F74B37DA0C" Ref="C37"  Part="1" 
AR Path="/402755814B37DA0C" Ref="C37"  Part="1" 
AR Path="/3FEFFFFF4B37DA0C" Ref="C37"  Part="1" 
AR Path="/4030AAC04B37DA0C" Ref="C37"  Part="1" 
AR Path="/FFFFFFF04B37DA0C" Ref="C37"  Part="1" 
AR Path="/5AD7153D4B37DA0C" Ref="C37"  Part="1" 
AR Path="/A84B37DA0C" Ref="C37"  Part="1" 
AR Path="/14B37DA0C" Ref="C37"  Part="1" 
AR Path="/6FF0DD404B37DA0C" Ref="C37"  Part="1" 
AR Path="/23D9304B37DA0C" Ref="C37"  Part="1" 
AR Path="/23D8D44B37DA0C" Ref="C37"  Part="1" 
AR Path="/4031DDF34B37DA0C" Ref="C37"  Part="1" 
AR Path="/3FE88B434B37DA0C" Ref="C37"  Part="1" 
AR Path="/4032778D4B37DA0C" Ref="C37"  Part="1" 
AR Path="/403091264B37DA0C" Ref="C37"  Part="1" 
AR Path="/403051264B37DA0C" Ref="C37"  Part="1" 
AR Path="/4032F78D4B37DA0C" Ref="C37"  Part="1" 
AR Path="/403251264B37DA0C" Ref="C37"  Part="1" 
AR Path="/4032AAC04B37DA0C" Ref="C37"  Part="1" 
AR Path="/4030D1264B37DA0C" Ref="C37"  Part="1" 
AR Path="/4031778D4B37DA0C" Ref="C37"  Part="1" 
AR Path="/3FEA24DD4B37DA0C" Ref="C37"  Part="1" 
AR Path="/2600004B37DA0C" Ref="C37"  Part="1" 
AR Path="/6FE934E34B37DA0C" Ref="C37"  Part="1" 
AR Path="/773F65F14B37DA0C" Ref="C37"  Part="1" 
AR Path="/773F8EB44B37DA0C" Ref="C37"  Part="1" 
AR Path="/23C9F04B37DA0C" Ref="C37"  Part="1" 
AR Path="/24B37DA0C" Ref="C37"  Part="1" 
AR Path="/23BC884B37DA0C" Ref="C37"  Part="1" 
AR Path="/DC0C124B37DA0C" Ref="C37"  Part="1" 
AR Path="/23C34C4B37DA0C" Ref="C37"  Part="1" 
AR Path="/23CBC44B37DA0C" Ref="C37"  Part="1" 
AR Path="/23C6504B37DA0C" Ref="C37"  Part="1" 
AR Path="/39803EA4B37DA0C" Ref="C37"  Part="1" 
AR Path="/D058A04B37DA0C" Ref="C37"  Part="1" 
AR Path="/1607D44B37DA0C" Ref="C37"  Part="1" 
AR Path="/D1C3804B37DA0C" Ref="C37"  Part="1" 
AR Path="/23D70C4B37DA0C" Ref="C37"  Part="1" 
AR Path="/2104B37DA0C" Ref="C37"  Part="1" 
AR Path="/D1CA184B37DA0C" Ref="C37"  Part="1" 
AR Path="/F4BF4B37DA0C" Ref="C37"  Part="1" 
AR Path="/262F604B37DA0C" Ref="C37"  Part="1" 
AR Path="/22C6E204B37DA0C" Ref="C37"  Part="1" 
AR Path="/384B37DA0C" Ref="C37"  Part="1" 
F 0 "C37" H 11900 15050 50  0000 L CNN
F 1 "0.1 uF" H 11900 14850 50  0000 L CNN
F 2 "C2" H 11850 14950 60  0001 C CNN
	1    11850 14950
	1    0    0    -1  
$EndComp
$Comp
L C C38
U 1 1 4B37DA08
P 12300 14950
AR Path="/FFFFFFFF4B37DA08" Ref="C38"  Part="1" 
AR Path="/4B37DA08" Ref="C38"  Part="1" 
AR Path="/94B37DA08" Ref="C38"  Part="1" 
AR Path="/A4B37DA08" Ref="C38"  Part="1" 
AR Path="/6FE901F74B37DA08" Ref="C38"  Part="1" 
AR Path="/402755814B37DA08" Ref="C38"  Part="1" 
AR Path="/3FEFFFFF4B37DA08" Ref="C38"  Part="1" 
AR Path="/4030AAC04B37DA08" Ref="C38"  Part="1" 
AR Path="/FFFFFFF04B37DA08" Ref="C38"  Part="1" 
AR Path="/5AD7153D4B37DA08" Ref="C38"  Part="1" 
AR Path="/A84B37DA08" Ref="C38"  Part="1" 
AR Path="/14B37DA08" Ref="C38"  Part="1" 
AR Path="/6FF0DD404B37DA08" Ref="C38"  Part="1" 
AR Path="/23D9304B37DA08" Ref="C38"  Part="1" 
AR Path="/23D8D44B37DA08" Ref="C38"  Part="1" 
AR Path="/4031DDF34B37DA08" Ref="C38"  Part="1" 
AR Path="/3FE88B434B37DA08" Ref="C38"  Part="1" 
AR Path="/4032778D4B37DA08" Ref="C38"  Part="1" 
AR Path="/403091264B37DA08" Ref="C38"  Part="1" 
AR Path="/403051264B37DA08" Ref="C38"  Part="1" 
AR Path="/4032F78D4B37DA08" Ref="C38"  Part="1" 
AR Path="/403251264B37DA08" Ref="C38"  Part="1" 
AR Path="/4032AAC04B37DA08" Ref="C38"  Part="1" 
AR Path="/4030D1264B37DA08" Ref="C38"  Part="1" 
AR Path="/4031778D4B37DA08" Ref="C38"  Part="1" 
AR Path="/3FEA24DD4B37DA08" Ref="C38"  Part="1" 
AR Path="/2600004B37DA08" Ref="C38"  Part="1" 
AR Path="/6FE934E34B37DA08" Ref="C38"  Part="1" 
AR Path="/773F65F14B37DA08" Ref="C38"  Part="1" 
AR Path="/773F8EB44B37DA08" Ref="C38"  Part="1" 
AR Path="/23C9F04B37DA08" Ref="C38"  Part="1" 
AR Path="/24B37DA08" Ref="C38"  Part="1" 
AR Path="/23BC884B37DA08" Ref="C38"  Part="1" 
AR Path="/DC0C124B37DA08" Ref="C38"  Part="1" 
AR Path="/23C34C4B37DA08" Ref="C38"  Part="1" 
AR Path="/23CBC44B37DA08" Ref="C38"  Part="1" 
AR Path="/23C6504B37DA08" Ref="C38"  Part="1" 
AR Path="/39803EA4B37DA08" Ref="C38"  Part="1" 
AR Path="/D058A04B37DA08" Ref="C38"  Part="1" 
AR Path="/1607D44B37DA08" Ref="C38"  Part="1" 
AR Path="/D1C3804B37DA08" Ref="C38"  Part="1" 
AR Path="/23D70C4B37DA08" Ref="C38"  Part="1" 
AR Path="/2104B37DA08" Ref="C38"  Part="1" 
AR Path="/D1CA184B37DA08" Ref="C38"  Part="1" 
AR Path="/F4BF4B37DA08" Ref="C38"  Part="1" 
AR Path="/262F604B37DA08" Ref="C38"  Part="1" 
AR Path="/22C6E204B37DA08" Ref="C38"  Part="1" 
AR Path="/384B37DA08" Ref="C38"  Part="1" 
F 0 "C38" H 12350 15050 50  0000 L CNN
F 1 "0.1 uF" H 12350 14850 50  0000 L CNN
F 2 "C2" H 12300 14950 60  0001 C CNN
	1    12300 14950
	1    0    0    -1  
$EndComp
$Comp
L C C39
U 1 1 4B37DA03
P 12750 14950
AR Path="/FFFFFFFF4B37DA03" Ref="C39"  Part="1" 
AR Path="/4B37DA03" Ref="C39"  Part="1" 
AR Path="/94B37DA03" Ref="C39"  Part="1" 
AR Path="/A4B37DA03" Ref="C39"  Part="1" 
AR Path="/6FE901F74B37DA03" Ref="C39"  Part="1" 
AR Path="/402755814B37DA03" Ref="C39"  Part="1" 
AR Path="/3FEFFFFF4B37DA03" Ref="C39"  Part="1" 
AR Path="/4030AAC04B37DA03" Ref="C39"  Part="1" 
AR Path="/FFFFFFF04B37DA03" Ref="C39"  Part="1" 
AR Path="/5AD7153D4B37DA03" Ref="C39"  Part="1" 
AR Path="/A84B37DA03" Ref="C39"  Part="1" 
AR Path="/14B37DA03" Ref="C39"  Part="1" 
AR Path="/6FF0DD404B37DA03" Ref="C39"  Part="1" 
AR Path="/23D9304B37DA03" Ref="C39"  Part="1" 
AR Path="/23D8D44B37DA03" Ref="C39"  Part="1" 
AR Path="/4031DDF34B37DA03" Ref="C39"  Part="1" 
AR Path="/3FE88B434B37DA03" Ref="C39"  Part="1" 
AR Path="/4032778D4B37DA03" Ref="C39"  Part="1" 
AR Path="/403091264B37DA03" Ref="C39"  Part="1" 
AR Path="/403051264B37DA03" Ref="C39"  Part="1" 
AR Path="/4032F78D4B37DA03" Ref="C39"  Part="1" 
AR Path="/403251264B37DA03" Ref="C39"  Part="1" 
AR Path="/4032AAC04B37DA03" Ref="C39"  Part="1" 
AR Path="/4030D1264B37DA03" Ref="C39"  Part="1" 
AR Path="/4031778D4B37DA03" Ref="C39"  Part="1" 
AR Path="/3FEA24DD4B37DA03" Ref="C39"  Part="1" 
AR Path="/2600004B37DA03" Ref="C39"  Part="1" 
AR Path="/6FE934E34B37DA03" Ref="C39"  Part="1" 
AR Path="/773F65F14B37DA03" Ref="C39"  Part="1" 
AR Path="/773F8EB44B37DA03" Ref="C39"  Part="1" 
AR Path="/23C9F04B37DA03" Ref="C39"  Part="1" 
AR Path="/24B37DA03" Ref="C39"  Part="1" 
AR Path="/23BC884B37DA03" Ref="C39"  Part="1" 
AR Path="/DC0C124B37DA03" Ref="C39"  Part="1" 
AR Path="/23C34C4B37DA03" Ref="C39"  Part="1" 
AR Path="/23CBC44B37DA03" Ref="C39"  Part="1" 
AR Path="/23C6504B37DA03" Ref="C39"  Part="1" 
AR Path="/39803EA4B37DA03" Ref="C39"  Part="1" 
AR Path="/D058A04B37DA03" Ref="C39"  Part="1" 
AR Path="/1607D44B37DA03" Ref="C39"  Part="1" 
AR Path="/D1C3804B37DA03" Ref="C39"  Part="1" 
AR Path="/23D70C4B37DA03" Ref="C39"  Part="1" 
AR Path="/2104B37DA03" Ref="C39"  Part="1" 
AR Path="/D1CA184B37DA03" Ref="C39"  Part="1" 
AR Path="/F4BF4B37DA03" Ref="C39"  Part="1" 
AR Path="/262F604B37DA03" Ref="C39"  Part="1" 
AR Path="/22C6E204B37DA03" Ref="C39"  Part="1" 
AR Path="/384B37DA03" Ref="C39"  Part="1" 
F 0 "C39" H 12800 15050 50  0000 L CNN
F 1 "0.1 uF" H 12800 14850 50  0000 L CNN
F 2 "C2" H 12750 14950 60  0001 C CNN
	1    12750 14950
	1    0    0    -1  
$EndComp
$Comp
L C C40
U 1 1 4B37DA01
P 13200 14950
AR Path="/FFFFFFFF4B37DA01" Ref="C40"  Part="1" 
AR Path="/4B37DA01" Ref="C40"  Part="1" 
AR Path="/94B37DA01" Ref="C40"  Part="1" 
AR Path="/A4B37DA01" Ref="C40"  Part="1" 
AR Path="/6FE901F74B37DA01" Ref="C40"  Part="1" 
AR Path="/402755814B37DA01" Ref="C40"  Part="1" 
AR Path="/3FEFFFFF4B37DA01" Ref="C40"  Part="1" 
AR Path="/4030AAC04B37DA01" Ref="C40"  Part="1" 
AR Path="/FFFFFFF04B37DA01" Ref="C40"  Part="1" 
AR Path="/5AD7153D4B37DA01" Ref="C40"  Part="1" 
AR Path="/A84B37DA01" Ref="C40"  Part="1" 
AR Path="/14B37DA01" Ref="C40"  Part="1" 
AR Path="/6FF0DD404B37DA01" Ref="C40"  Part="1" 
AR Path="/23D9304B37DA01" Ref="C40"  Part="1" 
AR Path="/23D8D44B37DA01" Ref="C40"  Part="1" 
AR Path="/4031DDF34B37DA01" Ref="C40"  Part="1" 
AR Path="/3FE88B434B37DA01" Ref="C40"  Part="1" 
AR Path="/4032778D4B37DA01" Ref="C40"  Part="1" 
AR Path="/403091264B37DA01" Ref="C40"  Part="1" 
AR Path="/403051264B37DA01" Ref="C40"  Part="1" 
AR Path="/4032F78D4B37DA01" Ref="C40"  Part="1" 
AR Path="/403251264B37DA01" Ref="C40"  Part="1" 
AR Path="/4032AAC04B37DA01" Ref="C40"  Part="1" 
AR Path="/4030D1264B37DA01" Ref="C40"  Part="1" 
AR Path="/4031778D4B37DA01" Ref="C40"  Part="1" 
AR Path="/3FEA24DD4B37DA01" Ref="C40"  Part="1" 
AR Path="/2600004B37DA01" Ref="C40"  Part="1" 
AR Path="/6FE934E34B37DA01" Ref="C40"  Part="1" 
AR Path="/773F65F14B37DA01" Ref="C40"  Part="1" 
AR Path="/773F8EB44B37DA01" Ref="C40"  Part="1" 
AR Path="/23C9F04B37DA01" Ref="C40"  Part="1" 
AR Path="/24B37DA01" Ref="C40"  Part="1" 
AR Path="/23BC884B37DA01" Ref="C40"  Part="1" 
AR Path="/DC0C124B37DA01" Ref="C40"  Part="1" 
AR Path="/23C34C4B37DA01" Ref="C40"  Part="1" 
AR Path="/23CBC44B37DA01" Ref="C40"  Part="1" 
AR Path="/23C6504B37DA01" Ref="C40"  Part="1" 
AR Path="/39803EA4B37DA01" Ref="C40"  Part="1" 
AR Path="/D058A04B37DA01" Ref="C40"  Part="1" 
AR Path="/1607D44B37DA01" Ref="C40"  Part="1" 
AR Path="/D1C3804B37DA01" Ref="C40"  Part="1" 
AR Path="/23D70C4B37DA01" Ref="C40"  Part="1" 
AR Path="/2104B37DA01" Ref="C40"  Part="1" 
AR Path="/D1CA184B37DA01" Ref="C40"  Part="1" 
AR Path="/F4BF4B37DA01" Ref="C40"  Part="1" 
AR Path="/262F604B37DA01" Ref="C40"  Part="1" 
AR Path="/22C6E204B37DA01" Ref="C40"  Part="1" 
AR Path="/384B37DA01" Ref="C40"  Part="1" 
F 0 "C40" H 13250 15050 50  0000 L CNN
F 1 "0.1 uF" H 13250 14850 50  0000 L CNN
F 2 "C2" H 13200 14950 60  0001 C CNN
	1    13200 14950
	1    0    0    -1  
$EndComp
$Comp
L SRAM_512KO U104
U 1 1 4B37D7DC
P 6750 11950
AR Path="/4B37D7DC" Ref="U104"  Part="1" 
AR Path="/94B37D7DC" Ref="U104"  Part="1" 
AR Path="/14B37D7DC" Ref="U104"  Part="1" 
AR Path="/6FF0DD404B37D7DC" Ref="U104"  Part="1" 
AR Path="/FFFFFFF04B37D7DC" Ref="U104"  Part="1" 
AR Path="/23D9304B37D7DC" Ref="U104"  Part="1" 
AR Path="/A4B37D7DC" Ref="U104"  Part="1" 
AR Path="/402755814B37D7DC" Ref="U104"  Part="1" 
AR Path="/6FE901F74B37D7DC" Ref="U104"  Part="1" 
AR Path="/3FEFFFFF4B37D7DC" Ref="U104"  Part="1" 
AR Path="/4030AAC04B37D7DC" Ref="U104"  Part="1" 
AR Path="/5AD7153D4B37D7DC" Ref="U104"  Part="1" 
AR Path="/A84B37D7DC" Ref="U104"  Part="1" 
AR Path="/23D8D44B37D7DC" Ref="U104"  Part="1" 
AR Path="/4031DDF34B37D7DC" Ref="U104"  Part="1" 
AR Path="/3FE88B434B37D7DC" Ref="U104"  Part="1" 
AR Path="/4032778D4B37D7DC" Ref="U104"  Part="1" 
AR Path="/403091264B37D7DC" Ref="U104"  Part="1" 
AR Path="/403051264B37D7DC" Ref="U104"  Part="1" 
AR Path="/4032F78D4B37D7DC" Ref="U104"  Part="1" 
AR Path="/403251264B37D7DC" Ref="U104"  Part="1" 
AR Path="/4032AAC04B37D7DC" Ref="U104"  Part="1" 
AR Path="/4030D1264B37D7DC" Ref="U104"  Part="1" 
AR Path="/4031778D4B37D7DC" Ref="U104"  Part="1" 
AR Path="/3FEA24DD4B37D7DC" Ref="U104"  Part="1" 
AR Path="/2600004B37D7DC" Ref="U104"  Part="1" 
AR Path="/6FE934E34B37D7DC" Ref="U104"  Part="1" 
AR Path="/773F65F14B37D7DC" Ref="U104"  Part="1" 
AR Path="/773F8EB44B37D7DC" Ref="U104"  Part="1" 
AR Path="/23C9F04B37D7DC" Ref="U104"  Part="1" 
AR Path="/24B37D7DC" Ref="U104"  Part="1" 
AR Path="/23BC884B37D7DC" Ref="U104"  Part="1" 
AR Path="/DC0C124B37D7DC" Ref="U104"  Part="1" 
AR Path="/23C34C4B37D7DC" Ref="U104"  Part="1" 
AR Path="/23CBC44B37D7DC" Ref="U104"  Part="1" 
AR Path="/23C6504B37D7DC" Ref="U104"  Part="1" 
AR Path="/39803EA4B37D7DC" Ref="U104"  Part="1" 
AR Path="/FFFFFFFF4B37D7DC" Ref="U104"  Part="1" 
AR Path="/D058A04B37D7DC" Ref="U104"  Part="1" 
AR Path="/1607D44B37D7DC" Ref="U104"  Part="1" 
AR Path="/D1C3804B37D7DC" Ref="U104"  Part="1" 
AR Path="/23D70C4B37D7DC" Ref="U104"  Part="1" 
AR Path="/2104B37D7DC" Ref="U104"  Part="1" 
AR Path="/D1CA184B37D7DC" Ref="U104"  Part="1" 
AR Path="/F4BF4B37D7DC" Ref="U104"  Part="1" 
AR Path="/262F604B37D7DC" Ref="U104"  Part="1" 
AR Path="/22C6E204B37D7DC" Ref="U104"  Part="1" 
AR Path="/384B37D7DC" Ref="U104"  Part="1" 
F 0 "U104" H 6850 13150 70  0000 L CNN
F 1 "SRAM_512KO" H 6850 10750 70  0000 L CNN
F 2 "32dip600" H 6750 11950 60  0001 C CNN
	1    6750 11950
	1    0    0    -1  
$EndComp
$Comp
L SRAM_512KO U105
U 1 1 4B37D7DB
P 6750 15100
AR Path="/4B37D7DB" Ref="U105"  Part="1" 
AR Path="/94B37D7DB" Ref="U105"  Part="1" 
AR Path="/14B37D7DB" Ref="U105"  Part="1" 
AR Path="/6FF0DD404B37D7DB" Ref="U105"  Part="1" 
AR Path="/FFFFFFF04B37D7DB" Ref="U105"  Part="1" 
AR Path="/1600AC44B37D7DB" Ref="U"  Part="1" 
AR Path="/A4B37D7DB" Ref="U105"  Part="1" 
AR Path="/402755814B37D7DB" Ref="U105"  Part="1" 
AR Path="/6FE901F74B37D7DB" Ref="U105"  Part="1" 
AR Path="/3FEFFFFF4B37D7DB" Ref="U105"  Part="1" 
AR Path="/4030AAC04B37D7DB" Ref="U105"  Part="1" 
AR Path="/5AD7153D4B37D7DB" Ref="U105"  Part="1" 
AR Path="/A84B37D7DB" Ref="U105"  Part="1" 
AR Path="/23D9304B37D7DB" Ref="U105"  Part="1" 
AR Path="/23D8D44B37D7DB" Ref="U105"  Part="1" 
AR Path="/4031DDF34B37D7DB" Ref="U105"  Part="1" 
AR Path="/3FE88B434B37D7DB" Ref="U105"  Part="1" 
AR Path="/4032778D4B37D7DB" Ref="U105"  Part="1" 
AR Path="/403091264B37D7DB" Ref="U105"  Part="1" 
AR Path="/403051264B37D7DB" Ref="U105"  Part="1" 
AR Path="/4032F78D4B37D7DB" Ref="U105"  Part="1" 
AR Path="/403251264B37D7DB" Ref="U105"  Part="1" 
AR Path="/4032AAC04B37D7DB" Ref="U105"  Part="1" 
AR Path="/4030D1264B37D7DB" Ref="U105"  Part="1" 
AR Path="/4031778D4B37D7DB" Ref="U105"  Part="1" 
AR Path="/3FEA24DD4B37D7DB" Ref="U105"  Part="1" 
AR Path="/2600004B37D7DB" Ref="U105"  Part="1" 
AR Path="/6FE934E34B37D7DB" Ref="U105"  Part="1" 
AR Path="/773F65F14B37D7DB" Ref="U105"  Part="1" 
AR Path="/773F8EB44B37D7DB" Ref="U105"  Part="1" 
AR Path="/23C9F04B37D7DB" Ref="U105"  Part="1" 
AR Path="/24B37D7DB" Ref="U105"  Part="1" 
AR Path="/23BC884B37D7DB" Ref="U105"  Part="1" 
AR Path="/DC0C124B37D7DB" Ref="U105"  Part="1" 
AR Path="/23C34C4B37D7DB" Ref="U105"  Part="1" 
AR Path="/23CBC44B37D7DB" Ref="U105"  Part="1" 
AR Path="/23C6504B37D7DB" Ref="U105"  Part="1" 
AR Path="/39803EA4B37D7DB" Ref="U105"  Part="1" 
AR Path="/FFFFFFFF4B37D7DB" Ref="U105"  Part="1" 
AR Path="/D058A04B37D7DB" Ref="U105"  Part="1" 
AR Path="/1607D44B37D7DB" Ref="U105"  Part="1" 
AR Path="/D1C3804B37D7DB" Ref="U105"  Part="1" 
AR Path="/23D70C4B37D7DB" Ref="U105"  Part="1" 
AR Path="/2104B37D7DB" Ref="U105"  Part="1" 
AR Path="/D1CA184B37D7DB" Ref="U105"  Part="1" 
AR Path="/F4BF4B37D7DB" Ref="U105"  Part="1" 
AR Path="/262F604B37D7DB" Ref="U105"  Part="1" 
AR Path="/22C6E204B37D7DB" Ref="U105"  Part="1" 
AR Path="/384B37D7DB" Ref="U105"  Part="1" 
F 0 "U105" H 6850 16300 70  0000 L CNN
F 1 "SRAM_512KO" H 6850 13900 70  0000 L CNN
F 2 "32dip600" H 6750 15100 60  0001 C CNN
	1    6750 15100
	1    0    0    -1  
$EndComp
Text Label 5750 10850 0    60   ~ 0
bA1
Text Label 5750 10950 0    60   ~ 0
bA2
Text Label 5750 11150 0    60   ~ 0
bA4
Text Label 5750 11050 0    60   ~ 0
bA3
Text Label 5750 11450 0    60   ~ 0
bA7
Text Label 5750 11550 0    60   ~ 0
bA8
Text Label 5750 11350 0    60   ~ 0
bA6
Text Label 5750 11250 0    60   ~ 0
bA5
Text Label 5750 12050 0    60   ~ 0
bA13
Text Label 5750 12150 0    60   ~ 0
bA14
Text Label 5750 11850 0    60   ~ 0
bA11
Text Label 5750 11950 0    60   ~ 0
bA12
Text Label 5750 11750 0    60   ~ 0
bA10
Text Label 5750 11650 0    60   ~ 0
bA9
Text Label 5750 14800 0    60   ~ 0
bA9
Text Label 5750 14900 0    60   ~ 0
bA10
Text Label 5750 15100 0    60   ~ 0
bA12
Text Label 5750 15000 0    60   ~ 0
bA11
Text Label 5750 15300 0    60   ~ 0
bA14
Text Label 5750 15200 0    60   ~ 0
bA13
Text Label 5750 14400 0    60   ~ 0
bA5
Text Label 5750 14500 0    60   ~ 0
bA6
Text Label 5750 14700 0    60   ~ 0
bA8
Text Label 5750 14600 0    60   ~ 0
bA7
Text Label 5750 14200 0    60   ~ 0
bA3
Text Label 5750 14300 0    60   ~ 0
bA4
Text Label 5750 14100 0    60   ~ 0
bA2
Text Label 5750 14000 0    60   ~ 0
bA1
Text Notes 6350 13700 0    60   ~ 0
EVEN BYTE RAM
Text Notes 6350 10550 0    60   ~ 0
ODD BYTE RAM
$Comp
L VCC #PWR020
U 1 1 4B37D7DA
P 6750 13850
AR Path="/4B37D7DA" Ref="#PWR020"  Part="1" 
AR Path="/94B37D7DA" Ref="#PWR31"  Part="1" 
AR Path="/25E4B37D7DA" Ref="#PWR017"  Part="1" 
AR Path="/FFFFFFF04B37D7DA" Ref="#PWR33"  Part="1" 
AR Path="/A4B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/6FE901F74B37D7DA" Ref="#PWR01"  Part="1" 
AR Path="/402755814B37D7DA" Ref="#PWR01"  Part="1" 
AR Path="/3FEFFFFF4B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/4030AAC04B37D7DA" Ref="#PWR01"  Part="1" 
AR Path="/5AD7153D4B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/A84B37D7DA" Ref="#PWR30"  Part="1" 
AR Path="/6FF0DD404B37D7DA" Ref="#PWR021"  Part="1" 
AR Path="/14B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/23D9304B37D7DA" Ref="#PWR03"  Part="1" 
AR Path="/23D8D44B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/4031DDF34B37D7DA" Ref="#PWR03"  Part="1" 
AR Path="/3FE88B434B37D7DA" Ref="#PWR03"  Part="1" 
AR Path="/4032778D4B37D7DA" Ref="#PWR09"  Part="1" 
AR Path="/363030314B37D7DA" Ref="#PWR017"  Part="1" 
AR Path="/403091264B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/403051264B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/4032F78D4B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/403251264B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/4032AAC04B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/4030D1264B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/4031778D4B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/3FEA24DD4B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/2600004B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/6FE934E34B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/773F65F14B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/773F8EB44B37D7DA" Ref="#PWR020"  Part="1" 
AR Path="/23C9F04B37D7DA" Ref="#PWR21"  Part="1" 
AR Path="/23BC884B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/DC0C124B37D7DA" Ref="#PWR018"  Part="1" 
AR Path="/6684D64B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/23C34C4B37D7DA" Ref="#PWR025"  Part="1" 
AR Path="/23CBC44B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/23C6504B37D7DA" Ref="#PWR020"  Part="1" 
AR Path="/39803EA4B37D7DA" Ref="#PWR023"  Part="1" 
AR Path="/FFFFFFFF4B37D7DA" Ref="#PWR025"  Part="1" 
AR Path="/D058A04B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/1607D44B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/D1C3804B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/24B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/23D70C4B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/2104B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/D1CA184B37D7DA" Ref="#PWR024"  Part="1" 
AR Path="/F4BF4B37D7DA" Ref="#PWR020"  Part="1" 
AR Path="/262F604B37D7DA" Ref="#PWR020"  Part="1" 
AR Path="/22C6E204B37D7DA" Ref="#PWR020"  Part="1" 
AR Path="/384B37D7DA" Ref="#PWR021"  Part="1" 
F 0 "#PWR020" H 6750 13950 30  0001 C CNN
F 1 "VCC" H 6750 13950 30  0000 C CNN
	1    6750 13850
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR021
U 1 1 4B37D7D8
P 6750 10700
AR Path="/4B37D7D8" Ref="#PWR021"  Part="1" 
AR Path="/94B37D7D8" Ref="#PWR7"  Part="1" 
AR Path="/25E4B37D7D8" Ref="#PWR018"  Part="1" 
AR Path="/FFFFFFF04B37D7D8" Ref="#PWR7"  Part="1" 
AR Path="/A4B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/6FE901F74B37D7D8" Ref="#PWR03"  Part="1" 
AR Path="/402755814B37D7D8" Ref="#PWR03"  Part="1" 
AR Path="/3FEFFFFF4B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/4030AAC04B37D7D8" Ref="#PWR03"  Part="1" 
AR Path="/5AD7153D4B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/A84B37D7D8" Ref="#PWR8"  Part="1" 
AR Path="/6FF0DD404B37D7D8" Ref="#PWR022"  Part="1" 
AR Path="/14B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/23D9304B37D7D8" Ref="#PWR05"  Part="1" 
AR Path="/23D8D44B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/4031DDF34B37D7D8" Ref="#PWR05"  Part="1" 
AR Path="/3FE88B434B37D7D8" Ref="#PWR05"  Part="1" 
AR Path="/4032778D4B37D7D8" Ref="#PWR011"  Part="1" 
AR Path="/363030314B37D7D8" Ref="#PWR018"  Part="1" 
AR Path="/403091264B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/403051264B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/4032F78D4B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/403251264B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/4032AAC04B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/4030D1264B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/4031778D4B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/3FEA24DD4B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/2600004B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/6FE934E34B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/773F65F14B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/773F8EB44B37D7D8" Ref="#PWR021"  Part="1" 
AR Path="/23C9F04B37D7D8" Ref="#PWR19"  Part="1" 
AR Path="/23BC884B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/DC0C124B37D7D8" Ref="#PWR019"  Part="1" 
AR Path="/6684D64B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/23C34C4B37D7D8" Ref="#PWR026"  Part="1" 
AR Path="/23CBC44B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/23C6504B37D7D8" Ref="#PWR021"  Part="1" 
AR Path="/39803EA4B37D7D8" Ref="#PWR024"  Part="1" 
AR Path="/FFFFFFFF4B37D7D8" Ref="#PWR026"  Part="1" 
AR Path="/D058A04B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/1607D44B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/D1C3804B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/24B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/23D70C4B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/2104B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/D1CA184B37D7D8" Ref="#PWR025"  Part="1" 
AR Path="/F4BF4B37D7D8" Ref="#PWR021"  Part="1" 
AR Path="/262F604B37D7D8" Ref="#PWR021"  Part="1" 
AR Path="/22C6E204B37D7D8" Ref="#PWR021"  Part="1" 
AR Path="/384B37D7D8" Ref="#PWR022"  Part="1" 
F 0 "#PWR021" H 6750 10800 30  0001 C CNN
F 1 "VCC" H 6750 10800 30  0000 C CNN
	1    6750 10700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 4B37D7D6
P 9100 13200
AR Path="/4B37D7D6" Ref="#PWR022"  Part="1" 
AR Path="/94B37D7D6" Ref="#PWR16"  Part="1" 
AR Path="/25E4B37D7D6" Ref="#PWR019"  Part="1" 
AR Path="/FFFFFFF04B37D7D6" Ref="#PWR17"  Part="1" 
AR Path="/A4B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/6FE901F74B37D7D6" Ref="#PWR05"  Part="1" 
AR Path="/402755814B37D7D6" Ref="#PWR05"  Part="1" 
AR Path="/3FEFFFFF4B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/4030AAC04B37D7D6" Ref="#PWR05"  Part="1" 
AR Path="/5AD7153D4B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/A84B37D7D6" Ref="#PWR15"  Part="1" 
AR Path="/6FF0DD404B37D7D6" Ref="#PWR023"  Part="1" 
AR Path="/14B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/23D9304B37D7D6" Ref="#PWR07"  Part="1" 
AR Path="/23D8D44B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/4031DDF34B37D7D6" Ref="#PWR07"  Part="1" 
AR Path="/3FE88B434B37D7D6" Ref="#PWR07"  Part="1" 
AR Path="/4032778D4B37D7D6" Ref="#PWR013"  Part="1" 
AR Path="/363030314B37D7D6" Ref="#PWR019"  Part="1" 
AR Path="/403091264B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/403051264B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/4032F78D4B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/403251264B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/4032AAC04B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/4030D1264B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/4031778D4B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/3FEA24DD4B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/2600004B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/6FE934E34B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/773F65F14B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/773F8EB44B37D7D6" Ref="#PWR022"  Part="1" 
AR Path="/23C9F04B37D7D6" Ref="#PWR25"  Part="1" 
AR Path="/23BC884B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/DC0C124B37D7D6" Ref="#PWR020"  Part="1" 
AR Path="/6684D64B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/23C34C4B37D7D6" Ref="#PWR027"  Part="1" 
AR Path="/23CBC44B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/23C6504B37D7D6" Ref="#PWR022"  Part="1" 
AR Path="/39803EA4B37D7D6" Ref="#PWR025"  Part="1" 
AR Path="/FFFFFFFF4B37D7D6" Ref="#PWR027"  Part="1" 
AR Path="/D058A04B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/1607D44B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/D1C3804B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/24B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/23D70C4B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/2104B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/D1CA184B37D7D6" Ref="#PWR026"  Part="1" 
AR Path="/F4BF4B37D7D6" Ref="#PWR022"  Part="1" 
AR Path="/262F604B37D7D6" Ref="#PWR022"  Part="1" 
AR Path="/22C6E204B37D7D6" Ref="#PWR022"  Part="1" 
AR Path="/384B37D7D6" Ref="#PWR023"  Part="1" 
F 0 "#PWR022" H 9100 13200 30  0001 C CNN
F 1 "GND" H 9100 13130 30  0001 C CNN
	1    9100 13200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR023
U 1 1 4B37D7D5
P 9100 10700
AR Path="/4B37D7D5" Ref="#PWR023"  Part="1" 
AR Path="/94B37D7D5" Ref="#PWR15"  Part="1" 
AR Path="/25E4B37D7D5" Ref="#PWR020"  Part="1" 
AR Path="/FFFFFFF04B37D7D5" Ref="#PWR16"  Part="1" 
AR Path="/A4B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/6FE901F74B37D7D5" Ref="#PWR06"  Part="1" 
AR Path="/402755814B37D7D5" Ref="#PWR06"  Part="1" 
AR Path="/3FEFFFFF4B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/4030AAC04B37D7D5" Ref="#PWR06"  Part="1" 
AR Path="/5AD7153D4B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/A84B37D7D5" Ref="#PWR14"  Part="1" 
AR Path="/6FF0DD404B37D7D5" Ref="#PWR024"  Part="1" 
AR Path="/14B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/23D9304B37D7D5" Ref="#PWR08"  Part="1" 
AR Path="/23D8D44B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/4031DDF34B37D7D5" Ref="#PWR08"  Part="1" 
AR Path="/3FE88B434B37D7D5" Ref="#PWR08"  Part="1" 
AR Path="/4032778D4B37D7D5" Ref="#PWR014"  Part="1" 
AR Path="/363030314B37D7D5" Ref="#PWR020"  Part="1" 
AR Path="/403091264B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/403051264B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/4032F78D4B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/403251264B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/4032AAC04B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/4030D1264B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/4031778D4B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/3FEA24DD4B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/2600004B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/6FE934E34B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/773F65F14B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/773F8EB44B37D7D5" Ref="#PWR023"  Part="1" 
AR Path="/23C9F04B37D7D5" Ref="#PWR24"  Part="1" 
AR Path="/23BC884B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/DC0C124B37D7D5" Ref="#PWR021"  Part="1" 
AR Path="/6684D64B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/23C34C4B37D7D5" Ref="#PWR028"  Part="1" 
AR Path="/23CBC44B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/23C6504B37D7D5" Ref="#PWR023"  Part="1" 
AR Path="/39803EA4B37D7D5" Ref="#PWR026"  Part="1" 
AR Path="/FFFFFFFF4B37D7D5" Ref="#PWR028"  Part="1" 
AR Path="/D058A04B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/1607D44B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/D1C3804B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/24B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/23D70C4B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/2104B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/D1CA184B37D7D5" Ref="#PWR027"  Part="1" 
AR Path="/F4BF4B37D7D5" Ref="#PWR023"  Part="1" 
AR Path="/262F604B37D7D5" Ref="#PWR023"  Part="1" 
AR Path="/22C6E204B37D7D5" Ref="#PWR023"  Part="1" 
AR Path="/384B37D7D5" Ref="#PWR024"  Part="1" 
F 0 "#PWR023" H 9100 10800 30  0001 C CNN
F 1 "VCC" H 9100 10800 30  0000 C CNN
	1    9100 10700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR024
U 1 1 4B37D7D4
P 9100 16350
AR Path="/4B37D7D4" Ref="#PWR024"  Part="1" 
AR Path="/94B37D7D4" Ref="#PWR34"  Part="1" 
AR Path="/25E4B37D7D4" Ref="#PWR021"  Part="1" 
AR Path="/FFFFFFF04B37D7D4" Ref="#PWR38"  Part="1" 
AR Path="/A4B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/6FE901F74B37D7D4" Ref="#PWR07"  Part="1" 
AR Path="/402755814B37D7D4" Ref="#PWR07"  Part="1" 
AR Path="/3FEFFFFF4B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/4030AAC04B37D7D4" Ref="#PWR07"  Part="1" 
AR Path="/5AD7153D4B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/A84B37D7D4" Ref="#PWR33"  Part="1" 
AR Path="/6FF0DD404B37D7D4" Ref="#PWR025"  Part="1" 
AR Path="/14B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/23D9304B37D7D4" Ref="#PWR09"  Part="1" 
AR Path="/23D8D44B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/4031DDF34B37D7D4" Ref="#PWR09"  Part="1" 
AR Path="/3FE88B434B37D7D4" Ref="#PWR09"  Part="1" 
AR Path="/4032778D4B37D7D4" Ref="#PWR015"  Part="1" 
AR Path="/363030314B37D7D4" Ref="#PWR021"  Part="1" 
AR Path="/403091264B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/403051264B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/4032F78D4B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/403251264B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/4032AAC04B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/4030D1264B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/4031778D4B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/3FEA24DD4B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/2600004B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/6FE934E34B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/773F65F14B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/773F8EB44B37D7D4" Ref="#PWR024"  Part="1" 
AR Path="/23C9F04B37D7D4" Ref="#PWR27"  Part="1" 
AR Path="/23BC884B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/DC0C124B37D7D4" Ref="#PWR022"  Part="1" 
AR Path="/6684D64B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/23C34C4B37D7D4" Ref="#PWR029"  Part="1" 
AR Path="/23CBC44B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/23C6504B37D7D4" Ref="#PWR024"  Part="1" 
AR Path="/39803EA4B37D7D4" Ref="#PWR027"  Part="1" 
AR Path="/FFFFFFFF4B37D7D4" Ref="#PWR029"  Part="1" 
AR Path="/D058A04B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/1607D44B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/D1C3804B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/24B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/23D70C4B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/2104B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/D1CA184B37D7D4" Ref="#PWR028"  Part="1" 
AR Path="/F4BF4B37D7D4" Ref="#PWR024"  Part="1" 
AR Path="/262F604B37D7D4" Ref="#PWR024"  Part="1" 
AR Path="/22C6E204B37D7D4" Ref="#PWR024"  Part="1" 
AR Path="/384B37D7D4" Ref="#PWR025"  Part="1" 
F 0 "#PWR024" H 9100 16350 30  0001 C CNN
F 1 "GND" H 9100 16280 30  0001 C CNN
	1    9100 16350
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR025
U 1 1 4B37D7D3
P 9100 13850
AR Path="/4B37D7D3" Ref="#PWR025"  Part="1" 
AR Path="/94B37D7D3" Ref="#PWR33"  Part="1" 
AR Path="/25E4B37D7D3" Ref="#PWR022"  Part="1" 
AR Path="/FFFFFFF04B37D7D3" Ref="#PWR37"  Part="1" 
AR Path="/A4B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/6FE901F74B37D7D3" Ref="#PWR08"  Part="1" 
AR Path="/402755814B37D7D3" Ref="#PWR08"  Part="1" 
AR Path="/3FEFFFFF4B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/4030AAC04B37D7D3" Ref="#PWR08"  Part="1" 
AR Path="/5AD7153D4B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/A84B37D7D3" Ref="#PWR32"  Part="1" 
AR Path="/6FF0DD404B37D7D3" Ref="#PWR026"  Part="1" 
AR Path="/14B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/23D9304B37D7D3" Ref="#PWR010"  Part="1" 
AR Path="/23D8D44B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/4031DDF34B37D7D3" Ref="#PWR010"  Part="1" 
AR Path="/3FE88B434B37D7D3" Ref="#PWR010"  Part="1" 
AR Path="/4032778D4B37D7D3" Ref="#PWR016"  Part="1" 
AR Path="/363030314B37D7D3" Ref="#PWR022"  Part="1" 
AR Path="/403091264B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/403051264B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/4032F78D4B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/403251264B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/4032AAC04B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/4030D1264B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/4031778D4B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/3FEA24DD4B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/2600004B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/6FE934E34B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/773F65F14B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/773F8EB44B37D7D3" Ref="#PWR025"  Part="1" 
AR Path="/23C9F04B37D7D3" Ref="#PWR26"  Part="1" 
AR Path="/23BC884B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/DC0C124B37D7D3" Ref="#PWR023"  Part="1" 
AR Path="/6684D64B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/23C34C4B37D7D3" Ref="#PWR030"  Part="1" 
AR Path="/23CBC44B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/23C6504B37D7D3" Ref="#PWR025"  Part="1" 
AR Path="/39803EA4B37D7D3" Ref="#PWR028"  Part="1" 
AR Path="/FFFFFFFF4B37D7D3" Ref="#PWR030"  Part="1" 
AR Path="/D058A04B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/1607D44B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/D1C3804B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/24B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/23D70C4B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/2104B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/D1CA184B37D7D3" Ref="#PWR029"  Part="1" 
AR Path="/F4BF4B37D7D3" Ref="#PWR025"  Part="1" 
AR Path="/262F604B37D7D3" Ref="#PWR025"  Part="1" 
AR Path="/22C6E204B37D7D3" Ref="#PWR025"  Part="1" 
AR Path="/384B37D7D3" Ref="#PWR026"  Part="1" 
F 0 "#PWR025" H 9100 13950 30  0001 C CNN
F 1 "VCC" H 9100 13950 30  0000 C CNN
	1    9100 13850
	1    0    0    -1  
$EndComp
Text Notes 8700 10550 0    60   ~ 0
ODD BYTE RAM
Text Notes 8700 13700 0    60   ~ 0
EVEN BYTE RAM
Text Label 8100 14000 0    60   ~ 0
bA1
Text Label 8100 14100 0    60   ~ 0
bA2
Text Label 8100 14300 0    60   ~ 0
bA4
Text Label 8100 14200 0    60   ~ 0
bA3
Text Label 8100 14600 0    60   ~ 0
bA7
Text Label 8100 14700 0    60   ~ 0
bA8
Text Label 8100 14500 0    60   ~ 0
bA6
Text Label 8100 14400 0    60   ~ 0
bA5
Text Label 8100 15200 0    60   ~ 0
bA13
Text Label 8100 15300 0    60   ~ 0
bA14
Text Label 8100 15000 0    60   ~ 0
bA11
Text Label 8100 15100 0    60   ~ 0
bA12
Text Label 8100 14900 0    60   ~ 0
bA10
Text Label 8100 14800 0    60   ~ 0
bA9
Text Label 8100 11650 0    60   ~ 0
bA9
Text Label 8100 11750 0    60   ~ 0
bA10
Text Label 8100 11950 0    60   ~ 0
bA12
Text Label 8100 11850 0    60   ~ 0
bA11
Text Label 8100 12150 0    60   ~ 0
bA14
Text Label 8100 12050 0    60   ~ 0
bA13
Text Label 8100 11250 0    60   ~ 0
bA5
Text Label 8100 11350 0    60   ~ 0
bA6
Text Label 8100 11550 0    60   ~ 0
bA8
Text Label 8100 11450 0    60   ~ 0
bA7
Text Label 8100 11050 0    60   ~ 0
bA3
Text Label 8100 11150 0    60   ~ 0
bA4
Text Label 8100 10950 0    60   ~ 0
bA2
Text Label 8100 10850 0    60   ~ 0
bA1
$Comp
L SRAM_512KO U107
U 1 1 4B37D7D2
P 9100 15100
AR Path="/4B37D7D2" Ref="U107"  Part="1" 
AR Path="/94B37D7D2" Ref="U107"  Part="1" 
AR Path="/14B37D7D2" Ref="U107"  Part="1" 
AR Path="/6FF0DD404B37D7D2" Ref="U107"  Part="1" 
AR Path="/FFFFFFF04B37D7D2" Ref="U107"  Part="1" 
AR Path="/23D6544B37D7D2" Ref="U108"  Part="1" 
AR Path="/20B0BC44B37D7D2" Ref="U"  Part="1" 
AR Path="/A4B37D7D2" Ref="U107"  Part="1" 
AR Path="/402755814B37D7D2" Ref="U107"  Part="1" 
AR Path="/6FE901F74B37D7D2" Ref="U107"  Part="1" 
AR Path="/3FEFFFFF4B37D7D2" Ref="U107"  Part="1" 
AR Path="/4030AAC04B37D7D2" Ref="U107"  Part="1" 
AR Path="/5AD7153D4B37D7D2" Ref="U107"  Part="1" 
AR Path="/A84B37D7D2" Ref="U107"  Part="1" 
AR Path="/23D9304B37D7D2" Ref="U107"  Part="1" 
AR Path="/23D8D44B37D7D2" Ref="U107"  Part="1" 
AR Path="/4031DDF34B37D7D2" Ref="U107"  Part="1" 
AR Path="/3FE88B434B37D7D2" Ref="U107"  Part="1" 
AR Path="/4032778D4B37D7D2" Ref="U107"  Part="1" 
AR Path="/403091264B37D7D2" Ref="U107"  Part="1" 
AR Path="/403051264B37D7D2" Ref="U107"  Part="1" 
AR Path="/4032F78D4B37D7D2" Ref="U107"  Part="1" 
AR Path="/403251264B37D7D2" Ref="U107"  Part="1" 
AR Path="/4032AAC04B37D7D2" Ref="U107"  Part="1" 
AR Path="/4030D1264B37D7D2" Ref="U107"  Part="1" 
AR Path="/4031778D4B37D7D2" Ref="U107"  Part="1" 
AR Path="/3FEA24DD4B37D7D2" Ref="U107"  Part="1" 
AR Path="/2600004B37D7D2" Ref="U107"  Part="1" 
AR Path="/6FE934E34B37D7D2" Ref="U107"  Part="1" 
AR Path="/773F65F14B37D7D2" Ref="U107"  Part="1" 
AR Path="/773F8EB44B37D7D2" Ref="U107"  Part="1" 
AR Path="/23C9F04B37D7D2" Ref="U107"  Part="1" 
AR Path="/24B37D7D2" Ref="U107"  Part="1" 
AR Path="/23BC884B37D7D2" Ref="U107"  Part="1" 
AR Path="/DC0C124B37D7D2" Ref="U107"  Part="1" 
AR Path="/23C34C4B37D7D2" Ref="U107"  Part="1" 
AR Path="/23CBC44B37D7D2" Ref="U107"  Part="1" 
AR Path="/23C6504B37D7D2" Ref="U107"  Part="1" 
AR Path="/39803EA4B37D7D2" Ref="U107"  Part="1" 
AR Path="/FFFFFFFF4B37D7D2" Ref="U107"  Part="1" 
AR Path="/D058A04B37D7D2" Ref="U107"  Part="1" 
AR Path="/1607D44B37D7D2" Ref="U107"  Part="1" 
AR Path="/D1C3804B37D7D2" Ref="U107"  Part="1" 
AR Path="/23D70C4B37D7D2" Ref="U107"  Part="1" 
AR Path="/2104B37D7D2" Ref="U107"  Part="1" 
AR Path="/D1CA184B37D7D2" Ref="U107"  Part="1" 
AR Path="/F4BF4B37D7D2" Ref="U107"  Part="1" 
AR Path="/262F604B37D7D2" Ref="U107"  Part="1" 
AR Path="/22C6E204B37D7D2" Ref="U107"  Part="1" 
AR Path="/384B37D7D2" Ref="U107"  Part="1" 
F 0 "U107" H 9200 16300 70  0000 L CNN
F 1 "SRAM_512KO" H 9200 13900 70  0000 L CNN
F 2 "32dip600" H 9100 15100 60  0001 C CNN
	1    9100 15100
	1    0    0    -1  
$EndComp
$Comp
L SRAM_512KO U106
U 1 1 4B37D7D1
P 9100 11950
AR Path="/4B37D7D1" Ref="U106"  Part="1" 
AR Path="/94B37D7D1" Ref="U106"  Part="1" 
AR Path="/14B37D7D1" Ref="U106"  Part="1" 
AR Path="/6FF0DD404B37D7D1" Ref="U106"  Part="1" 
AR Path="/FFFFFFF04B37D7D1" Ref="U106"  Part="1" 
AR Path="/23D6544B37D7D1" Ref="U107"  Part="1" 
AR Path="/1520AD84B37D7D1" Ref="U"  Part="1" 
AR Path="/A4B37D7D1" Ref="U106"  Part="1" 
AR Path="/402755814B37D7D1" Ref="U106"  Part="1" 
AR Path="/6FE901F74B37D7D1" Ref="U106"  Part="1" 
AR Path="/3FEFFFFF4B37D7D1" Ref="U106"  Part="1" 
AR Path="/4030AAC04B37D7D1" Ref="U106"  Part="1" 
AR Path="/5AD7153D4B37D7D1" Ref="U106"  Part="1" 
AR Path="/A84B37D7D1" Ref="U106"  Part="1" 
AR Path="/23D9304B37D7D1" Ref="U106"  Part="1" 
AR Path="/23D8D44B37D7D1" Ref="U106"  Part="1" 
AR Path="/4031DDF34B37D7D1" Ref="U106"  Part="1" 
AR Path="/3FE88B434B37D7D1" Ref="U106"  Part="1" 
AR Path="/4032778D4B37D7D1" Ref="U106"  Part="1" 
AR Path="/403091264B37D7D1" Ref="U106"  Part="1" 
AR Path="/403051264B37D7D1" Ref="U106"  Part="1" 
AR Path="/4032F78D4B37D7D1" Ref="U106"  Part="1" 
AR Path="/403251264B37D7D1" Ref="U106"  Part="1" 
AR Path="/4032AAC04B37D7D1" Ref="U106"  Part="1" 
AR Path="/4030D1264B37D7D1" Ref="U106"  Part="1" 
AR Path="/4031778D4B37D7D1" Ref="U106"  Part="1" 
AR Path="/3FEA24DD4B37D7D1" Ref="U106"  Part="1" 
AR Path="/2600004B37D7D1" Ref="U106"  Part="1" 
AR Path="/6FE934E34B37D7D1" Ref="U106"  Part="1" 
AR Path="/773F65F14B37D7D1" Ref="U106"  Part="1" 
AR Path="/773F8EB44B37D7D1" Ref="U106"  Part="1" 
AR Path="/23C9F04B37D7D1" Ref="U106"  Part="1" 
AR Path="/24B37D7D1" Ref="U106"  Part="1" 
AR Path="/23BC884B37D7D1" Ref="U106"  Part="1" 
AR Path="/DC0C124B37D7D1" Ref="U106"  Part="1" 
AR Path="/23C34C4B37D7D1" Ref="U106"  Part="1" 
AR Path="/23CBC44B37D7D1" Ref="U106"  Part="1" 
AR Path="/23C6504B37D7D1" Ref="U106"  Part="1" 
AR Path="/39803EA4B37D7D1" Ref="U106"  Part="1" 
AR Path="/FFFFFFFF4B37D7D1" Ref="U106"  Part="1" 
AR Path="/D058A04B37D7D1" Ref="U106"  Part="1" 
AR Path="/1607D44B37D7D1" Ref="U106"  Part="1" 
AR Path="/D1C3804B37D7D1" Ref="U106"  Part="1" 
AR Path="/23D70C4B37D7D1" Ref="U106"  Part="1" 
AR Path="/2104B37D7D1" Ref="U106"  Part="1" 
AR Path="/D1CA184B37D7D1" Ref="U106"  Part="1" 
AR Path="/F4BF4B37D7D1" Ref="U106"  Part="1" 
AR Path="/262F604B37D7D1" Ref="U106"  Part="1" 
AR Path="/22C6E204B37D7D1" Ref="U106"  Part="1" 
AR Path="/384B37D7D1" Ref="U106"  Part="1" 
F 0 "U106" H 9200 13150 70  0000 L CNN
F 1 "SRAM_512KO" H 9200 10750 70  0000 L CNN
F 2 "32dip600" H 9100 11950 60  0001 C CNN
	1    9100 11950
	1    0    0    -1  
$EndComp
Text Notes 20300 12050 0    60   ~ 0
SPARES
$Comp
L 74LS08 U?
U 4 1 4B3681D9
P 11200 1300
AR Path="/23D9D84B3681D9" Ref="U?"  Part="1" 
AR Path="/394433324B3681D9" Ref="U?"  Part="1" 
AR Path="/74B3681D9" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3681D9" Ref="U?"  Part="1" 
AR Path="/C50C984B3681D9" Ref="U"  Part="4" 
AR Path="/77F184884B3681D9" Ref="U17"  Part="4" 
AR Path="/4B3681D9" Ref="U17"  Part="4" 
AR Path="/94B3681D9" Ref="U17"  Part="4" 
AR Path="/5AD7153D4B3681D9" Ref="U17"  Part="4" 
AR Path="/DCBAABCD4B3681D9" Ref="U17"  Part="4" 
AR Path="/6FE901F74B3681D9" Ref="U17"  Part="4" 
AR Path="/402C55814B3681D9" Ref="U17"  Part="4" 
AR Path="/23D2034B3681D9" Ref="U17"  Part="4" 
AR Path="/A84B3681D9" Ref="U17"  Part="4" 
AR Path="/40273BE74B3681D9" Ref="U17"  Part="4" 
AR Path="/2600004B3681D9" Ref="U17"  Part="4" 
AR Path="/3D8EA0004B3681D9" Ref="U17"  Part="4" 
AR Path="/402908B44B3681D9" Ref="U17"  Part="4" 
AR Path="/A4B3681D9" Ref="U17"  Part="4" 
AR Path="/3D6CC0004B3681D9" Ref="U17"  Part="4" 
AR Path="/3D5A40004B3681D9" Ref="U17"  Part="4" 
AR Path="/FFFFFFF04B3681D9" Ref="U17"  Part="4" 
AR Path="/402755814B3681D9" Ref="U17"  Part="4" 
AR Path="/3FEFFFFF4B3681D9" Ref="U17"  Part="4" 
AR Path="/4030AAC04B3681D9" Ref="U17"  Part="4" 
AR Path="/6FF0DD404B3681D9" Ref="U17"  Part="4" 
AR Path="/23D9304B3681D9" Ref="U17"  Part="4" 
AR Path="/23D8D44B3681D9" Ref="U17"  Part="4" 
AR Path="/4031DDF34B3681D9" Ref="U17"  Part="4" 
AR Path="/3FE88B434B3681D9" Ref="U17"  Part="4" 
AR Path="/4032778D4B3681D9" Ref="U17"  Part="4" 
AR Path="/403091264B3681D9" Ref="U17"  Part="4" 
AR Path="/403051264B3681D9" Ref="U17"  Part="4" 
AR Path="/4032F78D4B3681D9" Ref="U17"  Part="4" 
AR Path="/403251264B3681D9" Ref="U17"  Part="4" 
AR Path="/4032AAC04B3681D9" Ref="U17"  Part="4" 
AR Path="/4030D1264B3681D9" Ref="U17"  Part="4" 
AR Path="/4031778D4B3681D9" Ref="U17"  Part="4" 
AR Path="/3FEA24DD4B3681D9" Ref="U17"  Part="4" 
AR Path="/6FE934E34B3681D9" Ref="U17"  Part="4" 
AR Path="/773F8EB44B3681D9" Ref="U17"  Part="4" 
AR Path="/23C9F04B3681D9" Ref="U17"  Part="4" 
AR Path="/24B3681D9" Ref="U17"  Part="4" 
AR Path="/23BC884B3681D9" Ref="U17"  Part="4" 
AR Path="/DC0C124B3681D9" Ref="U17"  Part="4" 
AR Path="/23C34C4B3681D9" Ref="U17"  Part="4" 
AR Path="/23CBC44B3681D9" Ref="U17"  Part="4" 
AR Path="/23C6504B3681D9" Ref="U17"  Part="4" 
AR Path="/39803EA4B3681D9" Ref="U17"  Part="4" 
AR Path="/FFFFFFFF4B3681D9" Ref="U17"  Part="4" 
AR Path="/D058A04B3681D9" Ref="U17"  Part="4" 
AR Path="/1607D44B3681D9" Ref="U17"  Part="4" 
AR Path="/D1C3804B3681D9" Ref="U17"  Part="4" 
AR Path="/23D70C4B3681D9" Ref="U17"  Part="4" 
AR Path="/14B3681D9" Ref="U17"  Part="4" 
AR Path="/2104B3681D9" Ref="U17"  Part="4" 
AR Path="/7E428DAC4B3681D9" Ref="U17"  Part="4" 
AR Path="/D1CA184B3681D9" Ref="U17"  Part="4" 
AR Path="/F4BF4B3681D9" Ref="U17"  Part="4" 
AR Path="/262F604B3681D9" Ref="U17"  Part="4" 
AR Path="/22C6E204B3681D9" Ref="U17"  Part="4" 
AR Path="/384B3681D9" Ref="U17"  Part="4" 
F 0 "U17" H 11200 1350 60  0000 C CNN
F 1 "74LS08" H 11200 1250 60  0000 C CNN
F 2 "14dip300" H 11200 1300 60  0001 C CNN
	4    11200 1300
	1    0    0    -1  
$EndComp
$Comp
L 74LS02 U?
U 2 1 4B368055
P 17050 11550
AR Path="/23D9B04B368055" Ref="U?"  Part="1" 
AR Path="/394433324B368055" Ref="U?"  Part="1" 
AR Path="/23D6B44B368055" Ref="U?"  Part="1" 
AR Path="/7E44048F4B368055" Ref="U?"  Part="1" 
AR Path="/680CE64B368055" Ref="U"  Part="2" 
AR Path="/4B368055" Ref="U15"  Part="2" 
AR Path="/94B368055" Ref="U15"  Part="2" 
AR Path="/14B368055" Ref="U15"  Part="2" 
AR Path="/6FF0DD404B368055" Ref="U15"  Part="2" 
AR Path="/5AD7153D4B368055" Ref="U15"  Part="2" 
AR Path="/DCBAABCD4B368055" Ref="U15"  Part="2" 
AR Path="/402C55814B368055" Ref="U15"  Part="2" 
AR Path="/23D2034B368055" Ref="U15"  Part="2" 
AR Path="/A84B368055" Ref="U15"  Part="2" 
AR Path="/40273BE74B368055" Ref="U15"  Part="2" 
AR Path="/2600004B368055" Ref="U15"  Part="2" 
AR Path="/3D8EA0004B368055" Ref="U15"  Part="2" 
AR Path="/6FE901F74B368055" Ref="U15"  Part="2" 
AR Path="/402908B44B368055" Ref="U15"  Part="2" 
AR Path="/6FF405304B368055" Ref="U15"  Part="2" 
AR Path="/A4B368055" Ref="U15"  Part="2" 
AR Path="/3D6CC0004B368055" Ref="U15"  Part="2" 
AR Path="/3D5A40004B368055" Ref="U15"  Part="2" 
AR Path="/FFFFFFF04B368055" Ref="U15"  Part="2" 
AR Path="/402755814B368055" Ref="U15"  Part="2" 
AR Path="/3FEFFFFF4B368055" Ref="U15"  Part="2" 
AR Path="/4030AAC04B368055" Ref="U15"  Part="2" 
AR Path="/23D9304B368055" Ref="U15"  Part="2" 
AR Path="/23D8D44B368055" Ref="U15"  Part="2" 
AR Path="/4031DDF34B368055" Ref="U15"  Part="2" 
AR Path="/3FE88B434B368055" Ref="U15"  Part="2" 
AR Path="/4032778D4B368055" Ref="U15"  Part="2" 
AR Path="/403091264B368055" Ref="U15"  Part="2" 
AR Path="/403051264B368055" Ref="U15"  Part="2" 
AR Path="/4032F78D4B368055" Ref="U15"  Part="2" 
AR Path="/403251264B368055" Ref="U15"  Part="2" 
AR Path="/4032AAC04B368055" Ref="U15"  Part="2" 
AR Path="/4030D1264B368055" Ref="U15"  Part="2" 
AR Path="/4031778D4B368055" Ref="U15"  Part="2" 
AR Path="/3FEA24DD4B368055" Ref="U15"  Part="2" 
AR Path="/6FE934E34B368055" Ref="U15"  Part="2" 
AR Path="/773F8EB44B368055" Ref="U15"  Part="2" 
AR Path="/23C9F04B368055" Ref="U15"  Part="2" 
AR Path="/24B368055" Ref="U15"  Part="2" 
AR Path="/23BC884B368055" Ref="U15"  Part="2" 
AR Path="/DC0C124B368055" Ref="U15"  Part="2" 
AR Path="/23C34C4B368055" Ref="U15"  Part="2" 
AR Path="/23CBC44B368055" Ref="U15"  Part="2" 
AR Path="/23C6504B368055" Ref="U15"  Part="2" 
AR Path="/39803EA4B368055" Ref="U15"  Part="2" 
AR Path="/FFFFFFFF4B368055" Ref="U15"  Part="2" 
AR Path="/D058A04B368055" Ref="U15"  Part="2" 
AR Path="/1607D44B368055" Ref="U15"  Part="2" 
AR Path="/D1C3804B368055" Ref="U15"  Part="2" 
AR Path="/23D70C4B368055" Ref="U15"  Part="2" 
AR Path="/2104B368055" Ref="U15"  Part="2" 
AR Path="/D1CA184B368055" Ref="U15"  Part="2" 
AR Path="/F4BF4B368055" Ref="U15"  Part="2" 
AR Path="/262F604B368055" Ref="U15"  Part="2" 
AR Path="/22C6E204B368055" Ref="U15"  Part="2" 
AR Path="/384B368055" Ref="U15"  Part="2" 
F 0 "U15" H 17050 11600 60  0000 C CNN
F 1 "74LS02" H 17100 11500 60  0000 C CNN
F 2 "14dip300" H 17050 11550 60  0001 C CNN
	2    17050 11550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4B367E99
P 20050 14250
AR Path="/23D9D84B367E99" Ref="#PWR?"  Part="1" 
AR Path="/394433324B367E99" Ref="#PWR?"  Part="1" 
AR Path="/25E4B367E99" Ref="#PWR023"  Part="1" 
AR Path="/FFFFFFFF4B367E99" Ref="#PWR031"  Part="1" 
AR Path="/4B367E99" Ref="#PWR026"  Part="1" 
AR Path="/94B367E99" Ref="#PWR36"  Part="1" 
AR Path="/5AD7153D4B367E99" Ref="#PWR024"  Part="1" 
AR Path="/DCBAABCD4B367E99" Ref="#PWR06"  Part="1" 
AR Path="/54B367E99" Ref="#PWR01"  Part="1" 
AR Path="/A84B367E99" Ref="#PWR35"  Part="1" 
AR Path="/6FE901F74B367E99" Ref="#PWR09"  Part="1" 
AR Path="/40293BE74B367E99" Ref="#PWR02"  Part="1" 
AR Path="/23D2034B367E99" Ref="#PWR05"  Part="1" 
AR Path="/402C55814B367E99" Ref="#PWR05"  Part="1" 
AR Path="/40273BE74B367E99" Ref="#PWR06"  Part="1" 
AR Path="/2600004B367E99" Ref="#PWR024"  Part="1" 
AR Path="/3D8EA0004B367E99" Ref="#PWR06"  Part="1" 
AR Path="/402908B44B367E99" Ref="#PWR02"  Part="1" 
AR Path="/A4B367E99" Ref="#PWR024"  Part="1" 
AR Path="/3D6CC0004B367E99" Ref="#PWR01"  Part="1" 
AR Path="/3D5A40004B367E99" Ref="#PWR01"  Part="1" 
AR Path="/FFFFFFF04B367E99" Ref="#PWR27"  Part="1" 
AR Path="/402755814B367E99" Ref="#PWR09"  Part="1" 
AR Path="/3FEFFFFF4B367E99" Ref="#PWR024"  Part="1" 
AR Path="/4030AAC04B367E99" Ref="#PWR09"  Part="1" 
AR Path="/6FF0DD404B367E99" Ref="#PWR027"  Part="1" 
AR Path="/14B367E99" Ref="#PWR030"  Part="1" 
AR Path="/23D9304B367E99" Ref="#PWR011"  Part="1" 
AR Path="/23D8D44B367E99" Ref="#PWR024"  Part="1" 
AR Path="/4031DDF34B367E99" Ref="#PWR011"  Part="1" 
AR Path="/3FE88B434B367E99" Ref="#PWR011"  Part="1" 
AR Path="/4032778D4B367E99" Ref="#PWR017"  Part="1" 
AR Path="/363030314B367E99" Ref="#PWR023"  Part="1" 
AR Path="/403091264B367E99" Ref="#PWR024"  Part="1" 
AR Path="/403051264B367E99" Ref="#PWR024"  Part="1" 
AR Path="/4032F78D4B367E99" Ref="#PWR024"  Part="1" 
AR Path="/403251264B367E99" Ref="#PWR024"  Part="1" 
AR Path="/4032AAC04B367E99" Ref="#PWR024"  Part="1" 
AR Path="/4030D1264B367E99" Ref="#PWR024"  Part="1" 
AR Path="/4031778D4B367E99" Ref="#PWR024"  Part="1" 
AR Path="/3FEA24DD4B367E99" Ref="#PWR024"  Part="1" 
AR Path="/6FE934E34B367E99" Ref="#PWR030"  Part="1" 
AR Path="/773F65F14B367E99" Ref="#PWR030"  Part="1" 
AR Path="/773F8EB44B367E99" Ref="#PWR026"  Part="1" 
AR Path="/23C9F04B367E99" Ref="#PWR38"  Part="1" 
AR Path="/23BC884B367E99" Ref="#PWR030"  Part="1" 
AR Path="/DC0C124B367E99" Ref="#PWR024"  Part="1" 
AR Path="/6684D64B367E99" Ref="#PWR030"  Part="1" 
AR Path="/23C34C4B367E99" Ref="#PWR031"  Part="1" 
AR Path="/23CBC44B367E99" Ref="#PWR030"  Part="1" 
AR Path="/23C6504B367E99" Ref="#PWR026"  Part="1" 
AR Path="/39803EA4B367E99" Ref="#PWR029"  Part="1" 
AR Path="/D058A04B367E99" Ref="#PWR030"  Part="1" 
AR Path="/1607D44B367E99" Ref="#PWR030"  Part="1" 
AR Path="/D1C3804B367E99" Ref="#PWR030"  Part="1" 
AR Path="/24B367E99" Ref="#PWR030"  Part="1" 
AR Path="/23D70C4B367E99" Ref="#PWR030"  Part="1" 
AR Path="/2104B367E99" Ref="#PWR030"  Part="1" 
AR Path="/D1CA184B367E99" Ref="#PWR030"  Part="1" 
AR Path="/7E428DAC4B367E99" Ref="#PWR030"  Part="1" 
AR Path="/F4BF4B367E99" Ref="#PWR026"  Part="1" 
AR Path="/262F604B367E99" Ref="#PWR026"  Part="1" 
AR Path="/22C6E204B367E99" Ref="#PWR026"  Part="1" 
AR Path="/384B367E99" Ref="#PWR027"  Part="1" 
F 0 "#PWR026" H 20050 14250 30  0001 C CNN
F 1 "GND" H 20050 14180 30  0001 C CNN
	1    20050 14250
	1    0    0    -1  
$EndComp
$Comp
L 74LS01 U?
U 4 1 4B367E68
P 2200 9400
AR Path="/23D9D84B367E68" Ref="U?"  Part="1" 
AR Path="/394433324B367E68" Ref="U?"  Part="1" 
AR Path="/23D6544B367E68" Ref="U?"  Part="1" 
AR Path="/7E44048F4B367E68" Ref="U?"  Part="1" 
AR Path="/16E0BEA4B367E68" Ref="U"  Part="4" 
AR Path="/FFFFFFFF4B367E68" Ref="U10"  Part="4" 
AR Path="/4B367E68" Ref="U10"  Part="4" 
AR Path="/94B367E68" Ref="U10"  Part="4" 
AR Path="/5AD7153D4B367E68" Ref="U10"  Part="4" 
AR Path="/DCBAABCD4B367E68" Ref="U10"  Part="4" 
AR Path="/54B367E68" Ref="U10"  Part="4" 
AR Path="/A84B367E68" Ref="U10"  Part="4" 
AR Path="/14B367E68" Ref="U10"  Part="4" 
AR Path="/40293BE74B367E68" Ref="U10"  Part="4" 
AR Path="/23D2034B367E68" Ref="U10"  Part="4" 
AR Path="/6FF0DD404B367E68" Ref="U10"  Part="4" 
AR Path="/402C55814B367E68" Ref="U10"  Part="4" 
AR Path="/40273BE74B367E68" Ref="U10"  Part="4" 
AR Path="/2600004B367E68" Ref="U10"  Part="4" 
AR Path="/3D8EA0004B367E68" Ref="U10"  Part="4" 
AR Path="/6FE901F74B367E68" Ref="U10"  Part="4" 
AR Path="/402908B44B367E68" Ref="U10"  Part="4" 
AR Path="/A4B367E68" Ref="U10"  Part="4" 
AR Path="/3D6CC0004B367E68" Ref="U10"  Part="4" 
AR Path="/3D5A40004B367E68" Ref="U10"  Part="4" 
AR Path="/FFFFFFF04B367E68" Ref="U10"  Part="4" 
AR Path="/402755814B367E68" Ref="U10"  Part="4" 
AR Path="/3FEFFFFF4B367E68" Ref="U10"  Part="4" 
AR Path="/4030AAC04B367E68" Ref="U10"  Part="4" 
AR Path="/23D9304B367E68" Ref="U10"  Part="4" 
AR Path="/23D8D44B367E68" Ref="U10"  Part="4" 
AR Path="/4031DDF34B367E68" Ref="U10"  Part="4" 
AR Path="/3FE88B434B367E68" Ref="U10"  Part="4" 
AR Path="/4032778D4B367E68" Ref="U10"  Part="4" 
AR Path="/403091264B367E68" Ref="U10"  Part="4" 
AR Path="/403051264B367E68" Ref="U10"  Part="4" 
AR Path="/4032F78D4B367E68" Ref="U10"  Part="4" 
AR Path="/403251264B367E68" Ref="U10"  Part="4" 
AR Path="/4032AAC04B367E68" Ref="U10"  Part="4" 
AR Path="/4030D1264B367E68" Ref="U10"  Part="4" 
AR Path="/4031778D4B367E68" Ref="U10"  Part="4" 
AR Path="/3FEA24DD4B367E68" Ref="U10"  Part="4" 
AR Path="/6FE934E34B367E68" Ref="U10"  Part="4" 
AR Path="/773F8EB44B367E68" Ref="U10"  Part="4" 
AR Path="/23C9F04B367E68" Ref="U10"  Part="4" 
AR Path="/24B367E68" Ref="U10"  Part="4" 
AR Path="/23BC884B367E68" Ref="U10"  Part="4" 
AR Path="/DC0C124B367E68" Ref="U10"  Part="4" 
AR Path="/23C34C4B367E68" Ref="U10"  Part="4" 
AR Path="/23CBC44B367E68" Ref="U10"  Part="4" 
AR Path="/23C6504B367E68" Ref="U10"  Part="4" 
AR Path="/39803EA4B367E68" Ref="U10"  Part="4" 
AR Path="/D058A04B367E68" Ref="U10"  Part="4" 
AR Path="/1607D44B367E68" Ref="U10"  Part="4" 
AR Path="/D1C3804B367E68" Ref="U10"  Part="4" 
AR Path="/23D70C4B367E68" Ref="U10"  Part="4" 
AR Path="/2104B367E68" Ref="U10"  Part="4" 
AR Path="/D1CA184B367E68" Ref="U10"  Part="4" 
AR Path="/F4BF4B367E68" Ref="U10"  Part="4" 
AR Path="/262F604B367E68" Ref="U10"  Part="4" 
AR Path="/22C6E204B367E68" Ref="U10"  Part="4" 
AR Path="/384B367E68" Ref="U10"  Part="4" 
F 0 "U10" H 2200 9450 60  0000 C CNN
F 1 "74LS01" H 2200 9350 60  0000 C CNN
F 2 "14dip300" H 2200 9400 60  0001 C CNN
	4    2200 9400
	1    0    0    -1  
$EndComp
$Comp
L 74LS01 U?
U 2 1 4B367E5B
P 14550 2300
AR Path="/23D9B04B367E5B" Ref="U?"  Part="1" 
AR Path="/394433324B367E5B" Ref="U?"  Part="1" 
AR Path="/74B367E5B" Ref="U?"  Part="1" 
AR Path="/7E44048F4B367E5B" Ref="U?"  Part="1" 
AR Path="/1E30AF04B367E5B" Ref="U"  Part="2" 
AR Path="/FFFFFFFF4B367E5B" Ref="U10"  Part="2" 
AR Path="/4B367E5B" Ref="U10"  Part="2" 
AR Path="/94B367E5B" Ref="U10"  Part="2" 
AR Path="/5AD7153D4B367E5B" Ref="U10"  Part="2" 
AR Path="/DCBAABCD4B367E5B" Ref="U10"  Part="2" 
AR Path="/54B367E5B" Ref="U10"  Part="2" 
AR Path="/A84B367E5B" Ref="U10"  Part="2" 
AR Path="/6FE901F74B367E5B" Ref="U10"  Part="2" 
AR Path="/40293BE74B367E5B" Ref="U10"  Part="2" 
AR Path="/23D2034B367E5B" Ref="U10"  Part="2" 
AR Path="/6FF0DD404B367E5B" Ref="U10"  Part="2" 
AR Path="/402C55814B367E5B" Ref="U10"  Part="2" 
AR Path="/40273BE74B367E5B" Ref="U10"  Part="2" 
AR Path="/2600004B367E5B" Ref="U10"  Part="2" 
AR Path="/3D8EA0004B367E5B" Ref="U10"  Part="2" 
AR Path="/402908B44B367E5B" Ref="U10"  Part="2" 
AR Path="/A4B367E5B" Ref="U10"  Part="2" 
AR Path="/3D6CC0004B367E5B" Ref="U10"  Part="2" 
AR Path="/3D5A40004B367E5B" Ref="U10"  Part="2" 
AR Path="/FFFFFFF04B367E5B" Ref="U10"  Part="2" 
AR Path="/402755814B367E5B" Ref="U10"  Part="2" 
AR Path="/3FEFFFFF4B367E5B" Ref="U10"  Part="2" 
AR Path="/4030AAC04B367E5B" Ref="U10"  Part="2" 
AR Path="/23D9304B367E5B" Ref="U10"  Part="2" 
AR Path="/23D8D44B367E5B" Ref="U10"  Part="2" 
AR Path="/4031DDF34B367E5B" Ref="U10"  Part="2" 
AR Path="/3FE88B434B367E5B" Ref="U10"  Part="2" 
AR Path="/4032778D4B367E5B" Ref="U10"  Part="2" 
AR Path="/403091264B367E5B" Ref="U10"  Part="2" 
AR Path="/403051264B367E5B" Ref="U10"  Part="2" 
AR Path="/4032F78D4B367E5B" Ref="U10"  Part="2" 
AR Path="/403251264B367E5B" Ref="U10"  Part="2" 
AR Path="/4032AAC04B367E5B" Ref="U10"  Part="2" 
AR Path="/4030D1264B367E5B" Ref="U10"  Part="2" 
AR Path="/4031778D4B367E5B" Ref="U10"  Part="2" 
AR Path="/3FEA24DD4B367E5B" Ref="U10"  Part="2" 
AR Path="/6FE934E34B367E5B" Ref="U10"  Part="2" 
AR Path="/773F8EB44B367E5B" Ref="U10"  Part="2" 
AR Path="/23C9F04B367E5B" Ref="U10"  Part="2" 
AR Path="/23BC884B367E5B" Ref="U10"  Part="2" 
AR Path="/DC0C124B367E5B" Ref="U10"  Part="2" 
AR Path="/23C34C4B367E5B" Ref="U10"  Part="2" 
AR Path="/23CBC44B367E5B" Ref="U10"  Part="2" 
AR Path="/23C6504B367E5B" Ref="U10"  Part="2" 
AR Path="/39803EA4B367E5B" Ref="U10"  Part="2" 
AR Path="/D058A04B367E5B" Ref="U10"  Part="2" 
AR Path="/1607D44B367E5B" Ref="U10"  Part="2" 
AR Path="/D1C3804B367E5B" Ref="U10"  Part="2" 
AR Path="/24B367E5B" Ref="U10"  Part="2" 
AR Path="/23D70C4B367E5B" Ref="U10"  Part="2" 
AR Path="/14B367E5B" Ref="U10"  Part="2" 
AR Path="/2104B367E5B" Ref="U10"  Part="2" 
AR Path="/F4BF4B367E5B" Ref="U10"  Part="2" 
AR Path="/262F604B367E5B" Ref="U10"  Part="2" 
AR Path="/22C6E204B367E5B" Ref="U10"  Part="2" 
AR Path="/384B367E5B" Ref="U10"  Part="2" 
F 0 "U10" H 14550 2350 60  0000 C CNN
F 1 "74LS01" H 14550 2250 60  0000 C CNN
F 2 "14dip300" H 14550 2300 60  0001 C CNN
	2    14550 2300
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U?
U 3 1 4B367ABC
P 20650 12400
AR Path="/69698AFC4B367ABC" Ref="U?"  Part="1" 
AR Path="/393639364B367ABC" Ref="U?"  Part="1" 
AR Path="/23D6B44B367ABC" Ref="U?"  Part="1" 
AR Path="/7E44048F4B367ABC" Ref="U?"  Part="1" 
AR Path="/14E0BEA4B367ABC" Ref="U"  Part="3" 
AR Path="/FFFFFFFF4B367ABC" Ref="U17"  Part="3" 
AR Path="/5AD7153D4B367ABC" Ref="U17"  Part="3" 
AR Path="/DCBAABCD4B367ABC" Ref="U17"  Part="3" 
AR Path="/4B367ABC" Ref="U17"  Part="3" 
AR Path="/23D2034B367ABC" Ref="U17"  Part="3" 
AR Path="/A84B367ABC" Ref="U17"  Part="3" 
AR Path="/4E4B367ABC" Ref="U17"  Part="3" 
AR Path="/94B367ABC" Ref="U17"  Part="3" 
AR Path="/6FE901F74B367ABC" Ref="U17"  Part="3" 
AR Path="/402BEF1A4B367ABC" Ref="U17"  Part="3" 
AR Path="/54B367ABC" Ref="U17"  Part="3" 
AR Path="/14B367ABC" Ref="U17"  Part="3" 
AR Path="/40293BE74B367ABC" Ref="U17"  Part="3" 
AR Path="/6FF0DD404B367ABC" Ref="U17"  Part="3" 
AR Path="/402C55814B367ABC" Ref="U17"  Part="3" 
AR Path="/40273BE74B367ABC" Ref="U17"  Part="3" 
AR Path="/2600004B367ABC" Ref="U17"  Part="3" 
AR Path="/3D8EA0004B367ABC" Ref="U17"  Part="3" 
AR Path="/402908B44B367ABC" Ref="U17"  Part="3" 
AR Path="/A4B367ABC" Ref="U17"  Part="3" 
AR Path="/3D6CC0004B367ABC" Ref="U17"  Part="3" 
AR Path="/3D5A40004B367ABC" Ref="U17"  Part="3" 
AR Path="/FFFFFFF04B367ABC" Ref="U17"  Part="3" 
AR Path="/402755814B367ABC" Ref="U17"  Part="3" 
AR Path="/3FEFFFFF4B367ABC" Ref="U17"  Part="3" 
AR Path="/4030AAC04B367ABC" Ref="U17"  Part="3" 
AR Path="/2B70A4E4B367ABC" Ref="U?"  Part="3" 
AR Path="/23D9304B367ABC" Ref="U17"  Part="3" 
AR Path="/23D8D44B367ABC" Ref="U17"  Part="3" 
AR Path="/4031DDF34B367ABC" Ref="U17"  Part="3" 
AR Path="/3FE88B434B367ABC" Ref="U17"  Part="3" 
AR Path="/4032778D4B367ABC" Ref="U17"  Part="3" 
AR Path="/403091264B367ABC" Ref="U17"  Part="3" 
AR Path="/403051264B367ABC" Ref="U17"  Part="3" 
AR Path="/4032F78D4B367ABC" Ref="U17"  Part="3" 
AR Path="/403251264B367ABC" Ref="U17"  Part="3" 
AR Path="/4032AAC04B367ABC" Ref="U17"  Part="3" 
AR Path="/4030D1264B367ABC" Ref="U17"  Part="3" 
AR Path="/4031778D4B367ABC" Ref="U17"  Part="3" 
AR Path="/3FEA24DD4B367ABC" Ref="U17"  Part="3" 
AR Path="/6FE934E34B367ABC" Ref="U17"  Part="3" 
AR Path="/773F8EB44B367ABC" Ref="U17"  Part="3" 
AR Path="/23C9F04B367ABC" Ref="U17"  Part="3" 
AR Path="/23BC884B367ABC" Ref="U17"  Part="3" 
AR Path="/DC0C124B367ABC" Ref="U17"  Part="3" 
AR Path="/23C34C4B367ABC" Ref="U17"  Part="3" 
AR Path="/23CBC44B367ABC" Ref="U17"  Part="3" 
AR Path="/23C6504B367ABC" Ref="U17"  Part="3" 
AR Path="/39803EA4B367ABC" Ref="U17"  Part="3" 
AR Path="/D058A04B367ABC" Ref="U17"  Part="3" 
AR Path="/1607D44B367ABC" Ref="U17"  Part="3" 
AR Path="/D1C3804B367ABC" Ref="U17"  Part="3" 
AR Path="/24B367ABC" Ref="U17"  Part="3" 
AR Path="/23D70C4B367ABC" Ref="U17"  Part="3" 
AR Path="/2104B367ABC" Ref="U17"  Part="3" 
AR Path="/F4BF4B367ABC" Ref="U17"  Part="3" 
AR Path="/262F604B367ABC" Ref="U17"  Part="3" 
AR Path="/384B367ABC" Ref="U17"  Part="3" 
F 0 "U17" H 20650 12450 60  0000 C CNN
F 1 "74LS08" H 20650 12350 60  0000 C CNN
F 2 "14dip300" H 20650 12400 60  0001 C CNN
	3    20650 12400
	1    0    0    1   
$EndComp
$Comp
L 74LS139 U?
U 2 1 4B36790C
P 6500 1600
AR Path="/23D9B04B36790C" Ref="U?"  Part="1" 
AR Path="/394433324B36790C" Ref="U?"  Part="1" 
AR Path="/23D6B44B36790C" Ref="U?"  Part="1" 
AR Path="/7E44048F4B36790C" Ref="U?"  Part="1" 
AR Path="/1440BEA4B36790C" Ref="U"  Part="2" 
AR Path="/4B36790C" Ref="U4"  Part="2" 
AR Path="/A84B36790C" Ref="U4"  Part="2" 
AR Path="/5AD7153D4B36790C" Ref="U4"  Part="2" 
AR Path="/DCBAABCD4B36790C" Ref="U4"  Part="2" 
AR Path="/A4B36790C" Ref="U4"  Part="2" 
AR Path="/14B36790C" Ref="U4"  Part="2" 
AR Path="/6FF0DD404B36790C" Ref="U4"  Part="2" 
AR Path="/402C08B44B36790C" Ref="U4"  Part="2" 
AR Path="/23D2034B36790C" Ref="U4"  Part="2" 
AR Path="/4E4B36790C" Ref="U4"  Part="2" 
AR Path="/94B36790C" Ref="U4"  Part="2" 
AR Path="/402BEF1A4B36790C" Ref="U4"  Part="2" 
AR Path="/54B36790C" Ref="U4"  Part="2" 
AR Path="/40293BE74B36790C" Ref="U4"  Part="2" 
AR Path="/402C55814B36790C" Ref="U4"  Part="2" 
AR Path="/40273BE74B36790C" Ref="U4"  Part="2" 
AR Path="/2600004B36790C" Ref="U4"  Part="2" 
AR Path="/3D8EA0004B36790C" Ref="U4"  Part="2" 
AR Path="/6FE901F74B36790C" Ref="U4"  Part="2" 
AR Path="/402908B44B36790C" Ref="U4"  Part="2" 
AR Path="/3D6CC0004B36790C" Ref="U4"  Part="2" 
AR Path="/3D5A40004B36790C" Ref="U4"  Part="2" 
AR Path="/FFFFFFF04B36790C" Ref="U4"  Part="2" 
AR Path="/402755814B36790C" Ref="U4"  Part="2" 
AR Path="/3FEFFFFF4B36790C" Ref="U4"  Part="2" 
AR Path="/4030AAC04B36790C" Ref="U4"  Part="2" 
AR Path="/23D9304B36790C" Ref="U4"  Part="2" 
AR Path="/23D8D44B36790C" Ref="U4"  Part="2" 
AR Path="/4031DDF34B36790C" Ref="U4"  Part="2" 
AR Path="/3FE88B434B36790C" Ref="U4"  Part="2" 
AR Path="/4032778D4B36790C" Ref="U4"  Part="2" 
AR Path="/403091264B36790C" Ref="U4"  Part="2" 
AR Path="/403051264B36790C" Ref="U4"  Part="2" 
AR Path="/4032F78D4B36790C" Ref="U4"  Part="2" 
AR Path="/403251264B36790C" Ref="U4"  Part="2" 
AR Path="/4032AAC04B36790C" Ref="U4"  Part="2" 
AR Path="/4030D1264B36790C" Ref="U4"  Part="2" 
AR Path="/4031778D4B36790C" Ref="U4"  Part="2" 
AR Path="/3FEA24DD4B36790C" Ref="U4"  Part="2" 
AR Path="/6FE934E34B36790C" Ref="U4"  Part="2" 
AR Path="/773F8EB44B36790C" Ref="U4"  Part="2" 
AR Path="/23C9F04B36790C" Ref="U4"  Part="2" 
AR Path="/24B36790C" Ref="U4"  Part="2" 
AR Path="/23BC884B36790C" Ref="U4"  Part="2" 
AR Path="/DC0C124B36790C" Ref="U4"  Part="2" 
AR Path="/23C34C4B36790C" Ref="U4"  Part="2" 
AR Path="/23CBC44B36790C" Ref="U4"  Part="2" 
AR Path="/23C6504B36790C" Ref="U4"  Part="2" 
AR Path="/39803EA4B36790C" Ref="U4"  Part="2" 
AR Path="/FFFFFFFF4B36790C" Ref="U4"  Part="2" 
AR Path="/D058A04B36790C" Ref="U4"  Part="2" 
AR Path="/1607D44B36790C" Ref="U4"  Part="2" 
AR Path="/D1C3804B36790C" Ref="U4"  Part="2" 
AR Path="/23D70C4B36790C" Ref="U4"  Part="2" 
AR Path="/2104B36790C" Ref="U4"  Part="2" 
AR Path="/F4BF4B36790C" Ref="U4"  Part="2" 
AR Path="/262F604B36790C" Ref="U4"  Part="2" 
AR Path="/384B36790C" Ref="U4"  Part="2" 
F 0 "U4" H 6500 1700 60  0000 C CNN
F 1 "74LS139" H 6500 1500 60  0000 C CNN
F 2 "16dip300" H 6500 1600 60  0001 C CNN
	2    6500 1600
	1    0    0    -1  
$EndComp
$Comp
L C C36
U 1 1 4B366FFB
P 14100 14950
AR Path="/4B366FFB" Ref="C36"  Part="1" 
AR Path="/94B366FFB" Ref="C36"  Part="1" 
AR Path="/14B366FFB" Ref="C36"  Part="1" 
AR Path="/6FF0DD404B366FFB" Ref="C36"  Part="1" 
AR Path="/FFFFFFF04B366FFB" Ref="C36"  Part="1" 
AR Path="/5AD7153D4B366FFB" Ref="C36"  Part="1" 
AR Path="/402955814B366FFB" Ref="C36"  Part="1" 
AR Path="/6FE901F74B366FFB" Ref="C36"  Part="1" 
AR Path="/3FEFFFFF4B366FFB" Ref="C36"  Part="1" 
AR Path="/DCBAABCD4B366FFB" Ref="C36"  Part="1" 
AR Path="/A84B366FFB" Ref="C36"  Part="1" 
AR Path="/A4B366FFB" Ref="C36"  Part="1" 
AR Path="/402C08B44B366FFB" Ref="C36"  Part="1" 
AR Path="/23D2034B366FFB" Ref="C36"  Part="1" 
AR Path="/4E4B366FFB" Ref="C36"  Part="1" 
AR Path="/402BEF1A4B366FFB" Ref="C36"  Part="1" 
AR Path="/54B366FFB" Ref="C36"  Part="1" 
AR Path="/40293BE74B366FFB" Ref="C36"  Part="1" 
AR Path="/402C55814B366FFB" Ref="C36"  Part="1" 
AR Path="/40273BE74B366FFB" Ref="C36"  Part="1" 
AR Path="/2600004B366FFB" Ref="C36"  Part="1" 
AR Path="/3D8EA0004B366FFB" Ref="C36"  Part="1" 
AR Path="/402908B44B366FFB" Ref="C36"  Part="1" 
AR Path="/3D6CC0004B366FFB" Ref="C36"  Part="1" 
AR Path="/3D5A40004B366FFB" Ref="C36"  Part="1" 
AR Path="/402755814B366FFB" Ref="C36"  Part="1" 
AR Path="/4030AAC04B366FFB" Ref="C36"  Part="1" 
AR Path="/23D9304B366FFB" Ref="C36"  Part="1" 
AR Path="/23D8D44B366FFB" Ref="C36"  Part="1" 
AR Path="/4031DDF34B366FFB" Ref="C36"  Part="1" 
AR Path="/3FE88B434B366FFB" Ref="C36"  Part="1" 
AR Path="/4032778D4B366FFB" Ref="C36"  Part="1" 
AR Path="/403091264B366FFB" Ref="C36"  Part="1" 
AR Path="/403051264B366FFB" Ref="C36"  Part="1" 
AR Path="/4032F78D4B366FFB" Ref="C36"  Part="1" 
AR Path="/403251264B366FFB" Ref="C36"  Part="1" 
AR Path="/4032AAC04B366FFB" Ref="C36"  Part="1" 
AR Path="/4030D1264B366FFB" Ref="C36"  Part="1" 
AR Path="/4031778D4B366FFB" Ref="C36"  Part="1" 
AR Path="/3FEA24DD4B366FFB" Ref="C36"  Part="1" 
AR Path="/6FE934E34B366FFB" Ref="C36"  Part="1" 
AR Path="/773F65F14B366FFB" Ref="C36"  Part="1" 
AR Path="/773F8EB44B366FFB" Ref="C36"  Part="1" 
AR Path="/23C9F04B366FFB" Ref="C36"  Part="1" 
AR Path="/24B366FFB" Ref="C36"  Part="1" 
AR Path="/23BC884B366FFB" Ref="C36"  Part="1" 
AR Path="/DC0C124B366FFB" Ref="C36"  Part="1" 
AR Path="/23C34C4B366FFB" Ref="C36"  Part="1" 
AR Path="/23CBC44B366FFB" Ref="C36"  Part="1" 
AR Path="/23C6504B366FFB" Ref="C36"  Part="1" 
AR Path="/39803EA4B366FFB" Ref="C36"  Part="1" 
AR Path="/FFFFFFFF4B366FFB" Ref="C36"  Part="1" 
AR Path="/D058A04B366FFB" Ref="C36"  Part="1" 
AR Path="/1607D44B366FFB" Ref="C36"  Part="1" 
AR Path="/D1C3804B366FFB" Ref="C36"  Part="1" 
AR Path="/23D70C4B366FFB" Ref="C36"  Part="1" 
AR Path="/2104B366FFB" Ref="C36"  Part="1" 
AR Path="/F4BF4B366FFB" Ref="C36"  Part="1" 
AR Path="/262F604B366FFB" Ref="C36"  Part="1" 
AR Path="/384B366FFB" Ref="C36"  Part="1" 
F 0 "C36" H 14150 15050 50  0000 L CNN
F 1 "0.1 uF" H 14150 14850 50  0000 L CNN
F 2 "C2" H 14100 14950 60  0001 C CNN
	1    14100 14950
	1    0    0    -1  
$EndComp
$Comp
L C C25
U 1 1 4B366286
P 15000 14350
AR Path="/4B366286" Ref="C25"  Part="1" 
AR Path="/94B366286" Ref="C25"  Part="1" 
AR Path="/5AD7153D4B366286" Ref="C25"  Part="1" 
AR Path="/DCBAABCD4B366286" Ref="C25"  Part="1" 
AR Path="/23D9304B366286" Ref="C25"  Part="1" 
AR Path="/6FE901F74B366286" Ref="C25"  Part="1" 
AR Path="/3FE224DD4B366286" Ref="C25"  Part="1" 
AR Path="/3FEFFFFF4B366286" Ref="C25"  Part="1" 
AR Path="/23D8D44B366286" Ref="C25"  Part="1" 
AR Path="/14B366286" Ref="C25"  Part="1" 
AR Path="/6FF0DD404B366286" Ref="C25"  Part="1" 
AR Path="/FFFFFFF04B366286" Ref="C25"  Part="1" 
AR Path="/402955814B366286" Ref="C25"  Part="1" 
AR Path="/A84B366286" Ref="C25"  Part="1" 
AR Path="/A4B366286" Ref="C25"  Part="1" 
AR Path="/402C08B44B366286" Ref="C25"  Part="1" 
AR Path="/23D2034B366286" Ref="C25"  Part="1" 
AR Path="/4E4B366286" Ref="C25"  Part="1" 
AR Path="/402BEF1A4B366286" Ref="C25"  Part="1" 
AR Path="/54B366286" Ref="C25"  Part="1" 
AR Path="/40293BE74B366286" Ref="C25"  Part="1" 
AR Path="/402C55814B366286" Ref="C25"  Part="1" 
AR Path="/40273BE74B366286" Ref="C25"  Part="1" 
AR Path="/2600004B366286" Ref="C25"  Part="1" 
AR Path="/3D8EA0004B366286" Ref="C25"  Part="1" 
AR Path="/402908B44B366286" Ref="C25"  Part="1" 
AR Path="/3D6CC0004B366286" Ref="C25"  Part="1" 
AR Path="/3D5A40004B366286" Ref="C25"  Part="1" 
AR Path="/402755814B366286" Ref="C25"  Part="1" 
AR Path="/4030AAC04B366286" Ref="C25"  Part="1" 
AR Path="/4031DDF34B366286" Ref="C25"  Part="1" 
AR Path="/3FE88B434B366286" Ref="C25"  Part="1" 
AR Path="/4032778D4B366286" Ref="C25"  Part="1" 
AR Path="/403091264B366286" Ref="C25"  Part="1" 
AR Path="/403051264B366286" Ref="C25"  Part="1" 
AR Path="/4032F78D4B366286" Ref="C25"  Part="1" 
AR Path="/403251264B366286" Ref="C25"  Part="1" 
AR Path="/4032AAC04B366286" Ref="C25"  Part="1" 
AR Path="/4030D1264B366286" Ref="C25"  Part="1" 
AR Path="/4031778D4B366286" Ref="C25"  Part="1" 
AR Path="/3FEA24DD4B366286" Ref="C25"  Part="1" 
AR Path="/6FE934E34B366286" Ref="C25"  Part="1" 
AR Path="/773F65F14B366286" Ref="C25"  Part="1" 
AR Path="/773F8EB44B366286" Ref="C25"  Part="1" 
AR Path="/23C9F04B366286" Ref="C25"  Part="1" 
AR Path="/24B366286" Ref="C25"  Part="1" 
AR Path="/23BC884B366286" Ref="C25"  Part="1" 
AR Path="/DC0C124B366286" Ref="C25"  Part="1" 
AR Path="/23C34C4B366286" Ref="C25"  Part="1" 
AR Path="/23CBC44B366286" Ref="C25"  Part="1" 
AR Path="/23C6504B366286" Ref="C25"  Part="1" 
AR Path="/39803EA4B366286" Ref="C25"  Part="1" 
AR Path="/FFFFFFFF4B366286" Ref="C25"  Part="1" 
AR Path="/D058A04B366286" Ref="C25"  Part="1" 
AR Path="/1607D44B366286" Ref="C25"  Part="1" 
AR Path="/D1C3804B366286" Ref="C25"  Part="1" 
AR Path="/23D70C4B366286" Ref="C25"  Part="1" 
AR Path="/2104B366286" Ref="C25"  Part="1" 
AR Path="/F4BF4B366286" Ref="C25"  Part="1" 
AR Path="/262F604B366286" Ref="C25"  Part="1" 
AR Path="/384B366286" Ref="C25"  Part="1" 
F 0 "C25" H 15050 14450 50  0000 L CNN
F 1 "0.1 uF" H 15050 14250 50  0000 L CNN
F 2 "C2" H 15000 14350 60  0001 C CNN
	1    15000 14350
	1    0    0    -1  
$EndComp
$Comp
L C C22
U 1 1 4B366285
P 14100 14350
AR Path="/4B366285" Ref="C22"  Part="1" 
AR Path="/94B366285" Ref="C22"  Part="1" 
AR Path="/5AD7153D4B366285" Ref="C22"  Part="1" 
AR Path="/23D9304B366285" Ref="C22"  Part="1" 
AR Path="/6FE901F74B366285" Ref="C22"  Part="1" 
AR Path="/3FE224DD4B366285" Ref="C22"  Part="1" 
AR Path="/3FEFFFFF4B366285" Ref="C22"  Part="1" 
AR Path="/23D8D44B366285" Ref="C22"  Part="1" 
AR Path="/14B366285" Ref="C22"  Part="1" 
AR Path="/6FF0DD404B366285" Ref="C22"  Part="1" 
AR Path="/FFFFFFF04B366285" Ref="C22"  Part="1" 
AR Path="/402955814B366285" Ref="C22"  Part="1" 
AR Path="/DCBAABCD4B366285" Ref="C22"  Part="1" 
AR Path="/A84B366285" Ref="C22"  Part="1" 
AR Path="/A4B366285" Ref="C22"  Part="1" 
AR Path="/402C08B44B366285" Ref="C22"  Part="1" 
AR Path="/23D2034B366285" Ref="C22"  Part="1" 
AR Path="/4E4B366285" Ref="C22"  Part="1" 
AR Path="/402BEF1A4B366285" Ref="C22"  Part="1" 
AR Path="/54B366285" Ref="C22"  Part="1" 
AR Path="/40293BE74B366285" Ref="C22"  Part="1" 
AR Path="/402C55814B366285" Ref="C22"  Part="1" 
AR Path="/40273BE74B366285" Ref="C22"  Part="1" 
AR Path="/2600004B366285" Ref="C22"  Part="1" 
AR Path="/3D8EA0004B366285" Ref="C22"  Part="1" 
AR Path="/402908B44B366285" Ref="C22"  Part="1" 
AR Path="/3D6CC0004B366285" Ref="C22"  Part="1" 
AR Path="/3D5A40004B366285" Ref="C22"  Part="1" 
AR Path="/402755814B366285" Ref="C22"  Part="1" 
AR Path="/4030AAC04B366285" Ref="C22"  Part="1" 
AR Path="/4031DDF34B366285" Ref="C22"  Part="1" 
AR Path="/3FE88B434B366285" Ref="C22"  Part="1" 
AR Path="/4032778D4B366285" Ref="C22"  Part="1" 
AR Path="/403091264B366285" Ref="C22"  Part="1" 
AR Path="/403051264B366285" Ref="C22"  Part="1" 
AR Path="/4032F78D4B366285" Ref="C22"  Part="1" 
AR Path="/403251264B366285" Ref="C22"  Part="1" 
AR Path="/4032AAC04B366285" Ref="C22"  Part="1" 
AR Path="/4030D1264B366285" Ref="C22"  Part="1" 
AR Path="/4031778D4B366285" Ref="C22"  Part="1" 
AR Path="/3FEA24DD4B366285" Ref="C22"  Part="1" 
AR Path="/6FE934E34B366285" Ref="C22"  Part="1" 
AR Path="/773F65F14B366285" Ref="C22"  Part="1" 
AR Path="/773F8EB44B366285" Ref="C22"  Part="1" 
AR Path="/23C9F04B366285" Ref="C22"  Part="1" 
AR Path="/24B366285" Ref="C22"  Part="1" 
AR Path="/23BC884B366285" Ref="C22"  Part="1" 
AR Path="/DC0C124B366285" Ref="C22"  Part="1" 
AR Path="/23C34C4B366285" Ref="C22"  Part="1" 
AR Path="/23CBC44B366285" Ref="C22"  Part="1" 
AR Path="/23C6504B366285" Ref="C22"  Part="1" 
AR Path="/39803EA4B366285" Ref="C22"  Part="1" 
AR Path="/FFFFFFFF4B366285" Ref="C22"  Part="1" 
AR Path="/D058A04B366285" Ref="C22"  Part="1" 
AR Path="/1607D44B366285" Ref="C22"  Part="1" 
AR Path="/D1C3804B366285" Ref="C22"  Part="1" 
AR Path="/23D70C4B366285" Ref="C22"  Part="1" 
AR Path="/2104B366285" Ref="C22"  Part="1" 
AR Path="/F4BF4B366285" Ref="C22"  Part="1" 
AR Path="/262F604B366285" Ref="C22"  Part="1" 
AR Path="/384B366285" Ref="C22"  Part="1" 
F 0 "C22" H 14150 14450 50  0000 L CNN
F 1 "0.1 uF" H 14150 14250 50  0000 L CNN
F 2 "C2" H 14100 14350 60  0001 C CNN
	1    14100 14350
	1    0    0    -1  
$EndComp
$Comp
L C C23
U 1 1 4B366284
P 14550 14350
AR Path="/4B366284" Ref="C23"  Part="1" 
AR Path="/94B366284" Ref="C23"  Part="1" 
AR Path="/5AD7153D4B366284" Ref="C23"  Part="1" 
AR Path="/23D9304B366284" Ref="C23"  Part="1" 
AR Path="/6FE901F74B366284" Ref="C23"  Part="1" 
AR Path="/3FE224DD4B366284" Ref="C23"  Part="1" 
AR Path="/3FEFFFFF4B366284" Ref="C23"  Part="1" 
AR Path="/23D8D44B366284" Ref="C23"  Part="1" 
AR Path="/14B366284" Ref="C23"  Part="1" 
AR Path="/6FF0DD404B366284" Ref="C23"  Part="1" 
AR Path="/FFFFFFF04B366284" Ref="C23"  Part="1" 
AR Path="/402955814B366284" Ref="C23"  Part="1" 
AR Path="/DCBAABCD4B366284" Ref="C23"  Part="1" 
AR Path="/A84B366284" Ref="C23"  Part="1" 
AR Path="/A4B366284" Ref="C23"  Part="1" 
AR Path="/402C08B44B366284" Ref="C23"  Part="1" 
AR Path="/23D2034B366284" Ref="C23"  Part="1" 
AR Path="/4E4B366284" Ref="C23"  Part="1" 
AR Path="/402BEF1A4B366284" Ref="C23"  Part="1" 
AR Path="/54B366284" Ref="C23"  Part="1" 
AR Path="/40293BE74B366284" Ref="C23"  Part="1" 
AR Path="/402C55814B366284" Ref="C23"  Part="1" 
AR Path="/40273BE74B366284" Ref="C23"  Part="1" 
AR Path="/2600004B366284" Ref="C23"  Part="1" 
AR Path="/3D8EA0004B366284" Ref="C23"  Part="1" 
AR Path="/402908B44B366284" Ref="C23"  Part="1" 
AR Path="/3D6CC0004B366284" Ref="C23"  Part="1" 
AR Path="/3D5A40004B366284" Ref="C23"  Part="1" 
AR Path="/402755814B366284" Ref="C23"  Part="1" 
AR Path="/4030AAC04B366284" Ref="C23"  Part="1" 
AR Path="/4031DDF34B366284" Ref="C23"  Part="1" 
AR Path="/3FE88B434B366284" Ref="C23"  Part="1" 
AR Path="/4032778D4B366284" Ref="C23"  Part="1" 
AR Path="/403091264B366284" Ref="C23"  Part="1" 
AR Path="/403051264B366284" Ref="C23"  Part="1" 
AR Path="/4032F78D4B366284" Ref="C23"  Part="1" 
AR Path="/403251264B366284" Ref="C23"  Part="1" 
AR Path="/4032AAC04B366284" Ref="C23"  Part="1" 
AR Path="/4030D1264B366284" Ref="C23"  Part="1" 
AR Path="/4031778D4B366284" Ref="C23"  Part="1" 
AR Path="/3FEA24DD4B366284" Ref="C23"  Part="1" 
AR Path="/6FE934E34B366284" Ref="C23"  Part="1" 
AR Path="/773F65F14B366284" Ref="C23"  Part="1" 
AR Path="/773F8EB44B366284" Ref="C23"  Part="1" 
AR Path="/23C9F04B366284" Ref="C23"  Part="1" 
AR Path="/24B366284" Ref="C23"  Part="1" 
AR Path="/23BC884B366284" Ref="C23"  Part="1" 
AR Path="/DC0C124B366284" Ref="C23"  Part="1" 
AR Path="/23C34C4B366284" Ref="C23"  Part="1" 
AR Path="/23CBC44B366284" Ref="C23"  Part="1" 
AR Path="/23C6504B366284" Ref="C23"  Part="1" 
AR Path="/39803EA4B366284" Ref="C23"  Part="1" 
AR Path="/FFFFFFFF4B366284" Ref="C23"  Part="1" 
AR Path="/D058A04B366284" Ref="C23"  Part="1" 
AR Path="/1607D44B366284" Ref="C23"  Part="1" 
AR Path="/D1C3804B366284" Ref="C23"  Part="1" 
AR Path="/23D70C4B366284" Ref="C23"  Part="1" 
AR Path="/2104B366284" Ref="C23"  Part="1" 
AR Path="/F4BF4B366284" Ref="C23"  Part="1" 
AR Path="/262F604B366284" Ref="C23"  Part="1" 
AR Path="/384B366284" Ref="C23"  Part="1" 
F 0 "C23" H 14600 14450 50  0000 L CNN
F 1 "0.1 uF" H 14600 14250 50  0000 L CNN
F 2 "C2" H 14550 14350 60  0001 C CNN
	1    14550 14350
	1    0    0    -1  
$EndComp
$Comp
L C C29
U 1 1 4B366280
P 15900 14350
AR Path="/4B366280" Ref="C29"  Part="1" 
AR Path="/94B366280" Ref="C29"  Part="1" 
AR Path="/5AD7153D4B366280" Ref="C29"  Part="1" 
AR Path="/23D9304B366280" Ref="C29"  Part="1" 
AR Path="/6FE901F74B366280" Ref="C29"  Part="1" 
AR Path="/3FE224DD4B366280" Ref="C29"  Part="1" 
AR Path="/3FEFFFFF4B366280" Ref="C29"  Part="1" 
AR Path="/23D8D44B366280" Ref="C29"  Part="1" 
AR Path="/14B366280" Ref="C29"  Part="1" 
AR Path="/6FF0DD404B366280" Ref="C29"  Part="1" 
AR Path="/FFFFFFF04B366280" Ref="C29"  Part="1" 
AR Path="/402955814B366280" Ref="C29"  Part="1" 
AR Path="/DCBAABCD4B366280" Ref="C29"  Part="1" 
AR Path="/A84B366280" Ref="C29"  Part="1" 
AR Path="/A4B366280" Ref="C29"  Part="1" 
AR Path="/402C08B44B366280" Ref="C29"  Part="1" 
AR Path="/23D2034B366280" Ref="C29"  Part="1" 
AR Path="/4E4B366280" Ref="C29"  Part="1" 
AR Path="/402BEF1A4B366280" Ref="C29"  Part="1" 
AR Path="/54B366280" Ref="C29"  Part="1" 
AR Path="/40293BE74B366280" Ref="C29"  Part="1" 
AR Path="/402C55814B366280" Ref="C29"  Part="1" 
AR Path="/40273BE74B366280" Ref="C29"  Part="1" 
AR Path="/2600004B366280" Ref="C29"  Part="1" 
AR Path="/3D8EA0004B366280" Ref="C29"  Part="1" 
AR Path="/402908B44B366280" Ref="C29"  Part="1" 
AR Path="/3D6CC0004B366280" Ref="C29"  Part="1" 
AR Path="/3D5A40004B366280" Ref="C29"  Part="1" 
AR Path="/402755814B366280" Ref="C29"  Part="1" 
AR Path="/4030AAC04B366280" Ref="C29"  Part="1" 
AR Path="/4031DDF34B366280" Ref="C29"  Part="1" 
AR Path="/3FE88B434B366280" Ref="C29"  Part="1" 
AR Path="/4032778D4B366280" Ref="C29"  Part="1" 
AR Path="/403091264B366280" Ref="C29"  Part="1" 
AR Path="/403051264B366280" Ref="C29"  Part="1" 
AR Path="/4032F78D4B366280" Ref="C29"  Part="1" 
AR Path="/403251264B366280" Ref="C29"  Part="1" 
AR Path="/4032AAC04B366280" Ref="C29"  Part="1" 
AR Path="/4030D1264B366280" Ref="C29"  Part="1" 
AR Path="/4031778D4B366280" Ref="C29"  Part="1" 
AR Path="/3FEA24DD4B366280" Ref="C29"  Part="1" 
AR Path="/6FE934E34B366280" Ref="C29"  Part="1" 
AR Path="/773F65F14B366280" Ref="C29"  Part="1" 
AR Path="/773F8EB44B366280" Ref="C29"  Part="1" 
AR Path="/23C9F04B366280" Ref="C29"  Part="1" 
AR Path="/24B366280" Ref="C29"  Part="1" 
AR Path="/23BC884B366280" Ref="C29"  Part="1" 
AR Path="/DC0C124B366280" Ref="C29"  Part="1" 
AR Path="/23C34C4B366280" Ref="C29"  Part="1" 
AR Path="/23CBC44B366280" Ref="C29"  Part="1" 
AR Path="/23C6504B366280" Ref="C29"  Part="1" 
AR Path="/39803EA4B366280" Ref="C29"  Part="1" 
AR Path="/FFFFFFFF4B366280" Ref="C29"  Part="1" 
AR Path="/D058A04B366280" Ref="C29"  Part="1" 
AR Path="/1607D44B366280" Ref="C29"  Part="1" 
AR Path="/D1C3804B366280" Ref="C29"  Part="1" 
AR Path="/23D70C4B366280" Ref="C29"  Part="1" 
AR Path="/2104B366280" Ref="C29"  Part="1" 
AR Path="/F4BF4B366280" Ref="C29"  Part="1" 
AR Path="/262F604B366280" Ref="C29"  Part="1" 
AR Path="/384B366280" Ref="C29"  Part="1" 
F 0 "C29" H 15950 14450 50  0000 L CNN
F 1 "0.1 uF" H 15950 14250 50  0000 L CNN
F 2 "C2" H 15900 14350 60  0001 C CNN
	1    15900 14350
	1    0    0    -1  
$EndComp
$Comp
L C C27
U 1 1 4B36627F
P 15450 14350
AR Path="/4B36627F" Ref="C27"  Part="1" 
AR Path="/94B36627F" Ref="C27"  Part="1" 
AR Path="/5AD7153D4B36627F" Ref="C27"  Part="1" 
AR Path="/23D9304B36627F" Ref="C27"  Part="1" 
AR Path="/6FE901F74B36627F" Ref="C27"  Part="1" 
AR Path="/3FE224DD4B36627F" Ref="C27"  Part="1" 
AR Path="/3FEFFFFF4B36627F" Ref="C27"  Part="1" 
AR Path="/23D8D44B36627F" Ref="C27"  Part="1" 
AR Path="/14B36627F" Ref="C27"  Part="1" 
AR Path="/6FF0DD404B36627F" Ref="C27"  Part="1" 
AR Path="/FFFFFFF04B36627F" Ref="C27"  Part="1" 
AR Path="/402955814B36627F" Ref="C27"  Part="1" 
AR Path="/DCBAABCD4B36627F" Ref="C27"  Part="1" 
AR Path="/A84B36627F" Ref="C27"  Part="1" 
AR Path="/A4B36627F" Ref="C27"  Part="1" 
AR Path="/402C08B44B36627F" Ref="C27"  Part="1" 
AR Path="/23D2034B36627F" Ref="C27"  Part="1" 
AR Path="/4E4B36627F" Ref="C27"  Part="1" 
AR Path="/402BEF1A4B36627F" Ref="C27"  Part="1" 
AR Path="/54B36627F" Ref="C27"  Part="1" 
AR Path="/40293BE74B36627F" Ref="C27"  Part="1" 
AR Path="/402C55814B36627F" Ref="C27"  Part="1" 
AR Path="/40273BE74B36627F" Ref="C27"  Part="1" 
AR Path="/2600004B36627F" Ref="C27"  Part="1" 
AR Path="/3D8EA0004B36627F" Ref="C27"  Part="1" 
AR Path="/402908B44B36627F" Ref="C27"  Part="1" 
AR Path="/3D6CC0004B36627F" Ref="C27"  Part="1" 
AR Path="/3D5A40004B36627F" Ref="C27"  Part="1" 
AR Path="/402755814B36627F" Ref="C27"  Part="1" 
AR Path="/4030AAC04B36627F" Ref="C27"  Part="1" 
AR Path="/4031DDF34B36627F" Ref="C27"  Part="1" 
AR Path="/3FE88B434B36627F" Ref="C27"  Part="1" 
AR Path="/4032778D4B36627F" Ref="C27"  Part="1" 
AR Path="/403091264B36627F" Ref="C27"  Part="1" 
AR Path="/403051264B36627F" Ref="C27"  Part="1" 
AR Path="/4032F78D4B36627F" Ref="C27"  Part="1" 
AR Path="/403251264B36627F" Ref="C27"  Part="1" 
AR Path="/4032AAC04B36627F" Ref="C27"  Part="1" 
AR Path="/4030D1264B36627F" Ref="C27"  Part="1" 
AR Path="/4031778D4B36627F" Ref="C27"  Part="1" 
AR Path="/3FEA24DD4B36627F" Ref="C27"  Part="1" 
AR Path="/6FE934E34B36627F" Ref="C27"  Part="1" 
AR Path="/773F65F14B36627F" Ref="C27"  Part="1" 
AR Path="/773F8EB44B36627F" Ref="C27"  Part="1" 
AR Path="/23C9F04B36627F" Ref="C27"  Part="1" 
AR Path="/24B36627F" Ref="C27"  Part="1" 
AR Path="/23BC884B36627F" Ref="C27"  Part="1" 
AR Path="/DC0C124B36627F" Ref="C27"  Part="1" 
AR Path="/23C34C4B36627F" Ref="C27"  Part="1" 
AR Path="/23CBC44B36627F" Ref="C27"  Part="1" 
AR Path="/23C6504B36627F" Ref="C27"  Part="1" 
AR Path="/39803EA4B36627F" Ref="C27"  Part="1" 
AR Path="/FFFFFFFF4B36627F" Ref="C27"  Part="1" 
AR Path="/D058A04B36627F" Ref="C27"  Part="1" 
AR Path="/1607D44B36627F" Ref="C27"  Part="1" 
AR Path="/D1C3804B36627F" Ref="C27"  Part="1" 
AR Path="/23D70C4B36627F" Ref="C27"  Part="1" 
AR Path="/2104B36627F" Ref="C27"  Part="1" 
AR Path="/F4BF4B36627F" Ref="C27"  Part="1" 
AR Path="/262F604B36627F" Ref="C27"  Part="1" 
AR Path="/384B36627F" Ref="C27"  Part="1" 
F 0 "C27" H 15500 14450 50  0000 L CNN
F 1 "0.1 uF" H 15500 14250 50  0000 L CNN
F 2 "C2" H 15450 14350 60  0001 C CNN
	1    15450 14350
	1    0    0    -1  
$EndComp
$Comp
L C C30
U 1 1 4B36627E
P 16350 14350
AR Path="/4B36627E" Ref="C30"  Part="1" 
AR Path="/94B36627E" Ref="C30"  Part="1" 
AR Path="/5AD7153D4B36627E" Ref="C30"  Part="1" 
AR Path="/23D9304B36627E" Ref="C30"  Part="1" 
AR Path="/6FE901F74B36627E" Ref="C30"  Part="1" 
AR Path="/3FE224DD4B36627E" Ref="C30"  Part="1" 
AR Path="/3FEFFFFF4B36627E" Ref="C30"  Part="1" 
AR Path="/23D8D44B36627E" Ref="C30"  Part="1" 
AR Path="/14B36627E" Ref="C30"  Part="1" 
AR Path="/6FF0DD404B36627E" Ref="C30"  Part="1" 
AR Path="/FFFFFFF04B36627E" Ref="C30"  Part="1" 
AR Path="/402955814B36627E" Ref="C30"  Part="1" 
AR Path="/DCBAABCD4B36627E" Ref="C30"  Part="1" 
AR Path="/A84B36627E" Ref="C30"  Part="1" 
AR Path="/A4B36627E" Ref="C30"  Part="1" 
AR Path="/402C08B44B36627E" Ref="C30"  Part="1" 
AR Path="/23D2034B36627E" Ref="C30"  Part="1" 
AR Path="/4E4B36627E" Ref="C30"  Part="1" 
AR Path="/402BEF1A4B36627E" Ref="C30"  Part="1" 
AR Path="/54B36627E" Ref="C30"  Part="1" 
AR Path="/40293BE74B36627E" Ref="C30"  Part="1" 
AR Path="/402C55814B36627E" Ref="C30"  Part="1" 
AR Path="/40273BE74B36627E" Ref="C30"  Part="1" 
AR Path="/2600004B36627E" Ref="C30"  Part="1" 
AR Path="/3D8EA0004B36627E" Ref="C30"  Part="1" 
AR Path="/402908B44B36627E" Ref="C30"  Part="1" 
AR Path="/3D6CC0004B36627E" Ref="C30"  Part="1" 
AR Path="/3D5A40004B36627E" Ref="C30"  Part="1" 
AR Path="/402755814B36627E" Ref="C30"  Part="1" 
AR Path="/4030AAC04B36627E" Ref="C30"  Part="1" 
AR Path="/4031DDF34B36627E" Ref="C30"  Part="1" 
AR Path="/3FE88B434B36627E" Ref="C30"  Part="1" 
AR Path="/4032778D4B36627E" Ref="C30"  Part="1" 
AR Path="/403091264B36627E" Ref="C30"  Part="1" 
AR Path="/403051264B36627E" Ref="C30"  Part="1" 
AR Path="/4032F78D4B36627E" Ref="C30"  Part="1" 
AR Path="/403251264B36627E" Ref="C30"  Part="1" 
AR Path="/4032AAC04B36627E" Ref="C30"  Part="1" 
AR Path="/4030D1264B36627E" Ref="C30"  Part="1" 
AR Path="/4031778D4B36627E" Ref="C30"  Part="1" 
AR Path="/3FEA24DD4B36627E" Ref="C30"  Part="1" 
AR Path="/6FE934E34B36627E" Ref="C30"  Part="1" 
AR Path="/773F65F14B36627E" Ref="C30"  Part="1" 
AR Path="/773F8EB44B36627E" Ref="C30"  Part="1" 
AR Path="/23C9F04B36627E" Ref="C30"  Part="1" 
AR Path="/24B36627E" Ref="C30"  Part="1" 
AR Path="/23BC884B36627E" Ref="C30"  Part="1" 
AR Path="/DC0C124B36627E" Ref="C30"  Part="1" 
AR Path="/23C34C4B36627E" Ref="C30"  Part="1" 
AR Path="/23CBC44B36627E" Ref="C30"  Part="1" 
AR Path="/23C6504B36627E" Ref="C30"  Part="1" 
AR Path="/39803EA4B36627E" Ref="C30"  Part="1" 
AR Path="/FFFFFFFF4B36627E" Ref="C30"  Part="1" 
AR Path="/D058A04B36627E" Ref="C30"  Part="1" 
AR Path="/1607D44B36627E" Ref="C30"  Part="1" 
AR Path="/D1C3804B36627E" Ref="C30"  Part="1" 
AR Path="/23D70C4B36627E" Ref="C30"  Part="1" 
AR Path="/2104B36627E" Ref="C30"  Part="1" 
AR Path="/F4BF4B36627E" Ref="C30"  Part="1" 
AR Path="/262F604B36627E" Ref="C30"  Part="1" 
AR Path="/384B36627E" Ref="C30"  Part="1" 
F 0 "C30" H 16400 14450 50  0000 L CNN
F 1 "0.1 uF" H 16400 14250 50  0000 L CNN
F 2 "C2" H 16350 14350 60  0001 C CNN
	1    16350 14350
	1    0    0    -1  
$EndComp
$Comp
L C C28
U 1 1 4B36626C
P 15450 14950
AR Path="/4B36626C" Ref="C28"  Part="1" 
AR Path="/94B36626C" Ref="C28"  Part="1" 
AR Path="/5AD7153D4B36626C" Ref="C28"  Part="1" 
AR Path="/23D9304B36626C" Ref="C28"  Part="1" 
AR Path="/6FE901F74B36626C" Ref="C28"  Part="1" 
AR Path="/3FE224DD4B36626C" Ref="C28"  Part="1" 
AR Path="/3FEFFFFF4B36626C" Ref="C28"  Part="1" 
AR Path="/23D8D44B36626C" Ref="C28"  Part="1" 
AR Path="/14B36626C" Ref="C28"  Part="1" 
AR Path="/6FF0DD404B36626C" Ref="C28"  Part="1" 
AR Path="/FFFFFFF04B36626C" Ref="C28"  Part="1" 
AR Path="/402955814B36626C" Ref="C28"  Part="1" 
AR Path="/DCBAABCD4B36626C" Ref="C28"  Part="1" 
AR Path="/A84B36626C" Ref="C28"  Part="1" 
AR Path="/A4B36626C" Ref="C28"  Part="1" 
AR Path="/402C08B44B36626C" Ref="C28"  Part="1" 
AR Path="/23D2034B36626C" Ref="C28"  Part="1" 
AR Path="/4E4B36626C" Ref="C28"  Part="1" 
AR Path="/402BEF1A4B36626C" Ref="C28"  Part="1" 
AR Path="/54B36626C" Ref="C28"  Part="1" 
AR Path="/40293BE74B36626C" Ref="C28"  Part="1" 
AR Path="/402C55814B36626C" Ref="C28"  Part="1" 
AR Path="/40273BE74B36626C" Ref="C28"  Part="1" 
AR Path="/2600004B36626C" Ref="C28"  Part="1" 
AR Path="/3D8EA0004B36626C" Ref="C28"  Part="1" 
AR Path="/402908B44B36626C" Ref="C28"  Part="1" 
AR Path="/3D6CC0004B36626C" Ref="C28"  Part="1" 
AR Path="/3D5A40004B36626C" Ref="C28"  Part="1" 
AR Path="/402755814B36626C" Ref="C28"  Part="1" 
AR Path="/4030AAC04B36626C" Ref="C28"  Part="1" 
AR Path="/4031DDF34B36626C" Ref="C28"  Part="1" 
AR Path="/3FE88B434B36626C" Ref="C28"  Part="1" 
AR Path="/4032778D4B36626C" Ref="C28"  Part="1" 
AR Path="/403091264B36626C" Ref="C28"  Part="1" 
AR Path="/403051264B36626C" Ref="C28"  Part="1" 
AR Path="/4032F78D4B36626C" Ref="C28"  Part="1" 
AR Path="/403251264B36626C" Ref="C28"  Part="1" 
AR Path="/4032AAC04B36626C" Ref="C28"  Part="1" 
AR Path="/4030D1264B36626C" Ref="C28"  Part="1" 
AR Path="/4031778D4B36626C" Ref="C28"  Part="1" 
AR Path="/3FEA24DD4B36626C" Ref="C28"  Part="1" 
AR Path="/6FE934E34B36626C" Ref="C28"  Part="1" 
AR Path="/773F65F14B36626C" Ref="C28"  Part="1" 
AR Path="/773F8EB44B36626C" Ref="C28"  Part="1" 
AR Path="/23C9F04B36626C" Ref="C28"  Part="1" 
AR Path="/24B36626C" Ref="C28"  Part="1" 
AR Path="/23BC884B36626C" Ref="C28"  Part="1" 
AR Path="/DC0C124B36626C" Ref="C28"  Part="1" 
AR Path="/23C34C4B36626C" Ref="C28"  Part="1" 
AR Path="/23CBC44B36626C" Ref="C28"  Part="1" 
AR Path="/23C6504B36626C" Ref="C28"  Part="1" 
AR Path="/39803EA4B36626C" Ref="C28"  Part="1" 
AR Path="/FFFFFFFF4B36626C" Ref="C28"  Part="1" 
AR Path="/D058A04B36626C" Ref="C28"  Part="1" 
AR Path="/1607D44B36626C" Ref="C28"  Part="1" 
AR Path="/D1C3804B36626C" Ref="C28"  Part="1" 
AR Path="/23D70C4B36626C" Ref="C28"  Part="1" 
AR Path="/2104B36626C" Ref="C28"  Part="1" 
AR Path="/F4BF4B36626C" Ref="C28"  Part="1" 
AR Path="/262F604B36626C" Ref="C28"  Part="1" 
AR Path="/384B36626C" Ref="C28"  Part="1" 
F 0 "C28" H 15500 15050 50  0000 L CNN
F 1 "0.1 uF" H 15500 14850 50  0000 L CNN
F 2 "C2" H 15450 14950 60  0001 C CNN
	1    15450 14950
	1    0    0    -1  
$EndComp
$Comp
L C C24
U 1 1 4B36626B
P 14550 14950
AR Path="/4B36626B" Ref="C24"  Part="1" 
AR Path="/94B36626B" Ref="C24"  Part="1" 
AR Path="/5AD7153D4B36626B" Ref="C24"  Part="1" 
AR Path="/23D9304B36626B" Ref="C24"  Part="1" 
AR Path="/6FE901F74B36626B" Ref="C24"  Part="1" 
AR Path="/3FE224DD4B36626B" Ref="C24"  Part="1" 
AR Path="/3FEFFFFF4B36626B" Ref="C24"  Part="1" 
AR Path="/23D8D44B36626B" Ref="C24"  Part="1" 
AR Path="/14B36626B" Ref="C24"  Part="1" 
AR Path="/6FF0DD404B36626B" Ref="C24"  Part="1" 
AR Path="/FFFFFFF04B36626B" Ref="C24"  Part="1" 
AR Path="/402955814B36626B" Ref="C24"  Part="1" 
AR Path="/DCBAABCD4B36626B" Ref="C24"  Part="1" 
AR Path="/A84B36626B" Ref="C24"  Part="1" 
AR Path="/A4B36626B" Ref="C24"  Part="1" 
AR Path="/402C08B44B36626B" Ref="C24"  Part="1" 
AR Path="/23D2034B36626B" Ref="C24"  Part="1" 
AR Path="/4E4B36626B" Ref="C24"  Part="1" 
AR Path="/402BEF1A4B36626B" Ref="C24"  Part="1" 
AR Path="/54B36626B" Ref="C24"  Part="1" 
AR Path="/40293BE74B36626B" Ref="C24"  Part="1" 
AR Path="/402C55814B36626B" Ref="C24"  Part="1" 
AR Path="/40273BE74B36626B" Ref="C24"  Part="1" 
AR Path="/2600004B36626B" Ref="C24"  Part="1" 
AR Path="/3D8EA0004B36626B" Ref="C24"  Part="1" 
AR Path="/402908B44B36626B" Ref="C24"  Part="1" 
AR Path="/3D6CC0004B36626B" Ref="C24"  Part="1" 
AR Path="/3D5A40004B36626B" Ref="C24"  Part="1" 
AR Path="/402755814B36626B" Ref="C24"  Part="1" 
AR Path="/4030AAC04B36626B" Ref="C24"  Part="1" 
AR Path="/4031DDF34B36626B" Ref="C24"  Part="1" 
AR Path="/3FE88B434B36626B" Ref="C24"  Part="1" 
AR Path="/4032778D4B36626B" Ref="C24"  Part="1" 
AR Path="/403091264B36626B" Ref="C24"  Part="1" 
AR Path="/403051264B36626B" Ref="C24"  Part="1" 
AR Path="/4032F78D4B36626B" Ref="C24"  Part="1" 
AR Path="/403251264B36626B" Ref="C24"  Part="1" 
AR Path="/4032AAC04B36626B" Ref="C24"  Part="1" 
AR Path="/4030D1264B36626B" Ref="C24"  Part="1" 
AR Path="/4031778D4B36626B" Ref="C24"  Part="1" 
AR Path="/3FEA24DD4B36626B" Ref="C24"  Part="1" 
AR Path="/6FE934E34B36626B" Ref="C24"  Part="1" 
AR Path="/773F65F14B36626B" Ref="C24"  Part="1" 
AR Path="/773F8EB44B36626B" Ref="C24"  Part="1" 
AR Path="/23C9F04B36626B" Ref="C24"  Part="1" 
AR Path="/24B36626B" Ref="C24"  Part="1" 
AR Path="/23BC884B36626B" Ref="C24"  Part="1" 
AR Path="/DC0C124B36626B" Ref="C24"  Part="1" 
AR Path="/23C34C4B36626B" Ref="C24"  Part="1" 
AR Path="/23CBC44B36626B" Ref="C24"  Part="1" 
AR Path="/23C6504B36626B" Ref="C24"  Part="1" 
AR Path="/39803EA4B36626B" Ref="C24"  Part="1" 
AR Path="/FFFFFFFF4B36626B" Ref="C24"  Part="1" 
AR Path="/D058A04B36626B" Ref="C24"  Part="1" 
AR Path="/1607D44B36626B" Ref="C24"  Part="1" 
AR Path="/D1C3804B36626B" Ref="C24"  Part="1" 
AR Path="/23D70C4B36626B" Ref="C24"  Part="1" 
AR Path="/2104B36626B" Ref="C24"  Part="1" 
AR Path="/F4BF4B36626B" Ref="C24"  Part="1" 
AR Path="/262F604B36626B" Ref="C24"  Part="1" 
AR Path="/384B36626B" Ref="C24"  Part="1" 
F 0 "C24" H 14600 15050 50  0000 L CNN
F 1 "0.1 uF" H 14600 14850 50  0000 L CNN
F 2 "C2" H 14550 14950 60  0001 C CNN
	1    14550 14950
	1    0    0    -1  
$EndComp
$Comp
L C C26
U 1 1 4B36626A
P 15000 14950
AR Path="/4B36626A" Ref="C26"  Part="1" 
AR Path="/94B36626A" Ref="C26"  Part="1" 
AR Path="/5AD7153D4B36626A" Ref="C26"  Part="1" 
AR Path="/23D9304B36626A" Ref="C26"  Part="1" 
AR Path="/6FE901F74B36626A" Ref="C26"  Part="1" 
AR Path="/3FE224DD4B36626A" Ref="C26"  Part="1" 
AR Path="/3FEFFFFF4B36626A" Ref="C26"  Part="1" 
AR Path="/23D8D44B36626A" Ref="C26"  Part="1" 
AR Path="/14B36626A" Ref="C26"  Part="1" 
AR Path="/6FF0DD404B36626A" Ref="C26"  Part="1" 
AR Path="/FFFFFFF04B36626A" Ref="C26"  Part="1" 
AR Path="/402955814B36626A" Ref="C26"  Part="1" 
AR Path="/DCBAABCD4B36626A" Ref="C26"  Part="1" 
AR Path="/A84B36626A" Ref="C26"  Part="1" 
AR Path="/A4B36626A" Ref="C26"  Part="1" 
AR Path="/402C08B44B36626A" Ref="C26"  Part="1" 
AR Path="/23D2034B36626A" Ref="C26"  Part="1" 
AR Path="/4E4B36626A" Ref="C26"  Part="1" 
AR Path="/402BEF1A4B36626A" Ref="C26"  Part="1" 
AR Path="/54B36626A" Ref="C26"  Part="1" 
AR Path="/40293BE74B36626A" Ref="C26"  Part="1" 
AR Path="/402C55814B36626A" Ref="C26"  Part="1" 
AR Path="/40273BE74B36626A" Ref="C26"  Part="1" 
AR Path="/2600004B36626A" Ref="C26"  Part="1" 
AR Path="/3D8EA0004B36626A" Ref="C26"  Part="1" 
AR Path="/402908B44B36626A" Ref="C26"  Part="1" 
AR Path="/3D6CC0004B36626A" Ref="C26"  Part="1" 
AR Path="/3D5A40004B36626A" Ref="C26"  Part="1" 
AR Path="/402755814B36626A" Ref="C26"  Part="1" 
AR Path="/4030AAC04B36626A" Ref="C26"  Part="1" 
AR Path="/4031DDF34B36626A" Ref="C26"  Part="1" 
AR Path="/3FE88B434B36626A" Ref="C26"  Part="1" 
AR Path="/4032778D4B36626A" Ref="C26"  Part="1" 
AR Path="/403091264B36626A" Ref="C26"  Part="1" 
AR Path="/403051264B36626A" Ref="C26"  Part="1" 
AR Path="/4032F78D4B36626A" Ref="C26"  Part="1" 
AR Path="/403251264B36626A" Ref="C26"  Part="1" 
AR Path="/4032AAC04B36626A" Ref="C26"  Part="1" 
AR Path="/4030D1264B36626A" Ref="C26"  Part="1" 
AR Path="/4031778D4B36626A" Ref="C26"  Part="1" 
AR Path="/3FEA24DD4B36626A" Ref="C26"  Part="1" 
AR Path="/6FE934E34B36626A" Ref="C26"  Part="1" 
AR Path="/773F65F14B36626A" Ref="C26"  Part="1" 
AR Path="/773F8EB44B36626A" Ref="C26"  Part="1" 
AR Path="/23C9F04B36626A" Ref="C26"  Part="1" 
AR Path="/24B36626A" Ref="C26"  Part="1" 
AR Path="/23BC884B36626A" Ref="C26"  Part="1" 
AR Path="/DC0C124B36626A" Ref="C26"  Part="1" 
AR Path="/23C34C4B36626A" Ref="C26"  Part="1" 
AR Path="/23CBC44B36626A" Ref="C26"  Part="1" 
AR Path="/23C6504B36626A" Ref="C26"  Part="1" 
AR Path="/39803EA4B36626A" Ref="C26"  Part="1" 
AR Path="/FFFFFFFF4B36626A" Ref="C26"  Part="1" 
AR Path="/D058A04B36626A" Ref="C26"  Part="1" 
AR Path="/1607D44B36626A" Ref="C26"  Part="1" 
AR Path="/D1C3804B36626A" Ref="C26"  Part="1" 
AR Path="/23D70C4B36626A" Ref="C26"  Part="1" 
AR Path="/2104B36626A" Ref="C26"  Part="1" 
AR Path="/F4BF4B36626A" Ref="C26"  Part="1" 
AR Path="/262F604B36626A" Ref="C26"  Part="1" 
AR Path="/384B36626A" Ref="C26"  Part="1" 
F 0 "C26" H 15050 15050 50  0000 L CNN
F 1 "0.1 uF" H 15050 14850 50  0000 L CNN
F 2 "C2" H 15000 14950 60  0001 C CNN
	1    15000 14950
	1    0    0    -1  
$EndComp
$Comp
L C C20
U 1 1 4B36625D
P 13200 14350
AR Path="/4B36625D" Ref="C20"  Part="1" 
AR Path="/94B36625D" Ref="C20"  Part="1" 
AR Path="/5AD7153D4B36625D" Ref="C20"  Part="1" 
AR Path="/23D9304B36625D" Ref="C20"  Part="1" 
AR Path="/6FE901F74B36625D" Ref="C20"  Part="1" 
AR Path="/3FE224DD4B36625D" Ref="C20"  Part="1" 
AR Path="/3FEFFFFF4B36625D" Ref="C20"  Part="1" 
AR Path="/23D8D44B36625D" Ref="C20"  Part="1" 
AR Path="/14B36625D" Ref="C20"  Part="1" 
AR Path="/6FF0DD404B36625D" Ref="C20"  Part="1" 
AR Path="/FFFFFFF04B36625D" Ref="C20"  Part="1" 
AR Path="/402955814B36625D" Ref="C20"  Part="1" 
AR Path="/DCBAABCD4B36625D" Ref="C20"  Part="1" 
AR Path="/A84B36625D" Ref="C20"  Part="1" 
AR Path="/A4B36625D" Ref="C20"  Part="1" 
AR Path="/402C08B44B36625D" Ref="C20"  Part="1" 
AR Path="/23D2034B36625D" Ref="C20"  Part="1" 
AR Path="/4E4B36625D" Ref="C20"  Part="1" 
AR Path="/402BEF1A4B36625D" Ref="C20"  Part="1" 
AR Path="/54B36625D" Ref="C20"  Part="1" 
AR Path="/40293BE74B36625D" Ref="C20"  Part="1" 
AR Path="/402C55814B36625D" Ref="C20"  Part="1" 
AR Path="/40273BE74B36625D" Ref="C20"  Part="1" 
AR Path="/2600004B36625D" Ref="C20"  Part="1" 
AR Path="/3D8EA0004B36625D" Ref="C20"  Part="1" 
AR Path="/402908B44B36625D" Ref="C20"  Part="1" 
AR Path="/3D6CC0004B36625D" Ref="C20"  Part="1" 
AR Path="/3D5A40004B36625D" Ref="C20"  Part="1" 
AR Path="/402755814B36625D" Ref="C20"  Part="1" 
AR Path="/4030AAC04B36625D" Ref="C20"  Part="1" 
AR Path="/4031DDF34B36625D" Ref="C20"  Part="1" 
AR Path="/3FE88B434B36625D" Ref="C20"  Part="1" 
AR Path="/4032778D4B36625D" Ref="C20"  Part="1" 
AR Path="/403091264B36625D" Ref="C20"  Part="1" 
AR Path="/403051264B36625D" Ref="C20"  Part="1" 
AR Path="/4032F78D4B36625D" Ref="C20"  Part="1" 
AR Path="/403251264B36625D" Ref="C20"  Part="1" 
AR Path="/4032AAC04B36625D" Ref="C20"  Part="1" 
AR Path="/4030D1264B36625D" Ref="C20"  Part="1" 
AR Path="/4031778D4B36625D" Ref="C20"  Part="1" 
AR Path="/3FEA24DD4B36625D" Ref="C20"  Part="1" 
AR Path="/6FE934E34B36625D" Ref="C20"  Part="1" 
AR Path="/773F65F14B36625D" Ref="C20"  Part="1" 
AR Path="/773F8EB44B36625D" Ref="C20"  Part="1" 
AR Path="/23C9F04B36625D" Ref="C20"  Part="1" 
AR Path="/24B36625D" Ref="C20"  Part="1" 
AR Path="/23BC884B36625D" Ref="C20"  Part="1" 
AR Path="/DC0C124B36625D" Ref="C20"  Part="1" 
AR Path="/23C34C4B36625D" Ref="C20"  Part="1" 
AR Path="/23CBC44B36625D" Ref="C20"  Part="1" 
AR Path="/23C6504B36625D" Ref="C20"  Part="1" 
AR Path="/39803EA4B36625D" Ref="C20"  Part="1" 
AR Path="/FFFFFFFF4B36625D" Ref="C20"  Part="1" 
AR Path="/D058A04B36625D" Ref="C20"  Part="1" 
AR Path="/1607D44B36625D" Ref="C20"  Part="1" 
AR Path="/D1C3804B36625D" Ref="C20"  Part="1" 
AR Path="/23D70C4B36625D" Ref="C20"  Part="1" 
AR Path="/2104B36625D" Ref="C20"  Part="1" 
AR Path="/F4BF4B36625D" Ref="C20"  Part="1" 
AR Path="/262F604B36625D" Ref="C20"  Part="1" 
AR Path="/384B36625D" Ref="C20"  Part="1" 
F 0 "C20" H 13250 14450 50  0000 L CNN
F 1 "0.1 uF" H 13250 14250 50  0000 L CNN
F 2 "C2" H 13200 14350 60  0001 C CNN
	1    13200 14350
	1    0    0    -1  
$EndComp
$Comp
L C C19
U 1 1 4B36624E
P 12750 14350
AR Path="/4B36624E" Ref="C19"  Part="1" 
AR Path="/94B36624E" Ref="C19"  Part="1" 
AR Path="/5AD7153D4B36624E" Ref="C19"  Part="1" 
AR Path="/23D9304B36624E" Ref="C19"  Part="1" 
AR Path="/6FE901F74B36624E" Ref="C19"  Part="1" 
AR Path="/3FE224DD4B36624E" Ref="C19"  Part="1" 
AR Path="/3FEFFFFF4B36624E" Ref="C19"  Part="1" 
AR Path="/23D8D44B36624E" Ref="C19"  Part="1" 
AR Path="/14B36624E" Ref="C19"  Part="1" 
AR Path="/6FF0DD404B36624E" Ref="C19"  Part="1" 
AR Path="/FFFFFFF04B36624E" Ref="C19"  Part="1" 
AR Path="/402955814B36624E" Ref="C19"  Part="1" 
AR Path="/DCBAABCD4B36624E" Ref="C19"  Part="1" 
AR Path="/A84B36624E" Ref="C19"  Part="1" 
AR Path="/A4B36624E" Ref="C19"  Part="1" 
AR Path="/402C08B44B36624E" Ref="C19"  Part="1" 
AR Path="/23D2034B36624E" Ref="C19"  Part="1" 
AR Path="/4E4B36624E" Ref="C19"  Part="1" 
AR Path="/402BEF1A4B36624E" Ref="C19"  Part="1" 
AR Path="/54B36624E" Ref="C19"  Part="1" 
AR Path="/40293BE74B36624E" Ref="C19"  Part="1" 
AR Path="/402C55814B36624E" Ref="C19"  Part="1" 
AR Path="/40273BE74B36624E" Ref="C19"  Part="1" 
AR Path="/2600004B36624E" Ref="C19"  Part="1" 
AR Path="/3D8EA0004B36624E" Ref="C19"  Part="1" 
AR Path="/402908B44B36624E" Ref="C19"  Part="1" 
AR Path="/3D6CC0004B36624E" Ref="C19"  Part="1" 
AR Path="/3D5A40004B36624E" Ref="C19"  Part="1" 
AR Path="/402755814B36624E" Ref="C19"  Part="1" 
AR Path="/4030AAC04B36624E" Ref="C19"  Part="1" 
AR Path="/4031DDF34B36624E" Ref="C19"  Part="1" 
AR Path="/3FE88B434B36624E" Ref="C19"  Part="1" 
AR Path="/4032778D4B36624E" Ref="C19"  Part="1" 
AR Path="/403091264B36624E" Ref="C19"  Part="1" 
AR Path="/403051264B36624E" Ref="C19"  Part="1" 
AR Path="/4032F78D4B36624E" Ref="C19"  Part="1" 
AR Path="/403251264B36624E" Ref="C19"  Part="1" 
AR Path="/4032AAC04B36624E" Ref="C19"  Part="1" 
AR Path="/4030D1264B36624E" Ref="C19"  Part="1" 
AR Path="/4031778D4B36624E" Ref="C19"  Part="1" 
AR Path="/3FEA24DD4B36624E" Ref="C19"  Part="1" 
AR Path="/6FE934E34B36624E" Ref="C19"  Part="1" 
AR Path="/773F65F14B36624E" Ref="C19"  Part="1" 
AR Path="/773F8EB44B36624E" Ref="C19"  Part="1" 
AR Path="/23C9F04B36624E" Ref="C19"  Part="1" 
AR Path="/24B36624E" Ref="C19"  Part="1" 
AR Path="/23BC884B36624E" Ref="C19"  Part="1" 
AR Path="/DC0C124B36624E" Ref="C19"  Part="1" 
AR Path="/23C34C4B36624E" Ref="C19"  Part="1" 
AR Path="/23CBC44B36624E" Ref="C19"  Part="1" 
AR Path="/23C6504B36624E" Ref="C19"  Part="1" 
AR Path="/39803EA4B36624E" Ref="C19"  Part="1" 
AR Path="/FFFFFFFF4B36624E" Ref="C19"  Part="1" 
AR Path="/D058A04B36624E" Ref="C19"  Part="1" 
AR Path="/1607D44B36624E" Ref="C19"  Part="1" 
AR Path="/D1C3804B36624E" Ref="C19"  Part="1" 
AR Path="/23D70C4B36624E" Ref="C19"  Part="1" 
AR Path="/2104B36624E" Ref="C19"  Part="1" 
AR Path="/F4BF4B36624E" Ref="C19"  Part="1" 
AR Path="/262F604B36624E" Ref="C19"  Part="1" 
AR Path="/384B36624E" Ref="C19"  Part="1" 
F 0 "C19" H 12800 14450 50  0000 L CNN
F 1 "0.1 uF" H 12800 14250 50  0000 L CNN
F 2 "C2" H 12750 14350 60  0001 C CNN
	1    12750 14350
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 4B366239
P 13650 14350
AR Path="/23D9D84B366239" Ref="C?"  Part="1" 
AR Path="/394433324B366239" Ref="C?"  Part="1" 
AR Path="/74B366239" Ref="C?"  Part="1" 
AR Path="/4B366239" Ref="C21"  Part="1" 
AR Path="/94B366239" Ref="C21"  Part="1" 
AR Path="/5AD7153D4B366239" Ref="C21"  Part="1" 
AR Path="/23D9304B366239" Ref="C21"  Part="1" 
AR Path="/6FE901F74B366239" Ref="C21"  Part="1" 
AR Path="/3FE224DD4B366239" Ref="C21"  Part="1" 
AR Path="/3FEFFFFF4B366239" Ref="C21"  Part="1" 
AR Path="/23D8D44B366239" Ref="C21"  Part="1" 
AR Path="/6FF0DD404B366239" Ref="C21"  Part="1" 
AR Path="/FFFFFFF04B366239" Ref="C21"  Part="1" 
AR Path="/402955814B366239" Ref="C21"  Part="1" 
AR Path="/DCBAABCD4B366239" Ref="C21"  Part="1" 
AR Path="/A84B366239" Ref="C21"  Part="1" 
AR Path="/A4B366239" Ref="C21"  Part="1" 
AR Path="/402C08B44B366239" Ref="C21"  Part="1" 
AR Path="/23D2034B366239" Ref="C21"  Part="1" 
AR Path="/4E4B366239" Ref="C21"  Part="1" 
AR Path="/402BEF1A4B366239" Ref="C21"  Part="1" 
AR Path="/54B366239" Ref="C21"  Part="1" 
AR Path="/40293BE74B366239" Ref="C21"  Part="1" 
AR Path="/402C55814B366239" Ref="C21"  Part="1" 
AR Path="/40273BE74B366239" Ref="C21"  Part="1" 
AR Path="/2600004B366239" Ref="C21"  Part="1" 
AR Path="/3D8EA0004B366239" Ref="C21"  Part="1" 
AR Path="/402908B44B366239" Ref="C21"  Part="1" 
AR Path="/3D6CC0004B366239" Ref="C21"  Part="1" 
AR Path="/3D5A40004B366239" Ref="C21"  Part="1" 
AR Path="/402755814B366239" Ref="C21"  Part="1" 
AR Path="/4030AAC04B366239" Ref="C21"  Part="1" 
AR Path="/4031DDF34B366239" Ref="C21"  Part="1" 
AR Path="/3FE88B434B366239" Ref="C21"  Part="1" 
AR Path="/4032778D4B366239" Ref="C21"  Part="1" 
AR Path="/403091264B366239" Ref="C21"  Part="1" 
AR Path="/403051264B366239" Ref="C21"  Part="1" 
AR Path="/4032F78D4B366239" Ref="C21"  Part="1" 
AR Path="/403251264B366239" Ref="C21"  Part="1" 
AR Path="/4032AAC04B366239" Ref="C21"  Part="1" 
AR Path="/4030D1264B366239" Ref="C21"  Part="1" 
AR Path="/4031778D4B366239" Ref="C21"  Part="1" 
AR Path="/3FEA24DD4B366239" Ref="C21"  Part="1" 
AR Path="/6FE934E34B366239" Ref="C21"  Part="1" 
AR Path="/773F65F14B366239" Ref="C21"  Part="1" 
AR Path="/773F8EB44B366239" Ref="C21"  Part="1" 
AR Path="/23C9F04B366239" Ref="C21"  Part="1" 
AR Path="/24B366239" Ref="C21"  Part="1" 
AR Path="/23BC884B366239" Ref="C21"  Part="1" 
AR Path="/DC0C124B366239" Ref="C21"  Part="1" 
AR Path="/23C34C4B366239" Ref="C21"  Part="1" 
AR Path="/23CBC44B366239" Ref="C21"  Part="1" 
AR Path="/23C6504B366239" Ref="C21"  Part="1" 
AR Path="/39803EA4B366239" Ref="C21"  Part="1" 
AR Path="/FFFFFFFF4B366239" Ref="C21"  Part="1" 
AR Path="/D058A04B366239" Ref="C21"  Part="1" 
AR Path="/1607D44B366239" Ref="C21"  Part="1" 
AR Path="/D1C3804B366239" Ref="C21"  Part="1" 
AR Path="/23D70C4B366239" Ref="C21"  Part="1" 
AR Path="/14B366239" Ref="C21"  Part="1" 
AR Path="/2104B366239" Ref="C21"  Part="1" 
AR Path="/F4BF4B366239" Ref="C21"  Part="1" 
AR Path="/262F604B366239" Ref="C21"  Part="1" 
AR Path="/384B366239" Ref="C21"  Part="1" 
F 0 "C21" H 13700 14450 50  0000 L CNN
F 1 "0.1 uF" H 13700 14250 50  0000 L CNN
F 2 "C2" H 13650 14350 60  0001 C CNN
	1    13650 14350
	1    0    0    -1  
$EndComp
Text Label 13600 9650 0    60   ~ 0
8wr
NoConn ~ 19400 8100
NoConn ~ 19400 6000
NoConn ~ 19400 1800
NoConn ~ 19400 1700
NoConn ~ 19400 1600
NoConn ~ 19400 1500
NoConn ~ 19400 1400
NoConn ~ 19400 1300
NoConn ~ 19400 1200
NoConn ~ 19400 1100
NoConn ~ 19400 1000
$Comp
L SRAM_512KO U102
U 1 1 4B365BEF
P 4400 11950
AR Path="/4B365BEF" Ref="U102"  Part="1" 
AR Path="/94B365BEF" Ref="U102"  Part="1" 
AR Path="/5AD7153D4B365BEF" Ref="U102"  Part="1" 
AR Path="/DCBAABCD4B365BEF" Ref="U102"  Part="1" 
AR Path="/A4B365BEF" Ref="U102"  Part="1" 
AR Path="/14B365BEF" Ref="U102"  Part="1" 
AR Path="/6FF0DD404B365BEF" Ref="U102"  Part="1" 
AR Path="/6FE901F74B365BEF" Ref="U102"  Part="1" 
AR Path="/3FE224DD4B365BEF" Ref="U102"  Part="1" 
AR Path="/3FEFFFFF4B365BEF" Ref="U102"  Part="1" 
AR Path="/23D8D44B365BEF" Ref="U102"  Part="1" 
AR Path="/FFFFFFF04B365BEF" Ref="U102"  Part="1" 
AR Path="/402955814B365BEF" Ref="U102"  Part="1" 
AR Path="/A84B365BEF" Ref="U102"  Part="1" 
AR Path="/402C08B44B365BEF" Ref="U102"  Part="1" 
AR Path="/23D2034B365BEF" Ref="U102"  Part="1" 
AR Path="/4E4B365BEF" Ref="U102"  Part="1" 
AR Path="/402BEF1A4B365BEF" Ref="U102"  Part="1" 
AR Path="/54B365BEF" Ref="U102"  Part="1" 
AR Path="/40293BE74B365BEF" Ref="U102"  Part="1" 
AR Path="/402C55814B365BEF" Ref="U102"  Part="1" 
AR Path="/40273BE74B365BEF" Ref="U102"  Part="1" 
AR Path="/2600004B365BEF" Ref="U102"  Part="1" 
AR Path="/3D8EA0004B365BEF" Ref="U102"  Part="1" 
AR Path="/402908B44B365BEF" Ref="U102"  Part="1" 
AR Path="/3D6CC0004B365BEF" Ref="U102"  Part="1" 
AR Path="/3D5A40004B365BEF" Ref="U102"  Part="1" 
AR Path="/402755814B365BEF" Ref="U102"  Part="1" 
AR Path="/4030AAC04B365BEF" Ref="U102"  Part="1" 
AR Path="/23D9304B365BEF" Ref="U102"  Part="1" 
AR Path="/4031DDF34B365BEF" Ref="U102"  Part="1" 
AR Path="/3FE88B434B365BEF" Ref="U102"  Part="1" 
AR Path="/4032778D4B365BEF" Ref="U102"  Part="1" 
AR Path="/403091264B365BEF" Ref="U102"  Part="1" 
AR Path="/403051264B365BEF" Ref="U102"  Part="1" 
AR Path="/4032F78D4B365BEF" Ref="U102"  Part="1" 
AR Path="/403251264B365BEF" Ref="U102"  Part="1" 
AR Path="/4032AAC04B365BEF" Ref="U102"  Part="1" 
AR Path="/4030D1264B365BEF" Ref="U102"  Part="1" 
AR Path="/4031778D4B365BEF" Ref="U102"  Part="1" 
AR Path="/3FEA24DD4B365BEF" Ref="U102"  Part="1" 
AR Path="/6FE934E34B365BEF" Ref="U102"  Part="1" 
AR Path="/773F65F14B365BEF" Ref="U102"  Part="1" 
AR Path="/773F8EB44B365BEF" Ref="U102"  Part="1" 
AR Path="/23C9F04B365BEF" Ref="U102"  Part="1" 
AR Path="/24B365BEF" Ref="U102"  Part="1" 
AR Path="/23BC884B365BEF" Ref="U102"  Part="1" 
AR Path="/DC0C124B365BEF" Ref="U102"  Part="1" 
AR Path="/23C34C4B365BEF" Ref="U102"  Part="1" 
AR Path="/23CBC44B365BEF" Ref="U102"  Part="1" 
AR Path="/23C6504B365BEF" Ref="U102"  Part="1" 
AR Path="/39803EA4B365BEF" Ref="U102"  Part="1" 
AR Path="/FFFFFFFF4B365BEF" Ref="U102"  Part="1" 
AR Path="/D058A04B365BEF" Ref="U102"  Part="1" 
AR Path="/1607D44B365BEF" Ref="U102"  Part="1" 
AR Path="/D1C3804B365BEF" Ref="U102"  Part="1" 
AR Path="/23D70C4B365BEF" Ref="U102"  Part="1" 
AR Path="/2104B365BEF" Ref="U102"  Part="1" 
AR Path="/F4BF4B365BEF" Ref="U102"  Part="1" 
AR Path="/262F604B365BEF" Ref="U102"  Part="1" 
AR Path="/384B365BEF" Ref="U102"  Part="1" 
F 0 "U102" H 4500 13150 70  0000 L CNN
F 1 "SRAM_512KO" H 4500 10750 70  0000 L CNN
F 2 "32dip600" H 4400 11950 60  0001 C CNN
	1    4400 11950
	1    0    0    -1  
$EndComp
$Comp
L SRAM_512KO U103
U 1 1 4B365BEE
P 4400 15100
AR Path="/4B365BEE" Ref="U103"  Part="1" 
AR Path="/94B365BEE" Ref="U103"  Part="1" 
AR Path="/5AD7153D4B365BEE" Ref="U103"  Part="1" 
AR Path="/A4B365BEE" Ref="U103"  Part="1" 
AR Path="/14B365BEE" Ref="U103"  Part="1" 
AR Path="/6FF0DD404B365BEE" Ref="U103"  Part="1" 
AR Path="/DCBAABCD4B365BEE" Ref="U103"  Part="1" 
AR Path="/6FE901F74B365BEE" Ref="U103"  Part="1" 
AR Path="/3FE224DD4B365BEE" Ref="U103"  Part="1" 
AR Path="/3FEFFFFF4B365BEE" Ref="U103"  Part="1" 
AR Path="/23D8D44B365BEE" Ref="U103"  Part="1" 
AR Path="/FFFFFFF04B365BEE" Ref="U103"  Part="1" 
AR Path="/402955814B365BEE" Ref="U103"  Part="1" 
AR Path="/A84B365BEE" Ref="U103"  Part="1" 
AR Path="/402C08B44B365BEE" Ref="U103"  Part="1" 
AR Path="/23D2034B365BEE" Ref="U103"  Part="1" 
AR Path="/4E4B365BEE" Ref="U103"  Part="1" 
AR Path="/402BEF1A4B365BEE" Ref="U103"  Part="1" 
AR Path="/54B365BEE" Ref="U103"  Part="1" 
AR Path="/40293BE74B365BEE" Ref="U103"  Part="1" 
AR Path="/402C55814B365BEE" Ref="U103"  Part="1" 
AR Path="/40273BE74B365BEE" Ref="U103"  Part="1" 
AR Path="/2600004B365BEE" Ref="U103"  Part="1" 
AR Path="/3D8EA0004B365BEE" Ref="U103"  Part="1" 
AR Path="/402908B44B365BEE" Ref="U103"  Part="1" 
AR Path="/3D6CC0004B365BEE" Ref="U103"  Part="1" 
AR Path="/3D5A40004B365BEE" Ref="U103"  Part="1" 
AR Path="/402755814B365BEE" Ref="U103"  Part="1" 
AR Path="/4030AAC04B365BEE" Ref="U103"  Part="1" 
AR Path="/23D9304B365BEE" Ref="U103"  Part="1" 
AR Path="/4031DDF34B365BEE" Ref="U103"  Part="1" 
AR Path="/3FE88B434B365BEE" Ref="U103"  Part="1" 
AR Path="/4032778D4B365BEE" Ref="U103"  Part="1" 
AR Path="/403091264B365BEE" Ref="U103"  Part="1" 
AR Path="/403051264B365BEE" Ref="U103"  Part="1" 
AR Path="/4032F78D4B365BEE" Ref="U103"  Part="1" 
AR Path="/403251264B365BEE" Ref="U103"  Part="1" 
AR Path="/4032AAC04B365BEE" Ref="U103"  Part="1" 
AR Path="/4030D1264B365BEE" Ref="U103"  Part="1" 
AR Path="/4031778D4B365BEE" Ref="U103"  Part="1" 
AR Path="/3FEA24DD4B365BEE" Ref="U103"  Part="1" 
AR Path="/6FE934E34B365BEE" Ref="U103"  Part="1" 
AR Path="/773F65F14B365BEE" Ref="U103"  Part="1" 
AR Path="/773F8EB44B365BEE" Ref="U103"  Part="1" 
AR Path="/23C9F04B365BEE" Ref="U103"  Part="1" 
AR Path="/24B365BEE" Ref="U103"  Part="1" 
AR Path="/23BC884B365BEE" Ref="U103"  Part="1" 
AR Path="/DC0C124B365BEE" Ref="U103"  Part="1" 
AR Path="/23C34C4B365BEE" Ref="U103"  Part="1" 
AR Path="/23CBC44B365BEE" Ref="U103"  Part="1" 
AR Path="/23C6504B365BEE" Ref="U103"  Part="1" 
AR Path="/39803EA4B365BEE" Ref="U103"  Part="1" 
AR Path="/FFFFFFFF4B365BEE" Ref="U103"  Part="1" 
AR Path="/D058A04B365BEE" Ref="U103"  Part="1" 
AR Path="/1607D44B365BEE" Ref="U103"  Part="1" 
AR Path="/D1C3804B365BEE" Ref="U103"  Part="1" 
AR Path="/23D70C4B365BEE" Ref="U103"  Part="1" 
AR Path="/2104B365BEE" Ref="U103"  Part="1" 
AR Path="/F4BF4B365BEE" Ref="U103"  Part="1" 
AR Path="/262F604B365BEE" Ref="U103"  Part="1" 
AR Path="/384B365BEE" Ref="U103"  Part="1" 
F 0 "U103" H 4500 16300 70  0000 L CNN
F 1 "SRAM_512KO" H 4500 13900 70  0000 L CNN
F 2 "32dip600" H 4400 15100 60  0001 C CNN
	1    4400 15100
	1    0    0    -1  
$EndComp
Text Label 3400 10850 0    60   ~ 0
bA1
Text Label 3400 10950 0    60   ~ 0
bA2
Text Label 3400 11150 0    60   ~ 0
bA4
Text Label 3400 11050 0    60   ~ 0
bA3
Text Label 3400 11450 0    60   ~ 0
bA7
Text Label 3400 11550 0    60   ~ 0
bA8
Text Label 3400 11350 0    60   ~ 0
bA6
Text Label 3400 11250 0    60   ~ 0
bA5
Text Label 3400 12050 0    60   ~ 0
bA13
Text Label 3400 12150 0    60   ~ 0
bA14
Text Label 3400 11850 0    60   ~ 0
bA11
Text Label 3400 11950 0    60   ~ 0
bA12
Text Label 3400 11750 0    60   ~ 0
bA10
Text Label 3400 11650 0    60   ~ 0
bA9
Text Label 3400 15500 0    60   ~ 0
bA16
Text Label 3400 15700 0    60   ~ 0
bA18
Text Label 3400 15600 0    60   ~ 0
bA17
Text Label 3400 14800 0    60   ~ 0
bA9
Text Label 3400 14900 0    60   ~ 0
bA10
Text Label 3400 15100 0    60   ~ 0
bA12
Text Label 3400 15000 0    60   ~ 0
bA11
Text Label 3400 15300 0    60   ~ 0
bA14
Text Label 3400 15200 0    60   ~ 0
bA13
Text Label 3400 14400 0    60   ~ 0
bA5
Text Label 3400 14500 0    60   ~ 0
bA6
Text Label 3400 14700 0    60   ~ 0
bA8
Text Label 3400 14600 0    60   ~ 0
bA7
Text Label 3400 14200 0    60   ~ 0
bA3
Text Label 3400 14300 0    60   ~ 0
bA4
Text Label 3400 14100 0    60   ~ 0
bA2
Text Label 3400 14000 0    60   ~ 0
bA1
Text Notes 4000 13700 0    60   ~ 0
EVEN BYTE RAM
Text Notes 4000 10550 0    60   ~ 0
ODD BYTE RAM
$Comp
L VCC #PWR027
U 1 1 4B365BED
P 4400 13850
AR Path="/4B365BED" Ref="#PWR027"  Part="1" 
AR Path="/94B365BED" Ref="#PWR23"  Part="1" 
AR Path="/5AD7153D4B365BED" Ref="#PWR025"  Part="1" 
AR Path="/A4B365BED" Ref="#PWR025"  Part="1" 
AR Path="/25E4B365BED" Ref="#PWR024"  Part="1" 
AR Path="/6FF0DD404B365BED" Ref="#PWR028"  Part="1" 
AR Path="/363030314B365BED" Ref="#PWR024"  Part="1" 
AR Path="/DCBAABCD4B365BED" Ref="#PWR09"  Part="1" 
AR Path="/6FE901F74B365BED" Ref="#PWR011"  Part="1" 
AR Path="/3FE224DD4B365BED" Ref="#PWR01"  Part="1" 
AR Path="/3FEFFFFF4B365BED" Ref="#PWR025"  Part="1" 
AR Path="/23D8D44B365BED" Ref="#PWR025"  Part="1" 
AR Path="/FFFFFFF04B365BED" Ref="#PWR31"  Part="1" 
AR Path="/402955814B365BED" Ref="#PWR01"  Part="1" 
AR Path="/A84B365BED" Ref="#PWR22"  Part="1" 
AR Path="/402C08B44B365BED" Ref="#PWR02"  Part="1" 
AR Path="/23D2034B365BED" Ref="#PWR08"  Part="1" 
AR Path="/4E4B365BED" Ref="#PWR02"  Part="1" 
AR Path="/314B365BED" Ref="#PWR03"  Part="1" 
AR Path="/402BEF1A4B365BED" Ref="#PWR03"  Part="1" 
AR Path="/54B365BED" Ref="#PWR04"  Part="1" 
AR Path="/40293BE74B365BED" Ref="#PWR05"  Part="1" 
AR Path="/402C55814B365BED" Ref="#PWR08"  Part="1" 
AR Path="/40273BE74B365BED" Ref="#PWR09"  Part="1" 
AR Path="/2600004B365BED" Ref="#PWR025"  Part="1" 
AR Path="/3D8EA0004B365BED" Ref="#PWR09"  Part="1" 
AR Path="/402908B44B365BED" Ref="#PWR05"  Part="1" 
AR Path="/3D6CC0004B365BED" Ref="#PWR03"  Part="1" 
AR Path="/3D5A40004B365BED" Ref="#PWR03"  Part="1" 
AR Path="/402755814B365BED" Ref="#PWR011"  Part="1" 
AR Path="/4030AAC04B365BED" Ref="#PWR011"  Part="1" 
AR Path="/14B365BED" Ref="#PWR031"  Part="1" 
AR Path="/23D9304B365BED" Ref="#PWR012"  Part="1" 
AR Path="/4031DDF34B365BED" Ref="#PWR012"  Part="1" 
AR Path="/3FE88B434B365BED" Ref="#PWR012"  Part="1" 
AR Path="/4032778D4B365BED" Ref="#PWR018"  Part="1" 
AR Path="/403091264B365BED" Ref="#PWR025"  Part="1" 
AR Path="/403051264B365BED" Ref="#PWR025"  Part="1" 
AR Path="/4032F78D4B365BED" Ref="#PWR025"  Part="1" 
AR Path="/403251264B365BED" Ref="#PWR025"  Part="1" 
AR Path="/4032AAC04B365BED" Ref="#PWR025"  Part="1" 
AR Path="/4030D1264B365BED" Ref="#PWR025"  Part="1" 
AR Path="/4031778D4B365BED" Ref="#PWR025"  Part="1" 
AR Path="/3FEA24DD4B365BED" Ref="#PWR025"  Part="1" 
AR Path="/6FE934E34B365BED" Ref="#PWR031"  Part="1" 
AR Path="/773F65F14B365BED" Ref="#PWR031"  Part="1" 
AR Path="/773F8EB44B365BED" Ref="#PWR027"  Part="1" 
AR Path="/23C9F04B365BED" Ref="#PWR17"  Part="1" 
AR Path="/23BC884B365BED" Ref="#PWR031"  Part="1" 
AR Path="/DC0C124B365BED" Ref="#PWR025"  Part="1" 
AR Path="/6684D64B365BED" Ref="#PWR031"  Part="1" 
AR Path="/23C34C4B365BED" Ref="#PWR032"  Part="1" 
AR Path="/23CBC44B365BED" Ref="#PWR031"  Part="1" 
AR Path="/23C6504B365BED" Ref="#PWR027"  Part="1" 
AR Path="/39803EA4B365BED" Ref="#PWR030"  Part="1" 
AR Path="/FFFFFFFF4B365BED" Ref="#PWR032"  Part="1" 
AR Path="/D058A04B365BED" Ref="#PWR031"  Part="1" 
AR Path="/1607D44B365BED" Ref="#PWR031"  Part="1" 
AR Path="/D1C3804B365BED" Ref="#PWR031"  Part="1" 
AR Path="/24B365BED" Ref="#PWR031"  Part="1" 
AR Path="/23D70C4B365BED" Ref="#PWR031"  Part="1" 
AR Path="/2104B365BED" Ref="#PWR031"  Part="1" 
AR Path="/F4BF4B365BED" Ref="#PWR027"  Part="1" 
AR Path="/262F604B365BED" Ref="#PWR027"  Part="1" 
AR Path="/384B365BED" Ref="#PWR028"  Part="1" 
F 0 "#PWR027" H 4400 13950 30  0001 C CNN
F 1 "VCC" H 4400 13950 30  0000 C CNN
	1    4400 13850
	1    0    0    -1  
$EndComp
Text Label 3400 16000 0    60   ~ 0
E*
$Comp
L VCC #PWR028
U 1 1 4B365BEB
P 4400 10700
AR Path="/4B365BEB" Ref="#PWR028"  Part="1" 
AR Path="/94B365BEB" Ref="#PWR4"  Part="1" 
AR Path="/5AD7153D4B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/A4B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/25E4B365BEB" Ref="#PWR025"  Part="1" 
AR Path="/6FF0DD404B365BEB" Ref="#PWR029"  Part="1" 
AR Path="/363030314B365BEB" Ref="#PWR025"  Part="1" 
AR Path="/DCBAABCD4B365BEB" Ref="#PWR011"  Part="1" 
AR Path="/6FE901F74B365BEB" Ref="#PWR013"  Part="1" 
AR Path="/3FE224DD4B365BEB" Ref="#PWR03"  Part="1" 
AR Path="/3FEFFFFF4B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/23D8D44B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/FFFFFFF04B365BEB" Ref="#PWR4"  Part="1" 
AR Path="/402955814B365BEB" Ref="#PWR03"  Part="1" 
AR Path="/A84B365BEB" Ref="#PWR5"  Part="1" 
AR Path="/402C08B44B365BEB" Ref="#PWR04"  Part="1" 
AR Path="/23D2034B365BEB" Ref="#PWR010"  Part="1" 
AR Path="/4E4B365BEB" Ref="#PWR04"  Part="1" 
AR Path="/314B365BEB" Ref="#PWR05"  Part="1" 
AR Path="/402BEF1A4B365BEB" Ref="#PWR05"  Part="1" 
AR Path="/54B365BEB" Ref="#PWR06"  Part="1" 
AR Path="/40293BE74B365BEB" Ref="#PWR07"  Part="1" 
AR Path="/402C55814B365BEB" Ref="#PWR010"  Part="1" 
AR Path="/40273BE74B365BEB" Ref="#PWR011"  Part="1" 
AR Path="/2600004B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/3D8EA0004B365BEB" Ref="#PWR011"  Part="1" 
AR Path="/402908B44B365BEB" Ref="#PWR07"  Part="1" 
AR Path="/3D6CC0004B365BEB" Ref="#PWR05"  Part="1" 
AR Path="/3D5A40004B365BEB" Ref="#PWR05"  Part="1" 
AR Path="/402755814B365BEB" Ref="#PWR013"  Part="1" 
AR Path="/4030AAC04B365BEB" Ref="#PWR013"  Part="1" 
AR Path="/14B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/23D9304B365BEB" Ref="#PWR014"  Part="1" 
AR Path="/4031DDF34B365BEB" Ref="#PWR014"  Part="1" 
AR Path="/3FE88B434B365BEB" Ref="#PWR014"  Part="1" 
AR Path="/4032778D4B365BEB" Ref="#PWR020"  Part="1" 
AR Path="/403091264B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/403051264B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/4032F78D4B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/403251264B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/4032AAC04B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/4030D1264B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/4031778D4B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/3FEA24DD4B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/6FE934E34B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/773F65F14B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/773F8EB44B365BEB" Ref="#PWR028"  Part="1" 
AR Path="/23C9F04B365BEB" Ref="#PWR15"  Part="1" 
AR Path="/23BC884B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/DC0C124B365BEB" Ref="#PWR026"  Part="1" 
AR Path="/6684D64B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/23C34C4B365BEB" Ref="#PWR033"  Part="1" 
AR Path="/23CBC44B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/23C6504B365BEB" Ref="#PWR028"  Part="1" 
AR Path="/39803EA4B365BEB" Ref="#PWR031"  Part="1" 
AR Path="/FFFFFFFF4B365BEB" Ref="#PWR033"  Part="1" 
AR Path="/D058A04B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/1607D44B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/D1C3804B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/24B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/23D70C4B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/2104B365BEB" Ref="#PWR032"  Part="1" 
AR Path="/F4BF4B365BEB" Ref="#PWR028"  Part="1" 
AR Path="/262F604B365BEB" Ref="#PWR028"  Part="1" 
AR Path="/384B365BEB" Ref="#PWR029"  Part="1" 
F 0 "#PWR028" H 4400 10800 30  0001 C CNN
F 1 "VCC" H 4400 10800 30  0000 C CNN
	1    4400 10700
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4B3658A6
P 2050 10700
AR Path="/23D9D84B3658A6" Ref="#PWR?"  Part="1" 
AR Path="/394433324B3658A6" Ref="#PWR?"  Part="1" 
AR Path="/4B3658A6" Ref="#PWR029"  Part="1" 
AR Path="/5AD7153D4B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/94B3658A6" Ref="#PWR2"  Part="1" 
AR Path="/755D912A4B3658A6" Ref="#PWR02"  Part="1" 
AR Path="/A4B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/25E4B3658A6" Ref="#PWR026"  Part="1" 
AR Path="/6FF0DD404B3658A6" Ref="#PWR030"  Part="1" 
AR Path="/363030314B3658A6" Ref="#PWR026"  Part="1" 
AR Path="/DCBAABCD4B3658A6" Ref="#PWR014"  Part="1" 
AR Path="/6FE901F74B3658A6" Ref="#PWR016"  Part="1" 
AR Path="/3FE224DD4B3658A6" Ref="#PWR06"  Part="1" 
AR Path="/3FEFFFFF4B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/23D8D44B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/FFFFFFF04B3658A6" Ref="#PWR2"  Part="1" 
AR Path="/402955814B3658A6" Ref="#PWR06"  Part="1" 
AR Path="/A84B3658A6" Ref="#PWR2"  Part="1" 
AR Path="/402C08B44B3658A6" Ref="#PWR07"  Part="1" 
AR Path="/23D2034B3658A6" Ref="#PWR013"  Part="1" 
AR Path="/4E4B3658A6" Ref="#PWR07"  Part="1" 
AR Path="/314B3658A6" Ref="#PWR08"  Part="1" 
AR Path="/402BEF1A4B3658A6" Ref="#PWR08"  Part="1" 
AR Path="/54B3658A6" Ref="#PWR09"  Part="1" 
AR Path="/40293BE74B3658A6" Ref="#PWR010"  Part="1" 
AR Path="/402C55814B3658A6" Ref="#PWR013"  Part="1" 
AR Path="/40273BE74B3658A6" Ref="#PWR014"  Part="1" 
AR Path="/2600004B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/3D8EA0004B3658A6" Ref="#PWR014"  Part="1" 
AR Path="/402908B44B3658A6" Ref="#PWR010"  Part="1" 
AR Path="/3D6CC0004B3658A6" Ref="#PWR08"  Part="1" 
AR Path="/3D5A40004B3658A6" Ref="#PWR08"  Part="1" 
AR Path="/402755814B3658A6" Ref="#PWR016"  Part="1" 
AR Path="/4030AAC04B3658A6" Ref="#PWR016"  Part="1" 
AR Path="/14B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/23D9304B3658A6" Ref="#PWR017"  Part="1" 
AR Path="/4031DDF34B3658A6" Ref="#PWR017"  Part="1" 
AR Path="/3FE88B434B3658A6" Ref="#PWR017"  Part="1" 
AR Path="/4032778D4B3658A6" Ref="#PWR023"  Part="1" 
AR Path="/403091264B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/403051264B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/4032F78D4B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/403251264B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/4032AAC04B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/4030D1264B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/4031778D4B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/3FEA24DD4B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/6FE934E34B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/773F65F14B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/773F8EB44B3658A6" Ref="#PWR029"  Part="1" 
AR Path="/23C9F04B3658A6" Ref="#PWR1"  Part="1" 
AR Path="/23BC884B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/DC0C124B3658A6" Ref="#PWR027"  Part="1" 
AR Path="/6684D64B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/23C34C4B3658A6" Ref="#PWR034"  Part="1" 
AR Path="/23CBC44B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/23C6504B3658A6" Ref="#PWR029"  Part="1" 
AR Path="/39803EA4B3658A6" Ref="#PWR032"  Part="1" 
AR Path="/FFFFFFFF4B3658A6" Ref="#PWR034"  Part="1" 
AR Path="/D058A04B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/1607D44B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/D1C3804B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/24B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/23D70C4B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/2104B3658A6" Ref="#PWR033"  Part="1" 
AR Path="/F4BF4B3658A6" Ref="#PWR029"  Part="1" 
AR Path="/262F604B3658A6" Ref="#PWR029"  Part="1" 
AR Path="/384B3658A6" Ref="#PWR030"  Part="1" 
F 0 "#PWR029" H 2050 10800 30  0001 C CNN
F 1 "VCC" H 2050 10800 30  0000 C CNN
	1    2050 10700
	1    0    0    -1  
$EndComp
Text Label 15750 3050 0    60   ~ 0
GND
Text Label 15750 3150 0    60   ~ 0
GND
Text Label 15750 1850 0    60   ~ 0
GND
Text Label 15750 1750 0    60   ~ 0
GND
Text Label 17400 2150 0    60   ~ 0
bA9
Text Label 17400 2250 0    60   ~ 0
bA10
Text Label 17400 2450 0    60   ~ 0
bA12
Text Label 17400 2350 0    60   ~ 0
bA11
Text Label 17400 2750 0    60   ~ 0
bA15
Text Label 17400 2650 0    60   ~ 0
bA14
Text Label 17400 2550 0    60   ~ 0
bA13
Text Label 17400 950  0    60   ~ 0
bA2
Text Label 17400 1150 0    60   ~ 0
bA4
Text Label 17400 1050 0    60   ~ 0
bA3
Text Label 17400 1450 0    60   ~ 0
bA7
Text Label 17400 1550 0    60   ~ 0
bA8
Text Label 17400 1350 0    60   ~ 0
bA6
Text Label 17400 1250 0    60   ~ 0
bA5
Text Label 1050 12850 0    60   ~ 0
D*
$Comp
L VCC #PWR?
U 1 1 4B365647
P 2050 13850
AR Path="/23D9D84B365647" Ref="#PWR?"  Part="1" 
AR Path="/394433324B365647" Ref="#PWR?"  Part="1" 
AR Path="/4B365647" Ref="#PWR030"  Part="1" 
AR Path="/5AD7153D4B365647" Ref="#PWR028"  Part="1" 
AR Path="/94B365647" Ref="#PWR21"  Part="1" 
AR Path="/755D912A4B365647" Ref="#PWR04"  Part="1" 
AR Path="/A4B365647" Ref="#PWR028"  Part="1" 
AR Path="/25E4B365647" Ref="#PWR027"  Part="1" 
AR Path="/6FF0DD404B365647" Ref="#PWR031"  Part="1" 
AR Path="/363030314B365647" Ref="#PWR027"  Part="1" 
AR Path="/DCBAABCD4B365647" Ref="#PWR016"  Part="1" 
AR Path="/6FE901F74B365647" Ref="#PWR018"  Part="1" 
AR Path="/3FE224DD4B365647" Ref="#PWR08"  Part="1" 
AR Path="/3FEFFFFF4B365647" Ref="#PWR028"  Part="1" 
AR Path="/23D8D44B365647" Ref="#PWR028"  Part="1" 
AR Path="/FFFFFFF04B365647" Ref="#PWR28"  Part="1" 
AR Path="/402955814B365647" Ref="#PWR08"  Part="1" 
AR Path="/A84B365647" Ref="#PWR20"  Part="1" 
AR Path="/402C08B44B365647" Ref="#PWR09"  Part="1" 
AR Path="/23D2034B365647" Ref="#PWR015"  Part="1" 
AR Path="/4E4B365647" Ref="#PWR09"  Part="1" 
AR Path="/314B365647" Ref="#PWR010"  Part="1" 
AR Path="/402BEF1A4B365647" Ref="#PWR010"  Part="1" 
AR Path="/54B365647" Ref="#PWR011"  Part="1" 
AR Path="/40293BE74B365647" Ref="#PWR012"  Part="1" 
AR Path="/402C55814B365647" Ref="#PWR015"  Part="1" 
AR Path="/40273BE74B365647" Ref="#PWR016"  Part="1" 
AR Path="/2600004B365647" Ref="#PWR028"  Part="1" 
AR Path="/3D8EA0004B365647" Ref="#PWR016"  Part="1" 
AR Path="/402908B44B365647" Ref="#PWR012"  Part="1" 
AR Path="/3D6CC0004B365647" Ref="#PWR010"  Part="1" 
AR Path="/3D5A40004B365647" Ref="#PWR010"  Part="1" 
AR Path="/402755814B365647" Ref="#PWR018"  Part="1" 
AR Path="/4030AAC04B365647" Ref="#PWR018"  Part="1" 
AR Path="/14B365647" Ref="#PWR034"  Part="1" 
AR Path="/23D9304B365647" Ref="#PWR019"  Part="1" 
AR Path="/4031DDF34B365647" Ref="#PWR019"  Part="1" 
AR Path="/3FE88B434B365647" Ref="#PWR019"  Part="1" 
AR Path="/4032778D4B365647" Ref="#PWR025"  Part="1" 
AR Path="/403091264B365647" Ref="#PWR028"  Part="1" 
AR Path="/403051264B365647" Ref="#PWR028"  Part="1" 
AR Path="/4032F78D4B365647" Ref="#PWR028"  Part="1" 
AR Path="/403251264B365647" Ref="#PWR028"  Part="1" 
AR Path="/4032AAC04B365647" Ref="#PWR028"  Part="1" 
AR Path="/4030D1264B365647" Ref="#PWR028"  Part="1" 
AR Path="/4031778D4B365647" Ref="#PWR028"  Part="1" 
AR Path="/3FEA24DD4B365647" Ref="#PWR028"  Part="1" 
AR Path="/6FE934E34B365647" Ref="#PWR034"  Part="1" 
AR Path="/773F65F14B365647" Ref="#PWR034"  Part="1" 
AR Path="/773F8EB44B365647" Ref="#PWR030"  Part="1" 
AR Path="/23C9F04B365647" Ref="#PWR5"  Part="1" 
AR Path="/23BC884B365647" Ref="#PWR034"  Part="1" 
AR Path="/DC0C124B365647" Ref="#PWR028"  Part="1" 
AR Path="/6684D64B365647" Ref="#PWR034"  Part="1" 
AR Path="/23C34C4B365647" Ref="#PWR035"  Part="1" 
AR Path="/23CBC44B365647" Ref="#PWR034"  Part="1" 
AR Path="/23C6504B365647" Ref="#PWR030"  Part="1" 
AR Path="/39803EA4B365647" Ref="#PWR033"  Part="1" 
AR Path="/FFFFFFFF4B365647" Ref="#PWR035"  Part="1" 
AR Path="/D058A04B365647" Ref="#PWR034"  Part="1" 
AR Path="/1607D44B365647" Ref="#PWR034"  Part="1" 
AR Path="/D1C3804B365647" Ref="#PWR034"  Part="1" 
AR Path="/24B365647" Ref="#PWR034"  Part="1" 
AR Path="/23D70C4B365647" Ref="#PWR034"  Part="1" 
AR Path="/2104B365647" Ref="#PWR034"  Part="1" 
AR Path="/F4BF4B365647" Ref="#PWR030"  Part="1" 
AR Path="/262F604B365647" Ref="#PWR030"  Part="1" 
AR Path="/384B365647" Ref="#PWR031"  Part="1" 
F 0 "#PWR030" H 2050 13950 30  0001 C CNN
F 1 "VCC" H 2050 13950 30  0000 C CNN
	1    2050 13850
	1    0    0    -1  
$EndComp
Text Label 15650 4350 0    60   ~ 0
sMEMR*
Text Label 15650 4450 0    60   ~ 0
A*
Text Label 17400 3450 0    60   ~ 0
OD0
Text Label 17400 3550 0    60   ~ 0
OD1
Text Label 17400 3650 0    60   ~ 0
OD2
Text Label 17400 3750 0    60   ~ 0
OD3
Text Label 17400 3850 0    60   ~ 0
OD4
Text Label 17400 3950 0    60   ~ 0
OD5
Text Label 17400 4050 0    60   ~ 0
OD6
Text Label 17400 4150 0    60   ~ 0
OD7
Text Label 17400 4750 0    60   ~ 0
ED0
Text Label 17400 4850 0    60   ~ 0
ED1
Text Label 17400 4950 0    60   ~ 0
ED2
Text Label 17400 5050 0    60   ~ 0
ED3
Text Label 17400 5150 0    60   ~ 0
ED4
Text Label 17400 5250 0    60   ~ 0
ED5
Text Label 17400 5350 0    60   ~ 0
ED6
Text Label 17400 5450 0    60   ~ 0
ED7
Text Notes 1650 10550 0    60   ~ 0
ODD BYTE RAM
Text Notes 1650 13700 0    60   ~ 0
EVEN BYTE RAM
Text Label 15650 5750 0    60   ~ 0
B*
Text Label 15650 7050 0    60   ~ 0
C*
Text Label 15650 6950 0    60   ~ 0
sMEMR*
Text Label 1050 14000 0    60   ~ 0
bA1
Text Label 1050 14100 0    60   ~ 0
bA2
Text Label 1050 14300 0    60   ~ 0
bA4
Text Label 1050 14200 0    60   ~ 0
bA3
Text Label 1050 14600 0    60   ~ 0
bA7
Text Label 1050 14700 0    60   ~ 0
bA8
Text Label 1050 14500 0    60   ~ 0
bA6
Text Label 1050 14400 0    60   ~ 0
bA5
Text Label 1050 15000 0    60   ~ 0
bA11
Text Label 1050 14900 0    60   ~ 0
bA10
Text Label 1050 14800 0    60   ~ 0
bA9
Text Label 1050 12350 0    60   ~ 0
bA16
Text Label 1050 12550 0    60   ~ 0
bA18
Text Label 1050 11650 0    60   ~ 0
bA9
Text Label 1050 11750 0    60   ~ 0
bA10
Text Label 1050 11850 0    60   ~ 0
bA11
Text Label 1050 12050 0    60   ~ 0
bA13
Text Label 1050 11250 0    60   ~ 0
bA5
Text Label 1050 11350 0    60   ~ 0
bA6
Text Label 1050 11550 0    60   ~ 0
bA8
Text Label 1050 11450 0    60   ~ 0
bA7
Text Label 1050 11050 0    60   ~ 0
bA3
Text Label 1050 11150 0    60   ~ 0
bA4
Text Label 1050 10950 0    60   ~ 0
bA2
Text Label 1050 10850 0    60   ~ 0
bA1
Text Label 15750 2750 0    60   ~ 0
A15
Text Label 15750 2650 0    60   ~ 0
A14
Text Label 15750 2550 0    60   ~ 0
A13
Text Label 15750 2450 0    60   ~ 0
A12
Text Label 15750 2350 0    60   ~ 0
A11
Text Label 15750 2250 0    60   ~ 0
A10
Text Label 15750 2150 0    60   ~ 0
A9
Text Label 15750 1550 0    60   ~ 0
A8
Text Label 15750 1450 0    60   ~ 0
A7
Text Label 15750 1350 0    60   ~ 0
A6
Text Label 15750 1250 0    60   ~ 0
A5
Text Label 15750 1150 0    60   ~ 0
A4
Text Label 15750 1050 0    60   ~ 0
A3
Text Label 15750 950  0    60   ~ 0
A2
Text Label 15750 850  0    60   ~ 0
A1
Text Label 15650 6050 0    60   ~ 0
DO0
Text Label 15650 6150 0    60   ~ 0
DO1
Text Label 15650 6250 0    60   ~ 0
DO2
Text Label 15650 6350 0    60   ~ 0
DO3
Text Label 15650 6450 0    60   ~ 0
DO4
Text Label 15650 6550 0    60   ~ 0
DO5
Text Label 15650 6650 0    60   ~ 0
DO6
Text Label 15650 6750 0    60   ~ 0
DO7
Text Label 15650 4150 0    60   ~ 0
DI7
Text Label 15650 4050 0    60   ~ 0
DI6
Text Label 15650 3950 0    60   ~ 0
DI5
Text Label 15650 3850 0    60   ~ 0
DI4
Text Label 15650 3750 0    60   ~ 0
DI3
Text Label 15650 3650 0    60   ~ 0
DI2
Text Label 15650 3550 0    60   ~ 0
DI1
Text Label 15650 3450 0    60   ~ 0
DI0
$Comp
L 74LS244 U?
U 1 1 4B364E8E
P 16650 2650
AR Path="/23D9D84B364E8E" Ref="U?"  Part="1" 
AR Path="/394433324B364E8E" Ref="U?"  Part="1" 
AR Path="/1C503B04B364E8E" Ref="U?"  Part="1" 
AR Path="/23D1104B364E8E" Ref="U24"  Part="1" 
AR Path="/314433324B364E8E" Ref="U24"  Part="1" 
AR Path="/5AD7153D4B364E8E" Ref="U24"  Part="1" 
AR Path="/23C2344B364E8E" Ref="U24"  Part="1" 
AR Path="/4B364E8E" Ref="U24"  Part="1" 
AR Path="/94B364E8E" Ref="U24"  Part="1" 
AR Path="/755D912A4B364E8E" Ref="U24"  Part="1" 
AR Path="/A4B364E8E" Ref="U24"  Part="1" 
AR Path="/14B364E8E" Ref="U24"  Part="1" 
AR Path="/6FF0DD404B364E8E" Ref="U24"  Part="1" 
AR Path="/DCBAABCD4B364E8E" Ref="U24"  Part="1" 
AR Path="/6FE901F74B364E8E" Ref="U24"  Part="1" 
AR Path="/3FE224DD4B364E8E" Ref="U24"  Part="1" 
AR Path="/3FEFFFFF4B364E8E" Ref="U24"  Part="1" 
AR Path="/23D8D44B364E8E" Ref="U24"  Part="1" 
AR Path="/FFFFFFF04B364E8E" Ref="U24"  Part="1" 
AR Path="/402955814B364E8E" Ref="U24"  Part="1" 
AR Path="/A84B364E8E" Ref="U24"  Part="1" 
AR Path="/402C08B44B364E8E" Ref="U24"  Part="1" 
AR Path="/23D2034B364E8E" Ref="U24"  Part="1" 
AR Path="/4E4B364E8E" Ref="U24"  Part="1" 
AR Path="/402BEF1A4B364E8E" Ref="U24"  Part="1" 
AR Path="/54B364E8E" Ref="U24"  Part="1" 
AR Path="/40293BE74B364E8E" Ref="U24"  Part="1" 
AR Path="/402C55814B364E8E" Ref="U24"  Part="1" 
AR Path="/40273BE74B364E8E" Ref="U24"  Part="1" 
AR Path="/2600004B364E8E" Ref="U24"  Part="1" 
AR Path="/3D8EA0004B364E8E" Ref="U24"  Part="1" 
AR Path="/402908B44B364E8E" Ref="U24"  Part="1" 
AR Path="/3D6CC0004B364E8E" Ref="U24"  Part="1" 
AR Path="/3D5A40004B364E8E" Ref="U24"  Part="1" 
AR Path="/402755814B364E8E" Ref="U24"  Part="1" 
AR Path="/4030AAC04B364E8E" Ref="U24"  Part="1" 
AR Path="/300004B364E8E" Ref="U24"  Part="1" 
AR Path="/23D9304B364E8E" Ref="U24"  Part="1" 
AR Path="/4031DDF34B364E8E" Ref="U24"  Part="1" 
AR Path="/3FE88B434B364E8E" Ref="U24"  Part="1" 
AR Path="/4032778D4B364E8E" Ref="U24"  Part="1" 
AR Path="/403091264B364E8E" Ref="U24"  Part="1" 
AR Path="/403051264B364E8E" Ref="U24"  Part="1" 
AR Path="/4032F78D4B364E8E" Ref="U24"  Part="1" 
AR Path="/403251264B364E8E" Ref="U24"  Part="1" 
AR Path="/4032AAC04B364E8E" Ref="U24"  Part="1" 
AR Path="/4030D1264B364E8E" Ref="U24"  Part="1" 
AR Path="/4031778D4B364E8E" Ref="U24"  Part="1" 
AR Path="/3FEA24DD4B364E8E" Ref="U24"  Part="1" 
AR Path="/6FE934E34B364E8E" Ref="U24"  Part="1" 
AR Path="/773F65F14B364E8E" Ref="U24"  Part="1" 
AR Path="/773F8EB44B364E8E" Ref="U24"  Part="1" 
AR Path="/23C9F04B364E8E" Ref="U24"  Part="1" 
AR Path="/24B364E8E" Ref="U24"  Part="1" 
AR Path="/23BC884B364E8E" Ref="U24"  Part="1" 
AR Path="/DC0C124B364E8E" Ref="U24"  Part="1" 
AR Path="/23C34C4B364E8E" Ref="U24"  Part="1" 
AR Path="/23CBC44B364E8E" Ref="U24"  Part="1" 
AR Path="/23C6504B364E8E" Ref="U24"  Part="1" 
AR Path="/39803EA4B364E8E" Ref="U24"  Part="1" 
AR Path="/FFFFFFFF4B364E8E" Ref="U24"  Part="1" 
AR Path="/D058A04B364E8E" Ref="U24"  Part="1" 
AR Path="/1607D44B364E8E" Ref="U24"  Part="1" 
AR Path="/D1C3804B364E8E" Ref="U24"  Part="1" 
AR Path="/23D70C4B364E8E" Ref="U24"  Part="1" 
AR Path="/2104B364E8E" Ref="U24"  Part="1" 
AR Path="/F4BF4B364E8E" Ref="U24"  Part="1" 
AR Path="/262F604B364E8E" Ref="U24"  Part="1" 
AR Path="/384B364E8E" Ref="U24"  Part="1" 
F 0 "U24" H 16700 2450 60  0000 C CNN
F 1 "74LS244" H 16750 2250 60  0000 C CNN
F 2 "20dip300" H 16650 2650 60  0001 C CNN
	1    16650 2650
	1    0    0    -1  
$EndComp
$Comp
L 74LS244 U?
U 1 1 4B364E81
P 16650 1350
AR Path="/23D9D84B364E81" Ref="U?"  Part="1" 
AR Path="/394433324B364E81" Ref="U?"  Part="1" 
AR Path="/1C503B04B364E81" Ref="U?"  Part="1" 
AR Path="/23D1104B364E81" Ref="U23"  Part="1" 
AR Path="/314433324B364E81" Ref="U23"  Part="1" 
AR Path="/5AD7153D4B364E81" Ref="U23"  Part="1" 
AR Path="/23C2344B364E81" Ref="U23"  Part="1" 
AR Path="/4B364E81" Ref="U23"  Part="1" 
AR Path="/94B364E81" Ref="U23"  Part="1" 
AR Path="/755D912A4B364E81" Ref="U23"  Part="1" 
AR Path="/A4B364E81" Ref="U23"  Part="1" 
AR Path="/14B364E81" Ref="U23"  Part="1" 
AR Path="/6FF0DD404B364E81" Ref="U23"  Part="1" 
AR Path="/DCBAABCD4B364E81" Ref="U23"  Part="1" 
AR Path="/6FE901F74B364E81" Ref="U23"  Part="1" 
AR Path="/3FE224DD4B364E81" Ref="U23"  Part="1" 
AR Path="/3FEFFFFF4B364E81" Ref="U23"  Part="1" 
AR Path="/23D8D44B364E81" Ref="U23"  Part="1" 
AR Path="/FFFFFFF04B364E81" Ref="U23"  Part="1" 
AR Path="/402955814B364E81" Ref="U23"  Part="1" 
AR Path="/A84B364E81" Ref="U23"  Part="1" 
AR Path="/402C08B44B364E81" Ref="U23"  Part="1" 
AR Path="/23D2034B364E81" Ref="U23"  Part="1" 
AR Path="/4E4B364E81" Ref="U23"  Part="1" 
AR Path="/402BEF1A4B364E81" Ref="U23"  Part="1" 
AR Path="/54B364E81" Ref="U23"  Part="1" 
AR Path="/40293BE74B364E81" Ref="U23"  Part="1" 
AR Path="/402C55814B364E81" Ref="U23"  Part="1" 
AR Path="/40273BE74B364E81" Ref="U23"  Part="1" 
AR Path="/2600004B364E81" Ref="U23"  Part="1" 
AR Path="/3D8EA0004B364E81" Ref="U23"  Part="1" 
AR Path="/402908B44B364E81" Ref="U23"  Part="1" 
AR Path="/3D6CC0004B364E81" Ref="U23"  Part="1" 
AR Path="/3D5A40004B364E81" Ref="U23"  Part="1" 
AR Path="/402755814B364E81" Ref="U23"  Part="1" 
AR Path="/4030AAC04B364E81" Ref="U23"  Part="1" 
AR Path="/23D9304B364E81" Ref="U23"  Part="1" 
AR Path="/4031DDF34B364E81" Ref="U23"  Part="1" 
AR Path="/3FE88B434B364E81" Ref="U23"  Part="1" 
AR Path="/4032778D4B364E81" Ref="U23"  Part="1" 
AR Path="/403091264B364E81" Ref="U23"  Part="1" 
AR Path="/403051264B364E81" Ref="U23"  Part="1" 
AR Path="/4032F78D4B364E81" Ref="U23"  Part="1" 
AR Path="/403251264B364E81" Ref="U23"  Part="1" 
AR Path="/4032AAC04B364E81" Ref="U23"  Part="1" 
AR Path="/4030D1264B364E81" Ref="U23"  Part="1" 
AR Path="/4031778D4B364E81" Ref="U23"  Part="1" 
AR Path="/3FEA24DD4B364E81" Ref="U23"  Part="1" 
AR Path="/6FE934E34B364E81" Ref="U23"  Part="1" 
AR Path="/773F65F14B364E81" Ref="U23"  Part="1" 
AR Path="/773F8EB44B364E81" Ref="U23"  Part="1" 
AR Path="/23C9F04B364E81" Ref="U23"  Part="1" 
AR Path="/24B364E81" Ref="U23"  Part="1" 
AR Path="/23BC884B364E81" Ref="U23"  Part="1" 
AR Path="/DC0C124B364E81" Ref="U23"  Part="1" 
AR Path="/23C34C4B364E81" Ref="U23"  Part="1" 
AR Path="/23CBC44B364E81" Ref="U23"  Part="1" 
AR Path="/23C6504B364E81" Ref="U23"  Part="1" 
AR Path="/39803EA4B364E81" Ref="U23"  Part="1" 
AR Path="/FFFFFFFF4B364E81" Ref="U23"  Part="1" 
AR Path="/D058A04B364E81" Ref="U23"  Part="1" 
AR Path="/1607D44B364E81" Ref="U23"  Part="1" 
AR Path="/D1C3804B364E81" Ref="U23"  Part="1" 
AR Path="/23D70C4B364E81" Ref="U23"  Part="1" 
AR Path="/2104B364E81" Ref="U23"  Part="1" 
AR Path="/F4BF4B364E81" Ref="U23"  Part="1" 
AR Path="/262F604B364E81" Ref="U23"  Part="1" 
AR Path="/384B364E81" Ref="U23"  Part="1" 
F 0 "U23" H 16700 1150 60  0000 C CNN
F 1 "74LS244" H 16750 950 60  0000 C CNN
F 2 "20dip300" H 16650 1350 60  0001 C CNN
	1    16650 1350
	1    0    0    -1  
$EndComp
$Comp
L SRAM_512KO U?
U 1 1 4B364E65
P 2050 15100
AR Path="/23D9D84B364E65" Ref="U?"  Part="1" 
AR Path="/394433324B364E65" Ref="U?"  Part="1" 
AR Path="/4B364E65" Ref="U101"  Part="1" 
AR Path="/23D1104B364E65" Ref="U101"  Part="1" 
AR Path="/5AD7153D4B364E65" Ref="U101"  Part="1" 
AR Path="/94B364E65" Ref="U101"  Part="1" 
AR Path="/755D912A4B364E65" Ref="U101"  Part="1" 
AR Path="/A4B364E65" Ref="U101"  Part="1" 
AR Path="/14B364E65" Ref="U101"  Part="1" 
AR Path="/6FF0DD404B364E65" Ref="U101"  Part="1" 
AR Path="/DCBAABCD4B364E65" Ref="U101"  Part="1" 
AR Path="/6FE901F74B364E65" Ref="U101"  Part="1" 
AR Path="/3FE224DD4B364E65" Ref="U101"  Part="1" 
AR Path="/3FEFFFFF4B364E65" Ref="U101"  Part="1" 
AR Path="/23D8D44B364E65" Ref="U101"  Part="1" 
AR Path="/FFFFFFF04B364E65" Ref="U101"  Part="1" 
AR Path="/402955814B364E65" Ref="U101"  Part="1" 
AR Path="/A84B364E65" Ref="U101"  Part="1" 
AR Path="/402C08B44B364E65" Ref="U101"  Part="1" 
AR Path="/23D2034B364E65" Ref="U101"  Part="1" 
AR Path="/4E4B364E65" Ref="U101"  Part="1" 
AR Path="/402BEF1A4B364E65" Ref="U101"  Part="1" 
AR Path="/54B364E65" Ref="U101"  Part="1" 
AR Path="/40293BE74B364E65" Ref="U101"  Part="1" 
AR Path="/402C55814B364E65" Ref="U101"  Part="1" 
AR Path="/40273BE74B364E65" Ref="U101"  Part="1" 
AR Path="/2600004B364E65" Ref="U101"  Part="1" 
AR Path="/3D8EA0004B364E65" Ref="U101"  Part="1" 
AR Path="/402908B44B364E65" Ref="U101"  Part="1" 
AR Path="/3D6CC0004B364E65" Ref="U101"  Part="1" 
AR Path="/3D5A40004B364E65" Ref="U101"  Part="1" 
AR Path="/402755814B364E65" Ref="U101"  Part="1" 
AR Path="/4030AAC04B364E65" Ref="U101"  Part="1" 
AR Path="/23D9304B364E65" Ref="U101"  Part="1" 
AR Path="/4031DDF34B364E65" Ref="U101"  Part="1" 
AR Path="/3FE88B434B364E65" Ref="U101"  Part="1" 
AR Path="/4032778D4B364E65" Ref="U101"  Part="1" 
AR Path="/403091264B364E65" Ref="U101"  Part="1" 
AR Path="/403051264B364E65" Ref="U101"  Part="1" 
AR Path="/4032F78D4B364E65" Ref="U101"  Part="1" 
AR Path="/403251264B364E65" Ref="U101"  Part="1" 
AR Path="/4032AAC04B364E65" Ref="U101"  Part="1" 
AR Path="/4030D1264B364E65" Ref="U101"  Part="1" 
AR Path="/4031778D4B364E65" Ref="U101"  Part="1" 
AR Path="/3FEA24DD4B364E65" Ref="U101"  Part="1" 
AR Path="/6FE934E34B364E65" Ref="U101"  Part="1" 
AR Path="/773F65F14B364E65" Ref="U101"  Part="1" 
AR Path="/773F8EB44B364E65" Ref="U101"  Part="1" 
AR Path="/23C9F04B364E65" Ref="U101"  Part="1" 
AR Path="/24B364E65" Ref="U101"  Part="1" 
AR Path="/23BC884B364E65" Ref="U101"  Part="1" 
AR Path="/DC0C124B364E65" Ref="U101"  Part="1" 
AR Path="/23C34C4B364E65" Ref="U101"  Part="1" 
AR Path="/23CBC44B364E65" Ref="U101"  Part="1" 
AR Path="/23C6504B364E65" Ref="U101"  Part="1" 
AR Path="/39803EA4B364E65" Ref="U101"  Part="1" 
AR Path="/FFFFFFFF4B364E65" Ref="U101"  Part="1" 
AR Path="/D058A04B364E65" Ref="U101"  Part="1" 
AR Path="/1607D44B364E65" Ref="U101"  Part="1" 
AR Path="/D1C3804B364E65" Ref="U101"  Part="1" 
AR Path="/23D70C4B364E65" Ref="U101"  Part="1" 
AR Path="/2104B364E65" Ref="U101"  Part="1" 
AR Path="/F4BF4B364E65" Ref="U101"  Part="1" 
AR Path="/262F604B364E65" Ref="U101"  Part="1" 
AR Path="/384B364E65" Ref="U101"  Part="1" 
F 0 "U101" H 2150 16300 70  0000 L CNN
F 1 "SRAM_512KO" H 2150 13900 70  0000 L CNN
F 2 "32dip600" H 2050 15100 60  0001 C CNN
	1    2050 15100
	1    0    0    -1  
$EndComp
$Comp
L SRAM_512KO U?
U 1 1 4B364E5B
P 2050 11950
AR Path="/CEEBE84B364E5B" Ref="U?"  Part="1" 
AR Path="/424545434B364E5B" Ref="U?"  Part="1" 
AR Path="/74B364E5B" Ref="U?"  Part="1" 
AR Path="/14B364E5B" Ref="U100"  Part="1" 
AR Path="/334234314B364E5B" Ref="U100"  Part="1" 
AR Path="/5AD7153D4B364E5B" Ref="U100"  Part="1" 
AR Path="/23C2344B364E5B" Ref="U100"  Part="1" 
AR Path="/4B364E5B" Ref="U100"  Part="1" 
AR Path="/94B364E5B" Ref="U100"  Part="1" 
AR Path="/755D912A4B364E5B" Ref="U100"  Part="1" 
AR Path="/A4B364E5B" Ref="U100"  Part="1" 
AR Path="/6FF0DD404B364E5B" Ref="U100"  Part="1" 
AR Path="/DCBAABCD4B364E5B" Ref="U100"  Part="1" 
AR Path="/6FE901F74B364E5B" Ref="U100"  Part="1" 
AR Path="/3FE224DD4B364E5B" Ref="U100"  Part="1" 
AR Path="/3FEFFFFF4B364E5B" Ref="U100"  Part="1" 
AR Path="/23D8D44B364E5B" Ref="U100"  Part="1" 
AR Path="/FFFFFFF04B364E5B" Ref="U100"  Part="1" 
AR Path="/402955814B364E5B" Ref="U100"  Part="1" 
AR Path="/A84B364E5B" Ref="U100"  Part="1" 
AR Path="/402C08B44B364E5B" Ref="U100"  Part="1" 
AR Path="/23D2034B364E5B" Ref="U100"  Part="1" 
AR Path="/4E4B364E5B" Ref="U100"  Part="1" 
AR Path="/402BEF1A4B364E5B" Ref="U100"  Part="1" 
AR Path="/54B364E5B" Ref="U100"  Part="1" 
AR Path="/40293BE74B364E5B" Ref="U100"  Part="1" 
AR Path="/402C55814B364E5B" Ref="U100"  Part="1" 
AR Path="/40273BE74B364E5B" Ref="U100"  Part="1" 
AR Path="/2600004B364E5B" Ref="U100"  Part="1" 
AR Path="/3D8EA0004B364E5B" Ref="U100"  Part="1" 
AR Path="/402908B44B364E5B" Ref="U100"  Part="1" 
AR Path="/3D6CC0004B364E5B" Ref="U100"  Part="1" 
AR Path="/3D5A40004B364E5B" Ref="U100"  Part="1" 
AR Path="/402755814B364E5B" Ref="U100"  Part="1" 
AR Path="/4030AAC04B364E5B" Ref="U100"  Part="1" 
AR Path="/23D9304B364E5B" Ref="U100"  Part="1" 
AR Path="/4031DDF34B364E5B" Ref="U100"  Part="1" 
AR Path="/3FE88B434B364E5B" Ref="U100"  Part="1" 
AR Path="/4032778D4B364E5B" Ref="U100"  Part="1" 
AR Path="/403091264B364E5B" Ref="U100"  Part="1" 
AR Path="/403051264B364E5B" Ref="U100"  Part="1" 
AR Path="/4032F78D4B364E5B" Ref="U100"  Part="1" 
AR Path="/403251264B364E5B" Ref="U100"  Part="1" 
AR Path="/4032AAC04B364E5B" Ref="U100"  Part="1" 
AR Path="/4030D1264B364E5B" Ref="U100"  Part="1" 
AR Path="/4031778D4B364E5B" Ref="U100"  Part="1" 
AR Path="/3FEA24DD4B364E5B" Ref="U100"  Part="1" 
AR Path="/6FE934E34B364E5B" Ref="U100"  Part="1" 
AR Path="/773F65F14B364E5B" Ref="U100"  Part="1" 
AR Path="/773F8EB44B364E5B" Ref="U100"  Part="1" 
AR Path="/23C9F04B364E5B" Ref="U100"  Part="1" 
AR Path="/24B364E5B" Ref="U100"  Part="1" 
AR Path="/23BC884B364E5B" Ref="U100"  Part="1" 
AR Path="/DC0C124B364E5B" Ref="U100"  Part="1" 
AR Path="/23C34C4B364E5B" Ref="U100"  Part="1" 
AR Path="/23CBC44B364E5B" Ref="U100"  Part="1" 
AR Path="/23C6504B364E5B" Ref="U100"  Part="1" 
AR Path="/39803EA4B364E5B" Ref="U100"  Part="1" 
AR Path="/FFFFFFFF4B364E5B" Ref="U100"  Part="1" 
AR Path="/D058A04B364E5B" Ref="U100"  Part="1" 
AR Path="/1607D44B364E5B" Ref="U100"  Part="1" 
AR Path="/D1C3804B364E5B" Ref="U100"  Part="1" 
AR Path="/23D70C4B364E5B" Ref="U100"  Part="1" 
AR Path="/2104B364E5B" Ref="U100"  Part="1" 
AR Path="/F4BF4B364E5B" Ref="U100"  Part="1" 
AR Path="/262F604B364E5B" Ref="U100"  Part="1" 
AR Path="/384B364E5B" Ref="U100"  Part="1" 
F 0 "U100" H 2150 13150 70  0000 L CNN
F 1 "SRAM_512KO" H 2150 10750 70  0000 L CNN
F 2 "32dip600" H 2050 11950 60  0001 C CNN
	1    2050 11950
	1    0    0    -1  
$EndComp
$Comp
L 74LS245 U?
U 1 1 4B364E0D
P 16650 6550
AR Path="/23D9D84B364E0D" Ref="U?"  Part="1" 
AR Path="/394433324B364E0D" Ref="U?"  Part="1" 
AR Path="/1C503B04B364E0D" Ref="U?"  Part="1" 
AR Path="/23D0D84B364E0D" Ref="U22"  Part="1" 
AR Path="/304433324B364E0D" Ref="U22"  Part="1" 
AR Path="/5AD7153D4B364E0D" Ref="U22"  Part="1" 
AR Path="/23C2344B364E0D" Ref="U22"  Part="1" 
AR Path="/4B364E0D" Ref="U22"  Part="1" 
AR Path="/94B364E0D" Ref="U22"  Part="1" 
AR Path="/755D912A4B364E0D" Ref="U22"  Part="1" 
AR Path="/A4B364E0D" Ref="U22"  Part="1" 
AR Path="/14B364E0D" Ref="U22"  Part="1" 
AR Path="/6FF0DD404B364E0D" Ref="U22"  Part="1" 
AR Path="/DCBAABCD4B364E0D" Ref="U22"  Part="1" 
AR Path="/6FE901F74B364E0D" Ref="U22"  Part="1" 
AR Path="/3FE224DD4B364E0D" Ref="U22"  Part="1" 
AR Path="/3FEFFFFF4B364E0D" Ref="U22"  Part="1" 
AR Path="/23D8D44B364E0D" Ref="U22"  Part="1" 
AR Path="/FFFFFFF04B364E0D" Ref="U22"  Part="1" 
AR Path="/402955814B364E0D" Ref="U22"  Part="1" 
AR Path="/A84B364E0D" Ref="U22"  Part="1" 
AR Path="/402C08B44B364E0D" Ref="U22"  Part="1" 
AR Path="/23D2034B364E0D" Ref="U22"  Part="1" 
AR Path="/4E4B364E0D" Ref="U22"  Part="1" 
AR Path="/402BEF1A4B364E0D" Ref="U22"  Part="1" 
AR Path="/54B364E0D" Ref="U22"  Part="1" 
AR Path="/40293BE74B364E0D" Ref="U22"  Part="1" 
AR Path="/402C55814B364E0D" Ref="U22"  Part="1" 
AR Path="/40273BE74B364E0D" Ref="U22"  Part="1" 
AR Path="/2600004B364E0D" Ref="U22"  Part="1" 
AR Path="/3D8EA0004B364E0D" Ref="U22"  Part="1" 
AR Path="/402908B44B364E0D" Ref="U22"  Part="1" 
AR Path="/3D6CC0004B364E0D" Ref="U22"  Part="1" 
AR Path="/3D5A40004B364E0D" Ref="U22"  Part="1" 
AR Path="/402755814B364E0D" Ref="U22"  Part="1" 
AR Path="/4030AAC04B364E0D" Ref="U22"  Part="1" 
AR Path="/23D9304B364E0D" Ref="U22"  Part="1" 
AR Path="/4031DDF34B364E0D" Ref="U22"  Part="1" 
AR Path="/3FE88B434B364E0D" Ref="U22"  Part="1" 
AR Path="/4032778D4B364E0D" Ref="U22"  Part="1" 
AR Path="/403091264B364E0D" Ref="U22"  Part="1" 
AR Path="/403051264B364E0D" Ref="U22"  Part="1" 
AR Path="/4032F78D4B364E0D" Ref="U22"  Part="1" 
AR Path="/403251264B364E0D" Ref="U22"  Part="1" 
AR Path="/4032AAC04B364E0D" Ref="U22"  Part="1" 
AR Path="/4030D1264B364E0D" Ref="U22"  Part="1" 
AR Path="/4031778D4B364E0D" Ref="U22"  Part="1" 
AR Path="/3FEA24DD4B364E0D" Ref="U22"  Part="1" 
AR Path="/6FE934E34B364E0D" Ref="U22"  Part="1" 
AR Path="/773F65F14B364E0D" Ref="U22"  Part="1" 
AR Path="/773F8EB44B364E0D" Ref="U22"  Part="1" 
AR Path="/23C9F04B364E0D" Ref="U22"  Part="1" 
AR Path="/24B364E0D" Ref="U22"  Part="1" 
AR Path="/23BC884B364E0D" Ref="U22"  Part="1" 
AR Path="/DC0C124B364E0D" Ref="U22"  Part="1" 
AR Path="/23C34C4B364E0D" Ref="U22"  Part="1" 
AR Path="/23CBC44B364E0D" Ref="U22"  Part="1" 
AR Path="/23C6504B364E0D" Ref="U22"  Part="1" 
AR Path="/39803EA4B364E0D" Ref="U22"  Part="1" 
AR Path="/FFFFFFFF4B364E0D" Ref="U22"  Part="1" 
AR Path="/D058A04B364E0D" Ref="U22"  Part="1" 
AR Path="/1607D44B364E0D" Ref="U22"  Part="1" 
AR Path="/D1C3804B364E0D" Ref="U22"  Part="1" 
AR Path="/23D70C4B364E0D" Ref="U22"  Part="1" 
AR Path="/2104B364E0D" Ref="U22"  Part="1" 
AR Path="/F4BF4B364E0D" Ref="U22"  Part="1" 
AR Path="/262F604B364E0D" Ref="U22"  Part="1" 
AR Path="/384B364E0D" Ref="U22"  Part="1" 
F 0 "U22" H 16750 7150 60  0000 L CNN
F 1 "74LS245" H 16750 5950 60  0000 L CNN
F 2 "20dip300" H 16650 6550 60  0001 C CNN
	1    16650 6550
	1    0    0    -1  
$EndComp
$Comp
L 74LS245 U?
U 1 1 4B364DE9
P 16650 3950
AR Path="/69698AFC4B364DE9" Ref="U?"  Part="1" 
AR Path="/393639364B364DE9" Ref="U?"  Part="1" 
AR Path="/4B364DE9" Ref="U19"  Part="1" 
AR Path="/23D1104B364DE9" Ref="U19"  Part="1" 
AR Path="/5AD7153D4B364DE9" Ref="U19"  Part="1" 
AR Path="/94B364DE9" Ref="U19"  Part="1" 
AR Path="/755D912A4B364DE9" Ref="U19"  Part="1" 
AR Path="/A4B364DE9" Ref="U19"  Part="1" 
AR Path="/14B364DE9" Ref="U19"  Part="1" 
AR Path="/6FF0DD404B364DE9" Ref="U19"  Part="1" 
AR Path="/DCBAABCD4B364DE9" Ref="U19"  Part="1" 
AR Path="/6FE901F74B364DE9" Ref="U19"  Part="1" 
AR Path="/3FE224DD4B364DE9" Ref="U19"  Part="1" 
AR Path="/3FEFFFFF4B364DE9" Ref="U19"  Part="1" 
AR Path="/23D8D44B364DE9" Ref="U19"  Part="1" 
AR Path="/FFFFFFF04B364DE9" Ref="U19"  Part="1" 
AR Path="/402955814B364DE9" Ref="U19"  Part="1" 
AR Path="/A84B364DE9" Ref="U19"  Part="1" 
AR Path="/402C08B44B364DE9" Ref="U19"  Part="1" 
AR Path="/23D2034B364DE9" Ref="U19"  Part="1" 
AR Path="/4E4B364DE9" Ref="U19"  Part="1" 
AR Path="/402BEF1A4B364DE9" Ref="U19"  Part="1" 
AR Path="/54B364DE9" Ref="U19"  Part="1" 
AR Path="/40293BE74B364DE9" Ref="U19"  Part="1" 
AR Path="/402C55814B364DE9" Ref="U19"  Part="1" 
AR Path="/40273BE74B364DE9" Ref="U19"  Part="1" 
AR Path="/2600004B364DE9" Ref="U19"  Part="1" 
AR Path="/3D8EA0004B364DE9" Ref="U19"  Part="1" 
AR Path="/402908B44B364DE9" Ref="U19"  Part="1" 
AR Path="/3D6CC0004B364DE9" Ref="U19"  Part="1" 
AR Path="/3D5A40004B364DE9" Ref="U19"  Part="1" 
AR Path="/402755814B364DE9" Ref="U19"  Part="1" 
AR Path="/4030AAC04B364DE9" Ref="U19"  Part="1" 
AR Path="/23D9304B364DE9" Ref="U19"  Part="1" 
AR Path="/4031DDF34B364DE9" Ref="U19"  Part="1" 
AR Path="/3FE88B434B364DE9" Ref="U19"  Part="1" 
AR Path="/4032778D4B364DE9" Ref="U19"  Part="1" 
AR Path="/403091264B364DE9" Ref="U19"  Part="1" 
AR Path="/403051264B364DE9" Ref="U19"  Part="1" 
AR Path="/4032F78D4B364DE9" Ref="U19"  Part="1" 
AR Path="/403251264B364DE9" Ref="U19"  Part="1" 
AR Path="/4032AAC04B364DE9" Ref="U19"  Part="1" 
AR Path="/4030D1264B364DE9" Ref="U19"  Part="1" 
AR Path="/4031778D4B364DE9" Ref="U19"  Part="1" 
AR Path="/3FEA24DD4B364DE9" Ref="U19"  Part="1" 
AR Path="/6FE934E34B364DE9" Ref="U19"  Part="1" 
AR Path="/773F65F14B364DE9" Ref="U19"  Part="1" 
AR Path="/773F8EB44B364DE9" Ref="U19"  Part="1" 
AR Path="/23C9F04B364DE9" Ref="U19"  Part="1" 
AR Path="/24B364DE9" Ref="U19"  Part="1" 
AR Path="/23BC884B364DE9" Ref="U19"  Part="1" 
AR Path="/DC0C124B364DE9" Ref="U19"  Part="1" 
AR Path="/23C34C4B364DE9" Ref="U19"  Part="1" 
AR Path="/23CBC44B364DE9" Ref="U19"  Part="1" 
AR Path="/23C6504B364DE9" Ref="U19"  Part="1" 
AR Path="/39803EA4B364DE9" Ref="U19"  Part="1" 
AR Path="/FFFFFFFF4B364DE9" Ref="U19"  Part="1" 
AR Path="/D058A04B364DE9" Ref="U19"  Part="1" 
AR Path="/1607D44B364DE9" Ref="U19"  Part="1" 
AR Path="/D1C3804B364DE9" Ref="U19"  Part="1" 
AR Path="/23D70C4B364DE9" Ref="U19"  Part="1" 
AR Path="/2104B364DE9" Ref="U19"  Part="1" 
AR Path="/F4BF4B364DE9" Ref="U19"  Part="1" 
AR Path="/262F604B364DE9" Ref="U19"  Part="1" 
AR Path="/384B364DE9" Ref="U19"  Part="1" 
F 0 "U19" H 16750 4550 60  0000 L CNN
F 1 "74LS245" H 16750 3350 60  0000 L CNN
F 2 "20dip300" H 16650 3950 60  0001 C CNN
	1    16650 3950
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U?
U 2 1 4B364BDB
P 15800 10350
AR Path="/23D9D84B364BDB" Ref="U?"  Part="1" 
AR Path="/394433324B364BDB" Ref="U?"  Part="1" 
AR Path="/74B364BDB" Ref="U?"  Part="1" 
AR Path="/7E44048F4B364BDB" Ref="U?"  Part="1" 
AR Path="/23007BC4B364BDB" Ref="U"  Part="2" 
AR Path="/5AD7153D4B364BDB" Ref="U17"  Part="2" 
AR Path="/DCBAABCD4B364BDB" Ref="U17"  Part="2" 
AR Path="/4B364BDB" Ref="U17"  Part="2" 
AR Path="/A4B364BDB" Ref="U17"  Part="2" 
AR Path="/94B364BDB" Ref="U17"  Part="2" 
AR Path="/755D912A4B364BDB" Ref="U17"  Part="2" 
AR Path="/6FF0DD404B364BDB" Ref="U17"  Part="2" 
AR Path="/6FE901F74B364BDB" Ref="U17"  Part="2" 
AR Path="/3FE224DD4B364BDB" Ref="U17"  Part="2" 
AR Path="/3FEFFFFF4B364BDB" Ref="U17"  Part="2" 
AR Path="/23D8D44B364BDB" Ref="U17"  Part="2" 
AR Path="/FFFFFFF04B364BDB" Ref="U17"  Part="2" 
AR Path="/402955814B364BDB" Ref="U17"  Part="2" 
AR Path="/A84B364BDB" Ref="U17"  Part="2" 
AR Path="/402C08B44B364BDB" Ref="U17"  Part="2" 
AR Path="/23D2034B364BDB" Ref="U17"  Part="2" 
AR Path="/4E4B364BDB" Ref="U17"  Part="2" 
AR Path="/402BEF1A4B364BDB" Ref="U17"  Part="2" 
AR Path="/54B364BDB" Ref="U17"  Part="2" 
AR Path="/40293BE74B364BDB" Ref="U17"  Part="2" 
AR Path="/402C55814B364BDB" Ref="U17"  Part="2" 
AR Path="/40273BE74B364BDB" Ref="U17"  Part="2" 
AR Path="/2600004B364BDB" Ref="U17"  Part="2" 
AR Path="/3D8EA0004B364BDB" Ref="U17"  Part="2" 
AR Path="/402908B44B364BDB" Ref="U17"  Part="2" 
AR Path="/324B364BDB" Ref="U17"  Part="2" 
AR Path="/3D6CC0004B364BDB" Ref="U17"  Part="2" 
AR Path="/3D5A40004B364BDB" Ref="U17"  Part="2" 
AR Path="/402755814B364BDB" Ref="U17"  Part="2" 
AR Path="/4030AAC04B364BDB" Ref="U17"  Part="2" 
AR Path="/4031DDF34B364BDB" Ref="U17"  Part="2" 
AR Path="/3FE88B434B364BDB" Ref="U17"  Part="2" 
AR Path="/4032778D4B364BDB" Ref="U17"  Part="2" 
AR Path="/403091264B364BDB" Ref="U17"  Part="2" 
AR Path="/403051264B364BDB" Ref="U17"  Part="2" 
AR Path="/4032F78D4B364BDB" Ref="U17"  Part="2" 
AR Path="/403251264B364BDB" Ref="U17"  Part="2" 
AR Path="/4032AAC04B364BDB" Ref="U17"  Part="2" 
AR Path="/4030D1264B364BDB" Ref="U17"  Part="2" 
AR Path="/4031778D4B364BDB" Ref="U17"  Part="2" 
AR Path="/3FEA24DD4B364BDB" Ref="U17"  Part="2" 
AR Path="/6FE934E34B364BDB" Ref="U17"  Part="2" 
AR Path="/773F8EB44B364BDB" Ref="U17"  Part="2" 
AR Path="/23C9F04B364BDB" Ref="U17"  Part="2" 
AR Path="/23BC884B364BDB" Ref="U17"  Part="2" 
AR Path="/DC0C124B364BDB" Ref="U17"  Part="2" 
AR Path="/23C34C4B364BDB" Ref="U17"  Part="2" 
AR Path="/23CBC44B364BDB" Ref="U17"  Part="2" 
AR Path="/23C6504B364BDB" Ref="U17"  Part="2" 
AR Path="/39803EA4B364BDB" Ref="U17"  Part="2" 
AR Path="/FFFFFFFF4B364BDB" Ref="U17"  Part="2" 
AR Path="/D058A04B364BDB" Ref="U17"  Part="2" 
AR Path="/1607D44B364BDB" Ref="U17"  Part="2" 
AR Path="/D1C3804B364BDB" Ref="U17"  Part="2" 
AR Path="/24B364BDB" Ref="U17"  Part="2" 
AR Path="/23D70C4B364BDB" Ref="U17"  Part="2" 
AR Path="/14B364BDB" Ref="U17"  Part="2" 
AR Path="/2104B364BDB" Ref="U17"  Part="2" 
AR Path="/F4BF4B364BDB" Ref="U17"  Part="2" 
AR Path="/262F604B364BDB" Ref="U17"  Part="2" 
AR Path="/384B364BDB" Ref="U17"  Part="2" 
F 0 "U17" H 15800 10400 60  0000 C CNN
F 1 "74LS08" H 15800 10300 60  0000 C CNN
F 2 "14dip300" H 15800 10350 60  0001 C CNN
	2    15800 10350
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U?
U 1 1 4B364BC6
P 15800 9800
AR Path="/23D9D84B364BC6" Ref="U?"  Part="1" 
AR Path="/394433324B364BC6" Ref="U?"  Part="1" 
AR Path="/74B364BC6" Ref="U?"  Part="1" 
AR Path="/7E44048F4B364BC6" Ref="U?"  Part="1" 
AR Path="/25D06C84B364BC6" Ref="U"  Part="1" 
AR Path="/5AD7153D4B364BC6" Ref="U17"  Part="1" 
AR Path="/DCBAABCD4B364BC6" Ref="U17"  Part="1" 
AR Path="/A4B364BC6" Ref="U17"  Part="1" 
AR Path="/23C2344B364BC6" Ref="U17"  Part="1" 
AR Path="/4B364BC6" Ref="U17"  Part="1" 
AR Path="/94B364BC6" Ref="U17"  Part="1" 
AR Path="/755D912A4B364BC6" Ref="U17"  Part="1" 
AR Path="/6FF0DD404B364BC6" Ref="U17"  Part="1" 
AR Path="/6FE901F74B364BC6" Ref="U17"  Part="1" 
AR Path="/3FE224DD4B364BC6" Ref="U17"  Part="1" 
AR Path="/3FEFFFFF4B364BC6" Ref="U17"  Part="1" 
AR Path="/23D8D44B364BC6" Ref="U17"  Part="1" 
AR Path="/FFFFFFF04B364BC6" Ref="U17"  Part="1" 
AR Path="/402955814B364BC6" Ref="U17"  Part="1" 
AR Path="/A84B364BC6" Ref="U17"  Part="1" 
AR Path="/402C08B44B364BC6" Ref="U17"  Part="1" 
AR Path="/23D2034B364BC6" Ref="U17"  Part="1" 
AR Path="/4E4B364BC6" Ref="U17"  Part="1" 
AR Path="/402BEF1A4B364BC6" Ref="U17"  Part="1" 
AR Path="/54B364BC6" Ref="U17"  Part="1" 
AR Path="/40293BE74B364BC6" Ref="U17"  Part="1" 
AR Path="/402C55814B364BC6" Ref="U17"  Part="1" 
AR Path="/40273BE74B364BC6" Ref="U17"  Part="1" 
AR Path="/2600004B364BC6" Ref="U17"  Part="1" 
AR Path="/3D8EA0004B364BC6" Ref="U17"  Part="1" 
AR Path="/402908B44B364BC6" Ref="U17"  Part="1" 
AR Path="/3D6CC0004B364BC6" Ref="U17"  Part="1" 
AR Path="/3D5A40004B364BC6" Ref="U17"  Part="1" 
AR Path="/402755814B364BC6" Ref="U17"  Part="1" 
AR Path="/4030AAC04B364BC6" Ref="U17"  Part="1" 
AR Path="/4031DDF34B364BC6" Ref="U17"  Part="1" 
AR Path="/3FE88B434B364BC6" Ref="U17"  Part="1" 
AR Path="/4032778D4B364BC6" Ref="U17"  Part="1" 
AR Path="/403091264B364BC6" Ref="U17"  Part="1" 
AR Path="/403051264B364BC6" Ref="U17"  Part="1" 
AR Path="/4032F78D4B364BC6" Ref="U17"  Part="1" 
AR Path="/403251264B364BC6" Ref="U17"  Part="1" 
AR Path="/4032AAC04B364BC6" Ref="U17"  Part="1" 
AR Path="/4030D1264B364BC6" Ref="U17"  Part="1" 
AR Path="/4031778D4B364BC6" Ref="U17"  Part="1" 
AR Path="/3FEA24DD4B364BC6" Ref="U17"  Part="1" 
AR Path="/6FE934E34B364BC6" Ref="U17"  Part="1" 
AR Path="/773F8EB44B364BC6" Ref="U17"  Part="1" 
AR Path="/23C9F04B364BC6" Ref="U17"  Part="1" 
AR Path="/23BC884B364BC6" Ref="U17"  Part="1" 
AR Path="/DC0C124B364BC6" Ref="U17"  Part="1" 
AR Path="/23C34C4B364BC6" Ref="U17"  Part="1" 
AR Path="/23CBC44B364BC6" Ref="U17"  Part="1" 
AR Path="/23C6504B364BC6" Ref="U17"  Part="1" 
AR Path="/39803EA4B364BC6" Ref="U17"  Part="1" 
AR Path="/FFFFFFFF4B364BC6" Ref="U17"  Part="1" 
AR Path="/D058A04B364BC6" Ref="U17"  Part="1" 
AR Path="/1607D44B364BC6" Ref="U17"  Part="1" 
AR Path="/D1C3804B364BC6" Ref="U17"  Part="1" 
AR Path="/24B364BC6" Ref="U17"  Part="1" 
AR Path="/23D70C4B364BC6" Ref="U17"  Part="1" 
AR Path="/14B364BC6" Ref="U17"  Part="1" 
AR Path="/2104B364BC6" Ref="U17"  Part="1" 
AR Path="/F4BF4B364BC6" Ref="U17"  Part="1" 
AR Path="/262F604B364BC6" Ref="U17"  Part="1" 
AR Path="/384B364BC6" Ref="U17"  Part="1" 
F 0 "U17" H 15800 9850 60  0000 C CNN
F 1 "74LS08" H 15800 9750 60  0000 C CNN
F 2 "14dip300" H 15800 9800 60  0001 C CNN
	1    15800 9800
	1    0    0    -1  
$EndComp
$Comp
L 74LS02 U?
U 1 1 4B3647E5
P 17050 11000
AR Path="/23D9B04B3647E5" Ref="U?"  Part="1" 
AR Path="/394433324B3647E5" Ref="U?"  Part="1" 
AR Path="/6FF405304B3647E5" Ref="U?"  Part="1" 
AR Path="/23D6544B3647E5" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3647E5" Ref="U?"  Part="1" 
AR Path="/1FE07EA4B3647E5" Ref="U"  Part="1" 
AR Path="/5AD7153D4B3647E5" Ref="U15"  Part="1" 
AR Path="/DCBAABCD4B3647E5" Ref="U15"  Part="1" 
AR Path="/4B3647E5" Ref="U15"  Part="1" 
AR Path="/A4B3647E5" Ref="U15"  Part="1" 
AR Path="/94B3647E5" Ref="U15"  Part="1" 
AR Path="/755D912A4B3647E5" Ref="U15"  Part="1" 
AR Path="/6FF0DD404B3647E5" Ref="U15"  Part="1" 
AR Path="/14B3647E5" Ref="U15"  Part="1" 
AR Path="/6FE901F74B3647E5" Ref="U15"  Part="1" 
AR Path="/3FE224DD4B3647E5" Ref="U15"  Part="1" 
AR Path="/3FEFFFFF4B3647E5" Ref="U15"  Part="1" 
AR Path="/23D8D44B3647E5" Ref="U15"  Part="1" 
AR Path="/FFFFFFF04B3647E5" Ref="U15"  Part="1" 
AR Path="/402955814B3647E5" Ref="U15"  Part="1" 
AR Path="/A84B3647E5" Ref="U15"  Part="1" 
AR Path="/402C08B44B3647E5" Ref="U15"  Part="1" 
AR Path="/23D2034B3647E5" Ref="U15"  Part="1" 
AR Path="/4E4B3647E5" Ref="U15"  Part="1" 
AR Path="/402BEF1A4B3647E5" Ref="U15"  Part="1" 
AR Path="/40293BE74B3647E5" Ref="U15"  Part="1" 
AR Path="/402C55814B3647E5" Ref="U15"  Part="1" 
AR Path="/40273BE74B3647E5" Ref="U15"  Part="1" 
AR Path="/2600004B3647E5" Ref="U15"  Part="1" 
AR Path="/3D8EA0004B3647E5" Ref="U15"  Part="1" 
AR Path="/402908B44B3647E5" Ref="U15"  Part="1" 
AR Path="/3D6CC0004B3647E5" Ref="U15"  Part="1" 
AR Path="/3D5A40004B3647E5" Ref="U15"  Part="1" 
AR Path="/402755814B3647E5" Ref="U15"  Part="1" 
AR Path="/4030AAC04B3647E5" Ref="U15"  Part="1" 
AR Path="/4031DDF34B3647E5" Ref="U15"  Part="1" 
AR Path="/3FE88B434B3647E5" Ref="U15"  Part="1" 
AR Path="/4032778D4B3647E5" Ref="U15"  Part="1" 
AR Path="/403091264B3647E5" Ref="U15"  Part="1" 
AR Path="/403051264B3647E5" Ref="U15"  Part="1" 
AR Path="/4032F78D4B3647E5" Ref="U15"  Part="1" 
AR Path="/403251264B3647E5" Ref="U15"  Part="1" 
AR Path="/4032AAC04B3647E5" Ref="U15"  Part="1" 
AR Path="/4030D1264B3647E5" Ref="U15"  Part="1" 
AR Path="/4031778D4B3647E5" Ref="U15"  Part="1" 
AR Path="/3FEA24DD4B3647E5" Ref="U15"  Part="1" 
AR Path="/6FE934E34B3647E5" Ref="U15"  Part="1" 
AR Path="/773F8EB44B3647E5" Ref="U15"  Part="1" 
AR Path="/23C9F04B3647E5" Ref="U15"  Part="1" 
AR Path="/23BC884B3647E5" Ref="U15"  Part="1" 
AR Path="/DC0C124B3647E5" Ref="U15"  Part="1" 
AR Path="/23C34C4B3647E5" Ref="U15"  Part="1" 
AR Path="/23CBC44B3647E5" Ref="U15"  Part="1" 
AR Path="/23C6504B3647E5" Ref="U15"  Part="1" 
AR Path="/39803EA4B3647E5" Ref="U15"  Part="1" 
AR Path="/FFFFFFFF4B3647E5" Ref="U15"  Part="1" 
AR Path="/D058A04B3647E5" Ref="U15"  Part="1" 
AR Path="/1607D44B3647E5" Ref="U15"  Part="1" 
AR Path="/D1C3804B3647E5" Ref="U15"  Part="1" 
AR Path="/24B3647E5" Ref="U15"  Part="1" 
AR Path="/23D70C4B3647E5" Ref="U15"  Part="1" 
AR Path="/2104B3647E5" Ref="U15"  Part="1" 
AR Path="/F4BF4B3647E5" Ref="U15"  Part="1" 
AR Path="/262F604B3647E5" Ref="U15"  Part="1" 
AR Path="/384B3647E5" Ref="U15"  Part="1" 
F 0 "U15" H 17050 11050 60  0000 C CNN
F 1 "74LS02" H 17100 10950 60  0000 C CNN
F 2 "14dip300" H 17050 11000 60  0001 C CNN
	1    17050 11000
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U?
U 3 1 4B364646
P 15800 11450
AR Path="/23D9D84B364646" Ref="U?"  Part="1" 
AR Path="/394433324B364646" Ref="U?"  Part="1" 
AR Path="/23D6544B364646" Ref="U?"  Part="1" 
AR Path="/7E44048F4B364646" Ref="U?"  Part="1" 
AR Path="/18207F24B364646" Ref="U"  Part="3" 
AR Path="/5AD7153D4B364646" Ref="U14"  Part="3" 
AR Path="/DCBAABCD4B364646" Ref="U14"  Part="3" 
AR Path="/4B364646" Ref="U14"  Part="3" 
AR Path="/A4B364646" Ref="U14"  Part="3" 
AR Path="/94B364646" Ref="U14"  Part="3" 
AR Path="/755D912A4B364646" Ref="U14"  Part="3" 
AR Path="/14B364646" Ref="U14"  Part="3" 
AR Path="/6FF0DD404B364646" Ref="U14"  Part="3" 
AR Path="/6FE901F74B364646" Ref="U14"  Part="3" 
AR Path="/3FE224DD4B364646" Ref="U14"  Part="3" 
AR Path="/3FEFFFFF4B364646" Ref="U14"  Part="3" 
AR Path="/23D8D44B364646" Ref="U14"  Part="3" 
AR Path="/FFFFFFF04B364646" Ref="U14"  Part="3" 
AR Path="/402955814B364646" Ref="U14"  Part="3" 
AR Path="/A84B364646" Ref="U14"  Part="3" 
AR Path="/402C08B44B364646" Ref="U14"  Part="3" 
AR Path="/4E4B364646" Ref="U14"  Part="3" 
AR Path="/23D2034B364646" Ref="U14"  Part="3" 
AR Path="/402BEF1A4B364646" Ref="U14"  Part="3" 
AR Path="/40293BE74B364646" Ref="U14"  Part="3" 
AR Path="/402C55814B364646" Ref="U14"  Part="3" 
AR Path="/40273BE74B364646" Ref="U14"  Part="3" 
AR Path="/2600004B364646" Ref="U14"  Part="3" 
AR Path="/3D8EA0004B364646" Ref="U14"  Part="3" 
AR Path="/402908B44B364646" Ref="U14"  Part="3" 
AR Path="/3D6CC0004B364646" Ref="U14"  Part="3" 
AR Path="/3D5A40004B364646" Ref="U14"  Part="3" 
AR Path="/402755814B364646" Ref="U14"  Part="3" 
AR Path="/4030AAC04B364646" Ref="U14"  Part="3" 
AR Path="/4031DDF34B364646" Ref="U14"  Part="3" 
AR Path="/3FE88B434B364646" Ref="U14"  Part="3" 
AR Path="/4032778D4B364646" Ref="U14"  Part="3" 
AR Path="/403091264B364646" Ref="U14"  Part="3" 
AR Path="/403051264B364646" Ref="U14"  Part="3" 
AR Path="/4032F78D4B364646" Ref="U14"  Part="3" 
AR Path="/403251264B364646" Ref="U14"  Part="3" 
AR Path="/4032AAC04B364646" Ref="U14"  Part="3" 
AR Path="/4030D1264B364646" Ref="U14"  Part="3" 
AR Path="/4031778D4B364646" Ref="U14"  Part="3" 
AR Path="/3FEA24DD4B364646" Ref="U14"  Part="3" 
AR Path="/6FE934E34B364646" Ref="U14"  Part="3" 
AR Path="/773F8EB44B364646" Ref="U14"  Part="3" 
AR Path="/23C9F04B364646" Ref="U14"  Part="3" 
AR Path="/24B364646" Ref="U14"  Part="3" 
AR Path="/23BC884B364646" Ref="U14"  Part="3" 
AR Path="/DC0C124B364646" Ref="U14"  Part="3" 
AR Path="/23C34C4B364646" Ref="U14"  Part="3" 
AR Path="/23CBC44B364646" Ref="U14"  Part="3" 
AR Path="/23C6504B364646" Ref="U14"  Part="3" 
AR Path="/39803EA4B364646" Ref="U14"  Part="3" 
AR Path="/FFFFFFFF4B364646" Ref="U14"  Part="3" 
AR Path="/D058A04B364646" Ref="U14"  Part="3" 
AR Path="/1607D44B364646" Ref="U14"  Part="3" 
AR Path="/D1C3804B364646" Ref="U14"  Part="3" 
AR Path="/23D70C4B364646" Ref="U14"  Part="3" 
AR Path="/2104B364646" Ref="U14"  Part="3" 
AR Path="/F4BF4B364646" Ref="U14"  Part="3" 
AR Path="/262F604B364646" Ref="U14"  Part="3" 
AR Path="/384B364646" Ref="U14"  Part="3" 
F 0 "U14" H 15800 11500 60  0000 C CNN
F 1 "74LS08" H 15800 11400 60  0000 C CNN
F 2 "14dip300" H 15800 11450 60  0001 C CNN
	3    15800 11450
	1    0    0    -1  
$EndComp
$Comp
L 74LS02 U?
U 3 1 4B364608
P 17050 12150
AR Path="/23D9D84B364608" Ref="U?"  Part="1" 
AR Path="/394433324B364608" Ref="U?"  Part="1" 
AR Path="/23D6544B364608" Ref="U15"  Part="1" 
AR Path="/7E44048F4B364608" Ref="U?"  Part="1" 
AR Path="/24107E84B364608" Ref="U"  Part="2" 
AR Path="/5AD73C024B364608" Ref="U"  Part="3" 
AR Path="/5AD7153D4B364608" Ref="U15"  Part="3" 
AR Path="/DCBAABCD4B364608" Ref="U15"  Part="3" 
AR Path="/A4B364608" Ref="U15"  Part="3" 
AR Path="/23C2344B364608" Ref="U15"  Part="3" 
AR Path="/4B364608" Ref="U15"  Part="3" 
AR Path="/94B364608" Ref="U15"  Part="3" 
AR Path="/755D912A4B364608" Ref="U15"  Part="3" 
AR Path="/14B364608" Ref="U15"  Part="3" 
AR Path="/6FF0DD404B364608" Ref="U15"  Part="3" 
AR Path="/6FE901F74B364608" Ref="U15"  Part="3" 
AR Path="/3FE224DD4B364608" Ref="U15"  Part="3" 
AR Path="/3FEFFFFF4B364608" Ref="U15"  Part="3" 
AR Path="/23D8D44B364608" Ref="U15"  Part="3" 
AR Path="/FFFFFFF04B364608" Ref="U15"  Part="3" 
AR Path="/402955814B364608" Ref="U15"  Part="3" 
AR Path="/A84B364608" Ref="U15"  Part="3" 
AR Path="/402C08B44B364608" Ref="U15"  Part="3" 
AR Path="/4E4B364608" Ref="U15"  Part="3" 
AR Path="/23D2034B364608" Ref="U15"  Part="3" 
AR Path="/402BEF1A4B364608" Ref="U15"  Part="3" 
AR Path="/40293BE74B364608" Ref="U15"  Part="3" 
AR Path="/402C55814B364608" Ref="U15"  Part="3" 
AR Path="/40273BE74B364608" Ref="U15"  Part="3" 
AR Path="/2600004B364608" Ref="U15"  Part="3" 
AR Path="/3D8EA0004B364608" Ref="U15"  Part="3" 
AR Path="/402908B44B364608" Ref="U15"  Part="3" 
AR Path="/3D6CC0004B364608" Ref="U15"  Part="3" 
AR Path="/3D5A40004B364608" Ref="U15"  Part="3" 
AR Path="/402755814B364608" Ref="U15"  Part="3" 
AR Path="/4030AAC04B364608" Ref="U15"  Part="3" 
AR Path="/4031DDF34B364608" Ref="U15"  Part="3" 
AR Path="/3FE88B434B364608" Ref="U15"  Part="3" 
AR Path="/4032778D4B364608" Ref="U15"  Part="3" 
AR Path="/403091264B364608" Ref="U15"  Part="3" 
AR Path="/403051264B364608" Ref="U15"  Part="3" 
AR Path="/4032F78D4B364608" Ref="U15"  Part="3" 
AR Path="/403251264B364608" Ref="U15"  Part="3" 
AR Path="/4032AAC04B364608" Ref="U15"  Part="3" 
AR Path="/4030D1264B364608" Ref="U15"  Part="3" 
AR Path="/4031778D4B364608" Ref="U15"  Part="3" 
AR Path="/3FEA24DD4B364608" Ref="U15"  Part="3" 
AR Path="/6FE934E34B364608" Ref="U15"  Part="3" 
AR Path="/773F8EB44B364608" Ref="U15"  Part="3" 
AR Path="/23C9F04B364608" Ref="U15"  Part="3" 
AR Path="/23BC884B364608" Ref="U15"  Part="3" 
AR Path="/DC0C124B364608" Ref="U15"  Part="3" 
AR Path="/23C34C4B364608" Ref="U15"  Part="3" 
AR Path="/23CBC44B364608" Ref="U15"  Part="3" 
AR Path="/23C6504B364608" Ref="U15"  Part="3" 
AR Path="/39803EA4B364608" Ref="U15"  Part="3" 
AR Path="/FFFFFFFF4B364608" Ref="U15"  Part="3" 
AR Path="/D058A04B364608" Ref="U15"  Part="3" 
AR Path="/1607D44B364608" Ref="U15"  Part="3" 
AR Path="/D1C3804B364608" Ref="U15"  Part="3" 
AR Path="/24B364608" Ref="U15"  Part="3" 
AR Path="/23D70C4B364608" Ref="U15"  Part="3" 
AR Path="/2104B364608" Ref="U15"  Part="3" 
AR Path="/F4BF4B364608" Ref="U15"  Part="3" 
AR Path="/262F604B364608" Ref="U15"  Part="3" 
AR Path="/384B364608" Ref="U15"  Part="3" 
F 0 "U15" H 17050 12200 60  0000 C CNN
F 1 "74LS02" H 17100 12100 60  0000 C CNN
F 2 "14dip300" H 17050 12150 60  0001 C CNN
	3    17050 12150
	1    0    0    -1  
$EndComp
$Comp
L 74LS02 U?
U 4 1 4B3645BE
P 17050 12850
AR Path="/69698AFC4B3645BE" Ref="U?"  Part="1" 
AR Path="/393639364B3645BE" Ref="U?"  Part="1" 
AR Path="/6FF405304B3645BE" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3645BE" Ref="U?"  Part="1" 
AR Path="/24606CE4B3645BE" Ref="U"  Part="4" 
AR Path="/5AD7153D4B3645BE" Ref="U15"  Part="4" 
AR Path="/DCBAABCD4B3645BE" Ref="U15"  Part="4" 
AR Path="/A4B3645BE" Ref="U15"  Part="4" 
AR Path="/23C2344B3645BE" Ref="U15"  Part="4" 
AR Path="/4B3645BE" Ref="U15"  Part="4" 
AR Path="/94B3645BE" Ref="U15"  Part="4" 
AR Path="/755D912A4B3645BE" Ref="U15"  Part="4" 
AR Path="/6FF0DD404B3645BE" Ref="U15"  Part="4" 
AR Path="/14B3645BE" Ref="U15"  Part="4" 
AR Path="/6FE901F74B3645BE" Ref="U15"  Part="4" 
AR Path="/3FE224DD4B3645BE" Ref="U15"  Part="4" 
AR Path="/3FEFFFFF4B3645BE" Ref="U15"  Part="4" 
AR Path="/23D8D44B3645BE" Ref="U15"  Part="4" 
AR Path="/FFFFFFF04B3645BE" Ref="U15"  Part="4" 
AR Path="/402955814B3645BE" Ref="U15"  Part="4" 
AR Path="/A84B3645BE" Ref="U15"  Part="4" 
AR Path="/402C08B44B3645BE" Ref="U15"  Part="4" 
AR Path="/4E4B3645BE" Ref="U15"  Part="4" 
AR Path="/23D2034B3645BE" Ref="U15"  Part="4" 
AR Path="/402BEF1A4B3645BE" Ref="U15"  Part="4" 
AR Path="/40293BE74B3645BE" Ref="U15"  Part="4" 
AR Path="/402C55814B3645BE" Ref="U15"  Part="4" 
AR Path="/40273BE74B3645BE" Ref="U15"  Part="4" 
AR Path="/2600004B3645BE" Ref="U15"  Part="4" 
AR Path="/3D8EA0004B3645BE" Ref="U15"  Part="4" 
AR Path="/402908B44B3645BE" Ref="U15"  Part="4" 
AR Path="/3D6CC0004B3645BE" Ref="U15"  Part="4" 
AR Path="/3D5A40004B3645BE" Ref="U15"  Part="4" 
AR Path="/402755814B3645BE" Ref="U15"  Part="4" 
AR Path="/4030AAC04B3645BE" Ref="U15"  Part="4" 
AR Path="/4031DDF34B3645BE" Ref="U15"  Part="4" 
AR Path="/3FE88B434B3645BE" Ref="U15"  Part="4" 
AR Path="/4032778D4B3645BE" Ref="U15"  Part="4" 
AR Path="/403091264B3645BE" Ref="U15"  Part="4" 
AR Path="/403051264B3645BE" Ref="U15"  Part="4" 
AR Path="/4032F78D4B3645BE" Ref="U15"  Part="4" 
AR Path="/403251264B3645BE" Ref="U15"  Part="4" 
AR Path="/4032AAC04B3645BE" Ref="U15"  Part="4" 
AR Path="/4030D1264B3645BE" Ref="U15"  Part="4" 
AR Path="/4031778D4B3645BE" Ref="U15"  Part="4" 
AR Path="/3FEA24DD4B3645BE" Ref="U15"  Part="4" 
AR Path="/6FE934E34B3645BE" Ref="U15"  Part="4" 
AR Path="/773F8EB44B3645BE" Ref="U15"  Part="4" 
AR Path="/23C9F04B3645BE" Ref="U15"  Part="4" 
AR Path="/23BC884B3645BE" Ref="U15"  Part="4" 
AR Path="/DC0C124B3645BE" Ref="U15"  Part="4" 
AR Path="/23C34C4B3645BE" Ref="U15"  Part="4" 
AR Path="/23CBC44B3645BE" Ref="U15"  Part="4" 
AR Path="/23C6504B3645BE" Ref="U15"  Part="4" 
AR Path="/39803EA4B3645BE" Ref="U15"  Part="4" 
AR Path="/FFFFFFFF4B3645BE" Ref="U15"  Part="4" 
AR Path="/D058A04B3645BE" Ref="U15"  Part="4" 
AR Path="/1607D44B3645BE" Ref="U15"  Part="4" 
AR Path="/D1C3804B3645BE" Ref="U15"  Part="4" 
AR Path="/24B3645BE" Ref="U15"  Part="4" 
AR Path="/23D70C4B3645BE" Ref="U15"  Part="4" 
AR Path="/2104B3645BE" Ref="U15"  Part="4" 
AR Path="/F4BF4B3645BE" Ref="U15"  Part="4" 
AR Path="/262F604B3645BE" Ref="U15"  Part="4" 
AR Path="/384B3645BE" Ref="U15"  Part="4" 
F 0 "U15" H 17050 12900 60  0000 C CNN
F 1 "74LS02" H 17100 12800 60  0000 C CNN
F 2 "14dip300" H 17050 12850 60  0001 C CNN
	4    17050 12850
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U?
U 4 1 4B36457A
P 15800 12750
AR Path="/23D9D84B36457A" Ref="U?"  Part="1" 
AR Path="/394433324B36457A" Ref="U?"  Part="1" 
AR Path="/23D6544B36457A" Ref="U14"  Part="1" 
AR Path="/7E44048F4B36457A" Ref="U14"  Part="1" 
AR Path="/22307BC4B36457A" Ref="U"  Part="3" 
AR Path="/17D07EE4B36457A" Ref="U"  Part="3" 
AR Path="/5AD73C024B36457A" Ref="U"  Part="4" 
AR Path="/5AD7153D4B36457A" Ref="U14"  Part="4" 
AR Path="/DCBAABCD4B36457A" Ref="U14"  Part="4" 
AR Path="/A4B36457A" Ref="U14"  Part="4" 
AR Path="/23C2344B36457A" Ref="U14"  Part="4" 
AR Path="/4B36457A" Ref="U14"  Part="4" 
AR Path="/94B36457A" Ref="U14"  Part="4" 
AR Path="/755D912A4B36457A" Ref="U14"  Part="4" 
AR Path="/6FF0DD404B36457A" Ref="U14"  Part="4" 
AR Path="/14B36457A" Ref="U14"  Part="4" 
AR Path="/6FE901F74B36457A" Ref="U14"  Part="4" 
AR Path="/3FE224DD4B36457A" Ref="U14"  Part="4" 
AR Path="/3FEFFFFF4B36457A" Ref="U14"  Part="4" 
AR Path="/23D8D44B36457A" Ref="U14"  Part="4" 
AR Path="/FFFFFFF04B36457A" Ref="U14"  Part="4" 
AR Path="/402955814B36457A" Ref="U14"  Part="4" 
AR Path="/A84B36457A" Ref="U14"  Part="4" 
AR Path="/402C08B44B36457A" Ref="U14"  Part="4" 
AR Path="/4E4B36457A" Ref="U14"  Part="4" 
AR Path="/23D2034B36457A" Ref="U14"  Part="4" 
AR Path="/402BEF1A4B36457A" Ref="U14"  Part="4" 
AR Path="/40293BE74B36457A" Ref="U14"  Part="4" 
AR Path="/402C55814B36457A" Ref="U14"  Part="4" 
AR Path="/40273BE74B36457A" Ref="U14"  Part="4" 
AR Path="/2600004B36457A" Ref="U14"  Part="4" 
AR Path="/3D8EA0004B36457A" Ref="U14"  Part="4" 
AR Path="/402908B44B36457A" Ref="U14"  Part="4" 
AR Path="/3D6CC0004B36457A" Ref="U14"  Part="4" 
AR Path="/3D5A40004B36457A" Ref="U14"  Part="4" 
AR Path="/402755814B36457A" Ref="U14"  Part="4" 
AR Path="/4030AAC04B36457A" Ref="U14"  Part="4" 
AR Path="/4031DDF34B36457A" Ref="U14"  Part="4" 
AR Path="/3FE88B434B36457A" Ref="U14"  Part="4" 
AR Path="/4032778D4B36457A" Ref="U14"  Part="4" 
AR Path="/403091264B36457A" Ref="U14"  Part="4" 
AR Path="/403051264B36457A" Ref="U14"  Part="4" 
AR Path="/4032F78D4B36457A" Ref="U14"  Part="4" 
AR Path="/403251264B36457A" Ref="U14"  Part="4" 
AR Path="/4032AAC04B36457A" Ref="U14"  Part="4" 
AR Path="/4030D1264B36457A" Ref="U14"  Part="4" 
AR Path="/4031778D4B36457A" Ref="U14"  Part="4" 
AR Path="/3FEA24DD4B36457A" Ref="U14"  Part="4" 
AR Path="/6FE934E34B36457A" Ref="U14"  Part="4" 
AR Path="/773F8EB44B36457A" Ref="U14"  Part="4" 
AR Path="/23C9F04B36457A" Ref="U14"  Part="4" 
AR Path="/23BC884B36457A" Ref="U14"  Part="4" 
AR Path="/DC0C124B36457A" Ref="U14"  Part="4" 
AR Path="/23C34C4B36457A" Ref="U14"  Part="4" 
AR Path="/23CBC44B36457A" Ref="U14"  Part="4" 
AR Path="/23C6504B36457A" Ref="U14"  Part="4" 
AR Path="/39803EA4B36457A" Ref="U14"  Part="4" 
AR Path="/FFFFFFFF4B36457A" Ref="U14"  Part="4" 
AR Path="/D058A04B36457A" Ref="U14"  Part="4" 
AR Path="/1607D44B36457A" Ref="U14"  Part="4" 
AR Path="/D1C3804B36457A" Ref="U14"  Part="4" 
AR Path="/24B36457A" Ref="U14"  Part="4" 
AR Path="/23D70C4B36457A" Ref="U14"  Part="4" 
AR Path="/2104B36457A" Ref="U14"  Part="4" 
AR Path="/F4BF4B36457A" Ref="U14"  Part="4" 
AR Path="/262F604B36457A" Ref="U14"  Part="4" 
AR Path="/384B36457A" Ref="U14"  Part="4" 
F 0 "U14" H 15800 12800 60  0000 C CNN
F 1 "74LS08" H 15800 12700 60  0000 C CNN
F 2 "14dip300" H 15800 12750 60  0001 C CNN
	4    15800 12750
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U?
U 2 1 4B364544
P 15800 12050
AR Path="/23D9B04B364544" Ref="U?"  Part="1" 
AR Path="/394433324B364544" Ref="U?"  Part="1" 
AR Path="/23D6544B364544" Ref="U?"  Part="1" 
AR Path="/7E44048F4B364544" Ref="U?"  Part="1" 
AR Path="/22E07144B364544" Ref="U"  Part="2" 
AR Path="/5AD7153D4B364544" Ref="U14"  Part="2" 
AR Path="/DCBAABCD4B364544" Ref="U14"  Part="2" 
AR Path="/A4B364544" Ref="U14"  Part="2" 
AR Path="/23C2344B364544" Ref="U14"  Part="2" 
AR Path="/4B364544" Ref="U14"  Part="2" 
AR Path="/94B364544" Ref="U14"  Part="2" 
AR Path="/755D912A4B364544" Ref="U14"  Part="2" 
AR Path="/6FF0DD404B364544" Ref="U14"  Part="2" 
AR Path="/6FE901F74B364544" Ref="U14"  Part="2" 
AR Path="/3FE224DD4B364544" Ref="U14"  Part="2" 
AR Path="/3FEFFFFF4B364544" Ref="U14"  Part="2" 
AR Path="/23D8D44B364544" Ref="U14"  Part="2" 
AR Path="/FFFFFFF04B364544" Ref="U14"  Part="2" 
AR Path="/402955814B364544" Ref="U14"  Part="2" 
AR Path="/A84B364544" Ref="U14"  Part="2" 
AR Path="/402C08B44B364544" Ref="U14"  Part="2" 
AR Path="/4E4B364544" Ref="U14"  Part="2" 
AR Path="/23D2034B364544" Ref="U14"  Part="2" 
AR Path="/402BEF1A4B364544" Ref="U14"  Part="2" 
AR Path="/14B364544" Ref="U14"  Part="2" 
AR Path="/40293BE74B364544" Ref="U14"  Part="2" 
AR Path="/402C55814B364544" Ref="U14"  Part="2" 
AR Path="/40273BE74B364544" Ref="U14"  Part="2" 
AR Path="/2600004B364544" Ref="U14"  Part="2" 
AR Path="/3D8EA0004B364544" Ref="U14"  Part="2" 
AR Path="/402908B44B364544" Ref="U14"  Part="2" 
AR Path="/3D6CC0004B364544" Ref="U14"  Part="2" 
AR Path="/3D5A40004B364544" Ref="U14"  Part="2" 
AR Path="/402755814B364544" Ref="U14"  Part="2" 
AR Path="/4030AAC04B364544" Ref="U14"  Part="2" 
AR Path="/4031DDF34B364544" Ref="U14"  Part="2" 
AR Path="/3FE88B434B364544" Ref="U14"  Part="2" 
AR Path="/4032778D4B364544" Ref="U14"  Part="2" 
AR Path="/403091264B364544" Ref="U14"  Part="2" 
AR Path="/403051264B364544" Ref="U14"  Part="2" 
AR Path="/4032F78D4B364544" Ref="U14"  Part="2" 
AR Path="/403251264B364544" Ref="U14"  Part="2" 
AR Path="/4032AAC04B364544" Ref="U14"  Part="2" 
AR Path="/4030D1264B364544" Ref="U14"  Part="2" 
AR Path="/4031778D4B364544" Ref="U14"  Part="2" 
AR Path="/3FEA24DD4B364544" Ref="U14"  Part="2" 
AR Path="/6FE934E34B364544" Ref="U14"  Part="2" 
AR Path="/773F8EB44B364544" Ref="U14"  Part="2" 
AR Path="/23C9F04B364544" Ref="U14"  Part="2" 
AR Path="/23BC884B364544" Ref="U14"  Part="2" 
AR Path="/DC0C124B364544" Ref="U14"  Part="2" 
AR Path="/23C34C4B364544" Ref="U14"  Part="2" 
AR Path="/23CBC44B364544" Ref="U14"  Part="2" 
AR Path="/23C6504B364544" Ref="U14"  Part="2" 
AR Path="/39803EA4B364544" Ref="U14"  Part="2" 
AR Path="/FFFFFFFF4B364544" Ref="U14"  Part="2" 
AR Path="/D058A04B364544" Ref="U14"  Part="2" 
AR Path="/1607D44B364544" Ref="U14"  Part="2" 
AR Path="/D1C3804B364544" Ref="U14"  Part="2" 
AR Path="/24B364544" Ref="U14"  Part="2" 
AR Path="/23D70C4B364544" Ref="U14"  Part="2" 
AR Path="/2104B364544" Ref="U14"  Part="2" 
AR Path="/F4BF4B364544" Ref="U14"  Part="2" 
AR Path="/262F604B364544" Ref="U14"  Part="2" 
AR Path="/384B364544" Ref="U14"  Part="2" 
F 0 "U14" H 15800 12100 60  0000 C CNN
F 1 "74LS08" H 15800 12000 60  0000 C CNN
F 2 "14dip300" H 15800 12050 60  0001 C CNN
	2    15800 12050
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U?
U 1 1 4B3644CD
P 15800 10900
AR Path="/69698AFC4B3644CD" Ref="U?"  Part="1" 
AR Path="/393639364B3644CD" Ref="U?"  Part="1" 
AR Path="/23D6544B3644CD" Ref="U?"  Part="1" 
AR Path="/7E44048F4B3644CD" Ref="U?"  Part="1" 
AR Path="/1C807204B3644CD" Ref="U"  Part="1" 
AR Path="/5AD7153D4B3644CD" Ref="U14"  Part="1" 
AR Path="/DCBAABCD4B3644CD" Ref="U14"  Part="1" 
AR Path="/A4B3644CD" Ref="U14"  Part="1" 
AR Path="/23C2344B3644CD" Ref="U14"  Part="1" 
AR Path="/4B3644CD" Ref="U14"  Part="1" 
AR Path="/94B3644CD" Ref="U14"  Part="1" 
AR Path="/755D912A4B3644CD" Ref="U14"  Part="1" 
AR Path="/6FF0DD404B3644CD" Ref="U14"  Part="1" 
AR Path="/14B3644CD" Ref="U14"  Part="1" 
AR Path="/6FE901F74B3644CD" Ref="U14"  Part="1" 
AR Path="/3FE224DD4B3644CD" Ref="U14"  Part="1" 
AR Path="/3FEFFFFF4B3644CD" Ref="U14"  Part="1" 
AR Path="/23D8D44B3644CD" Ref="U14"  Part="1" 
AR Path="/FFFFFFF04B3644CD" Ref="U14"  Part="1" 
AR Path="/402955814B3644CD" Ref="U14"  Part="1" 
AR Path="/A84B3644CD" Ref="U14"  Part="1" 
AR Path="/402C08B44B3644CD" Ref="U14"  Part="1" 
AR Path="/4E4B3644CD" Ref="U14"  Part="1" 
AR Path="/23D2034B3644CD" Ref="U14"  Part="1" 
AR Path="/402BEF1A4B3644CD" Ref="U14"  Part="1" 
AR Path="/40293BE74B3644CD" Ref="U14"  Part="1" 
AR Path="/402C55814B3644CD" Ref="U14"  Part="1" 
AR Path="/40273BE74B3644CD" Ref="U14"  Part="1" 
AR Path="/2600004B3644CD" Ref="U14"  Part="1" 
AR Path="/3D8EA0004B3644CD" Ref="U14"  Part="1" 
AR Path="/402908B44B3644CD" Ref="U14"  Part="1" 
AR Path="/3D6CC0004B3644CD" Ref="U14"  Part="1" 
AR Path="/3D5A40004B3644CD" Ref="U14"  Part="1" 
AR Path="/402755814B3644CD" Ref="U14"  Part="1" 
AR Path="/4030AAC04B3644CD" Ref="U14"  Part="1" 
AR Path="/4031DDF34B3644CD" Ref="U14"  Part="1" 
AR Path="/3FE88B434B3644CD" Ref="U14"  Part="1" 
AR Path="/4032778D4B3644CD" Ref="U14"  Part="1" 
AR Path="/403091264B3644CD" Ref="U14"  Part="1" 
AR Path="/403051264B3644CD" Ref="U14"  Part="1" 
AR Path="/4032F78D4B3644CD" Ref="U14"  Part="1" 
AR Path="/403251264B3644CD" Ref="U14"  Part="1" 
AR Path="/4032AAC04B3644CD" Ref="U14"  Part="1" 
AR Path="/4030D1264B3644CD" Ref="U14"  Part="1" 
AR Path="/4031778D4B3644CD" Ref="U14"  Part="1" 
AR Path="/3FEA24DD4B3644CD" Ref="U14"  Part="1" 
AR Path="/6FE934E34B3644CD" Ref="U14"  Part="1" 
AR Path="/773F8EB44B3644CD" Ref="U14"  Part="1" 
AR Path="/23C9F04B3644CD" Ref="U14"  Part="1" 
AR Path="/23BC884B3644CD" Ref="U14"  Part="1" 
AR Path="/DC0C124B3644CD" Ref="U14"  Part="1" 
AR Path="/23C34C4B3644CD" Ref="U14"  Part="1" 
AR Path="/23CBC44B3644CD" Ref="U14"  Part="1" 
AR Path="/23C6504B3644CD" Ref="U14"  Part="1" 
AR Path="/39803EA4B3644CD" Ref="U14"  Part="1" 
AR Path="/FFFFFFFF4B3644CD" Ref="U14"  Part="1" 
AR Path="/D058A04B3644CD" Ref="U14"  Part="1" 
AR Path="/1607D44B3644CD" Ref="U14"  Part="1" 
AR Path="/D1C3804B3644CD" Ref="U14"  Part="1" 
AR Path="/24B3644CD" Ref="U14"  Part="1" 
AR Path="/23D70C4B3644CD" Ref="U14"  Part="1" 
AR Path="/2104B3644CD" Ref="U14"  Part="1" 
AR Path="/F4BF4B3644CD" Ref="U14"  Part="1" 
AR Path="/262F604B3644CD" Ref="U14"  Part="1" 
AR Path="/384B3644CD" Ref="U14"  Part="1" 
F 0 "U14" H 15800 10950 60  0000 C CNN
F 1 "74LS08" H 15800 10850 60  0000 C CNN
F 2 "14dip300" H 15800 10900 60  0001 C CNN
	1    15800 10900
	1    0    0    -1  
$EndComp
Text Label 13600 10450 0    60   ~ 0
16wr
Text Label 13550 8900 0    60   ~ 0
16rd
Text Label 13550 7900 0    60   ~ 0
8rd
Text Label 8450 8050 0    60   ~ 0
pDBIN
Text Label 8450 8250 0    60   ~ 0
sXTRQ*
Text Label 8450 8450 0    60   ~ 0
pWR*
Text Label 8450 8550 0    60   ~ 0
A0
Text Label 8450 9050 0    60   ~ 0
GND
Text Label 8450 8950 0    60   ~ 0
GND
Text Label 17050 8700 0    60   ~ 0
SIXTN*
$Comp
L 74LS01 U?
U 1 1 4B363E4A
P 15800 8700
AR Path="/69698AFC4B363E4A" Ref="U?"  Part="1" 
AR Path="/393639364B363E4A" Ref="U?"  Part="1" 
AR Path="/23D6B04B363E4A" Ref="U?"  Part="1" 
AR Path="/23D1104B363E4A" Ref="U10"  Part="1" 
AR Path="/314433324B363E4A" Ref="U10"  Part="1" 
AR Path="/5AD7153D4B363E4A" Ref="U10"  Part="1" 
AR Path="/DCBAABCD4B363E4A" Ref="U10"  Part="1" 
AR Path="/4B363E4A" Ref="U10"  Part="1" 
AR Path="/A4B363E4A" Ref="U10"  Part="1" 
AR Path="/94B363E4A" Ref="U10"  Part="1" 
AR Path="/755D912A4B363E4A" Ref="U10"  Part="1" 
AR Path="/6FF0DD404B363E4A" Ref="U10"  Part="1" 
AR Path="/14B363E4A" Ref="U10"  Part="1" 
AR Path="/6FE901F74B363E4A" Ref="U10"  Part="1" 
AR Path="/3FE224DD4B363E4A" Ref="U10"  Part="1" 
AR Path="/3FEFFFFF4B363E4A" Ref="U10"  Part="1" 
AR Path="/23D8D44B363E4A" Ref="U10"  Part="1" 
AR Path="/FFFFFFF04B363E4A" Ref="U10"  Part="1" 
AR Path="/402955814B363E4A" Ref="U10"  Part="1" 
AR Path="/A84B363E4A" Ref="U10"  Part="1" 
AR Path="/402C08B44B363E4A" Ref="U10"  Part="1" 
AR Path="/23D2034B363E4A" Ref="U10"  Part="1" 
AR Path="/402BEF1A4B363E4A" Ref="U10"  Part="1" 
AR Path="/40293BE74B363E4A" Ref="U10"  Part="1" 
AR Path="/402C55814B363E4A" Ref="U10"  Part="1" 
AR Path="/40273BE74B363E4A" Ref="U10"  Part="1" 
AR Path="/2600004B363E4A" Ref="U10"  Part="1" 
AR Path="/3D8EA0004B363E4A" Ref="U10"  Part="1" 
AR Path="/402908B44B363E4A" Ref="U10"  Part="1" 
AR Path="/3D6CC0004B363E4A" Ref="U10"  Part="1" 
AR Path="/3D5A40004B363E4A" Ref="U10"  Part="1" 
AR Path="/402755814B363E4A" Ref="U10"  Part="1" 
AR Path="/4030AAC04B363E4A" Ref="U10"  Part="1" 
AR Path="/4031DDF34B363E4A" Ref="U10"  Part="1" 
AR Path="/3FE88B434B363E4A" Ref="U10"  Part="1" 
AR Path="/4032778D4B363E4A" Ref="U10"  Part="1" 
AR Path="/403091264B363E4A" Ref="U10"  Part="1" 
AR Path="/403051264B363E4A" Ref="U10"  Part="1" 
AR Path="/4032F78D4B363E4A" Ref="U10"  Part="1" 
AR Path="/403251264B363E4A" Ref="U10"  Part="1" 
AR Path="/4032AAC04B363E4A" Ref="U10"  Part="1" 
AR Path="/4030D1264B363E4A" Ref="U10"  Part="1" 
AR Path="/4031778D4B363E4A" Ref="U10"  Part="1" 
AR Path="/3FEA24DD4B363E4A" Ref="U10"  Part="1" 
AR Path="/6FE934E34B363E4A" Ref="U10"  Part="1" 
AR Path="/773F65F14B363E4A" Ref="U10"  Part="1" 
AR Path="/773F8EB44B363E4A" Ref="U10"  Part="1" 
AR Path="/23C9F04B363E4A" Ref="U10"  Part="1" 
AR Path="/23BC884B363E4A" Ref="U10"  Part="1" 
AR Path="/DC0C124B363E4A" Ref="U10"  Part="1" 
AR Path="/23C34C4B363E4A" Ref="U10"  Part="1" 
AR Path="/23CBC44B363E4A" Ref="U10"  Part="1" 
AR Path="/23C6504B363E4A" Ref="U10"  Part="1" 
AR Path="/39803EA4B363E4A" Ref="U10"  Part="1" 
AR Path="/FFFFFFFF4B363E4A" Ref="U10"  Part="1" 
AR Path="/D058A04B363E4A" Ref="U10"  Part="1" 
AR Path="/1607D44B363E4A" Ref="U10"  Part="1" 
AR Path="/D1C3804B363E4A" Ref="U10"  Part="1" 
AR Path="/24B363E4A" Ref="U10"  Part="1" 
AR Path="/23D70C4B363E4A" Ref="U10"  Part="1" 
AR Path="/2104B363E4A" Ref="U10"  Part="1" 
AR Path="/F4BF4B363E4A" Ref="U10"  Part="1" 
AR Path="/262F604B363E4A" Ref="U10"  Part="1" 
AR Path="/384B363E4A" Ref="U10"  Part="1" 
F 0 "U10" H 15800 8750 60  0000 C CNN
F 1 "74LS01" H 15800 8650 60  0000 C CNN
F 2 "14dip300" H 15800 8700 60  0001 C CNN
	1    15800 8700
	1    0    0    -1  
$EndComp
Text Notes 4200 1250 0    60   ~ 0
BOARD RAM SELECT LED
$Comp
L LED D?
U 1 1 4B363B26
P 3950 1250
AR Path="/23D9B04B363B26" Ref="D?"  Part="1" 
AR Path="/394433324B363B26" Ref="D?"  Part="1" 
AR Path="/4B363B26" Ref="D1"  Part="1" 
AR Path="/94B363B26" Ref="D1"  Part="1" 
AR Path="/FFFFFFF04B363B26" Ref="D1"  Part="1" 
AR Path="/5AD7153D4B363B26" Ref="D1"  Part="1" 
AR Path="/DCBAABCD4B363B26" Ref="D1"  Part="1" 
AR Path="/A4B363B26" Ref="D1"  Part="1" 
AR Path="/755D912A4B363B26" Ref="D1"  Part="1" 
AR Path="/6FF0DD404B363B26" Ref="D1"  Part="1" 
AR Path="/14B363B26" Ref="D1"  Part="1" 
AR Path="/6FE901F74B363B26" Ref="D1"  Part="1" 
AR Path="/3FE224DD4B363B26" Ref="D1"  Part="1" 
AR Path="/3FEFFFFF4B363B26" Ref="D1"  Part="1" 
AR Path="/23D8D44B363B26" Ref="D1"  Part="1" 
AR Path="/402955814B363B26" Ref="D1"  Part="1" 
AR Path="/A84B363B26" Ref="D1"  Part="1" 
AR Path="/402C08B44B363B26" Ref="D1"  Part="1" 
AR Path="/402BEF1A4B363B26" Ref="D1"  Part="1" 
AR Path="/40293BE74B363B26" Ref="D1"  Part="1" 
AR Path="/402C55814B363B26" Ref="D1"  Part="1" 
AR Path="/40273BE74B363B26" Ref="D1"  Part="1" 
AR Path="/2600004B363B26" Ref="D1"  Part="1" 
AR Path="/3D8EA0004B363B26" Ref="D1"  Part="1" 
AR Path="/402908B44B363B26" Ref="D1"  Part="1" 
AR Path="/3D6CC0004B363B26" Ref="D1"  Part="1" 
AR Path="/3D5A40004B363B26" Ref="D1"  Part="1" 
AR Path="/402755814B363B26" Ref="D1"  Part="1" 
AR Path="/4030AAC04B363B26" Ref="D1"  Part="1" 
AR Path="/4031DDF34B363B26" Ref="D1"  Part="1" 
AR Path="/3FE88B434B363B26" Ref="D1"  Part="1" 
AR Path="/4032778D4B363B26" Ref="D1"  Part="1" 
AR Path="/403091264B363B26" Ref="D1"  Part="1" 
AR Path="/403051264B363B26" Ref="D1"  Part="1" 
AR Path="/4032F78D4B363B26" Ref="D1"  Part="1" 
AR Path="/403251264B363B26" Ref="D1"  Part="1" 
AR Path="/4032AAC04B363B26" Ref="D1"  Part="1" 
AR Path="/4030D1264B363B26" Ref="D1"  Part="1" 
AR Path="/4031778D4B363B26" Ref="D1"  Part="1" 
AR Path="/3FEA24DD4B363B26" Ref="D1"  Part="1" 
AR Path="/6FE934E34B363B26" Ref="D1"  Part="1" 
AR Path="/773F65F14B363B26" Ref="D1"  Part="1" 
AR Path="/773F8EB44B363B26" Ref="D1"  Part="1" 
AR Path="/23C9F04B363B26" Ref="D1"  Part="1" 
AR Path="/24B363B26" Ref="D1"  Part="1" 
AR Path="/23BC884B363B26" Ref="D1"  Part="1" 
AR Path="/DC0C124B363B26" Ref="D1"  Part="1" 
AR Path="/23C34C4B363B26" Ref="D1"  Part="1" 
AR Path="/23CBC44B363B26" Ref="D1"  Part="1" 
AR Path="/23C6504B363B26" Ref="D1"  Part="1" 
AR Path="/39803EA4B363B26" Ref="D1"  Part="1" 
AR Path="/FFFFFFFF4B363B26" Ref="D1"  Part="1" 
AR Path="/D058A04B363B26" Ref="D1"  Part="1" 
AR Path="/1607D44B363B26" Ref="D1"  Part="1" 
AR Path="/D1C3804B363B26" Ref="D1"  Part="1" 
AR Path="/23D70C4B363B26" Ref="D1"  Part="1" 
AR Path="/2104B363B26" Ref="D1"  Part="1" 
AR Path="/F4BF4B363B26" Ref="D1"  Part="1" 
AR Path="/262F604B363B26" Ref="D1"  Part="1" 
AR Path="/384B363B26" Ref="D1"  Part="1" 
AR Path="/23D6704B363B26" Ref="D1"  Part="1" 
F 0 "D1" H 3950 1350 50  0000 C CNN
F 1 "LED" H 3950 1150 50  0000 C CNN
F 2 "LEDV" H 3950 1250 60  0001 C CNN
	1    3950 1250
	-1   0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 4B363B1B
P 3500 1250
AR Path="/23D9B04B363B1B" Ref="R?"  Part="1" 
AR Path="/394433324B363B1B" Ref="R?"  Part="1" 
AR Path="/1C503B04B363B1B" Ref="R?"  Part="1" 
AR Path="/4B363B1B" Ref="R1"  Part="1" 
AR Path="/94B363B1B" Ref="R1"  Part="1" 
AR Path="/FFFFFFF04B363B1B" Ref="R1"  Part="1" 
AR Path="/5AD7153D4B363B1B" Ref="R1"  Part="1" 
AR Path="/DCBAABCD4B363B1B" Ref="R1"  Part="1" 
AR Path="/A4B363B1B" Ref="R1"  Part="1" 
AR Path="/755D912A4B363B1B" Ref="R1"  Part="1" 
AR Path="/6FF0DD404B363B1B" Ref="R1"  Part="1" 
AR Path="/14B363B1B" Ref="R1"  Part="1" 
AR Path="/6FE901F74B363B1B" Ref="R1"  Part="1" 
AR Path="/3FE224DD4B363B1B" Ref="R1"  Part="1" 
AR Path="/3FEFFFFF4B363B1B" Ref="R1"  Part="1" 
AR Path="/23D8D44B363B1B" Ref="R1"  Part="1" 
AR Path="/402955814B363B1B" Ref="R1"  Part="1" 
AR Path="/A84B363B1B" Ref="R1"  Part="1" 
AR Path="/402C08B44B363B1B" Ref="R1"  Part="1" 
AR Path="/402BEF1A4B363B1B" Ref="R1"  Part="1" 
AR Path="/40293BE74B363B1B" Ref="R1"  Part="1" 
AR Path="/402C55814B363B1B" Ref="R1"  Part="1" 
AR Path="/40273BE74B363B1B" Ref="R1"  Part="1" 
AR Path="/2600004B363B1B" Ref="R1"  Part="1" 
AR Path="/3D8EA0004B363B1B" Ref="R1"  Part="1" 
AR Path="/402908B44B363B1B" Ref="R1"  Part="1" 
AR Path="/3D6CC0004B363B1B" Ref="R1"  Part="1" 
AR Path="/3D5A40004B363B1B" Ref="R1"  Part="1" 
AR Path="/402755814B363B1B" Ref="R1"  Part="1" 
AR Path="/4030AAC04B363B1B" Ref="R1"  Part="1" 
AR Path="/4031DDF34B363B1B" Ref="R1"  Part="1" 
AR Path="/3FE88B434B363B1B" Ref="R1"  Part="1" 
AR Path="/4032778D4B363B1B" Ref="R1"  Part="1" 
AR Path="/403091264B363B1B" Ref="R1"  Part="1" 
AR Path="/403051264B363B1B" Ref="R1"  Part="1" 
AR Path="/4032F78D4B363B1B" Ref="R1"  Part="1" 
AR Path="/403251264B363B1B" Ref="R1"  Part="1" 
AR Path="/4032AAC04B363B1B" Ref="R1"  Part="1" 
AR Path="/4030D1264B363B1B" Ref="R1"  Part="1" 
AR Path="/4031778D4B363B1B" Ref="R1"  Part="1" 
AR Path="/3FEA24DD4B363B1B" Ref="R1"  Part="1" 
AR Path="/6FE934E34B363B1B" Ref="R1"  Part="1" 
AR Path="/773F65F14B363B1B" Ref="R1"  Part="1" 
AR Path="/773F8EB44B363B1B" Ref="R1"  Part="1" 
AR Path="/23C9F04B363B1B" Ref="R1"  Part="1" 
AR Path="/24B363B1B" Ref="R1"  Part="1" 
AR Path="/23BC884B363B1B" Ref="R1"  Part="1" 
AR Path="/DC0C124B363B1B" Ref="R1"  Part="1" 
AR Path="/23C34C4B363B1B" Ref="R1"  Part="1" 
AR Path="/23CBC44B363B1B" Ref="R1"  Part="1" 
AR Path="/23C6504B363B1B" Ref="R1"  Part="1" 
AR Path="/39803EA4B363B1B" Ref="R1"  Part="1" 
AR Path="/FFFFFFFF4B363B1B" Ref="R1"  Part="1" 
AR Path="/D058A04B363B1B" Ref="R1"  Part="1" 
AR Path="/1607D44B363B1B" Ref="R1"  Part="1" 
AR Path="/D1C3804B363B1B" Ref="R1"  Part="1" 
AR Path="/23D70C4B363B1B" Ref="R1"  Part="1" 
AR Path="/2104B363B1B" Ref="R1"  Part="1" 
AR Path="/F4BF4B363B1B" Ref="R1"  Part="1" 
AR Path="/262F604B363B1B" Ref="R1"  Part="1" 
AR Path="/384B363B1B" Ref="R1"  Part="1" 
F 0 "R1" V 3580 1250 50  0000 C CNN
F 1 "470" V 3500 1250 50  0000 C CNN
F 2 "R3" H 3500 1250 60  0001 C CNN
	1    3500 1250
	0    -1   1    0   
$EndComp
Text Label 800  3950 0    60   ~ 0
bA20
$Comp
L 74LS139 U?
U 1 1 4B3635D4
P 6500 2500
AR Path="/69698AFC4B3635D4" Ref="U?"  Part="1" 
AR Path="/393639364B3635D4" Ref="U?"  Part="1" 
AR Path="/4B3635D4" Ref="U4"  Part="1" 
AR Path="/94B3635D4" Ref="U4"  Part="1" 
AR Path="/FFFFFFF04B3635D4" Ref="U4"  Part="1" 
AR Path="/5AD7153D4B3635D4" Ref="U4"  Part="1" 
AR Path="/DCBAABCD4B3635D4" Ref="U4"  Part="1" 
AR Path="/A4B3635D4" Ref="U4"  Part="1" 
AR Path="/755D912A4B3635D4" Ref="U4"  Part="1" 
AR Path="/14B3635D4" Ref="U4"  Part="1" 
AR Path="/6FF0DD404B3635D4" Ref="U4"  Part="1" 
AR Path="/3FE224DD4B3635D4" Ref="U4"  Part="1" 
AR Path="/3FEFFFFF4B3635D4" Ref="U4"  Part="1" 
AR Path="/23D8D44B3635D4" Ref="U4"  Part="1" 
AR Path="/402955814B3635D4" Ref="U4"  Part="1" 
AR Path="/A84B3635D4" Ref="U4"  Part="1" 
AR Path="/402C08B44B3635D4" Ref="U4"  Part="1" 
AR Path="/402BEF1A4B3635D4" Ref="U4"  Part="1" 
AR Path="/40293BE74B3635D4" Ref="U4"  Part="1" 
AR Path="/402C55814B3635D4" Ref="U4"  Part="1" 
AR Path="/40273BE74B3635D4" Ref="U4"  Part="1" 
AR Path="/2600004B3635D4" Ref="U4"  Part="1" 
AR Path="/3D8EA0004B3635D4" Ref="U4"  Part="1" 
AR Path="/402908B44B3635D4" Ref="U4"  Part="1" 
AR Path="/3D6CC0004B3635D4" Ref="U4"  Part="1" 
AR Path="/3D5A40004B3635D4" Ref="U4"  Part="1" 
AR Path="/402755814B3635D4" Ref="U4"  Part="1" 
AR Path="/4030AAC04B3635D4" Ref="U4"  Part="1" 
AR Path="/4031DDF34B3635D4" Ref="U4"  Part="1" 
AR Path="/3FE88B434B3635D4" Ref="U4"  Part="1" 
AR Path="/4032778D4B3635D4" Ref="U4"  Part="1" 
AR Path="/403091264B3635D4" Ref="U4"  Part="1" 
AR Path="/403051264B3635D4" Ref="U4"  Part="1" 
AR Path="/4032F78D4B3635D4" Ref="U4"  Part="1" 
AR Path="/403251264B3635D4" Ref="U4"  Part="1" 
AR Path="/4032AAC04B3635D4" Ref="U4"  Part="1" 
AR Path="/4030D1264B3635D4" Ref="U4"  Part="1" 
AR Path="/4031778D4B3635D4" Ref="U4"  Part="1" 
AR Path="/3FEA24DD4B3635D4" Ref="U4"  Part="1" 
AR Path="/6FE934E34B3635D4" Ref="U4"  Part="1" 
AR Path="/773F65F14B3635D4" Ref="U4"  Part="1" 
AR Path="/773F8EB44B3635D4" Ref="U4"  Part="1" 
AR Path="/23C9F04B3635D4" Ref="U4"  Part="1" 
AR Path="/23BC884B3635D4" Ref="U4"  Part="1" 
AR Path="/DC0C124B3635D4" Ref="U4"  Part="1" 
AR Path="/23C34C4B3635D4" Ref="U4"  Part="1" 
AR Path="/23CBC44B3635D4" Ref="U4"  Part="1" 
AR Path="/23C6504B3635D4" Ref="U4"  Part="1" 
AR Path="/39803EA4B3635D4" Ref="U4"  Part="1" 
AR Path="/FFFFFFFF4B3635D4" Ref="U4"  Part="1" 
AR Path="/D058A04B3635D4" Ref="U4"  Part="1" 
AR Path="/1607D44B3635D4" Ref="U4"  Part="1" 
AR Path="/D1C3804B3635D4" Ref="U4"  Part="1" 
AR Path="/24B3635D4" Ref="U4"  Part="1" 
AR Path="/23D70C4B3635D4" Ref="U4"  Part="1" 
AR Path="/2104B3635D4" Ref="U4"  Part="1" 
AR Path="/F4BF4B3635D4" Ref="U4"  Part="1" 
AR Path="/262F604B3635D4" Ref="U4"  Part="1" 
AR Path="/384B3635D4" Ref="U4"  Part="1" 
F 0 "U4" H 6500 2600 60  0000 C CNN
F 1 "74LS139" H 6500 2400 60  0000 C CNN
F 2 "16dip300" H 6500 2500 60  0001 C CNN
	1    6500 2500
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 4B2EC5DA
P 11850 15600
AR Path="/69698AFC4B2EC5DA" Ref="C?"  Part="1" 
AR Path="/393639364B2EC5DA" Ref="C?"  Part="1" 
AR Path="/4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23CF784B2EC5DA" Ref="C?"  Part="1" 
AR Path="/94B2EC5DA" Ref="C18"  Part="1" 
AR Path="/FFFFFFF04B2EC5DA" Ref="C18"  Part="1" 
AR Path="/5AD7153D4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/DCBAABCD4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/6FE901F74B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3FEFFFFF4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23D2034B2EC5DA" Ref="C18"  Part="1" 
AR Path="/A84B2EC5DA" Ref="C18"  Part="1" 
AR Path="/A4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/755D912A4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/6FF0DD404B2EC5DA" Ref="C18"  Part="1" 
AR Path="/14B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3FE224DD4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23D8D44B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402955814B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402C08B44B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402BEF1A4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/40293BE74B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402C55814B2EC5DA" Ref="C18"  Part="1" 
AR Path="/40273BE74B2EC5DA" Ref="C18"  Part="1" 
AR Path="/2600004B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3D8EA0004B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402908B44B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3D6CC0004B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3D5A40004B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402755814B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4030AAC04B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4031DDF34B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3FE88B434B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4032778D4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/403091264B2EC5DA" Ref="C18"  Part="1" 
AR Path="/403051264B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4032F78D4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/403251264B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4032AAC04B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4030D1264B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4031778D4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3FEA24DD4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/6FE934E34B2EC5DA" Ref="C18"  Part="1" 
AR Path="/773F65F14B2EC5DA" Ref="C18"  Part="1" 
AR Path="/773F8EB44B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23C9F04B2EC5DA" Ref="C18"  Part="1" 
AR Path="/24B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23BC884B2EC5DA" Ref="C18"  Part="1" 
AR Path="/DC0C124B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23C34C4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23CBC44B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23C6504B2EC5DA" Ref="C18"  Part="1" 
AR Path="/39803EA4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/FFFFFFFF4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/D058A04B2EC5DA" Ref="C18"  Part="1" 
AR Path="/1607D44B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23D70C4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/2104B2EC5DA" Ref="C18"  Part="1" 
AR Path="/F4BF4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/262F604B2EC5DA" Ref="C18"  Part="1" 
AR Path="/384B2EC5DA" Ref="C18"  Part="1" 
F 0 "C18" H 11900 15700 50  0000 L CNN
F 1 "0.1 uF" H 11900 15500 50  0000 L CNN
F 2 "C2" H 11850 15600 60  0001 C CNN
	1    11850 15600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4B04BD81
P 19050 11500
AR Path="/69698AFC4B04BD81" Ref="#PWR?"  Part="1" 
AR Path="/393639364B04BD81" Ref="#PWR?"  Part="1" 
AR Path="/4B04BD81" Ref="#PWR031"  Part="1" 
AR Path="/DCBAABCD4B04BD81" Ref="#PWR024"  Part="1" 
AR Path="/94B04BD81" Ref="#PWR35"  Part="1" 
AR Path="/25E4B04BD81" Ref="#PWR034"  Part="1" 
AR Path="/5AD7153D4B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/A4B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/40256F1A4B04BD81" Ref="#PWR08"  Part="1" 
AR Path="/6FE901F74B04BD81" Ref="#PWR018"  Part="1" 
AR Path="/3FEFFFFF4B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/23D8D44B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/3D8EA0004B04BD81" Ref="#PWR024"  Part="1" 
AR Path="/3DA360004B04BD81" Ref="#PWR08"  Part="1" 
AR Path="/3D7E00004B04BD81" Ref="#PWR08"  Part="1" 
AR Path="/40286F1A4B04BD81" Ref="#PWR08"  Part="1" 
AR Path="/402A08B44B04BD81" Ref="#PWR08"  Part="1" 
AR Path="/402588B44B04BD81" Ref="#PWR08"  Part="1" 
AR Path="/FFFFFFF04B04BD81" Ref="#PWR35"  Part="1" 
AR Path="/23D2034B04BD81" Ref="#PWR08"  Part="1" 
AR Path="/A84B04BD81" Ref="#PWR34"  Part="1" 
AR Path="/755D912A4B04BD81" Ref="#PWR012"  Part="1" 
AR Path="/6FF0DD404B04BD81" Ref="#PWR032"  Part="1" 
AR Path="/363030314B04BD81" Ref="#PWR034"  Part="1" 
AR Path="/3FE224DD4B04BD81" Ref="#PWR016"  Part="1" 
AR Path="/402955814B04BD81" Ref="#PWR016"  Part="1" 
AR Path="/402C08B44B04BD81" Ref="#PWR017"  Part="1" 
AR Path="/314B04BD81" Ref="#PWR018"  Part="1" 
AR Path="/402BEF1A4B04BD81" Ref="#PWR018"  Part="1" 
AR Path="/40293BE74B04BD81" Ref="#PWR020"  Part="1" 
AR Path="/402C55814B04BD81" Ref="#PWR023"  Part="1" 
AR Path="/40273BE74B04BD81" Ref="#PWR024"  Part="1" 
AR Path="/2600004B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/402908B44B04BD81" Ref="#PWR020"  Part="1" 
AR Path="/3D6CC0004B04BD81" Ref="#PWR017"  Part="1" 
AR Path="/3D5A40004B04BD81" Ref="#PWR017"  Part="1" 
AR Path="/402755814B04BD81" Ref="#PWR025"  Part="1" 
AR Path="/4030AAC04B04BD81" Ref="#PWR025"  Part="1" 
AR Path="/14B04BD81" Ref="#PWR037"  Part="1" 
AR Path="/4031DDF34B04BD81" Ref="#PWR026"  Part="1" 
AR Path="/3FE88B434B04BD81" Ref="#PWR026"  Part="1" 
AR Path="/4032778D4B04BD81" Ref="#PWR032"  Part="1" 
AR Path="/403091264B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/403051264B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/4032F78D4B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/403251264B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/4032AAC04B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/4030D1264B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/4031778D4B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/3FEA24DD4B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/6FE934E34B04BD81" Ref="#PWR034"  Part="1" 
AR Path="/773F65F14B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/773F8EB44B04BD81" Ref="#PWR031"  Part="1" 
AR Path="/23C9F04B04BD81" Ref="#PWR37"  Part="1" 
AR Path="/23BC884B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/DC0C124B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/6684D64B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/23C34C4B04BD81" Ref="#PWR036"  Part="1" 
AR Path="/23CBC44B04BD81" Ref="#PWR035"  Part="1" 
AR Path="/23C6504B04BD81" Ref="#PWR031"  Part="1" 
AR Path="/39803EA4B04BD81" Ref="#PWR036"  Part="1" 
AR Path="/FFFFFFFF4B04BD81" Ref="#PWR036"  Part="1" 
AR Path="/D058A04B04BD81" Ref="#PWR037"  Part="1" 
AR Path="/1607D44B04BD81" Ref="#PWR037"  Part="1" 
AR Path="/24B04BD81" Ref="#PWR037"  Part="1" 
AR Path="/23D70C4B04BD81" Ref="#PWR037"  Part="1" 
AR Path="/2104B04BD81" Ref="#PWR037"  Part="1" 
AR Path="/F4BF4B04BD81" Ref="#PWR031"  Part="1" 
AR Path="/262F604B04BD81" Ref="#PWR031"  Part="1" 
AR Path="/384B04BD81" Ref="#PWR032"  Part="1" 
F 0 "#PWR031" H 19050 11500 30  0001 C CNN
F 1 "GND" H 19050 11430 30  0001 C CNN
	1    19050 11500
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 4AEDE3A8
P 12300 15600
AR Path="/23D9B04AEDE3A8" Ref="C?"  Part="1" 
AR Path="/394433324AEDE3A8" Ref="C?"  Part="1" 
AR Path="/23D6B44AEDE3A8" Ref="C?"  Part="1" 
AR Path="/7E44048F4AEDE3A8" Ref="0.1"  Part="0" 
AR Path="/2306E64AEDE3A8" Ref="C"  Part="1" 
AR Path="/23D6544AEDE3A8" Ref="0.1"  Part="0" 
AR Path="/5D06BC4AEDE3A8" Ref="C"  Part="1" 
AR Path="/4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/94AEDE3A8" Ref="C17"  Part="1" 
AR Path="/5AD7153D4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/DCBAABCD4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/755D912A4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/6FE901F74AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402688B44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/A4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3FEFFFFF4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23D9304AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3FE441894AEDE3A8" Ref="C17"  Part="1" 
AR Path="/2600004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/B84AEDE3A8" Ref="C17"  Part="1" 
AR Path="/FFFFFFF04AEDE3A8" Ref="C17"  Part="1" 
AR Path="/6FF0DD404AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3DA360004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4029D5814AEDE3A8" Ref="C17"  Part="1" 
AR Path="/40256F1A4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23D8D44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3D8EA0004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3D7E00004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/40286F1A4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402A08B44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402588B44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23D2034AEDE3A8" Ref="C17"  Part="1" 
AR Path="/A84AEDE3A8" Ref="C17"  Part="1" 
AR Path="/14AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3FE224DD4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402955814AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402C08B44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402BEF1A4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/40293BE74AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402C55814AEDE3A8" Ref="C17"  Part="1" 
AR Path="/40273BE74AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402908B44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3D6CC0004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3D5A40004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402755814AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4030AAC04AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4031DDF34AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3FE88B434AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4032778D4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/403091264AEDE3A8" Ref="C17"  Part="1" 
AR Path="/403051264AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4032F78D4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/403251264AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4032AAC04AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4030D1264AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4031778D4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3FEA24DD4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/6FE934E34AEDE3A8" Ref="C17"  Part="1" 
AR Path="/773F8EB44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23C9F04AEDE3A8" Ref="C17"  Part="1" 
AR Path="/24AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23BC884AEDE3A8" Ref="C17"  Part="1" 
AR Path="/DC0C124AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23C34C4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23CBC44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23C6504AEDE3A8" Ref="C17"  Part="1" 
AR Path="/39803EA4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/FFFFFFFF4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/D058A04AEDE3A8" Ref="C17"  Part="1" 
AR Path="/1607D44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23D70C4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/2104AEDE3A8" Ref="C17"  Part="1" 
AR Path="/F4BF4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/262F604AEDE3A8" Ref="C17"  Part="1" 
AR Path="/384AEDE3A8" Ref="C17"  Part="1" 
F 0 "C17" H 12350 15700 50  0000 L CNN
F 1 "0.1 uF" H 12350 15500 50  0000 L CNN
F 2 "C2" H 12300 15600 60  0001 C CNN
	1    12300 15600
	1    0    0    -1  
$EndComp
Text Label 20600 15600 0    60   ~ 0
0V
$Comp
L PWR_FLAG #FLG?
U 1 1 4AECD1B8
P 20850 15200
AR Path="/69698AFC4AECD1B8" Ref="#FLG?"  Part="1" 
AR Path="/393639364AECD1B8" Ref="#FLG?"  Part="1" 
AR Path="/25E4AECD1B8" Ref="#FLG035"  Part="1" 
AR Path="/FFFFFFFF4AECD1B8" Ref="#FLG037"  Part="1" 
AR Path="/4AECD1B8" Ref="#FLG032"  Part="1" 
AR Path="/94AECD1B8" Ref="#FLG2"  Part="1" 
AR Path="/23C7004AECD1B8" Ref="#FLG07"  Part="1" 
AR Path="/5AD7153D4AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/DCBAABCD4AECD1B8" Ref="#FLG025"  Part="1" 
AR Path="/FFFFFFF04AECD1B8" Ref="#FLG2"  Part="1" 
AR Path="/A4AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/14AECD1B8" Ref="#FLG038"  Part="1" 
AR Path="/6FF0DD404AECD1B8" Ref="#FLG033"  Part="1" 
AR Path="/3FEFFFFF4AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/23C2344AECD1B8" Ref="#FLG01"  Part="1" 
AR Path="/755D912A4AECD1B8" Ref="#FLG013"  Part="1" 
AR Path="/3D7E00004AECD1B8" Ref="#FLG017"  Part="1" 
AR Path="/3D8EA0004AECD1B8" Ref="#FLG025"  Part="1" 
AR Path="/23D9304AECD1B8" Ref="#FLG02"  Part="1" 
AR Path="/6FE901F74AECD1B8" Ref="#FLG017"  Part="1" 
AR Path="/5AD746F64AECD1B8" Ref="#FLG02"  Part="1" 
AR Path="/4027EF1A4AECD1B8" Ref="#FLG02"  Part="1" 
AR Path="/402A6F1A4AECD1B8" Ref="#FLG02"  Part="1" 
AR Path="/4029BBE74AECD1B8" Ref="#FLG02"  Part="1" 
AR Path="/402A224D4AECD1B8" Ref="#FLG02"  Part="1" 
AR Path="/23D8D44AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/402A55814AECD1B8" Ref="#FLG01"  Part="1" 
AR Path="/4026224D4AECD1B8" Ref="#FLG06"  Part="1" 
AR Path="/3D6220004AECD1B8" Ref="#FLG01"  Part="1" 
AR Path="/3D6CC0004AECD1B8" Ref="#FLG018"  Part="1" 
AR Path="/3D9720004AECD1B8" Ref="#FLG02"  Part="1" 
AR Path="/2600004AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/402C3BE74AECD1B8" Ref="#FLG06"  Part="1" 
AR Path="/402955814AECD1B8" Ref="#FLG017"  Part="1" 
AR Path="/40293BE74AECD1B8" Ref="#FLG021"  Part="1" 
AR Path="/402555814AECD1B8" Ref="#FLG06"  Part="1" 
AR Path="/3FE88B434AECD1B8" Ref="#FLG027"  Part="1" 
AR Path="/23D2034AECD1B8" Ref="#FLG017"  Part="1" 
AR Path="/402988B44AECD1B8" Ref="#FLG06"  Part="1" 
AR Path="/3FED58104AECD1B8" Ref="#FLG06"  Part="1" 
AR Path="/402688B44AECD1B8" Ref="#FLG06"  Part="1" 
AR Path="/3FE441894AECD1B8" Ref="#FLG07"  Part="1" 
AR Path="/B84AECD1B8" Ref="#FLG2"  Part="1" 
AR Path="/3DA360004AECD1B8" Ref="#FLG017"  Part="1" 
AR Path="/4029D5814AECD1B8" Ref="#FLG09"  Part="1" 
AR Path="/40256F1A4AECD1B8" Ref="#FLG017"  Part="1" 
AR Path="/40286F1A4AECD1B8" Ref="#FLG017"  Part="1" 
AR Path="/402A08B44AECD1B8" Ref="#FLG017"  Part="1" 
AR Path="/402588B44AECD1B8" Ref="#FLG017"  Part="1" 
AR Path="/A84AECD1B8" Ref="#FLG2"  Part="1" 
AR Path="/363030314AECD1B8" Ref="#FLG035"  Part="1" 
AR Path="/3FE224DD4AECD1B8" Ref="#FLG017"  Part="1" 
AR Path="/402C08B44AECD1B8" Ref="#FLG018"  Part="1" 
AR Path="/314AECD1B8" Ref="#FLG019"  Part="1" 
AR Path="/402BEF1A4AECD1B8" Ref="#FLG019"  Part="1" 
AR Path="/402C55814AECD1B8" Ref="#FLG024"  Part="1" 
AR Path="/40273BE74AECD1B8" Ref="#FLG025"  Part="1" 
AR Path="/402908B44AECD1B8" Ref="#FLG021"  Part="1" 
AR Path="/3D5A40004AECD1B8" Ref="#FLG018"  Part="1" 
AR Path="/402755814AECD1B8" Ref="#FLG026"  Part="1" 
AR Path="/4030AAC04AECD1B8" Ref="#FLG026"  Part="1" 
AR Path="/4031DDF34AECD1B8" Ref="#FLG027"  Part="1" 
AR Path="/4032778D4AECD1B8" Ref="#FLG033"  Part="1" 
AR Path="/403091264AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/403051264AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/4032F78D4AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/403251264AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/4032AAC04AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/4030D1264AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/4031778D4AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/3FEA24DD4AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/6FE934E34AECD1B8" Ref="#FLG035"  Part="1" 
AR Path="/773F65F14AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/773F8EB44AECD1B8" Ref="#FLG032"  Part="1" 
AR Path="/23C9F04AECD1B8" Ref="#FLG2"  Part="1" 
AR Path="/23BC884AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/DC0C124AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/6684D64AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/23C34C4AECD1B8" Ref="#FLG037"  Part="1" 
AR Path="/23CBC44AECD1B8" Ref="#FLG036"  Part="1" 
AR Path="/39803EA4AECD1B8" Ref="#FLG037"  Part="1" 
AR Path="/D058A04AECD1B8" Ref="#FLG038"  Part="1" 
AR Path="/1607D44AECD1B8" Ref="#FLG038"  Part="1" 
AR Path="/24AECD1B8" Ref="#FLG038"  Part="1" 
AR Path="/23D70C4AECD1B8" Ref="#FLG038"  Part="1" 
AR Path="/2104AECD1B8" Ref="#FLG038"  Part="1" 
AR Path="/23C6504AECD1B8" Ref="#FLG032"  Part="1" 
AR Path="/F4BF4AECD1B8" Ref="#FLG032"  Part="1" 
AR Path="/262F604AECD1B8" Ref="#FLG032"  Part="1" 
AR Path="/384AECD1B8" Ref="#FLG033"  Part="1" 
F 0 "#FLG032" H 20850 15470 30  0001 C CNN
F 1 "PWR_FLAG" H 20850 15430 30  0000 C CNN
	1    20850 15200
	1    0    0    -1  
$EndComp
NoConn ~ 19400 5800
NoConn ~ 19400 800 
NoConn ~ 19400 10500
NoConn ~ 19400 10400
NoConn ~ 19400 10300
NoConn ~ 19400 8000
NoConn ~ 19400 7900
NoConn ~ 19400 7700
NoConn ~ 19400 7200
NoConn ~ 19400 7100
NoConn ~ 19400 6300
NoConn ~ 19400 6200
NoConn ~ 19400 5500
NoConn ~ 19400 5400
NoConn ~ 19400 5000
NoConn ~ 19400 3400
NoConn ~ 19400 3300
NoConn ~ 19400 3200
NoConn ~ 19400 3100
NoConn ~ 19400 2900
NoConn ~ 19400 2800
NoConn ~ 19400 2700
NoConn ~ 19400 2500
NoConn ~ 19400 2400
NoConn ~ 19400 2000
NoConn ~ 19400 1900
NoConn ~ 19400 900 
Text Label 18250 7600 0    60   ~ 0
GND
$Comp
L JUMPER JP3
U 1 1 4ADA2D9F
P 19100 7600
AR Path="/4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/94ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/DCBAABCD4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/755D912A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23D9304ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/5AD7153D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/6FE901F74ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D6CC0004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/6FF0DD404ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/14ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/FFFFFFF04ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FEFFFFF4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23C2344ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D7E00004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D8EA0004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/5AD746F64ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4027EF1A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402A6F1A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4029BBE74ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402A224D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23D8D44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402A55814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4026224D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D6220004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D9720004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/2600004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402C3BE74ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402955814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/40293BE74ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402555814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FE88B434ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23D2034ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402988B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FED58104ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402688B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FE441894ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/B84ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3DA360004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4029D5814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/40256F1A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/40286F1A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402A08B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402588B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/A84ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FE224DD4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402C08B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402BEF1A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402C55814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/40273BE74ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402908B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D5A40004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402755814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4030AAC04ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4031DDF34ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4032778D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/403091264ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/403051264ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4032F78D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/403251264ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4032AAC04ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4030D1264ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4031778D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FEA24DD4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/6FE934E34ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/773F65F14ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/773F8EB44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23C9F04ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/24ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23BC884ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/DC0C124ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23C34C4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23CBC44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23C6504ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/39803EA4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/FFFFFFFF4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/D058A04ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/1607D44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23D70C4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/2104ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/F4BF4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/262F604ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/384ADA2D9F" Ref="JP3"  Part="1" 
F 0 "JP3" H 19100 7750 60  0000 C CNN
F 1 "JUMPER" H 19100 7520 40  0000 C CNN
F 2 "PIN_ARRAY_2X1" H 19100 7600 60  0001 C CNN
	1    19100 7600
	1    0    0    -1  
$EndComp
$Comp
L JUMPER JP2
U 1 1 4ADA2D99
P 19100 5900
AR Path="/4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/94ADA2D99" Ref="JP2"  Part="1" 
AR Path="/755D912A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/5AD7153D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/6FE901F74ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D6CC0004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/6FF0DD404ADA2D99" Ref="JP2"  Part="1" 
AR Path="/14ADA2D99" Ref="JP2"  Part="1" 
AR Path="/FFFFFFF04ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FEFFFFF4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23C2344ADA2D99" Ref="JP2"  Part="1" 
AR Path="/DCBAABCD4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D7E00004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D8EA0004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/5AD746F64ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4027EF1A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402A6F1A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4029BBE74ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402A224D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23D8D44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402A55814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4026224D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D6220004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D9720004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/2600004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402C3BE74ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402955814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/40293BE74ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402555814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FE88B434ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23D2034ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402988B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FED58104ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402688B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FE441894ADA2D99" Ref="JP2"  Part="1" 
AR Path="/B84ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3DA360004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4029D5814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/40256F1A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/40286F1A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402A08B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402588B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/A84ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FE224DD4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402C08B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402BEF1A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402C55814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/40273BE74ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402908B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D5A40004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402755814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4030AAC04ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4031DDF34ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4032778D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/403091264ADA2D99" Ref="JP2"  Part="1" 
AR Path="/403051264ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4032F78D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/403251264ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4032AAC04ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4030D1264ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4031778D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FEA24DD4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/6FE934E34ADA2D99" Ref="JP2"  Part="1" 
AR Path="/773F65F14ADA2D99" Ref="JP2"  Part="1" 
AR Path="/773F8EB44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23C9F04ADA2D99" Ref="JP2"  Part="1" 
AR Path="/24ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23BC884ADA2D99" Ref="JP2"  Part="1" 
AR Path="/DC0C124ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23C34C4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23CBC44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23C6504ADA2D99" Ref="JP2"  Part="1" 
AR Path="/39803EA4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/FFFFFFFF4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/D058A04ADA2D99" Ref="JP2"  Part="1" 
AR Path="/1607D44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23D70C4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/2104ADA2D99" Ref="JP2"  Part="1" 
AR Path="/F4BF4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/262F604ADA2D99" Ref="JP2"  Part="1" 
AR Path="/384ADA2D99" Ref="JP2"  Part="1" 
F 0 "JP2" H 19100 6050 60  0000 C CNN
F 1 "JUMPER" H 19100 5820 40  0000 C CNN
F 2 "PIN_ARRAY_2X1" H 19100 5900 60  0001 C CNN
	1    19100 5900
	1    0    0    -1  
$EndComp
Text Label 18250 5900 0    60   ~ 0
GND
Text Label 18250 2600 0    60   ~ 0
GND
$Comp
L JUMPER JP?
U 1 1 4ADA2D5E
P 19100 2600
AR Path="/23D9B04ADA2D5E" Ref="JP?"  Part="1" 
AR Path="/394433324ADA2D5E" Ref="JP?"  Part="1" 
AR Path="/4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/94ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/755D912A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/5AD7153D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/6FE901F74ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D6CC0004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/6FF0DD404ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/14ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/FFFFFFF04ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FEFFFFF4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23C2344ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/DCBAABCD4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D7E00004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D8EA0004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/5AD746F64ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4027EF1A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402A6F1A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4029BBE74ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402A224D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23D8D44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402A55814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4026224D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D6220004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D9720004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/2600004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402C3BE74ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402955814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/40293BE74ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402555814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FE88B434ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23D2034ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402988B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FED58104ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402688B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FE441894ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/B84ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3DA360004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4029D5814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/40256F1A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/40286F1A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402A08B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402588B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/A84ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FE224DD4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402C08B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402BEF1A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402C55814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/40273BE74ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402908B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D5A40004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402755814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4030AAC04ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4031DDF34ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4032778D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/403091264ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/403051264ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4032F78D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/403251264ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4032AAC04ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4030D1264ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4031778D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FEA24DD4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/6FE934E34ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/773F65F14ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/773F8EB44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23C9F04ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/24ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23BC884ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/DC0C124ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23C34C4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23CBC44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23C6504ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/39803EA4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/FFFFFFFF4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/D058A04ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/1607D44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23D70C4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/2104ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/F4BF4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/262F604ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/384ADA2D5E" Ref="JP1"  Part="1" 
F 0 "JP1" H 19100 2750 60  0000 C CNN
F 1 "JUMPER" H 19100 2520 40  0000 C CNN
F 2 "PIN_ARRAY_2X1" H 19100 2600 60  0001 C CNN
	1    19100 2600
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 4AD0EE1B
P 12300 14350
AR Path="/7A4AD0EE1B" Ref="C?"  Part="1" 
AR Path="/23D1104AD0EE1B" Ref="C16"  Part="1" 
AR Path="/314433324AD0EE1B" Ref="C16"  Part="1" 
AR Path="/5AD7153D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/DCBAABCD4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/6FE901F74AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402988B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/94AD0EE1B" Ref="C16"  Part="1" 
AR Path="/755D912A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D6CC0004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/6FF0DD404AD0EE1B" Ref="C16"  Part="1" 
AR Path="/14AD0EE1B" Ref="C16"  Part="1" 
AR Path="/FFFFFFF04AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FEFFFFF4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23C2344AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D7E00004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D8EA0004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/5AD746F64AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4027EF1A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402A6F1A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4029BBE74AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402A224D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23D8D44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402A55814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4026224D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D6220004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D9720004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/2600004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402C3BE74AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402955814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/40293BE74AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402555814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FE88B434AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23D2034AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FED58104AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402688B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FE441894AD0EE1B" Ref="C16"  Part="1" 
AR Path="/B84AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3DA360004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4029D5814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/40256F1A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/40286F1A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402A08B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402588B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/A84AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FE224DD4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402C08B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402BEF1A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402C55814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/40273BE74AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402908B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D5A40004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402755814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4030AAC04AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4031DDF34AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4032778D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/403091264AD0EE1B" Ref="C16"  Part="1" 
AR Path="/403051264AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4032F78D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/403251264AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4032AAC04AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4030D1264AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4031778D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FEA24DD4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/6FE934E34AD0EE1B" Ref="C16"  Part="1" 
AR Path="/773F65F14AD0EE1B" Ref="C16"  Part="1" 
AR Path="/773F8EB44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23C9F04AD0EE1B" Ref="C16"  Part="1" 
AR Path="/24AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23BC884AD0EE1B" Ref="C16"  Part="1" 
AR Path="/DC0C124AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23C34C4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23CBC44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23C6504AD0EE1B" Ref="C16"  Part="1" 
AR Path="/39803EA4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/FFFFFFFF4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/D058A04AD0EE1B" Ref="C16"  Part="1" 
AR Path="/1607D44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23D70C4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/2104AD0EE1B" Ref="C16"  Part="1" 
AR Path="/F4BF4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/262F604AD0EE1B" Ref="C16"  Part="1" 
AR Path="/384AD0EE1B" Ref="C16"  Part="1" 
F 0 "C16" H 12350 14450 50  0000 L CNN
F 1 "0.1 uF" H 12350 14250 50  0000 L CNN
F 2 "C2" H 12300 14350 60  0001 C CNN
	1    12300 14350
	1    0    0    -1  
$EndComp
Text Label 19450 5700 0    60   ~ 0
+8V
$Comp
L VCC #PWR?
U 1 1 4AC79AEE
P 11850 14150
AR Path="/69698AFC4AC79AEE" Ref="#PWR?"  Part="1" 
AR Path="/393639364AC79AEE" Ref="#PWR?"  Part="1" 
AR Path="/4AC79AEE" Ref="#PWR033"  Part="1" 
AR Path="/94AC79AEE" Ref="#PWR25"  Part="1" 
AR Path="/25E4AC79AEE" Ref="#PWR036"  Part="1" 
AR Path="/6FF0DD404AC79AEE" Ref="#PWR034"  Part="1" 
AR Path="/DCBAABCD4AC79AEE" Ref="#PWR026"  Part="1" 
AR Path="/755D912A4AC79AEE" Ref="#PWR014"  Part="1" 
AR Path="/6FE901F74AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/3FE08B434AC79AEE" Ref="#PWR01"  Part="1" 
AR Path="/3DA360004AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/5AD7153D4AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/A4AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/23D9304AC79AEE" Ref="#PWR01"  Part="1" 
AR Path="/3FEFFFFF4AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/23D8D44AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/3D8EA0004AC79AEE" Ref="#PWR026"  Part="1" 
AR Path="/402B08B44AC79AEE" Ref="#PWR01"  Part="1" 
AR Path="/4026A24D4AC79AEE" Ref="#PWR01"  Part="1" 
AR Path="/23C7004AC79AEE" Ref="#PWR012"  Part="1" 
AR Path="/3FE88B434AC79AEE" Ref="#PWR028"  Part="1" 
AR Path="/3D7E00004AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/402988B44AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/3D6CC0004AC79AEE" Ref="#PWR019"  Part="1" 
AR Path="/14AC79AEE" Ref="#PWR039"  Part="1" 
AR Path="/363030314AC79AEE" Ref="#PWR036"  Part="1" 
AR Path="/FFFFFFF04AC79AEE" Ref="#PWR21"  Part="1" 
AR Path="/23C2344AC79AEE" Ref="#PWR023"  Part="1" 
AR Path="/5AD746F64AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/4027EF1A4AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/402A6F1A4AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/4029BBE74AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/402A224D4AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/402A55814AC79AEE" Ref="#PWR023"  Part="1" 
AR Path="/4026224D4AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/3D6220004AC79AEE" Ref="#PWR023"  Part="1" 
AR Path="/3D9720004AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/2600004AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/402C3BE74AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/402955814AC79AEE" Ref="#PWR018"  Part="1" 
AR Path="/40293BE74AC79AEE" Ref="#PWR022"  Part="1" 
AR Path="/402555814AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/23D2034AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/3FED58104AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/402688B44AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/3FE441894AC79AEE" Ref="#PWR012"  Part="1" 
AR Path="/B84AC79AEE" Ref="#PWR12"  Part="1" 
AR Path="/4029D5814AC79AEE" Ref="#PWR012"  Part="1" 
AR Path="/40256F1A4AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/40286F1A4AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/402A08B44AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/402588B44AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/A84AC79AEE" Ref="#PWR24"  Part="1" 
AR Path="/3FE224DD4AC79AEE" Ref="#PWR018"  Part="1" 
AR Path="/402C08B44AC79AEE" Ref="#PWR019"  Part="1" 
AR Path="/314AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/402BEF1A4AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/402C55814AC79AEE" Ref="#PWR025"  Part="1" 
AR Path="/40273BE74AC79AEE" Ref="#PWR026"  Part="1" 
AR Path="/402908B44AC79AEE" Ref="#PWR022"  Part="1" 
AR Path="/3D5A40004AC79AEE" Ref="#PWR019"  Part="1" 
AR Path="/402755814AC79AEE" Ref="#PWR027"  Part="1" 
AR Path="/4030AAC04AC79AEE" Ref="#PWR027"  Part="1" 
AR Path="/4031DDF34AC79AEE" Ref="#PWR028"  Part="1" 
AR Path="/4032778D4AC79AEE" Ref="#PWR034"  Part="1" 
AR Path="/403091264AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/403051264AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/4032F78D4AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/403251264AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/4032AAC04AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/4030D1264AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/4031778D4AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/3FEA24DD4AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/6FE934E34AC79AEE" Ref="#PWR036"  Part="1" 
AR Path="/773F65F14AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/773F8EB44AC79AEE" Ref="#PWR033"  Part="1" 
AR Path="/23C9F04AC79AEE" Ref="#PWR30"  Part="1" 
AR Path="/23BC884AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/DC0C124AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/6684D64AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/23C34C4AC79AEE" Ref="#PWR038"  Part="1" 
AR Path="/23CBC44AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/39803EA4AC79AEE" Ref="#PWR038"  Part="1" 
AR Path="/FFFFFFFF4AC79AEE" Ref="#PWR038"  Part="1" 
AR Path="/D058A04AC79AEE" Ref="#PWR039"  Part="1" 
AR Path="/1607D44AC79AEE" Ref="#PWR039"  Part="1" 
AR Path="/24AC79AEE" Ref="#PWR039"  Part="1" 
AR Path="/23D70C4AC79AEE" Ref="#PWR039"  Part="1" 
AR Path="/2104AC79AEE" Ref="#PWR039"  Part="1" 
AR Path="/23C6504AC79AEE" Ref="#PWR033"  Part="1" 
AR Path="/F4BF4AC79AEE" Ref="#PWR033"  Part="1" 
AR Path="/262F604AC79AEE" Ref="#PWR033"  Part="1" 
AR Path="/384AC79AEE" Ref="#PWR034"  Part="1" 
F 0 "#PWR033" H 11850 14250 30  0001 C CNN
F 1 "VCC" H 11850 14250 30  0000 C CNN
	1    11850 14150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4AC79AE6
P 12300 14600
AR Path="/69698AFC4AC79AE6" Ref="#PWR?"  Part="1" 
AR Path="/393639364AC79AE6" Ref="#PWR?"  Part="1" 
AR Path="/4AC79AE6" Ref="#PWR034"  Part="1" 
AR Path="/94AC79AE6" Ref="#PWR28"  Part="1" 
AR Path="/25E4AC79AE6" Ref="#PWR037"  Part="1" 
AR Path="/6FF0DD404AC79AE6" Ref="#PWR035"  Part="1" 
AR Path="/DCBAABCD4AC79AE6" Ref="#PWR027"  Part="1" 
AR Path="/755D912A4AC79AE6" Ref="#PWR015"  Part="1" 
AR Path="/6FE901F74AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/3FE08B434AC79AE6" Ref="#PWR02"  Part="1" 
AR Path="/3DA360004AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/5AD7153D4AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/A4AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/23D9304AC79AE6" Ref="#PWR02"  Part="1" 
AR Path="/3FEFFFFF4AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/23D8D44AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/3D8EA0004AC79AE6" Ref="#PWR027"  Part="1" 
AR Path="/402B08B44AC79AE6" Ref="#PWR02"  Part="1" 
AR Path="/4026A24D4AC79AE6" Ref="#PWR02"  Part="1" 
AR Path="/23C7004AC79AE6" Ref="#PWR013"  Part="1" 
AR Path="/3FE88B434AC79AE6" Ref="#PWR029"  Part="1" 
AR Path="/3D7E00004AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/402988B44AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/3D6CC0004AC79AE6" Ref="#PWR020"  Part="1" 
AR Path="/14AC79AE6" Ref="#PWR040"  Part="1" 
AR Path="/363030314AC79AE6" Ref="#PWR037"  Part="1" 
AR Path="/FFFFFFF04AC79AE6" Ref="#PWR24"  Part="1" 
AR Path="/23C2344AC79AE6" Ref="#PWR024"  Part="1" 
AR Path="/5AD746F64AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/4027EF1A4AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/402A6F1A4AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/4029BBE74AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/402A224D4AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/402A55814AC79AE6" Ref="#PWR024"  Part="1" 
AR Path="/4026224D4AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/3D6220004AC79AE6" Ref="#PWR024"  Part="1" 
AR Path="/3D9720004AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/2600004AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/402C3BE74AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/402955814AC79AE6" Ref="#PWR019"  Part="1" 
AR Path="/40293BE74AC79AE6" Ref="#PWR023"  Part="1" 
AR Path="/402555814AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/23D2034AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/3FED58104AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/402688B44AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/3FE441894AC79AE6" Ref="#PWR013"  Part="1" 
AR Path="/B84AC79AE6" Ref="#PWR13"  Part="1" 
AR Path="/4029D5814AC79AE6" Ref="#PWR013"  Part="1" 
AR Path="/40256F1A4AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/40286F1A4AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/402A08B44AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/402588B44AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/A84AC79AE6" Ref="#PWR27"  Part="1" 
AR Path="/3FE224DD4AC79AE6" Ref="#PWR019"  Part="1" 
AR Path="/402C08B44AC79AE6" Ref="#PWR020"  Part="1" 
AR Path="/314AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/402BEF1A4AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/402C55814AC79AE6" Ref="#PWR026"  Part="1" 
AR Path="/40273BE74AC79AE6" Ref="#PWR027"  Part="1" 
AR Path="/402908B44AC79AE6" Ref="#PWR023"  Part="1" 
AR Path="/3D5A40004AC79AE6" Ref="#PWR020"  Part="1" 
AR Path="/402755814AC79AE6" Ref="#PWR028"  Part="1" 
AR Path="/4030AAC04AC79AE6" Ref="#PWR028"  Part="1" 
AR Path="/4031DDF34AC79AE6" Ref="#PWR029"  Part="1" 
AR Path="/74AC79AE6" Ref="#PWR20"  Part="1" 
AR Path="/4032778D4AC79AE6" Ref="#PWR035"  Part="1" 
AR Path="/403091264AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/403051264AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/4032F78D4AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/403251264AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/4032AAC04AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/4030D1264AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/4031778D4AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/3FEA24DD4AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/6FE934E34AC79AE6" Ref="#PWR037"  Part="1" 
AR Path="/773F65F14AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/773F8EB44AC79AE6" Ref="#PWR034"  Part="1" 
AR Path="/23C9F04AC79AE6" Ref="#PWR33"  Part="1" 
AR Path="/23BC884AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/DC0C124AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/6684D64AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/23C34C4AC79AE6" Ref="#PWR039"  Part="1" 
AR Path="/23CBC44AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/39803EA4AC79AE6" Ref="#PWR039"  Part="1" 
AR Path="/FFFFFFFF4AC79AE6" Ref="#PWR039"  Part="1" 
AR Path="/D058A04AC79AE6" Ref="#PWR040"  Part="1" 
AR Path="/1607D44AC79AE6" Ref="#PWR040"  Part="1" 
AR Path="/24AC79AE6" Ref="#PWR040"  Part="1" 
AR Path="/23D70C4AC79AE6" Ref="#PWR040"  Part="1" 
AR Path="/2104AC79AE6" Ref="#PWR040"  Part="1" 
AR Path="/23C6504AC79AE6" Ref="#PWR034"  Part="1" 
AR Path="/F4BF4AC79AE6" Ref="#PWR034"  Part="1" 
AR Path="/262F604AC79AE6" Ref="#PWR034"  Part="1" 
AR Path="/384AC79AE6" Ref="#PWR035"  Part="1" 
F 0 "#PWR034" H 12300 14600 30  0001 C CNN
F 1 "GND" H 12300 14530 30  0001 C CNN
	1    12300 14600
	1    0    0    -1  
$EndComp
$Comp
L C C15
U 1 1 4AC79AB8
P 11850 14350
AR Path="/4AC79AB8" Ref="C15"  Part="1" 
AR Path="/94AC79AB8" Ref="C15"  Part="1" 
AR Path="/14AC79AB8" Ref="C15"  Part="1" 
AR Path="/6FF0DD404AC79AB8" Ref="C15"  Part="1" 
AR Path="/DCBAABCD4AC79AB8" Ref="C15"  Part="1" 
AR Path="/755D912A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/6FE901F74AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FE08B434AC79AB8" Ref="C15"  Part="1" 
AR Path="/3DA360004AC79AB8" Ref="C15"  Part="1" 
AR Path="/5AD7153D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/23D9304AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FEFFFFF4AC79AB8" Ref="C15"  Part="1" 
AR Path="/23D8D44AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D8EA0004AC79AB8" Ref="C15"  Part="1" 
AR Path="/402B08B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/4026A24D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FE88B434AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D7E00004AC79AB8" Ref="C15"  Part="1" 
AR Path="/402988B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D6CC0004AC79AB8" Ref="C15"  Part="1" 
AR Path="/FFFFFFF04AC79AB8" Ref="C15"  Part="1" 
AR Path="/23C2344AC79AB8" Ref="C15"  Part="1" 
AR Path="/5AD746F64AC79AB8" Ref="C15"  Part="1" 
AR Path="/4027EF1A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/402A6F1A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/4029BBE74AC79AB8" Ref="C15"  Part="1" 
AR Path="/402A224D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/402A55814AC79AB8" Ref="C15"  Part="1" 
AR Path="/4026224D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D6220004AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D9720004AC79AB8" Ref="C15"  Part="1" 
AR Path="/2600004AC79AB8" Ref="C15"  Part="1" 
AR Path="/402C3BE74AC79AB8" Ref="C15"  Part="1" 
AR Path="/402955814AC79AB8" Ref="C15"  Part="1" 
AR Path="/40293BE74AC79AB8" Ref="C15"  Part="1" 
AR Path="/402555814AC79AB8" Ref="C15"  Part="1" 
AR Path="/23D2034AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FED58104AC79AB8" Ref="C15"  Part="1" 
AR Path="/402688B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FE441894AC79AB8" Ref="C15"  Part="1" 
AR Path="/B84AC79AB8" Ref="C15"  Part="1" 
AR Path="/4029D5814AC79AB8" Ref="C15"  Part="1" 
AR Path="/40256F1A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/40286F1A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/402A08B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/402588B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/A84AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FE224DD4AC79AB8" Ref="C15"  Part="1" 
AR Path="/402C08B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/402BEF1A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/402C55814AC79AB8" Ref="C15"  Part="1" 
AR Path="/40273BE74AC79AB8" Ref="C15"  Part="1" 
AR Path="/402908B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D5A40004AC79AB8" Ref="C15"  Part="1" 
AR Path="/402755814AC79AB8" Ref="C15"  Part="1" 
AR Path="/4030AAC04AC79AB8" Ref="C15"  Part="1" 
AR Path="/4031DDF34AC79AB8" Ref="C15"  Part="1" 
AR Path="/4032778D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/403091264AC79AB8" Ref="C15"  Part="1" 
AR Path="/403051264AC79AB8" Ref="C15"  Part="1" 
AR Path="/4032F78D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/403251264AC79AB8" Ref="C15"  Part="1" 
AR Path="/4032AAC04AC79AB8" Ref="C15"  Part="1" 
AR Path="/4030D1264AC79AB8" Ref="C15"  Part="1" 
AR Path="/4031778D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FEA24DD4AC79AB8" Ref="C15"  Part="1" 
AR Path="/6FE934E34AC79AB8" Ref="C15"  Part="1" 
AR Path="/773F65F14AC79AB8" Ref="C15"  Part="1" 
AR Path="/773F8EB44AC79AB8" Ref="C15"  Part="1" 
AR Path="/23C9F04AC79AB8" Ref="C15"  Part="1" 
AR Path="/24AC79AB8" Ref="C15"  Part="1" 
AR Path="/23BC884AC79AB8" Ref="C15"  Part="1" 
AR Path="/DC0C124AC79AB8" Ref="C15"  Part="1" 
AR Path="/23C34C4AC79AB8" Ref="C15"  Part="1" 
AR Path="/23CBC44AC79AB8" Ref="C15"  Part="1" 
AR Path="/23C6504AC79AB8" Ref="C15"  Part="1" 
AR Path="/39803EA4AC79AB8" Ref="C15"  Part="1" 
AR Path="/FFFFFFFF4AC79AB8" Ref="C15"  Part="1" 
AR Path="/D058A04AC79AB8" Ref="C15"  Part="1" 
AR Path="/1607D44AC79AB8" Ref="C15"  Part="1" 
AR Path="/23D70C4AC79AB8" Ref="C15"  Part="1" 
AR Path="/2104AC79AB8" Ref="C15"  Part="1" 
AR Path="/F4BF4AC79AB8" Ref="C15"  Part="1" 
AR Path="/262F604AC79AB8" Ref="C15"  Part="1" 
AR Path="/384AC79AB8" Ref="C15"  Part="1" 
F 0 "C15" H 11900 14450 50  0000 L CNN
F 1 "0.1 uF" H 11900 14250 50  0000 L CNN
F 2 "C2" H 11850 14350 60  0001 C CNN
	1    11850 14350
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 4AC79AA4
P 14550 15600
AR Path="/4AC79AA4" Ref="C11"  Part="1" 
AR Path="/94AC79AA4" Ref="C11"  Part="1" 
AR Path="/14AC79AA4" Ref="C11"  Part="1" 
AR Path="/6FF0DD404AC79AA4" Ref="C11"  Part="1" 
AR Path="/755D912A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/6FE901F74AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FE08B434AC79AA4" Ref="C11"  Part="1" 
AR Path="/3DA360004AC79AA4" Ref="C11"  Part="1" 
AR Path="/5AD7153D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/23D9304AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FEFFFFF4AC79AA4" Ref="C11"  Part="1" 
AR Path="/23D8D44AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D8EA0004AC79AA4" Ref="C11"  Part="1" 
AR Path="/402B08B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/4026A24D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FE88B434AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D7E00004AC79AA4" Ref="C11"  Part="1" 
AR Path="/402988B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D6CC0004AC79AA4" Ref="C11"  Part="1" 
AR Path="/FFFFFFF04AC79AA4" Ref="C11"  Part="1" 
AR Path="/23C2344AC79AA4" Ref="C11"  Part="1" 
AR Path="/DCBAABCD4AC79AA4" Ref="C11"  Part="1" 
AR Path="/5AD746F64AC79AA4" Ref="C11"  Part="1" 
AR Path="/4027EF1A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/402A6F1A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/4029BBE74AC79AA4" Ref="C11"  Part="1" 
AR Path="/402A224D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/402A55814AC79AA4" Ref="C11"  Part="1" 
AR Path="/4026224D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D6220004AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D9720004AC79AA4" Ref="C11"  Part="1" 
AR Path="/2600004AC79AA4" Ref="C11"  Part="1" 
AR Path="/402C3BE74AC79AA4" Ref="C11"  Part="1" 
AR Path="/402955814AC79AA4" Ref="C11"  Part="1" 
AR Path="/40293BE74AC79AA4" Ref="C11"  Part="1" 
AR Path="/402555814AC79AA4" Ref="C11"  Part="1" 
AR Path="/23D2034AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FED58104AC79AA4" Ref="C11"  Part="1" 
AR Path="/402688B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FE441894AC79AA4" Ref="C11"  Part="1" 
AR Path="/B84AC79AA4" Ref="C11"  Part="1" 
AR Path="/4029D5814AC79AA4" Ref="C11"  Part="1" 
AR Path="/40256F1A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/40286F1A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/402A08B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/402588B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/A84AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FE224DD4AC79AA4" Ref="C11"  Part="1" 
AR Path="/402C08B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/402BEF1A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/402C55814AC79AA4" Ref="C11"  Part="1" 
AR Path="/40273BE74AC79AA4" Ref="C11"  Part="1" 
AR Path="/402908B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D5A40004AC79AA4" Ref="C11"  Part="1" 
AR Path="/402755814AC79AA4" Ref="C11"  Part="1" 
AR Path="/4030AAC04AC79AA4" Ref="C11"  Part="1" 
AR Path="/4031DDF34AC79AA4" Ref="C11"  Part="1" 
AR Path="/4032778D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/403091264AC79AA4" Ref="C11"  Part="1" 
AR Path="/403051264AC79AA4" Ref="C11"  Part="1" 
AR Path="/4032F78D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/403251264AC79AA4" Ref="C11"  Part="1" 
AR Path="/4032AAC04AC79AA4" Ref="C11"  Part="1" 
AR Path="/4030D1264AC79AA4" Ref="C11"  Part="1" 
AR Path="/4031778D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FEA24DD4AC79AA4" Ref="C11"  Part="1" 
AR Path="/6FE934E34AC79AA4" Ref="C11"  Part="1" 
AR Path="/773F65F14AC79AA4" Ref="C11"  Part="1" 
AR Path="/773F8EB44AC79AA4" Ref="C11"  Part="1" 
AR Path="/23C9F04AC79AA4" Ref="C11"  Part="1" 
AR Path="/24AC79AA4" Ref="C11"  Part="1" 
AR Path="/23BC884AC79AA4" Ref="C11"  Part="1" 
AR Path="/DC0C124AC79AA4" Ref="C11"  Part="1" 
AR Path="/23C34C4AC79AA4" Ref="C11"  Part="1" 
AR Path="/23CBC44AC79AA4" Ref="C11"  Part="1" 
AR Path="/23C6504AC79AA4" Ref="C11"  Part="1" 
AR Path="/39803EA4AC79AA4" Ref="C11"  Part="1" 
AR Path="/FFFFFFFF4AC79AA4" Ref="C11"  Part="1" 
AR Path="/D058A04AC79AA4" Ref="C11"  Part="1" 
AR Path="/1607D44AC79AA4" Ref="C11"  Part="1" 
AR Path="/23D70C4AC79AA4" Ref="C11"  Part="1" 
AR Path="/2104AC79AA4" Ref="C11"  Part="1" 
AR Path="/F4BF4AC79AA4" Ref="C11"  Part="1" 
AR Path="/262F604AC79AA4" Ref="C11"  Part="1" 
AR Path="/384AC79AA4" Ref="C11"  Part="1" 
F 0 "C11" H 14600 15700 50  0000 L CNN
F 1 "0.1 uF" H 14600 15500 50  0000 L CNN
F 2 "C2" H 14550 15600 60  0001 C CNN
	1    14550 15600
	1    0    0    -1  
$EndComp
$Comp
L C C12
U 1 1 4AC79AA3
P 15000 15600
AR Path="/4AC79AA3" Ref="C12"  Part="1" 
AR Path="/94AC79AA3" Ref="C12"  Part="1" 
AR Path="/14AC79AA3" Ref="C12"  Part="1" 
AR Path="/6FF0DD404AC79AA3" Ref="C12"  Part="1" 
AR Path="/755D912A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/6FE901F74AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FE08B434AC79AA3" Ref="C12"  Part="1" 
AR Path="/3DA360004AC79AA3" Ref="C12"  Part="1" 
AR Path="/5AD7153D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/23D9304AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FEFFFFF4AC79AA3" Ref="C12"  Part="1" 
AR Path="/23D8D44AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D8EA0004AC79AA3" Ref="C12"  Part="1" 
AR Path="/402B08B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/4026A24D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FE88B434AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D7E00004AC79AA3" Ref="C12"  Part="1" 
AR Path="/402988B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D6CC0004AC79AA3" Ref="C12"  Part="1" 
AR Path="/FFFFFFF04AC79AA3" Ref="C12"  Part="1" 
AR Path="/23C2344AC79AA3" Ref="C12"  Part="1" 
AR Path="/DCBAABCD4AC79AA3" Ref="C12"  Part="1" 
AR Path="/5AD746F64AC79AA3" Ref="C12"  Part="1" 
AR Path="/4027EF1A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/402A6F1A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/4029BBE74AC79AA3" Ref="C12"  Part="1" 
AR Path="/402A224D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/402A55814AC79AA3" Ref="C12"  Part="1" 
AR Path="/4026224D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D6220004AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D9720004AC79AA3" Ref="C12"  Part="1" 
AR Path="/2600004AC79AA3" Ref="C12"  Part="1" 
AR Path="/402C3BE74AC79AA3" Ref="C12"  Part="1" 
AR Path="/402955814AC79AA3" Ref="C12"  Part="1" 
AR Path="/40293BE74AC79AA3" Ref="C12"  Part="1" 
AR Path="/402555814AC79AA3" Ref="C12"  Part="1" 
AR Path="/23D2034AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FED58104AC79AA3" Ref="C12"  Part="1" 
AR Path="/402688B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FE441894AC79AA3" Ref="C12"  Part="1" 
AR Path="/B84AC79AA3" Ref="C12"  Part="1" 
AR Path="/4029D5814AC79AA3" Ref="C12"  Part="1" 
AR Path="/40256F1A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/40286F1A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/402A08B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/402588B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/A84AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FE224DD4AC79AA3" Ref="C12"  Part="1" 
AR Path="/402C08B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/402BEF1A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/402C55814AC79AA3" Ref="C12"  Part="1" 
AR Path="/40273BE74AC79AA3" Ref="C12"  Part="1" 
AR Path="/402908B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D5A40004AC79AA3" Ref="C12"  Part="1" 
AR Path="/402755814AC79AA3" Ref="C12"  Part="1" 
AR Path="/4030AAC04AC79AA3" Ref="C12"  Part="1" 
AR Path="/4031DDF34AC79AA3" Ref="C12"  Part="1" 
AR Path="/4032778D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/403091264AC79AA3" Ref="C12"  Part="1" 
AR Path="/403051264AC79AA3" Ref="C12"  Part="1" 
AR Path="/4032F78D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/403251264AC79AA3" Ref="C12"  Part="1" 
AR Path="/4032AAC04AC79AA3" Ref="C12"  Part="1" 
AR Path="/4030D1264AC79AA3" Ref="C12"  Part="1" 
AR Path="/4031778D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FEA24DD4AC79AA3" Ref="C12"  Part="1" 
AR Path="/6FE934E34AC79AA3" Ref="C12"  Part="1" 
AR Path="/773F65F14AC79AA3" Ref="C12"  Part="1" 
AR Path="/773F8EB44AC79AA3" Ref="C12"  Part="1" 
AR Path="/23C9F04AC79AA3" Ref="C12"  Part="1" 
AR Path="/24AC79AA3" Ref="C12"  Part="1" 
AR Path="/23BC884AC79AA3" Ref="C12"  Part="1" 
AR Path="/DC0C124AC79AA3" Ref="C12"  Part="1" 
AR Path="/23C34C4AC79AA3" Ref="C12"  Part="1" 
AR Path="/23CBC44AC79AA3" Ref="C12"  Part="1" 
AR Path="/23C6504AC79AA3" Ref="C12"  Part="1" 
AR Path="/39803EA4AC79AA3" Ref="C12"  Part="1" 
AR Path="/FFFFFFFF4AC79AA3" Ref="C12"  Part="1" 
AR Path="/D058A04AC79AA3" Ref="C12"  Part="1" 
AR Path="/1607D44AC79AA3" Ref="C12"  Part="1" 
AR Path="/23D70C4AC79AA3" Ref="C12"  Part="1" 
AR Path="/2104AC79AA3" Ref="C12"  Part="1" 
AR Path="/F4BF4AC79AA3" Ref="C12"  Part="1" 
AR Path="/262F604AC79AA3" Ref="C12"  Part="1" 
AR Path="/384AC79AA3" Ref="C12"  Part="1" 
F 0 "C12" H 15050 15700 50  0000 L CNN
F 1 "0.1 uF" H 15050 15500 50  0000 L CNN
F 2 "C2" H 15000 15600 60  0001 C CNN
	1    15000 15600
	1    0    0    -1  
$EndComp
$Comp
L C C14
U 1 1 4AC79AA2
P 13650 14950
AR Path="/4AC79AA2" Ref="C14"  Part="1" 
AR Path="/94AC79AA2" Ref="C14"  Part="1" 
AR Path="/14AC79AA2" Ref="C14"  Part="1" 
AR Path="/6FF0DD404AC79AA2" Ref="C14"  Part="1" 
AR Path="/755D912A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/6FE901F74AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FE08B434AC79AA2" Ref="C14"  Part="1" 
AR Path="/3DA360004AC79AA2" Ref="C14"  Part="1" 
AR Path="/5AD7153D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/23D9304AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FEFFFFF4AC79AA2" Ref="C14"  Part="1" 
AR Path="/23D8D44AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D8EA0004AC79AA2" Ref="C14"  Part="1" 
AR Path="/402B08B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/4026A24D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FE88B434AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D7E00004AC79AA2" Ref="C14"  Part="1" 
AR Path="/402988B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D6CC0004AC79AA2" Ref="C14"  Part="1" 
AR Path="/FFFFFFF04AC79AA2" Ref="C14"  Part="1" 
AR Path="/23C2344AC79AA2" Ref="C14"  Part="1" 
AR Path="/DCBAABCD4AC79AA2" Ref="C14"  Part="1" 
AR Path="/5AD746F64AC79AA2" Ref="C14"  Part="1" 
AR Path="/4027EF1A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/402A6F1A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/4029BBE74AC79AA2" Ref="C14"  Part="1" 
AR Path="/402A224D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/402A55814AC79AA2" Ref="C14"  Part="1" 
AR Path="/4026224D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D6220004AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D9720004AC79AA2" Ref="C14"  Part="1" 
AR Path="/2600004AC79AA2" Ref="C14"  Part="1" 
AR Path="/402C3BE74AC79AA2" Ref="C14"  Part="1" 
AR Path="/402955814AC79AA2" Ref="C14"  Part="1" 
AR Path="/40293BE74AC79AA2" Ref="C14"  Part="1" 
AR Path="/402555814AC79AA2" Ref="C14"  Part="1" 
AR Path="/23D2034AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FED58104AC79AA2" Ref="C14"  Part="1" 
AR Path="/402688B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FE441894AC79AA2" Ref="C14"  Part="1" 
AR Path="/B84AC79AA2" Ref="C14"  Part="1" 
AR Path="/4029D5814AC79AA2" Ref="C14"  Part="1" 
AR Path="/40256F1A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/40286F1A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/402A08B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/402588B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/A84AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FE224DD4AC79AA2" Ref="C14"  Part="1" 
AR Path="/402C08B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/402BEF1A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/402C55814AC79AA2" Ref="C14"  Part="1" 
AR Path="/40273BE74AC79AA2" Ref="C14"  Part="1" 
AR Path="/402908B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D5A40004AC79AA2" Ref="C14"  Part="1" 
AR Path="/402755814AC79AA2" Ref="C14"  Part="1" 
AR Path="/4030AAC04AC79AA2" Ref="C14"  Part="1" 
AR Path="/4031DDF34AC79AA2" Ref="C14"  Part="1" 
AR Path="/4032778D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/403091264AC79AA2" Ref="C14"  Part="1" 
AR Path="/403051264AC79AA2" Ref="C14"  Part="1" 
AR Path="/4032F78D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/403251264AC79AA2" Ref="C14"  Part="1" 
AR Path="/4032AAC04AC79AA2" Ref="C14"  Part="1" 
AR Path="/4030D1264AC79AA2" Ref="C14"  Part="1" 
AR Path="/4031778D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FEA24DD4AC79AA2" Ref="C14"  Part="1" 
AR Path="/6FE934E34AC79AA2" Ref="C14"  Part="1" 
AR Path="/773F65F14AC79AA2" Ref="C14"  Part="1" 
AR Path="/773F8EB44AC79AA2" Ref="C14"  Part="1" 
AR Path="/23C9F04AC79AA2" Ref="C14"  Part="1" 
AR Path="/24AC79AA2" Ref="C14"  Part="1" 
AR Path="/23BC884AC79AA2" Ref="C14"  Part="1" 
AR Path="/DC0C124AC79AA2" Ref="C14"  Part="1" 
AR Path="/23C34C4AC79AA2" Ref="C14"  Part="1" 
AR Path="/23CBC44AC79AA2" Ref="C14"  Part="1" 
AR Path="/23C6504AC79AA2" Ref="C14"  Part="1" 
AR Path="/39803EA4AC79AA2" Ref="C14"  Part="1" 
AR Path="/FFFFFFFF4AC79AA2" Ref="C14"  Part="1" 
AR Path="/D058A04AC79AA2" Ref="C14"  Part="1" 
AR Path="/1607D44AC79AA2" Ref="C14"  Part="1" 
AR Path="/23D70C4AC79AA2" Ref="C14"  Part="1" 
AR Path="/2104AC79AA2" Ref="C14"  Part="1" 
AR Path="/F4BF4AC79AA2" Ref="C14"  Part="1" 
AR Path="/262F604AC79AA2" Ref="C14"  Part="1" 
AR Path="/384AC79AA2" Ref="C14"  Part="1" 
F 0 "C14" H 13700 15050 50  0000 L CNN
F 1 "0.1 uF" H 13700 14850 50  0000 L CNN
F 2 "C2" H 13650 14950 60  0001 C CNN
	1    13650 14950
	1    0    0    -1  
$EndComp
$Comp
L C C13
U 1 1 4AC79AA1
P 15450 15600
AR Path="/4AC79AA1" Ref="C13"  Part="1" 
AR Path="/94AC79AA1" Ref="C13"  Part="1" 
AR Path="/14AC79AA1" Ref="C13"  Part="1" 
AR Path="/6FF0DD404AC79AA1" Ref="C13"  Part="1" 
AR Path="/755D912A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/6FE901F74AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FE08B434AC79AA1" Ref="C13"  Part="1" 
AR Path="/3DA360004AC79AA1" Ref="C13"  Part="1" 
AR Path="/5AD7153D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/23D9304AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FEFFFFF4AC79AA1" Ref="C13"  Part="1" 
AR Path="/23D8D44AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D8EA0004AC79AA1" Ref="C13"  Part="1" 
AR Path="/402B08B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/4026A24D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FE88B434AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D7E00004AC79AA1" Ref="C13"  Part="1" 
AR Path="/402988B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D6CC0004AC79AA1" Ref="C13"  Part="1" 
AR Path="/FFFFFFF04AC79AA1" Ref="C13"  Part="1" 
AR Path="/23C2344AC79AA1" Ref="C13"  Part="1" 
AR Path="/DCBAABCD4AC79AA1" Ref="C13"  Part="1" 
AR Path="/5AD746F64AC79AA1" Ref="C13"  Part="1" 
AR Path="/4027EF1A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/402A6F1A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/4029BBE74AC79AA1" Ref="C13"  Part="1" 
AR Path="/402A224D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/402A55814AC79AA1" Ref="C13"  Part="1" 
AR Path="/4026224D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D6220004AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D9720004AC79AA1" Ref="C13"  Part="1" 
AR Path="/2600004AC79AA1" Ref="C13"  Part="1" 
AR Path="/402C3BE74AC79AA1" Ref="C13"  Part="1" 
AR Path="/402955814AC79AA1" Ref="C13"  Part="1" 
AR Path="/40293BE74AC79AA1" Ref="C13"  Part="1" 
AR Path="/402555814AC79AA1" Ref="C13"  Part="1" 
AR Path="/23D2034AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FED58104AC79AA1" Ref="C13"  Part="1" 
AR Path="/402688B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FE441894AC79AA1" Ref="C13"  Part="1" 
AR Path="/B84AC79AA1" Ref="C13"  Part="1" 
AR Path="/4029D5814AC79AA1" Ref="C13"  Part="1" 
AR Path="/40256F1A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/40286F1A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/402A08B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/402588B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/A84AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FE224DD4AC79AA1" Ref="C13"  Part="1" 
AR Path="/402C08B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/402BEF1A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/402C55814AC79AA1" Ref="C13"  Part="1" 
AR Path="/40273BE74AC79AA1" Ref="C13"  Part="1" 
AR Path="/402908B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D5A40004AC79AA1" Ref="C13"  Part="1" 
AR Path="/402755814AC79AA1" Ref="C13"  Part="1" 
AR Path="/4030AAC04AC79AA1" Ref="C13"  Part="1" 
AR Path="/4031DDF34AC79AA1" Ref="C13"  Part="1" 
AR Path="/4032778D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/403091264AC79AA1" Ref="C13"  Part="1" 
AR Path="/403051264AC79AA1" Ref="C13"  Part="1" 
AR Path="/4032F78D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/403251264AC79AA1" Ref="C13"  Part="1" 
AR Path="/4032AAC04AC79AA1" Ref="C13"  Part="1" 
AR Path="/4030D1264AC79AA1" Ref="C13"  Part="1" 
AR Path="/4031778D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FEA24DD4AC79AA1" Ref="C13"  Part="1" 
AR Path="/6FE934E34AC79AA1" Ref="C13"  Part="1" 
AR Path="/773F65F14AC79AA1" Ref="C13"  Part="1" 
AR Path="/773F8EB44AC79AA1" Ref="C13"  Part="1" 
AR Path="/23C9F04AC79AA1" Ref="C13"  Part="1" 
AR Path="/24AC79AA1" Ref="C13"  Part="1" 
AR Path="/23BC884AC79AA1" Ref="C13"  Part="1" 
AR Path="/DC0C124AC79AA1" Ref="C13"  Part="1" 
AR Path="/23C34C4AC79AA1" Ref="C13"  Part="1" 
AR Path="/23CBC44AC79AA1" Ref="C13"  Part="1" 
AR Path="/23C6504AC79AA1" Ref="C13"  Part="1" 
AR Path="/39803EA4AC79AA1" Ref="C13"  Part="1" 
AR Path="/FFFFFFFF4AC79AA1" Ref="C13"  Part="1" 
AR Path="/D058A04AC79AA1" Ref="C13"  Part="1" 
AR Path="/1607D44AC79AA1" Ref="C13"  Part="1" 
AR Path="/23D70C4AC79AA1" Ref="C13"  Part="1" 
AR Path="/2104AC79AA1" Ref="C13"  Part="1" 
AR Path="/F4BF4AC79AA1" Ref="C13"  Part="1" 
AR Path="/262F604AC79AA1" Ref="C13"  Part="1" 
AR Path="/384AC79AA1" Ref="C13"  Part="1" 
F 0 "C13" H 15500 15700 50  0000 L CNN
F 1 "0.1 uF" H 15500 15500 50  0000 L CNN
F 2 "C2" H 15450 15600 60  0001 C CNN
	1    15450 15600
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 4AC79A7D
P 13650 15600
AR Path="/4AC79A7D" Ref="C9"  Part="1" 
AR Path="/94AC79A7D" Ref="C9"  Part="1" 
AR Path="/14AC79A7D" Ref="C9"  Part="1" 
AR Path="/6FF0DD404AC79A7D" Ref="C9"  Part="1" 
AR Path="/755D912A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/6FE901F74AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FE08B434AC79A7D" Ref="C9"  Part="1" 
AR Path="/3DA360004AC79A7D" Ref="C9"  Part="1" 
AR Path="/5AD7153D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/23D9304AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FEFFFFF4AC79A7D" Ref="C9"  Part="1" 
AR Path="/23D8D44AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D8EA0004AC79A7D" Ref="C9"  Part="1" 
AR Path="/402B08B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/4026A24D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FE88B434AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D7E00004AC79A7D" Ref="C9"  Part="1" 
AR Path="/402988B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D6CC0004AC79A7D" Ref="C9"  Part="1" 
AR Path="/FFFFFFF04AC79A7D" Ref="C9"  Part="1" 
AR Path="/23C2344AC79A7D" Ref="C9"  Part="1" 
AR Path="/DCBAABCD4AC79A7D" Ref="C9"  Part="1" 
AR Path="/5AD746F64AC79A7D" Ref="C9"  Part="1" 
AR Path="/4027EF1A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/402A6F1A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/4029BBE74AC79A7D" Ref="C9"  Part="1" 
AR Path="/402A224D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/402A55814AC79A7D" Ref="C9"  Part="1" 
AR Path="/4026224D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D6220004AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D9720004AC79A7D" Ref="C9"  Part="1" 
AR Path="/2600004AC79A7D" Ref="C9"  Part="1" 
AR Path="/402C3BE74AC79A7D" Ref="C9"  Part="1" 
AR Path="/402955814AC79A7D" Ref="C9"  Part="1" 
AR Path="/40293BE74AC79A7D" Ref="C9"  Part="1" 
AR Path="/402555814AC79A7D" Ref="C9"  Part="1" 
AR Path="/23D2034AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FED58104AC79A7D" Ref="C9"  Part="1" 
AR Path="/402688B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FE441894AC79A7D" Ref="C9"  Part="1" 
AR Path="/B84AC79A7D" Ref="C9"  Part="1" 
AR Path="/4029D5814AC79A7D" Ref="C9"  Part="1" 
AR Path="/40256F1A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/40286F1A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/402A08B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/402588B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/A84AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FE224DD4AC79A7D" Ref="C9"  Part="1" 
AR Path="/402C08B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/402BEF1A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/402C55814AC79A7D" Ref="C9"  Part="1" 
AR Path="/40273BE74AC79A7D" Ref="C9"  Part="1" 
AR Path="/402908B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D5A40004AC79A7D" Ref="C9"  Part="1" 
AR Path="/402755814AC79A7D" Ref="C9"  Part="1" 
AR Path="/4030AAC04AC79A7D" Ref="C9"  Part="1" 
AR Path="/4031DDF34AC79A7D" Ref="C9"  Part="1" 
AR Path="/4032778D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/403091264AC79A7D" Ref="C9"  Part="1" 
AR Path="/403051264AC79A7D" Ref="C9"  Part="1" 
AR Path="/4032F78D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/403251264AC79A7D" Ref="C9"  Part="1" 
AR Path="/4032AAC04AC79A7D" Ref="C9"  Part="1" 
AR Path="/4030D1264AC79A7D" Ref="C9"  Part="1" 
AR Path="/4031778D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FEA24DD4AC79A7D" Ref="C9"  Part="1" 
AR Path="/6FE934E34AC79A7D" Ref="C9"  Part="1" 
AR Path="/773F65F14AC79A7D" Ref="C9"  Part="1" 
AR Path="/773F8EB44AC79A7D" Ref="C9"  Part="1" 
AR Path="/23C9F04AC79A7D" Ref="C9"  Part="1" 
AR Path="/24AC79A7D" Ref="C9"  Part="1" 
AR Path="/23BC884AC79A7D" Ref="C9"  Part="1" 
AR Path="/DC0C124AC79A7D" Ref="C9"  Part="1" 
AR Path="/23C34C4AC79A7D" Ref="C9"  Part="1" 
AR Path="/23CBC44AC79A7D" Ref="C9"  Part="1" 
AR Path="/23C6504AC79A7D" Ref="C9"  Part="1" 
AR Path="/39803EA4AC79A7D" Ref="C9"  Part="1" 
AR Path="/FFFFFFFF4AC79A7D" Ref="C9"  Part="1" 
AR Path="/D058A04AC79A7D" Ref="C9"  Part="1" 
AR Path="/1607D44AC79A7D" Ref="C9"  Part="1" 
AR Path="/23D70C4AC79A7D" Ref="C9"  Part="1" 
AR Path="/2104AC79A7D" Ref="C9"  Part="1" 
AR Path="/F4BF4AC79A7D" Ref="C9"  Part="1" 
AR Path="/262F604AC79A7D" Ref="C9"  Part="1" 
AR Path="/384AC79A7D" Ref="C9"  Part="1" 
F 0 "C9" H 13700 15700 50  0000 L CNN
F 1 "0.1 uF" H 13700 15500 50  0000 L CNN
F 2 "C2" H 13650 15600 60  0001 C CNN
	1    13650 15600
	1    0    0    -1  
$EndComp
$Comp
L C C10
U 1 1 4AC79A7C
P 14100 15600
AR Path="/4AC79A7C" Ref="C10"  Part="1" 
AR Path="/94AC79A7C" Ref="C10"  Part="1" 
AR Path="/14AC79A7C" Ref="C10"  Part="1" 
AR Path="/6FF0DD404AC79A7C" Ref="C10"  Part="1" 
AR Path="/755D912A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/6FE901F74AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FE08B434AC79A7C" Ref="C10"  Part="1" 
AR Path="/3DA360004AC79A7C" Ref="C10"  Part="1" 
AR Path="/5AD7153D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/23D9304AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FEFFFFF4AC79A7C" Ref="C10"  Part="1" 
AR Path="/23D8D44AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D8EA0004AC79A7C" Ref="C10"  Part="1" 
AR Path="/402B08B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/4026A24D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FE88B434AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D7E00004AC79A7C" Ref="C10"  Part="1" 
AR Path="/402988B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D6CC0004AC79A7C" Ref="C10"  Part="1" 
AR Path="/FFFFFFF04AC79A7C" Ref="C10"  Part="1" 
AR Path="/23C2344AC79A7C" Ref="C10"  Part="1" 
AR Path="/DCBAABCD4AC79A7C" Ref="C10"  Part="1" 
AR Path="/5AD746F64AC79A7C" Ref="C10"  Part="1" 
AR Path="/4027EF1A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/402A6F1A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/4029BBE74AC79A7C" Ref="C10"  Part="1" 
AR Path="/402A224D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/402A55814AC79A7C" Ref="C10"  Part="1" 
AR Path="/4026224D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D6220004AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D9720004AC79A7C" Ref="C10"  Part="1" 
AR Path="/2600004AC79A7C" Ref="C10"  Part="1" 
AR Path="/402C3BE74AC79A7C" Ref="C10"  Part="1" 
AR Path="/402955814AC79A7C" Ref="C10"  Part="1" 
AR Path="/40293BE74AC79A7C" Ref="C10"  Part="1" 
AR Path="/402555814AC79A7C" Ref="C10"  Part="1" 
AR Path="/23D2034AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FED58104AC79A7C" Ref="C10"  Part="1" 
AR Path="/402688B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FE441894AC79A7C" Ref="C10"  Part="1" 
AR Path="/B84AC79A7C" Ref="C10"  Part="1" 
AR Path="/4029D5814AC79A7C" Ref="C10"  Part="1" 
AR Path="/40256F1A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/40286F1A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/402A08B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/402588B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/A84AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FE224DD4AC79A7C" Ref="C10"  Part="1" 
AR Path="/402C08B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/402BEF1A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/402C55814AC79A7C" Ref="C10"  Part="1" 
AR Path="/40273BE74AC79A7C" Ref="C10"  Part="1" 
AR Path="/402908B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D5A40004AC79A7C" Ref="C10"  Part="1" 
AR Path="/402755814AC79A7C" Ref="C10"  Part="1" 
AR Path="/4030AAC04AC79A7C" Ref="C10"  Part="1" 
AR Path="/4031DDF34AC79A7C" Ref="C10"  Part="1" 
AR Path="/4032778D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/403091264AC79A7C" Ref="C10"  Part="1" 
AR Path="/403051264AC79A7C" Ref="C10"  Part="1" 
AR Path="/4032F78D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/403251264AC79A7C" Ref="C10"  Part="1" 
AR Path="/4032AAC04AC79A7C" Ref="C10"  Part="1" 
AR Path="/4030D1264AC79A7C" Ref="C10"  Part="1" 
AR Path="/4031778D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FEA24DD4AC79A7C" Ref="C10"  Part="1" 
AR Path="/6FE934E34AC79A7C" Ref="C10"  Part="1" 
AR Path="/773F65F14AC79A7C" Ref="C10"  Part="1" 
AR Path="/773F8EB44AC79A7C" Ref="C10"  Part="1" 
AR Path="/23C9F04AC79A7C" Ref="C10"  Part="1" 
AR Path="/24AC79A7C" Ref="C10"  Part="1" 
AR Path="/23BC884AC79A7C" Ref="C10"  Part="1" 
AR Path="/DC0C124AC79A7C" Ref="C10"  Part="1" 
AR Path="/23C34C4AC79A7C" Ref="C10"  Part="1" 
AR Path="/23CBC44AC79A7C" Ref="C10"  Part="1" 
AR Path="/23C6504AC79A7C" Ref="C10"  Part="1" 
AR Path="/39803EA4AC79A7C" Ref="C10"  Part="1" 
AR Path="/FFFFFFFF4AC79A7C" Ref="C10"  Part="1" 
AR Path="/D058A04AC79A7C" Ref="C10"  Part="1" 
AR Path="/1607D44AC79A7C" Ref="C10"  Part="1" 
AR Path="/23D70C4AC79A7C" Ref="C10"  Part="1" 
AR Path="/2104AC79A7C" Ref="C10"  Part="1" 
AR Path="/F4BF4AC79A7C" Ref="C10"  Part="1" 
AR Path="/262F604AC79A7C" Ref="C10"  Part="1" 
AR Path="/384AC79A7C" Ref="C10"  Part="1" 
F 0 "C10" H 14150 15700 50  0000 L CNN
F 1 "0.1 uF" H 14150 15500 50  0000 L CNN
F 2 "C2" H 14100 15600 60  0001 C CNN
	1    14100 15600
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 4AC79A72
P 13200 15600
AR Path="/4AC79A72" Ref="C8"  Part="1" 
AR Path="/94AC79A72" Ref="C8"  Part="1" 
AR Path="/14AC79A72" Ref="C8"  Part="1" 
AR Path="/6FF0DD404AC79A72" Ref="C8"  Part="1" 
AR Path="/755D912A4AC79A72" Ref="C8"  Part="1" 
AR Path="/6FE901F74AC79A72" Ref="C8"  Part="1" 
AR Path="/3FE08B434AC79A72" Ref="C8"  Part="1" 
AR Path="/3DA360004AC79A72" Ref="C8"  Part="1" 
AR Path="/5AD7153D4AC79A72" Ref="C8"  Part="1" 
AR Path="/A4AC79A72" Ref="C8"  Part="1" 
AR Path="/23D9304AC79A72" Ref="C8"  Part="1" 
AR Path="/3FEFFFFF4AC79A72" Ref="C8"  Part="1" 
AR Path="/23D8D44AC79A72" Ref="C8"  Part="1" 
AR Path="/3D8EA0004AC79A72" Ref="C8"  Part="1" 
AR Path="/402B08B44AC79A72" Ref="C8"  Part="1" 
AR Path="/4026A24D4AC79A72" Ref="C8"  Part="1" 
AR Path="/3FE88B434AC79A72" Ref="C8"  Part="1" 
AR Path="/3D7E00004AC79A72" Ref="C8"  Part="1" 
AR Path="/402988B44AC79A72" Ref="C8"  Part="1" 
AR Path="/3D6CC0004AC79A72" Ref="C8"  Part="1" 
AR Path="/FFFFFFF04AC79A72" Ref="C8"  Part="1" 
AR Path="/23C2344AC79A72" Ref="C8"  Part="1" 
AR Path="/DCBAABCD4AC79A72" Ref="C8"  Part="1" 
AR Path="/5AD746F64AC79A72" Ref="C8"  Part="1" 
AR Path="/4027EF1A4AC79A72" Ref="C8"  Part="1" 
AR Path="/402A6F1A4AC79A72" Ref="C8"  Part="1" 
AR Path="/4029BBE74AC79A72" Ref="C8"  Part="1" 
AR Path="/402A224D4AC79A72" Ref="C8"  Part="1" 
AR Path="/402A55814AC79A72" Ref="C8"  Part="1" 
AR Path="/4026224D4AC79A72" Ref="C8"  Part="1" 
AR Path="/3D6220004AC79A72" Ref="C8"  Part="1" 
AR Path="/3D9720004AC79A72" Ref="C8"  Part="1" 
AR Path="/2600004AC79A72" Ref="C8"  Part="1" 
AR Path="/402C3BE74AC79A72" Ref="C8"  Part="1" 
AR Path="/402955814AC79A72" Ref="C8"  Part="1" 
AR Path="/40293BE74AC79A72" Ref="C8"  Part="1" 
AR Path="/402555814AC79A72" Ref="C8"  Part="1" 
AR Path="/23D2034AC79A72" Ref="C8"  Part="1" 
AR Path="/3FED58104AC79A72" Ref="C8"  Part="1" 
AR Path="/402688B44AC79A72" Ref="C8"  Part="1" 
AR Path="/3FE441894AC79A72" Ref="C8"  Part="1" 
AR Path="/B84AC79A72" Ref="C8"  Part="1" 
AR Path="/4029D5814AC79A72" Ref="C8"  Part="1" 
AR Path="/40256F1A4AC79A72" Ref="C8"  Part="1" 
AR Path="/40286F1A4AC79A72" Ref="C8"  Part="1" 
AR Path="/402A08B44AC79A72" Ref="C8"  Part="1" 
AR Path="/402588B44AC79A72" Ref="C8"  Part="1" 
AR Path="/A84AC79A72" Ref="C8"  Part="1" 
AR Path="/3FE224DD4AC79A72" Ref="C8"  Part="1" 
AR Path="/402C08B44AC79A72" Ref="C8"  Part="1" 
AR Path="/402BEF1A4AC79A72" Ref="C8"  Part="1" 
AR Path="/402C55814AC79A72" Ref="C8"  Part="1" 
AR Path="/40273BE74AC79A72" Ref="C8"  Part="1" 
AR Path="/402908B44AC79A72" Ref="C8"  Part="1" 
AR Path="/3D5A40004AC79A72" Ref="C8"  Part="1" 
AR Path="/402755814AC79A72" Ref="C8"  Part="1" 
AR Path="/4030AAC04AC79A72" Ref="C8"  Part="1" 
AR Path="/4031DDF34AC79A72" Ref="C8"  Part="1" 
AR Path="/4032778D4AC79A72" Ref="C8"  Part="1" 
AR Path="/403091264AC79A72" Ref="C8"  Part="1" 
AR Path="/403051264AC79A72" Ref="C8"  Part="1" 
AR Path="/4032F78D4AC79A72" Ref="C8"  Part="1" 
AR Path="/403251264AC79A72" Ref="C8"  Part="1" 
AR Path="/4032AAC04AC79A72" Ref="C8"  Part="1" 
AR Path="/4030D1264AC79A72" Ref="C8"  Part="1" 
AR Path="/4031778D4AC79A72" Ref="C8"  Part="1" 
AR Path="/3FEA24DD4AC79A72" Ref="C8"  Part="1" 
AR Path="/6FE934E34AC79A72" Ref="C8"  Part="1" 
AR Path="/773F65F14AC79A72" Ref="C8"  Part="1" 
AR Path="/773F8EB44AC79A72" Ref="C8"  Part="1" 
AR Path="/23C9F04AC79A72" Ref="C8"  Part="1" 
AR Path="/24AC79A72" Ref="C8"  Part="1" 
AR Path="/23BC884AC79A72" Ref="C8"  Part="1" 
AR Path="/DC0C124AC79A72" Ref="C8"  Part="1" 
AR Path="/23C34C4AC79A72" Ref="C8"  Part="1" 
AR Path="/23CBC44AC79A72" Ref="C8"  Part="1" 
AR Path="/23C6504AC79A72" Ref="C8"  Part="1" 
AR Path="/39803EA4AC79A72" Ref="C8"  Part="1" 
AR Path="/FFFFFFFF4AC79A72" Ref="C8"  Part="1" 
AR Path="/D058A04AC79A72" Ref="C8"  Part="1" 
AR Path="/1607D44AC79A72" Ref="C8"  Part="1" 
AR Path="/23D70C4AC79A72" Ref="C8"  Part="1" 
AR Path="/2104AC79A72" Ref="C8"  Part="1" 
AR Path="/F4BF4AC79A72" Ref="C8"  Part="1" 
AR Path="/262F604AC79A72" Ref="C8"  Part="1" 
AR Path="/384AC79A72" Ref="C8"  Part="1" 
F 0 "C8" H 13250 15700 50  0000 L CNN
F 1 "0.1 uF" H 13250 15500 50  0000 L CNN
F 2 "C2" H 13200 15600 60  0001 C CNN
	1    13200 15600
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 4AC79A4A
P 12750 15600
AR Path="/4AC79A4A" Ref="C7"  Part="1" 
AR Path="/7C9114604AC79A4A" Ref="C?"  Part="1" 
AR Path="/404AC79A4A" Ref="C?"  Part="1" 
AR Path="/94AC79A4A" Ref="C7"  Part="1" 
AR Path="/14AC79A4A" Ref="C7"  Part="1" 
AR Path="/6FF0DD404AC79A4A" Ref="C7"  Part="1" 
AR Path="/755D912A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/6FE901F74AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FE08B434AC79A4A" Ref="C7"  Part="1" 
AR Path="/3DA360004AC79A4A" Ref="C7"  Part="1" 
AR Path="/5AD7153D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/23D9304AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FEFFFFF4AC79A4A" Ref="C7"  Part="1" 
AR Path="/23D8D44AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D8EA0004AC79A4A" Ref="C7"  Part="1" 
AR Path="/402B08B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/4026A24D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FE88B434AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D7E00004AC79A4A" Ref="C7"  Part="1" 
AR Path="/402988B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D6CC0004AC79A4A" Ref="C7"  Part="1" 
AR Path="/FFFFFFF04AC79A4A" Ref="C7"  Part="1" 
AR Path="/23C2344AC79A4A" Ref="C7"  Part="1" 
AR Path="/DCBAABCD4AC79A4A" Ref="C7"  Part="1" 
AR Path="/5AD746F64AC79A4A" Ref="C7"  Part="1" 
AR Path="/4027EF1A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/402A6F1A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/4029BBE74AC79A4A" Ref="C7"  Part="1" 
AR Path="/402A224D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/402A55814AC79A4A" Ref="C7"  Part="1" 
AR Path="/4026224D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D6220004AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D9720004AC79A4A" Ref="C7"  Part="1" 
AR Path="/2600004AC79A4A" Ref="C7"  Part="1" 
AR Path="/402C3BE74AC79A4A" Ref="C7"  Part="1" 
AR Path="/402955814AC79A4A" Ref="C7"  Part="1" 
AR Path="/40293BE74AC79A4A" Ref="C7"  Part="1" 
AR Path="/402555814AC79A4A" Ref="C7"  Part="1" 
AR Path="/23D2034AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FED58104AC79A4A" Ref="C7"  Part="1" 
AR Path="/402688B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FE441894AC79A4A" Ref="C7"  Part="1" 
AR Path="/B84AC79A4A" Ref="C7"  Part="1" 
AR Path="/4029D5814AC79A4A" Ref="C7"  Part="1" 
AR Path="/40256F1A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/40286F1A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/402A08B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/402588B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/A84AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FE224DD4AC79A4A" Ref="C7"  Part="1" 
AR Path="/402C08B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/402BEF1A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/402C55814AC79A4A" Ref="C7"  Part="1" 
AR Path="/40273BE74AC79A4A" Ref="C7"  Part="1" 
AR Path="/402908B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D5A40004AC79A4A" Ref="C7"  Part="1" 
AR Path="/402755814AC79A4A" Ref="C7"  Part="1" 
AR Path="/4030AAC04AC79A4A" Ref="C7"  Part="1" 
AR Path="/4031DDF34AC79A4A" Ref="C7"  Part="1" 
AR Path="/4032778D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/403091264AC79A4A" Ref="C7"  Part="1" 
AR Path="/403051264AC79A4A" Ref="C7"  Part="1" 
AR Path="/4032F78D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/403251264AC79A4A" Ref="C7"  Part="1" 
AR Path="/4032AAC04AC79A4A" Ref="C7"  Part="1" 
AR Path="/4030D1264AC79A4A" Ref="C7"  Part="1" 
AR Path="/4031778D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FEA24DD4AC79A4A" Ref="C7"  Part="1" 
AR Path="/6FE934E34AC79A4A" Ref="C7"  Part="1" 
AR Path="/773F65F14AC79A4A" Ref="C7"  Part="1" 
AR Path="/773F8EB44AC79A4A" Ref="C7"  Part="1" 
AR Path="/23C9F04AC79A4A" Ref="C7"  Part="1" 
AR Path="/24AC79A4A" Ref="C7"  Part="1" 
AR Path="/23BC884AC79A4A" Ref="C7"  Part="1" 
AR Path="/DC0C124AC79A4A" Ref="C7"  Part="1" 
AR Path="/23C34C4AC79A4A" Ref="C7"  Part="1" 
AR Path="/23CBC44AC79A4A" Ref="C7"  Part="1" 
AR Path="/23C6504AC79A4A" Ref="C7"  Part="1" 
AR Path="/39803EA4AC79A4A" Ref="C7"  Part="1" 
AR Path="/FFFFFFFF4AC79A4A" Ref="C7"  Part="1" 
AR Path="/D058A04AC79A4A" Ref="C7"  Part="1" 
AR Path="/1607D44AC79A4A" Ref="C7"  Part="1" 
AR Path="/23D70C4AC79A4A" Ref="C7"  Part="1" 
AR Path="/2104AC79A4A" Ref="C7"  Part="1" 
AR Path="/F4BF4AC79A4A" Ref="C7"  Part="1" 
AR Path="/262F604AC79A4A" Ref="C7"  Part="1" 
AR Path="/384AC79A4A" Ref="C7"  Part="1" 
F 0 "C7" H 12800 15700 50  0000 L CNN
F 1 "0.1 uF" H 12800 15500 50  0000 L CNN
F 2 "C2" H 12750 15600 60  0001 C CNN
	1    12750 15600
	1    0    0    -1  
$EndComp
Text Label 20550 15200 0    60   ~ 0
VCC
$Comp
L GND #PWR?
U 1 1 4AC3EEFD
P 20850 15650
AR Path="/69698AFC4AC3EEFD" Ref="#PWR?"  Part="1" 
AR Path="/393639364AC3EEFD" Ref="#PWR?"  Part="1" 
AR Path="/25E4AC3EEFD" Ref="#PWR038"  Part="1" 
AR Path="/FFFFFFFF4AC3EEFD" Ref="#PWR040"  Part="1" 
AR Path="/23D6E04AC3EEFD" Ref="#PWR01"  Part="1" 
AR Path="/6FF0DD404AC3EEFD" Ref="#PWR036"  Part="1" 
AR Path="/363030314AC3EEFD" Ref="#PWR038"  Part="1" 
AR Path="/23C5884AC3EEFD" Ref="#PWR01"  Part="1" 
AR Path="/23C6384AC3EEFD" Ref="#PWR01"  Part="1" 
AR Path="/74AC3EEFD" Ref="#PWR01"  Part="1" 
AR Path="/314AC3EEFD" Ref="#PWR022"  Part="1" 
AR Path="/4AC3EEFD" Ref="#PWR035"  Part="1" 
AR Path="/23C7004AC3EEFD" Ref="#PWR014"  Part="1" 
AR Path="/5AD7153D4AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/A4AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/94AC3EEFD" Ref="#PWR37"  Part="1" 
AR Path="/755D912A4AC3EEFD" Ref="#PWR016"  Part="1" 
AR Path="/3FE08B434AC3EEFD" Ref="#PWR03"  Part="1" 
AR Path="/3DA360004AC3EEFD" Ref="#PWR022"  Part="1" 
AR Path="/23D9304AC3EEFD" Ref="#PWR03"  Part="1" 
AR Path="/3FEFFFFF4AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/23D8D44AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/3D8EA0004AC3EEFD" Ref="#PWR028"  Part="1" 
AR Path="/402B08B44AC3EEFD" Ref="#PWR03"  Part="1" 
AR Path="/4026A24D4AC3EEFD" Ref="#PWR03"  Part="1" 
AR Path="/6FE901F74AC3EEFD" Ref="#PWR022"  Part="1" 
AR Path="/3FE88B434AC3EEFD" Ref="#PWR030"  Part="1" 
AR Path="/3D7E00004AC3EEFD" Ref="#PWR022"  Part="1" 
AR Path="/402988B44AC3EEFD" Ref="#PWR013"  Part="1" 
AR Path="/3D6CC0004AC3EEFD" Ref="#PWR021"  Part="1" 
AR Path="/FFFFFFF04AC3EEFD" Ref="#PWR36"  Part="1" 
AR Path="/23C2344AC3EEFD" Ref="#PWR025"  Part="1" 
AR Path="/DCBAABCD4AC3EEFD" Ref="#PWR028"  Part="1" 
AR Path="/5AD746F64AC3EEFD" Ref="#PWR026"  Part="1" 
AR Path="/4027EF1A4AC3EEFD" Ref="#PWR026"  Part="1" 
AR Path="/402A6F1A4AC3EEFD" Ref="#PWR026"  Part="1" 
AR Path="/4029BBE74AC3EEFD" Ref="#PWR026"  Part="1" 
AR Path="/402A224D4AC3EEFD" Ref="#PWR026"  Part="1" 
AR Path="/402A55814AC3EEFD" Ref="#PWR025"  Part="1" 
AR Path="/4026224D4AC3EEFD" Ref="#PWR013"  Part="1" 
AR Path="/3D6220004AC3EEFD" Ref="#PWR025"  Part="1" 
AR Path="/3D9720004AC3EEFD" Ref="#PWR026"  Part="1" 
AR Path="/2600004AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/402C3BE74AC3EEFD" Ref="#PWR013"  Part="1" 
AR Path="/402955814AC3EEFD" Ref="#PWR020"  Part="1" 
AR Path="/40293BE74AC3EEFD" Ref="#PWR024"  Part="1" 
AR Path="/402555814AC3EEFD" Ref="#PWR013"  Part="1" 
AR Path="/23D2034AC3EEFD" Ref="#PWR022"  Part="1" 
AR Path="/3FED58104AC3EEFD" Ref="#PWR013"  Part="1" 
AR Path="/402688B44AC3EEFD" Ref="#PWR013"  Part="1" 
AR Path="/3FE441894AC3EEFD" Ref="#PWR014"  Part="1" 
AR Path="/B84AC3EEFD" Ref="#PWR8"  Part="1" 
AR Path="/4029D5814AC3EEFD" Ref="#PWR014"  Part="1" 
AR Path="/40256F1A4AC3EEFD" Ref="#PWR022"  Part="1" 
AR Path="/40286F1A4AC3EEFD" Ref="#PWR022"  Part="1" 
AR Path="/402A08B44AC3EEFD" Ref="#PWR022"  Part="1" 
AR Path="/402588B44AC3EEFD" Ref="#PWR022"  Part="1" 
AR Path="/A84AC3EEFD" Ref="#PWR36"  Part="1" 
AR Path="/3FE224DD4AC3EEFD" Ref="#PWR020"  Part="1" 
AR Path="/402C08B44AC3EEFD" Ref="#PWR021"  Part="1" 
AR Path="/402BEF1A4AC3EEFD" Ref="#PWR022"  Part="1" 
AR Path="/402C55814AC3EEFD" Ref="#PWR027"  Part="1" 
AR Path="/40273BE74AC3EEFD" Ref="#PWR028"  Part="1" 
AR Path="/402908B44AC3EEFD" Ref="#PWR024"  Part="1" 
AR Path="/3D5A40004AC3EEFD" Ref="#PWR021"  Part="1" 
AR Path="/402755814AC3EEFD" Ref="#PWR029"  Part="1" 
AR Path="/4030AAC04AC3EEFD" Ref="#PWR029"  Part="1" 
AR Path="/4031DDF34AC3EEFD" Ref="#PWR030"  Part="1" 
AR Path="/4032778D4AC3EEFD" Ref="#PWR036"  Part="1" 
AR Path="/403091264AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/403051264AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/4032F78D4AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/403251264AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/4032AAC04AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/4030D1264AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/4031778D4AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/3FEA24DD4AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/6FE934E34AC3EEFD" Ref="#PWR038"  Part="1" 
AR Path="/773F65F14AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/773F8EB44AC3EEFD" Ref="#PWR035"  Part="1" 
AR Path="/23C9F04AC3EEFD" Ref="#PWR39"  Part="1" 
AR Path="/23BC884AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/DC0C124AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/6684D64AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/23C34C4AC3EEFD" Ref="#PWR040"  Part="1" 
AR Path="/23CBC44AC3EEFD" Ref="#PWR039"  Part="1" 
AR Path="/39803EA4AC3EEFD" Ref="#PWR040"  Part="1" 
AR Path="/D058A04AC3EEFD" Ref="#PWR041"  Part="1" 
AR Path="/1607D44AC3EEFD" Ref="#PWR041"  Part="1" 
AR Path="/24AC3EEFD" Ref="#PWR041"  Part="1" 
AR Path="/23D70C4AC3EEFD" Ref="#PWR041"  Part="1" 
AR Path="/14AC3EEFD" Ref="#PWR041"  Part="1" 
AR Path="/2104AC3EEFD" Ref="#PWR041"  Part="1" 
AR Path="/23C6504AC3EEFD" Ref="#PWR035"  Part="1" 
AR Path="/F4BF4AC3EEFD" Ref="#PWR035"  Part="1" 
AR Path="/262F604AC3EEFD" Ref="#PWR035"  Part="1" 
AR Path="/384AC3EEFD" Ref="#PWR036"  Part="1" 
F 0 "#PWR035" H 20850 15650 30  0001 C CNN
F 1 "GND" H 20850 15580 30  0001 C CNN
	1    20850 15650
	1    0    0    -1  
$EndComp
Text Label 18950 15200 0    60   ~ 0
+8V
Text Label 19450 10600 0    60   ~ 0
0V
Text Label 19450 10500 0    60   ~ 0
POC*
Text Label 19450 10400 0    60   ~ 0
ERROR*
Text Label 19450 10300 0    60   ~ 0
sWO*
Text Label 19450 10200 0    60   ~ 0
sINTA
Text Label 19450 10100 0    60   ~ 0
DI0
Text Label 19450 10000 0    60   ~ 0
DI1
Text Label 19450 9900 0    60   ~ 0
DI6
Text Label 19450 9800 0    60   ~ 0
DI5
Text Label 19450 9700 0    60   ~ 0
DI4
Text Label 19450 9600 0    60   ~ 0
DO7
Text Label 19450 9500 0    60   ~ 0
DO3
Text Label 19450 9400 0    60   ~ 0
DO2
Text Label 19450 9300 0    60   ~ 0
A11
Text Label 19450 9200 0    60   ~ 0
A14
Text Label 19450 9100 0    60   ~ 0
A13
Text Label 19450 9000 0    60   ~ 0
A8
Text Label 19450 8900 0    60   ~ 0
A7
Text Label 19450 8800 0    60   ~ 0
A6
Text Label 19450 8700 0    60   ~ 0
A2
Text Label 19450 8600 0    60   ~ 0
A1
Text Label 19450 8500 0    60   ~ 0
A0
Text Label 19450 8400 0    60   ~ 0
pDBIN
Text Label 19450 8300 0    60   ~ 0
pWR*
Text Label 19450 8200 0    60   ~ 0
pSYNC
Text Label 19450 8100 0    60   ~ 0
RESET*
Text Label 19450 8000 0    60   ~ 0
HOLD*
Text Label 19450 7900 0    60   ~ 0
INT*
Text Label 19450 7800 0    60   ~ 0
RDY
Text Label 19450 7700 0    60   ~ 0
RFU4
Text Label 19450 7600 0    60   ~ 0
0V_C
Text Label 19450 7500 0    60   ~ 0
RFU3
Text Label 19450 7400 0    60   ~ 0
MWRT
Text Label 19450 7300 0    60   ~ 0
PHANTOM*
Text Label 19450 7200 0    60   ~ 0
NDEF3
Text Label 19450 7100 0    60   ~ 0
NDEF2
Text Label 19450 7000 0    60   ~ 0
A23
Text Label 19450 6900 0    60   ~ 0
A22
Text Label 19450 6800 0    60   ~ 0
A21
Text Label 19450 6700 0    60   ~ 0
A20
Text Label 19450 6600 0    60   ~ 0
SIXTN*
Text Label 19450 6500 0    60   ~ 0
A19
Text Label 19450 6400 0    60   ~ 0
sXTRQ*
Text Label 19450 6300 0    60   ~ 0
TMA2*
Text Label 19450 6200 0    60   ~ 0
TMA1*
Text Label 19450 6100 0    60   ~ 0
TMA0*
Text Label 19450 6000 0    60   ~ 0
SLAVE_CLR*
Text Label 19450 5900 0    60   ~ 0
0V_B
Text Label 19450 5800 0    60   ~ 0
-16V
Text Label 19450 5600 0    60   ~ 0
0V
Text Label 19450 5500 0    60   ~ 0
CLOCK
Text Label 19450 5400 0    60   ~ 0
sHLTA
Text Label 19450 5300 0    60   ~ 0
sMEMR
Text Label 19450 5200 0    60   ~ 0
sINP
Text Label 19450 5100 0    60   ~ 0
sOUT
Text Label 19450 5000 0    60   ~ 0
sM1
Text Label 19450 4900 0    60   ~ 0
DI7
Text Label 19450 4800 0    60   ~ 0
DI3
Text Label 19450 4700 0    60   ~ 0
DI2
Text Label 19450 4600 0    60   ~ 0
DO6
Text Label 19450 4500 0    60   ~ 0
DO5
Text Label 19450 4400 0    60   ~ 0
DO4
Text Label 19450 4300 0    60   ~ 0
A10
Text Label 19450 4200 0    60   ~ 0
DO0
Text Label 19450 4100 0    60   ~ 0
DO1
Text Label 19450 4000 0    60   ~ 0
A9
Text Label 19450 3900 0    60   ~ 0
A12
Text Label 19450 3800 0    60   ~ 0
A15
Text Label 19450 3700 0    60   ~ 0
A3
Text Label 19450 3600 0    60   ~ 0
A4
Text Label 19450 3500 0    60   ~ 0
A5
Text Label 19450 3400 0    60   ~ 0
RFU2
Text Label 19450 3300 0    60   ~ 0
RFU1
Text Label 19450 3200 0    60   ~ 0
pHLDA
Text Label 19450 3100 0    60   ~ 0
pSTVAL*
Text Label 19450 3000 0    60   ~ 0
PHI
Text Label 19450 2900 0    60   ~ 0
DODSB*
Text Label 19450 2800 0    60   ~ 0
ADSB*
Text Label 19450 2700 0    60   ~ 0
NDEF1
Text Label 19450 2600 0    60   ~ 0
0V_A
Text Label 19450 2500 0    60   ~ 0
CDSB*
Text Label 19450 2400 0    60   ~ 0
SDSB*
Text Label 19450 2300 0    60   ~ 0
A17
Text Label 19450 2200 0    60   ~ 0
A16
Text Label 19450 2100 0    60   ~ 0
A18
Text Label 19450 2000 0    60   ~ 0
TMA3*
Text Label 19450 1900 0    60   ~ 0
PWRFAIL*
Text Label 19450 1800 0    60   ~ 0
NMI*
Text Label 19450 1700 0    60   ~ 0
VI7*
Text Label 19450 1600 0    60   ~ 0
VI6*
Text Label 19450 1500 0    60   ~ 0
VI5*
Text Label 19450 1400 0    60   ~ 0
VI4*
Text Label 19450 1300 0    60   ~ 0
VI3*
Text Label 19450 1200 0    60   ~ 0
VI2*
Text Label 19450 1100 0    60   ~ 0
VI1*
Text Label 19450 1000 0    60   ~ 0
VI0*
Text Label 19450 900  0    60   ~ 0
XRDY
Text Label 19450 800  0    60   ~ 0
+16V
Text Label 19450 700  0    60   ~ 0
+8V
Text Notes 18650 11250 0    60   ~ 0
Card Ejector Mounting Holes
$Comp
L CONN_1 P34
U 1 1 4A0CC099
P 19200 11400
AR Path="/FFFFFFFF4A0CC099" Ref="P34"  Part="1" 
AR Path="/4A0CC099" Ref="P34"  Part="1" 
AR Path="/94A0CC099" Ref="P34"  Part="1" 
AR Path="/6FF0DD404A0CC099" Ref="P34"  Part="1" 
AR Path="/23D6E04A0CC099" Ref="P34"  Part="1" 
AR Path="/5AD7153D4A0CC099" Ref="P34"  Part="1" 
AR Path="/A4A0CC099" Ref="P34"  Part="1" 
AR Path="/755D912A4A0CC099" Ref="P34"  Part="1" 
AR Path="/3FE08B434A0CC099" Ref="P34"  Part="1" 
AR Path="/3DA360004A0CC099" Ref="P34"  Part="1" 
AR Path="/23D9304A0CC099" Ref="P34"  Part="1" 
AR Path="/3FEFFFFF4A0CC099" Ref="P34"  Part="1" 
AR Path="/23D8D44A0CC099" Ref="P34"  Part="1" 
AR Path="/3D8EA0004A0CC099" Ref="P34"  Part="1" 
AR Path="/402B08B44A0CC099" Ref="P34"  Part="1" 
AR Path="/4026A24D4A0CC099" Ref="P34"  Part="1" 
AR Path="/3FE88B434A0CC099" Ref="P34"  Part="1" 
AR Path="/3D7E00004A0CC099" Ref="P34"  Part="1" 
AR Path="/402988B44A0CC099" Ref="P34"  Part="1" 
AR Path="/6FE901F74A0CC099" Ref="P34"  Part="1" 
AR Path="/3D6CC0004A0CC099" Ref="P34"  Part="1" 
AR Path="/14A0CC099" Ref="P34"  Part="1" 
AR Path="/FFFFFFF04A0CC099" Ref="P34"  Part="1" 
AR Path="/23C2344A0CC099" Ref="P34"  Part="1" 
AR Path="/DCBAABCD4A0CC099" Ref="P34"  Part="1" 
AR Path="/5AD746F64A0CC099" Ref="P34"  Part="1" 
AR Path="/4027EF1A4A0CC099" Ref="P34"  Part="1" 
AR Path="/402A6F1A4A0CC099" Ref="P34"  Part="1" 
AR Path="/4029BBE74A0CC099" Ref="P34"  Part="1" 
AR Path="/402A224D4A0CC099" Ref="P34"  Part="1" 
AR Path="/402A55814A0CC099" Ref="P34"  Part="1" 
AR Path="/4026224D4A0CC099" Ref="P34"  Part="1" 
AR Path="/3D6220004A0CC099" Ref="P34"  Part="1" 
AR Path="/3D9720004A0CC099" Ref="P34"  Part="1" 
AR Path="/2600004A0CC099" Ref="P34"  Part="1" 
AR Path="/402C3BE74A0CC099" Ref="P34"  Part="1" 
AR Path="/402955814A0CC099" Ref="P34"  Part="1" 
AR Path="/40293BE74A0CC099" Ref="P34"  Part="1" 
AR Path="/402555814A0CC099" Ref="P34"  Part="1" 
AR Path="/3FED58104A0CC099" Ref="P34"  Part="1" 
AR Path="/402688B44A0CC099" Ref="P34"  Part="1" 
AR Path="/3FE441894A0CC099" Ref="P34"  Part="1" 
AR Path="/B84A0CC099" Ref="P34"  Part="1" 
AR Path="/4029D5814A0CC099" Ref="P34"  Part="1" 
AR Path="/6FF405304A0CC099" Ref="P34"  Part="1" 
AR Path="/40256F1A4A0CC099" Ref="P34"  Part="1" 
AR Path="/40286F1A4A0CC099" Ref="P34"  Part="1" 
AR Path="/402A08B44A0CC099" Ref="P34"  Part="1" 
AR Path="/402588B44A0CC099" Ref="P34"  Part="1" 
AR Path="/23D2034A0CC099" Ref="P34"  Part="1" 
AR Path="/A84A0CC099" Ref="P34"  Part="1" 
AR Path="/3FE224DD4A0CC099" Ref="P34"  Part="1" 
AR Path="/402C08B44A0CC099" Ref="P34"  Part="1" 
AR Path="/402BEF1A4A0CC099" Ref="P34"  Part="1" 
AR Path="/402C55814A0CC099" Ref="P34"  Part="1" 
AR Path="/40273BE74A0CC099" Ref="P34"  Part="1" 
AR Path="/402908B44A0CC099" Ref="P34"  Part="1" 
AR Path="/3D5A40004A0CC099" Ref="P34"  Part="1" 
AR Path="/402755814A0CC099" Ref="P34"  Part="1" 
AR Path="/4030AAC04A0CC099" Ref="P34"  Part="1" 
AR Path="/4031DDF34A0CC099" Ref="P34"  Part="1" 
AR Path="/4032778D4A0CC099" Ref="P34"  Part="1" 
AR Path="/403091264A0CC099" Ref="P34"  Part="1" 
AR Path="/403051264A0CC099" Ref="P34"  Part="1" 
AR Path="/4032F78D4A0CC099" Ref="P34"  Part="1" 
AR Path="/403251264A0CC099" Ref="P34"  Part="1" 
AR Path="/4032AAC04A0CC099" Ref="P34"  Part="1" 
AR Path="/4030D1264A0CC099" Ref="P34"  Part="1" 
AR Path="/4031778D4A0CC099" Ref="P34"  Part="1" 
AR Path="/3FEA24DD4A0CC099" Ref="P34"  Part="1" 
AR Path="/6FE934E34A0CC099" Ref="P34"  Part="1" 
AR Path="/773F65F14A0CC099" Ref="P34"  Part="1" 
AR Path="/773F8EB44A0CC099" Ref="P34"  Part="1" 
AR Path="/23C9F04A0CC099" Ref="P34"  Part="1" 
AR Path="/24A0CC099" Ref="P34"  Part="1" 
AR Path="/23BC884A0CC099" Ref="P34"  Part="1" 
AR Path="/DC0C124A0CC099" Ref="P34"  Part="1" 
AR Path="/23C34C4A0CC099" Ref="P34"  Part="1" 
AR Path="/23CBC44A0CC099" Ref="P34"  Part="1" 
AR Path="/23C6504A0CC099" Ref="P34"  Part="1" 
AR Path="/39803EA4A0CC099" Ref="P34"  Part="1" 
AR Path="/D058A04A0CC099" Ref="P34"  Part="1" 
AR Path="/1607D44A0CC099" Ref="P34"  Part="1" 
AR Path="/23D70C4A0CC099" Ref="P34"  Part="1" 
AR Path="/2104A0CC099" Ref="P34"  Part="1" 
AR Path="/F4BF4A0CC099" Ref="P34"  Part="1" 
AR Path="/262F604A0CC099" Ref="P34"  Part="1" 
AR Path="/384A0CC099" Ref="P34"  Part="1" 
F 0 "P34" H 19280 11400 40  0000 C CNN
F 1 "CONN_1" H 19150 11440 30  0001 C CNN
F 2 "1pin" H 19200 11400 60  0001 C CNN
	1    19200 11400
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P35
U 1 1 4A0CC090
P 19200 11300
AR Path="/FFFFFFFF4A0CC090" Ref="P35"  Part="1" 
AR Path="/4A0CC090" Ref="P35"  Part="1" 
AR Path="/94A0CC090" Ref="P35"  Part="1" 
AR Path="/6FF0DD404A0CC090" Ref="P35"  Part="1" 
AR Path="/23D6E04A0CC090" Ref="P35"  Part="1" 
AR Path="/5AD7153D4A0CC090" Ref="P35"  Part="1" 
AR Path="/A4A0CC090" Ref="P35"  Part="1" 
AR Path="/755D912A4A0CC090" Ref="P35"  Part="1" 
AR Path="/3FE08B434A0CC090" Ref="P35"  Part="1" 
AR Path="/3DA360004A0CC090" Ref="P35"  Part="1" 
AR Path="/23D9304A0CC090" Ref="P35"  Part="1" 
AR Path="/3FEFFFFF4A0CC090" Ref="P35"  Part="1" 
AR Path="/23D8D44A0CC090" Ref="P35"  Part="1" 
AR Path="/3D8EA0004A0CC090" Ref="P35"  Part="1" 
AR Path="/402B08B44A0CC090" Ref="P35"  Part="1" 
AR Path="/4026A24D4A0CC090" Ref="P35"  Part="1" 
AR Path="/3FE88B434A0CC090" Ref="P35"  Part="1" 
AR Path="/3D7E00004A0CC090" Ref="P35"  Part="1" 
AR Path="/402988B44A0CC090" Ref="P35"  Part="1" 
AR Path="/6FE901F74A0CC090" Ref="P35"  Part="1" 
AR Path="/3D6CC0004A0CC090" Ref="P35"  Part="1" 
AR Path="/14A0CC090" Ref="P35"  Part="1" 
AR Path="/FFFFFFF04A0CC090" Ref="P35"  Part="1" 
AR Path="/23C2344A0CC090" Ref="P35"  Part="1" 
AR Path="/DCBAABCD4A0CC090" Ref="P35"  Part="1" 
AR Path="/5AD746F64A0CC090" Ref="P35"  Part="1" 
AR Path="/4027EF1A4A0CC090" Ref="P35"  Part="1" 
AR Path="/402A6F1A4A0CC090" Ref="P35"  Part="1" 
AR Path="/4029BBE74A0CC090" Ref="P35"  Part="1" 
AR Path="/402A224D4A0CC090" Ref="P35"  Part="1" 
AR Path="/402A55814A0CC090" Ref="P35"  Part="1" 
AR Path="/4026224D4A0CC090" Ref="P35"  Part="1" 
AR Path="/3D6220004A0CC090" Ref="P35"  Part="1" 
AR Path="/3D9720004A0CC090" Ref="P35"  Part="1" 
AR Path="/2600004A0CC090" Ref="P35"  Part="1" 
AR Path="/402C3BE74A0CC090" Ref="P35"  Part="1" 
AR Path="/402955814A0CC090" Ref="P35"  Part="1" 
AR Path="/40293BE74A0CC090" Ref="P35"  Part="1" 
AR Path="/402555814A0CC090" Ref="P35"  Part="1" 
AR Path="/3FED58104A0CC090" Ref="P35"  Part="1" 
AR Path="/402688B44A0CC090" Ref="P35"  Part="1" 
AR Path="/3FE441894A0CC090" Ref="P35"  Part="1" 
AR Path="/B84A0CC090" Ref="P35"  Part="1" 
AR Path="/4029D5814A0CC090" Ref="P35"  Part="1" 
AR Path="/6FF405304A0CC090" Ref="P35"  Part="1" 
AR Path="/40256F1A4A0CC090" Ref="P35"  Part="1" 
AR Path="/40286F1A4A0CC090" Ref="P35"  Part="1" 
AR Path="/402A08B44A0CC090" Ref="P35"  Part="1" 
AR Path="/402588B44A0CC090" Ref="P35"  Part="1" 
AR Path="/23D2034A0CC090" Ref="P35"  Part="1" 
AR Path="/A84A0CC090" Ref="P35"  Part="1" 
AR Path="/3FE224DD4A0CC090" Ref="P35"  Part="1" 
AR Path="/402C08B44A0CC090" Ref="P35"  Part="1" 
AR Path="/402BEF1A4A0CC090" Ref="P35"  Part="1" 
AR Path="/402C55814A0CC090" Ref="P35"  Part="1" 
AR Path="/40273BE74A0CC090" Ref="P35"  Part="1" 
AR Path="/402908B44A0CC090" Ref="P35"  Part="1" 
AR Path="/3D5A40004A0CC090" Ref="P35"  Part="1" 
AR Path="/402755814A0CC090" Ref="P35"  Part="1" 
AR Path="/4030AAC04A0CC090" Ref="P35"  Part="1" 
AR Path="/4031DDF34A0CC090" Ref="P35"  Part="1" 
AR Path="/4032778D4A0CC090" Ref="P35"  Part="1" 
AR Path="/403091264A0CC090" Ref="P35"  Part="1" 
AR Path="/403051264A0CC090" Ref="P35"  Part="1" 
AR Path="/4032F78D4A0CC090" Ref="P35"  Part="1" 
AR Path="/403251264A0CC090" Ref="P35"  Part="1" 
AR Path="/4032AAC04A0CC090" Ref="P35"  Part="1" 
AR Path="/4030D1264A0CC090" Ref="P35"  Part="1" 
AR Path="/4031778D4A0CC090" Ref="P35"  Part="1" 
AR Path="/3FEA24DD4A0CC090" Ref="P35"  Part="1" 
AR Path="/6FE934E34A0CC090" Ref="P35"  Part="1" 
AR Path="/773F65F14A0CC090" Ref="P35"  Part="1" 
AR Path="/773F8EB44A0CC090" Ref="P35"  Part="1" 
AR Path="/23C9F04A0CC090" Ref="P35"  Part="1" 
AR Path="/24A0CC090" Ref="P35"  Part="1" 
AR Path="/23BC884A0CC090" Ref="P35"  Part="1" 
AR Path="/DC0C124A0CC090" Ref="P35"  Part="1" 
AR Path="/23C34C4A0CC090" Ref="P35"  Part="1" 
AR Path="/23CBC44A0CC090" Ref="P35"  Part="1" 
AR Path="/23C6504A0CC090" Ref="P35"  Part="1" 
AR Path="/39803EA4A0CC090" Ref="P35"  Part="1" 
AR Path="/D058A04A0CC090" Ref="P35"  Part="1" 
AR Path="/1607D44A0CC090" Ref="P35"  Part="1" 
AR Path="/23D70C4A0CC090" Ref="P35"  Part="1" 
AR Path="/2104A0CC090" Ref="P35"  Part="1" 
AR Path="/F4BF4A0CC090" Ref="P35"  Part="1" 
AR Path="/262F604A0CC090" Ref="P35"  Part="1" 
AR Path="/384A0CC090" Ref="P35"  Part="1" 
F 0 "P35" H 19280 11300 40  0000 C CNN
F 1 "CONN_1" H 19150 11340 30  0001 C CNN
F 2 "1pin" H 19200 11300 60  0001 C CNN
	1    19200 11300
	1    0    0    -1  
$EndComp
$Comp
L CP C3
U 1 1 4A074947
P 20250 15400
AR Path="/FFFFFFFF4A074947" Ref="C3"  Part="1" 
AR Path="/4A074947" Ref="C3"  Part="1" 
AR Path="/94A074947" Ref="C3"  Part="1" 
AR Path="/6FF0DD404A074947" Ref="C3"  Part="1" 
AR Path="/23D6E04A074947" Ref="C3"  Part="1" 
AR Path="/14A074947" Ref="C3"  Part="1" 
AR Path="/5AD7153D4A074947" Ref="C3"  Part="1" 
AR Path="/A4A074947" Ref="C3"  Part="1" 
AR Path="/755D912A4A074947" Ref="C3"  Part="1" 
AR Path="/3FE08B434A074947" Ref="C3"  Part="1" 
AR Path="/3DA360004A074947" Ref="C3"  Part="1" 
AR Path="/23D9304A074947" Ref="C3"  Part="1" 
AR Path="/3FEFFFFF4A074947" Ref="C3"  Part="1" 
AR Path="/23D8D44A074947" Ref="C3"  Part="1" 
AR Path="/3D8EA0004A074947" Ref="C3"  Part="1" 
AR Path="/402B08B44A074947" Ref="C3"  Part="1" 
AR Path="/4026A24D4A074947" Ref="C3"  Part="1" 
AR Path="/3FE88B434A074947" Ref="C3"  Part="1" 
AR Path="/3D7E00004A074947" Ref="C3"  Part="1" 
AR Path="/402988B44A074947" Ref="C3"  Part="1" 
AR Path="/3D6CC0004A074947" Ref="C3"  Part="1" 
AR Path="/FFFFFFF04A074947" Ref="C3"  Part="1" 
AR Path="/23C2344A074947" Ref="C3"  Part="1" 
AR Path="/DCBAABCD4A074947" Ref="C3"  Part="1" 
AR Path="/5AD746F64A074947" Ref="C3"  Part="1" 
AR Path="/4027EF1A4A074947" Ref="C3"  Part="1" 
AR Path="/402A6F1A4A074947" Ref="C3"  Part="1" 
AR Path="/4029BBE74A074947" Ref="C3"  Part="1" 
AR Path="/402A224D4A074947" Ref="C3"  Part="1" 
AR Path="/402A55814A074947" Ref="C3"  Part="1" 
AR Path="/4026224D4A074947" Ref="C3"  Part="1" 
AR Path="/3D6220004A074947" Ref="C3"  Part="1" 
AR Path="/3D9720004A074947" Ref="C3"  Part="1" 
AR Path="/2600004A074947" Ref="C3"  Part="1" 
AR Path="/402C3BE74A074947" Ref="C3"  Part="1" 
AR Path="/402955814A074947" Ref="C3"  Part="1" 
AR Path="/40293BE74A074947" Ref="C3"  Part="1" 
AR Path="/402555814A074947" Ref="C3"  Part="1" 
AR Path="/6FE901F74A074947" Ref="C3"  Part="1" 
AR Path="/3FED58104A074947" Ref="C3"  Part="1" 
AR Path="/402688B44A074947" Ref="C3"  Part="1" 
AR Path="/3FE441894A074947" Ref="C3"  Part="1" 
AR Path="/B84A074947" Ref="C3"  Part="1" 
AR Path="/4029D5814A074947" Ref="C3"  Part="1" 
AR Path="/40256F1A4A074947" Ref="C3"  Part="1" 
AR Path="/40286F1A4A074947" Ref="C3"  Part="1" 
AR Path="/402A08B44A074947" Ref="C3"  Part="1" 
AR Path="/402588B44A074947" Ref="C3"  Part="1" 
AR Path="/A84A074947" Ref="C3"  Part="1" 
AR Path="/3FE224DD4A074947" Ref="C3"  Part="1" 
AR Path="/402C08B44A074947" Ref="C3"  Part="1" 
AR Path="/402BEF1A4A074947" Ref="C3"  Part="1" 
AR Path="/402C55814A074947" Ref="C3"  Part="1" 
AR Path="/40273BE74A074947" Ref="C3"  Part="1" 
AR Path="/402908B44A074947" Ref="C3"  Part="1" 
AR Path="/3D5A40004A074947" Ref="C3"  Part="1" 
AR Path="/402755814A074947" Ref="C3"  Part="1" 
AR Path="/4030AAC04A074947" Ref="C3"  Part="1" 
AR Path="/4031DDF34A074947" Ref="C3"  Part="1" 
AR Path="/4032778D4A074947" Ref="C3"  Part="1" 
AR Path="/403091264A074947" Ref="C3"  Part="1" 
AR Path="/403051264A074947" Ref="C3"  Part="1" 
AR Path="/4032F78D4A074947" Ref="C3"  Part="1" 
AR Path="/403251264A074947" Ref="C3"  Part="1" 
AR Path="/4032AAC04A074947" Ref="C3"  Part="1" 
AR Path="/4030D1264A074947" Ref="C3"  Part="1" 
AR Path="/4031778D4A074947" Ref="C3"  Part="1" 
AR Path="/3FEA24DD4A074947" Ref="C3"  Part="1" 
AR Path="/6FE934E34A074947" Ref="C3"  Part="1" 
AR Path="/773F65F14A074947" Ref="C3"  Part="1" 
AR Path="/773F8EB44A074947" Ref="C3"  Part="1" 
AR Path="/23C9F04A074947" Ref="C3"  Part="1" 
AR Path="/24A074947" Ref="C3"  Part="1" 
AR Path="/23BC884A074947" Ref="C3"  Part="1" 
AR Path="/DC0C124A074947" Ref="C3"  Part="1" 
AR Path="/23C34C4A074947" Ref="C3"  Part="1" 
AR Path="/23CBC44A074947" Ref="C3"  Part="1" 
AR Path="/23C6504A074947" Ref="C3"  Part="1" 
AR Path="/39803EA4A074947" Ref="C3"  Part="1" 
AR Path="/D058A04A074947" Ref="C3"  Part="1" 
AR Path="/1607D44A074947" Ref="C3"  Part="1" 
AR Path="/23D70C4A074947" Ref="C3"  Part="1" 
AR Path="/2104A074947" Ref="C3"  Part="1" 
AR Path="/F4BF4A074947" Ref="C3"  Part="1" 
AR Path="/262F604A074947" Ref="C3"  Part="1" 
AR Path="/384A074947" Ref="C3"  Part="1" 
F 0 "C3" H 20300 15500 50  0000 L CNN
F 1 "0.1 uF" H 20300 15300 50  0000 L CNN
F 2 "C1V5" H 20250 15400 60  0001 C CNN
	1    20250 15400
	1    0    0    -1  
$EndComp
$Comp
L CP C1
U 1 1 4A074934
P 19450 15400
AR Path="/FFFFFFFF4A074934" Ref="C1"  Part="1" 
AR Path="/4A074934" Ref="C1"  Part="1" 
AR Path="/94A074934" Ref="C1"  Part="1" 
AR Path="/6FF0DD404A074934" Ref="C1"  Part="1" 
AR Path="/23D6E04A074934" Ref="C1"  Part="1" 
AR Path="/14A074934" Ref="C1"  Part="1" 
AR Path="/5AD7153D4A074934" Ref="C1"  Part="1" 
AR Path="/A4A074934" Ref="C1"  Part="1" 
AR Path="/755D912A4A074934" Ref="C1"  Part="1" 
AR Path="/3FE08B434A074934" Ref="C1"  Part="1" 
AR Path="/3DA360004A074934" Ref="C1"  Part="1" 
AR Path="/23D9304A074934" Ref="C1"  Part="1" 
AR Path="/3FEFFFFF4A074934" Ref="C1"  Part="1" 
AR Path="/23D8D44A074934" Ref="C1"  Part="1" 
AR Path="/3D8EA0004A074934" Ref="C1"  Part="1" 
AR Path="/402B08B44A074934" Ref="C1"  Part="1" 
AR Path="/4026A24D4A074934" Ref="C1"  Part="1" 
AR Path="/3FE88B434A074934" Ref="C1"  Part="1" 
AR Path="/3D7E00004A074934" Ref="C1"  Part="1" 
AR Path="/402988B44A074934" Ref="C1"  Part="1" 
AR Path="/3D6CC0004A074934" Ref="C1"  Part="1" 
AR Path="/FFFFFFF04A074934" Ref="C1"  Part="1" 
AR Path="/23C2344A074934" Ref="C1"  Part="1" 
AR Path="/DCBAABCD4A074934" Ref="C1"  Part="1" 
AR Path="/5AD746F64A074934" Ref="C1"  Part="1" 
AR Path="/4027EF1A4A074934" Ref="C1"  Part="1" 
AR Path="/402A6F1A4A074934" Ref="C1"  Part="1" 
AR Path="/4029BBE74A074934" Ref="C1"  Part="1" 
AR Path="/402A224D4A074934" Ref="C1"  Part="1" 
AR Path="/402A55814A074934" Ref="C1"  Part="1" 
AR Path="/4026224D4A074934" Ref="C1"  Part="1" 
AR Path="/3D6220004A074934" Ref="C1"  Part="1" 
AR Path="/3D9720004A074934" Ref="C1"  Part="1" 
AR Path="/2600004A074934" Ref="C1"  Part="1" 
AR Path="/402C3BE74A074934" Ref="C1"  Part="1" 
AR Path="/402955814A074934" Ref="C1"  Part="1" 
AR Path="/40293BE74A074934" Ref="C1"  Part="1" 
AR Path="/402555814A074934" Ref="C1"  Part="1" 
AR Path="/6FE901F74A074934" Ref="C1"  Part="1" 
AR Path="/3FED58104A074934" Ref="C1"  Part="1" 
AR Path="/402688B44A074934" Ref="C1"  Part="1" 
AR Path="/3FE441894A074934" Ref="C1"  Part="1" 
AR Path="/B84A074934" Ref="C1"  Part="1" 
AR Path="/4029D5814A074934" Ref="C1"  Part="1" 
AR Path="/40256F1A4A074934" Ref="C1"  Part="1" 
AR Path="/40286F1A4A074934" Ref="C1"  Part="1" 
AR Path="/402A08B44A074934" Ref="C1"  Part="1" 
AR Path="/402588B44A074934" Ref="C1"  Part="1" 
AR Path="/A84A074934" Ref="C1"  Part="1" 
AR Path="/3FE224DD4A074934" Ref="C1"  Part="1" 
AR Path="/402C08B44A074934" Ref="C1"  Part="1" 
AR Path="/402BEF1A4A074934" Ref="C1"  Part="1" 
AR Path="/402C55814A074934" Ref="C1"  Part="1" 
AR Path="/40273BE74A074934" Ref="C1"  Part="1" 
AR Path="/402908B44A074934" Ref="C1"  Part="1" 
AR Path="/3D5A40004A074934" Ref="C1"  Part="1" 
AR Path="/402755814A074934" Ref="C1"  Part="1" 
AR Path="/4030AAC04A074934" Ref="C1"  Part="1" 
AR Path="/4031DDF34A074934" Ref="C1"  Part="1" 
AR Path="/4032778D4A074934" Ref="C1"  Part="1" 
AR Path="/403091264A074934" Ref="C1"  Part="1" 
AR Path="/403051264A074934" Ref="C1"  Part="1" 
AR Path="/4032F78D4A074934" Ref="C1"  Part="1" 
AR Path="/403251264A074934" Ref="C1"  Part="1" 
AR Path="/4032AAC04A074934" Ref="C1"  Part="1" 
AR Path="/4030D1264A074934" Ref="C1"  Part="1" 
AR Path="/4031778D4A074934" Ref="C1"  Part="1" 
AR Path="/3FEA24DD4A074934" Ref="C1"  Part="1" 
AR Path="/6FE934E34A074934" Ref="C1"  Part="1" 
AR Path="/773F65F14A074934" Ref="C1"  Part="1" 
AR Path="/773F8EB44A074934" Ref="C1"  Part="1" 
AR Path="/23C9F04A074934" Ref="C1"  Part="1" 
AR Path="/24A074934" Ref="C1"  Part="1" 
AR Path="/23BC884A074934" Ref="C1"  Part="1" 
AR Path="/DC0C124A074934" Ref="C1"  Part="1" 
AR Path="/23C34C4A074934" Ref="C1"  Part="1" 
AR Path="/23CBC44A074934" Ref="C1"  Part="1" 
AR Path="/23C6504A074934" Ref="C1"  Part="1" 
AR Path="/39803EA4A074934" Ref="C1"  Part="1" 
AR Path="/D058A04A074934" Ref="C1"  Part="1" 
AR Path="/1607D44A074934" Ref="C1"  Part="1" 
AR Path="/23D70C4A074934" Ref="C1"  Part="1" 
AR Path="/2104A074934" Ref="C1"  Part="1" 
AR Path="/F4BF4A074934" Ref="C1"  Part="1" 
AR Path="/262F604A074934" Ref="C1"  Part="1" 
AR Path="/384A074934" Ref="C1"  Part="1" 
F 0 "C1" H 19500 15500 50  0000 L CNN
F 1 "0.33 uF" H 19550 15200 50  0000 L CNN
F 2 "C1V5" H 19450 15400 60  0001 C CNN
	1    19450 15400
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG039
U 1 1 4A0732EB
P 20850 15600
AR Path="/25E4A0732EB" Ref="#FLG039"  Part="1" 
AR Path="/FFFFFFFF4A0732EB" Ref="#FLG041"  Part="1" 
AR Path="/4A0732EB" Ref="#FLG036"  Part="1" 
AR Path="/94A0732EB" Ref="#FLG3"  Part="1" 
AR Path="/6FF0DD404A0732EB" Ref="#FLG037"  Part="1" 
AR Path="/23D6E04A0732EB" Ref="#FLG013"  Part="1" 
AR Path="/363030314A0732EB" Ref="#FLG039"  Part="1" 
AR Path="/23C6384A0732EB" Ref="#FLG013"  Part="1" 
AR Path="/74A0732EB" Ref="#FLG013"  Part="1" 
AR Path="/314A0732EB" Ref="#FLG023"  Part="1" 
AR Path="/23C7004A0732EB" Ref="#FLG015"  Part="1" 
AR Path="/5AD7153D4A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/A4A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/755D912A4A0732EB" Ref="#FLG017"  Part="1" 
AR Path="/3FE08B434A0732EB" Ref="#FLG015"  Part="1" 
AR Path="/3DA360004A0732EB" Ref="#FLG023"  Part="1" 
AR Path="/23D9304A0732EB" Ref="#FLG015"  Part="1" 
AR Path="/3FEFFFFF4A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/23D8D44A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/3D8EA0004A0732EB" Ref="#FLG029"  Part="1" 
AR Path="/402B08B44A0732EB" Ref="#FLG015"  Part="1" 
AR Path="/4026A24D4A0732EB" Ref="#FLG015"  Part="1" 
AR Path="/3FE88B434A0732EB" Ref="#FLG031"  Part="1" 
AR Path="/3D7E00004A0732EB" Ref="#FLG023"  Part="1" 
AR Path="/402988B44A0732EB" Ref="#FLG014"  Part="1" 
AR Path="/6FE901F74A0732EB" Ref="#FLG023"  Part="1" 
AR Path="/3D6CC0004A0732EB" Ref="#FLG022"  Part="1" 
AR Path="/FFFFFFF04A0732EB" Ref="#FLG3"  Part="1" 
AR Path="/23C2344A0732EB" Ref="#FLG026"  Part="1" 
AR Path="/DCBAABCD4A0732EB" Ref="#FLG029"  Part="1" 
AR Path="/5AD746F64A0732EB" Ref="#FLG027"  Part="1" 
AR Path="/4027EF1A4A0732EB" Ref="#FLG027"  Part="1" 
AR Path="/402A6F1A4A0732EB" Ref="#FLG027"  Part="1" 
AR Path="/4029BBE74A0732EB" Ref="#FLG027"  Part="1" 
AR Path="/402A224D4A0732EB" Ref="#FLG027"  Part="1" 
AR Path="/402A55814A0732EB" Ref="#FLG026"  Part="1" 
AR Path="/4026224D4A0732EB" Ref="#FLG014"  Part="1" 
AR Path="/3D6220004A0732EB" Ref="#FLG026"  Part="1" 
AR Path="/3D9720004A0732EB" Ref="#FLG027"  Part="1" 
AR Path="/2600004A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/402C3BE74A0732EB" Ref="#FLG014"  Part="1" 
AR Path="/402955814A0732EB" Ref="#FLG021"  Part="1" 
AR Path="/40293BE74A0732EB" Ref="#FLG025"  Part="1" 
AR Path="/402555814A0732EB" Ref="#FLG014"  Part="1" 
AR Path="/3FED58104A0732EB" Ref="#FLG014"  Part="1" 
AR Path="/402688B44A0732EB" Ref="#FLG014"  Part="1" 
AR Path="/3FE441894A0732EB" Ref="#FLG015"  Part="1" 
AR Path="/B84A0732EB" Ref="#FLG3"  Part="1" 
AR Path="/4029D5814A0732EB" Ref="#FLG015"  Part="1" 
AR Path="/40256F1A4A0732EB" Ref="#FLG023"  Part="1" 
AR Path="/40286F1A4A0732EB" Ref="#FLG023"  Part="1" 
AR Path="/402A08B44A0732EB" Ref="#FLG023"  Part="1" 
AR Path="/402588B44A0732EB" Ref="#FLG023"  Part="1" 
AR Path="/A84A0732EB" Ref="#FLG030"  Part="1" 
AR Path="/3FE224DD4A0732EB" Ref="#FLG021"  Part="1" 
AR Path="/402C08B44A0732EB" Ref="#FLG022"  Part="1" 
AR Path="/402BEF1A4A0732EB" Ref="#FLG023"  Part="1" 
AR Path="/402C55814A0732EB" Ref="#FLG028"  Part="1" 
AR Path="/40273BE74A0732EB" Ref="#FLG029"  Part="1" 
AR Path="/402908B44A0732EB" Ref="#FLG025"  Part="1" 
AR Path="/3D5A40004A0732EB" Ref="#FLG022"  Part="1" 
AR Path="/402755814A0732EB" Ref="#FLG030"  Part="1" 
AR Path="/4030AAC04A0732EB" Ref="#FLG030"  Part="1" 
AR Path="/4031DDF34A0732EB" Ref="#FLG031"  Part="1" 
AR Path="/4032778D4A0732EB" Ref="#FLG037"  Part="1" 
AR Path="/403091264A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/403051264A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/4032F78D4A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/403251264A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/4032AAC04A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/4030D1264A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/4031778D4A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/3FEA24DD4A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/6FE934E34A0732EB" Ref="#FLG039"  Part="1" 
AR Path="/773F65F14A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/773F8EB44A0732EB" Ref="#FLG036"  Part="1" 
AR Path="/23C9F04A0732EB" Ref="#FLG3"  Part="1" 
AR Path="/23BC884A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/DC0C124A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/6684D64A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/23C34C4A0732EB" Ref="#FLG041"  Part="1" 
AR Path="/23CBC44A0732EB" Ref="#FLG040"  Part="1" 
AR Path="/39803EA4A0732EB" Ref="#FLG041"  Part="1" 
AR Path="/D058A04A0732EB" Ref="#FLG042"  Part="1" 
AR Path="/1607D44A0732EB" Ref="#FLG042"  Part="1" 
AR Path="/24A0732EB" Ref="#FLG042"  Part="1" 
AR Path="/23D70C4A0732EB" Ref="#FLG042"  Part="1" 
AR Path="/14A0732EB" Ref="#FLG042"  Part="1" 
AR Path="/2104A0732EB" Ref="#FLG042"  Part="1" 
AR Path="/23C6504A0732EB" Ref="#FLG036"  Part="1" 
AR Path="/F4BF4A0732EB" Ref="#FLG036"  Part="1" 
AR Path="/262F604A0732EB" Ref="#FLG036"  Part="1" 
AR Path="/384A0732EB" Ref="#FLG037"  Part="1" 
F 0 "#FLG036" H 20850 15870 30  0001 C CNN
F 1 "PWR_FLAG" H 20850 15830 30  0000 C CNN
	1    20850 15600
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG014
U 1 1 4A0732B7
P 18900 15200
AR Path="/6FF405304A0732B7" Ref="#FLG014"  Part="1" 
AR Path="/25E4A0732B7" Ref="#FLG040"  Part="1" 
AR Path="/FFFFFFFF4A0732B7" Ref="#FLG042"  Part="1" 
AR Path="/4A0732B7" Ref="#FLG037"  Part="1" 
AR Path="/6FF0DD404A0732B7" Ref="#FLG038"  Part="1" 
AR Path="/23D6E04A0732B7" Ref="#FLG015"  Part="1" 
AR Path="/363030314A0732B7" Ref="#FLG040"  Part="1" 
AR Path="/23C6384A0732B7" Ref="#FLG015"  Part="1" 
AR Path="/74A0732B7" Ref="#FLG015"  Part="1" 
AR Path="/314A0732B7" Ref="#FLG024"  Part="1" 
AR Path="/23C7004A0732B7" Ref="#FLG016"  Part="1" 
AR Path="/5AD7153D4A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/A4A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/755D912A4A0732B7" Ref="#FLG018"  Part="1" 
AR Path="/3FE08B434A0732B7" Ref="#FLG017"  Part="1" 
AR Path="/3DA360004A0732B7" Ref="#FLG024"  Part="1" 
AR Path="/23D9304A0732B7" Ref="#FLG017"  Part="1" 
AR Path="/3FEFFFFF4A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/23D8D44A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/3D8EA0004A0732B7" Ref="#FLG030"  Part="1" 
AR Path="/402B08B44A0732B7" Ref="#FLG017"  Part="1" 
AR Path="/4026A24D4A0732B7" Ref="#FLG017"  Part="1" 
AR Path="/3FE88B434A0732B7" Ref="#FLG032"  Part="1" 
AR Path="/3D7E00004A0732B7" Ref="#FLG024"  Part="1" 
AR Path="/402988B44A0732B7" Ref="#FLG015"  Part="1" 
AR Path="/6FE901F74A0732B7" Ref="#FLG024"  Part="1" 
AR Path="/3D6CC0004A0732B7" Ref="#FLG023"  Part="1" 
AR Path="/24A0732B7" Ref="#FLG043"  Part="1" 
AR Path="/FFFFFFF04A0732B7" Ref="#FLG1"  Part="1" 
AR Path="/23C2344A0732B7" Ref="#FLG027"  Part="1" 
AR Path="/DCBAABCD4A0732B7" Ref="#FLG030"  Part="1" 
AR Path="/5AD746F64A0732B7" Ref="#FLG028"  Part="1" 
AR Path="/4027EF1A4A0732B7" Ref="#FLG028"  Part="1" 
AR Path="/402A6F1A4A0732B7" Ref="#FLG028"  Part="1" 
AR Path="/4029BBE74A0732B7" Ref="#FLG028"  Part="1" 
AR Path="/402A224D4A0732B7" Ref="#FLG028"  Part="1" 
AR Path="/402A55814A0732B7" Ref="#FLG027"  Part="1" 
AR Path="/4026224D4A0732B7" Ref="#FLG015"  Part="1" 
AR Path="/3D6220004A0732B7" Ref="#FLG027"  Part="1" 
AR Path="/3D9720004A0732B7" Ref="#FLG028"  Part="1" 
AR Path="/2600004A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/402C3BE74A0732B7" Ref="#FLG015"  Part="1" 
AR Path="/402955814A0732B7" Ref="#FLG022"  Part="1" 
AR Path="/40293BE74A0732B7" Ref="#FLG026"  Part="1" 
AR Path="/402555814A0732B7" Ref="#FLG015"  Part="1" 
AR Path="/3FED58104A0732B7" Ref="#FLG015"  Part="1" 
AR Path="/402688B44A0732B7" Ref="#FLG015"  Part="1" 
AR Path="/3FE441894A0732B7" Ref="#FLG016"  Part="1" 
AR Path="/B84A0732B7" Ref="#FLG1"  Part="1" 
AR Path="/4029D5814A0732B7" Ref="#FLG016"  Part="1" 
AR Path="/40256F1A4A0732B7" Ref="#FLG024"  Part="1" 
AR Path="/40286F1A4A0732B7" Ref="#FLG024"  Part="1" 
AR Path="/402A08B44A0732B7" Ref="#FLG024"  Part="1" 
AR Path="/402588B44A0732B7" Ref="#FLG024"  Part="1" 
AR Path="/A84A0732B7" Ref="#FLG031"  Part="1" 
AR Path="/3FE224DD4A0732B7" Ref="#FLG022"  Part="1" 
AR Path="/402C08B44A0732B7" Ref="#FLG023"  Part="1" 
AR Path="/402BEF1A4A0732B7" Ref="#FLG024"  Part="1" 
AR Path="/402C55814A0732B7" Ref="#FLG029"  Part="1" 
AR Path="/40273BE74A0732B7" Ref="#FLG030"  Part="1" 
AR Path="/402908B44A0732B7" Ref="#FLG026"  Part="1" 
AR Path="/3D5A40004A0732B7" Ref="#FLG023"  Part="1" 
AR Path="/402755814A0732B7" Ref="#FLG031"  Part="1" 
AR Path="/4030AAC04A0732B7" Ref="#FLG031"  Part="1" 
AR Path="/4031DDF34A0732B7" Ref="#FLG032"  Part="1" 
AR Path="/4032778D4A0732B7" Ref="#FLG038"  Part="1" 
AR Path="/403091264A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/403051264A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/4032F78D4A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/403251264A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/4032AAC04A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/4030D1264A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/4031778D4A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/3FEA24DD4A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/6FE934E34A0732B7" Ref="#FLG040"  Part="1" 
AR Path="/773F65F14A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/773F8EB44A0732B7" Ref="#FLG037"  Part="1" 
AR Path="/21602704A0732B7" Ref="#FLG1"  Part="1" 
AR Path="/20EAA484A0732B7" Ref="#FLG1"  Part="1" 
AR Path="/20EB1284A0732B7" Ref="#FLG1"  Part="1" 
AR Path="/20E75884A0732B7" Ref="#FLG1"  Part="1" 
AR Path="/23BC884A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/DC0C124A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/6684D64A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/23C34C4A0732B7" Ref="#FLG042"  Part="1" 
AR Path="/23CBC44A0732B7" Ref="#FLG041"  Part="1" 
AR Path="/39803EA4A0732B7" Ref="#FLG042"  Part="1" 
AR Path="/D058A04A0732B7" Ref="#FLG043"  Part="1" 
AR Path="/1607D44A0732B7" Ref="#FLG043"  Part="1" 
AR Path="/23D70C4A0732B7" Ref="#FLG043"  Part="1" 
AR Path="/14A0732B7" Ref="#FLG043"  Part="1" 
AR Path="/2104A0732B7" Ref="#FLG043"  Part="1" 
AR Path="/23C6504A0732B7" Ref="#FLG037"  Part="1" 
AR Path="/F4BF4A0732B7" Ref="#FLG037"  Part="1" 
AR Path="/262F604A0732B7" Ref="#FLG037"  Part="1" 
AR Path="/384A0732B7" Ref="#FLG038"  Part="1" 
F 0 "#FLG037" H 18900 15470 30  0001 C CNN
F 1 "PWR_FLAG" H 18900 15430 30  0000 C CNN
	1    18900 15200
	1    0    0    -1  
$EndComp
$Comp
L LM7805 U1
U 1 1 4A073194
P 19850 15250
AR Path="/FFFFFFFF4A073194" Ref="U1"  Part="1" 
AR Path="/4A073194" Ref="U1"  Part="1" 
AR Path="/94A073194" Ref="U1"  Part="1" 
AR Path="/23D6E04A073194" Ref="U1"  Part="1" 
AR Path="/5AD7153D4A073194" Ref="U1"  Part="1" 
AR Path="/A4A073194" Ref="U1"  Part="1" 
AR Path="/755D912A4A073194" Ref="U1"  Part="1" 
AR Path="/3FE08B434A073194" Ref="U1"  Part="1" 
AR Path="/3DA360004A073194" Ref="U1"  Part="1" 
AR Path="/23D9304A073194" Ref="U1"  Part="1" 
AR Path="/3FEFFFFF4A073194" Ref="U1"  Part="1" 
AR Path="/23D8D44A073194" Ref="U1"  Part="1" 
AR Path="/3D8EA0004A073194" Ref="U1"  Part="1" 
AR Path="/402B08B44A073194" Ref="U1"  Part="1" 
AR Path="/4026A24D4A073194" Ref="U1"  Part="1" 
AR Path="/3FE88B434A073194" Ref="U1"  Part="1" 
AR Path="/3D7E00004A073194" Ref="U1"  Part="1" 
AR Path="/402988B44A073194" Ref="U1"  Part="1" 
AR Path="/6FE901F74A073194" Ref="U1"  Part="1" 
AR Path="/3D6CC0004A073194" Ref="U1"  Part="1" 
AR Path="/FFFFFFF04A073194" Ref="U1"  Part="1" 
AR Path="/23C2344A073194" Ref="U1"  Part="1" 
AR Path="/DCBAABCD4A073194" Ref="U1"  Part="1" 
AR Path="/5AD746F64A073194" Ref="U1"  Part="1" 
AR Path="/4027EF1A4A073194" Ref="U1"  Part="1" 
AR Path="/402A6F1A4A073194" Ref="U1"  Part="1" 
AR Path="/4029BBE74A073194" Ref="U1"  Part="1" 
AR Path="/402A224D4A073194" Ref="U1"  Part="1" 
AR Path="/402A55814A073194" Ref="U1"  Part="1" 
AR Path="/4026224D4A073194" Ref="U1"  Part="1" 
AR Path="/3D6220004A073194" Ref="U1"  Part="1" 
AR Path="/3D9720004A073194" Ref="U1"  Part="1" 
AR Path="/2600004A073194" Ref="U1"  Part="1" 
AR Path="/402C3BE74A073194" Ref="U1"  Part="1" 
AR Path="/402955814A073194" Ref="U1"  Part="1" 
AR Path="/40293BE74A073194" Ref="U1"  Part="1" 
AR Path="/402555814A073194" Ref="U1"  Part="1" 
AR Path="/3FED58104A073194" Ref="U1"  Part="1" 
AR Path="/402688B44A073194" Ref="U1"  Part="1" 
AR Path="/3FE441894A073194" Ref="U1"  Part="1" 
AR Path="/B84A073194" Ref="U1"  Part="1" 
AR Path="/4029D5814A073194" Ref="U1"  Part="1" 
AR Path="/40256F1A4A073194" Ref="U1"  Part="1" 
AR Path="/40286F1A4A073194" Ref="U1"  Part="1" 
AR Path="/402A08B44A073194" Ref="U1"  Part="1" 
AR Path="/402588B44A073194" Ref="U1"  Part="1" 
AR Path="/A84A073194" Ref="U1"  Part="1" 
AR Path="/6FF0DD404A073194" Ref="U1"  Part="1" 
AR Path="/3FE224DD4A073194" Ref="U1"  Part="1" 
AR Path="/402C08B44A073194" Ref="U1"  Part="1" 
AR Path="/402BEF1A4A073194" Ref="U1"  Part="1" 
AR Path="/402C55814A073194" Ref="U1"  Part="1" 
AR Path="/40273BE74A073194" Ref="U1"  Part="1" 
AR Path="/402908B44A073194" Ref="U1"  Part="1" 
AR Path="/3D5A40004A073194" Ref="U1"  Part="1" 
AR Path="/402755814A073194" Ref="U1"  Part="1" 
AR Path="/4030AAC04A073194" Ref="U1"  Part="1" 
AR Path="/4031DDF34A073194" Ref="U1"  Part="1" 
AR Path="/4032778D4A073194" Ref="U1"  Part="1" 
AR Path="/403091264A073194" Ref="U1"  Part="1" 
AR Path="/403051264A073194" Ref="U1"  Part="1" 
AR Path="/4032F78D4A073194" Ref="U1"  Part="1" 
AR Path="/403251264A073194" Ref="U1"  Part="1" 
AR Path="/4032AAC04A073194" Ref="U1"  Part="1" 
AR Path="/4030D1264A073194" Ref="U1"  Part="1" 
AR Path="/4031778D4A073194" Ref="U1"  Part="1" 
AR Path="/3FEA24DD4A073194" Ref="U1"  Part="1" 
AR Path="/6FE934E34A073194" Ref="U1"  Part="1" 
AR Path="/773F65F14A073194" Ref="U1"  Part="1" 
AR Path="/773F8EB44A073194" Ref="U1"  Part="1" 
AR Path="/23C9F04A073194" Ref="U1"  Part="1" 
AR Path="/24A073194" Ref="U1"  Part="1" 
AR Path="/23BC884A073194" Ref="U1"  Part="1" 
AR Path="/DC0C124A073194" Ref="U1"  Part="1" 
AR Path="/23C34C4A073194" Ref="U1"  Part="1" 
AR Path="/23CBC44A073194" Ref="U1"  Part="1" 
AR Path="/39803EA4A073194" Ref="U1"  Part="1" 
AR Path="/D058A04A073194" Ref="U1"  Part="1" 
AR Path="/1607D44A073194" Ref="U1"  Part="1" 
AR Path="/23D70C4A073194" Ref="U1"  Part="1" 
AR Path="/14A073194" Ref="U1"  Part="1" 
AR Path="/2104A073194" Ref="U1"  Part="1" 
AR Path="/F4BF4A073194" Ref="U1"  Part="1" 
AR Path="/262F604A073194" Ref="U1"  Part="1" 
AR Path="/384A073194" Ref="U1"  Part="1" 
AR Path="/23C6504A073194" Ref="U1"  Part="1" 
F 0 "U1" H 20000 15054 60  0000 C CNN
F 1 "LM7805" H 19850 15450 60  0000 C CNN
F 2 "TO220" H 19850 15250 60  0001 C CNN
	1    19850 15250
	1    0    0    -1  
$EndComp
$Comp
L S100_MALE P1
U 1 1 4A06E881
P 20650 5650
AR Path="/4A06E881" Ref="P1"  Part="1" 
AR Path="/94A06E881" Ref="P1"  Part="1" 
AR Path="/6FF0DD404A06E881" Ref="P1"  Part="1" 
AR Path="/14A06E881" Ref="P1"  Part="1" 
AR Path="/23D6E04A06E881" Ref="P1"  Part="1" 
AR Path="/5AD7153D4A06E881" Ref="P1"  Part="1" 
AR Path="/A4A06E881" Ref="P1"  Part="1" 
AR Path="/755D912A4A06E881" Ref="P1"  Part="1" 
AR Path="/3FE08B434A06E881" Ref="P1"  Part="1" 
AR Path="/3DA360004A06E881" Ref="P1"  Part="1" 
AR Path="/3FEFFFFF4A06E881" Ref="P1"  Part="1" 
AR Path="/23D8D44A06E881" Ref="P1"  Part="1" 
AR Path="/3D8EA0004A06E881" Ref="P1"  Part="1" 
AR Path="/402B08B44A06E881" Ref="P1"  Part="1" 
AR Path="/4026A24D4A06E881" Ref="P1"  Part="1" 
AR Path="/3FE88B434A06E881" Ref="P1"  Part="1" 
AR Path="/3D7E00004A06E881" Ref="P1"  Part="1" 
AR Path="/402988B44A06E881" Ref="P1"  Part="1" 
AR Path="/3D6CC0004A06E881" Ref="P1"  Part="1" 
AR Path="/FFFFFFF04A06E881" Ref="P1"  Part="1" 
AR Path="/23C2344A06E881" Ref="P1"  Part="1" 
AR Path="/DCBAABCD4A06E881" Ref="P1"  Part="1" 
AR Path="/5AD746F64A06E881" Ref="P1"  Part="1" 
AR Path="/4027EF1A4A06E881" Ref="P1"  Part="1" 
AR Path="/402A6F1A4A06E881" Ref="P1"  Part="1" 
AR Path="/4029BBE74A06E881" Ref="P1"  Part="1" 
AR Path="/402A224D4A06E881" Ref="P1"  Part="1" 
AR Path="/402A55814A06E881" Ref="P1"  Part="1" 
AR Path="/4026224D4A06E881" Ref="P1"  Part="1" 
AR Path="/3D6220004A06E881" Ref="P1"  Part="1" 
AR Path="/3D9720004A06E881" Ref="P1"  Part="1" 
AR Path="/2600004A06E881" Ref="P1"  Part="1" 
AR Path="/402C3BE74A06E881" Ref="P1"  Part="1" 
AR Path="/402955814A06E881" Ref="P1"  Part="1" 
AR Path="/40293BE74A06E881" Ref="P1"  Part="1" 
AR Path="/402555814A06E881" Ref="P1"  Part="1" 
AR Path="/3FED58104A06E881" Ref="P1"  Part="1" 
AR Path="/402688B44A06E881" Ref="P1"  Part="1" 
AR Path="/3FE441894A06E881" Ref="P1"  Part="1" 
AR Path="/B84A06E881" Ref="P1"  Part="1" 
AR Path="/990A17DE4A06E881" Ref="P1"  Part="1" 
AR Path="/4029D5814A06E881" Ref="P1"  Part="1" 
AR Path="/40256F1A4A06E881" Ref="P1"  Part="1" 
AR Path="/40286F1A4A06E881" Ref="P1"  Part="1" 
AR Path="/402A08B44A06E881" Ref="P1"  Part="1" 
AR Path="/402588B44A06E881" Ref="P1"  Part="1" 
AR Path="/A84A06E881" Ref="P1"  Part="1" 
AR Path="/3FE224DD4A06E881" Ref="P1"  Part="1" 
AR Path="/402C08B44A06E881" Ref="P1"  Part="1" 
AR Path="/402BEF1A4A06E881" Ref="P1"  Part="1" 
AR Path="/402C55814A06E881" Ref="P1"  Part="1" 
AR Path="/40273BE74A06E881" Ref="P1"  Part="1" 
AR Path="/402908B44A06E881" Ref="P1"  Part="1" 
AR Path="/3D5A40004A06E881" Ref="P1"  Part="1" 
AR Path="/402755814A06E881" Ref="P1"  Part="1" 
AR Path="/4030AAC04A06E881" Ref="P1"  Part="1" 
AR Path="/4031DDF34A06E881" Ref="P1"  Part="1" 
AR Path="/4032778D4A06E881" Ref="P1"  Part="1" 
AR Path="/403091264A06E881" Ref="P1"  Part="1" 
AR Path="/403051264A06E881" Ref="P1"  Part="1" 
AR Path="/4032F78D4A06E881" Ref="P1"  Part="1" 
AR Path="/403251264A06E881" Ref="P1"  Part="1" 
AR Path="/4032AAC04A06E881" Ref="P1"  Part="1" 
AR Path="/4030D1264A06E881" Ref="P1"  Part="1" 
AR Path="/4031778D4A06E881" Ref="P1"  Part="1" 
AR Path="/3FEA24DD4A06E881" Ref="P1"  Part="1" 
AR Path="/6FE934E34A06E881" Ref="P1"  Part="1" 
AR Path="/773F65F14A06E881" Ref="P1"  Part="1" 
AR Path="/773F8EB44A06E881" Ref="P1"  Part="1" 
AR Path="/23C9F04A06E881" Ref="P1"  Part="1" 
AR Path="/24A06E881" Ref="P1"  Part="1" 
AR Path="/23BC884A06E881" Ref="P1"  Part="1" 
AR Path="/DC0C124A06E881" Ref="P1"  Part="1" 
AR Path="/23C34C4A06E881" Ref="P1"  Part="1" 
AR Path="/23CBC44A06E881" Ref="P1"  Part="1" 
AR Path="/23C6504A06E881" Ref="P1"  Part="1" 
AR Path="/39803EA4A06E881" Ref="P1"  Part="1" 
AR Path="/FFFFFFFF4A06E881" Ref="P1"  Part="1" 
AR Path="/D058A04A06E881" Ref="P1"  Part="1" 
AR Path="/1607D44A06E881" Ref="P1"  Part="1" 
AR Path="/23D70C4A06E881" Ref="P1"  Part="1" 
AR Path="/2104A06E881" Ref="P1"  Part="1" 
AR Path="/F4BF4A06E881" Ref="P1"  Part="1" 
AR Path="/262F604A06E881" Ref="P1"  Part="1" 
AR Path="/384A06E881" Ref="P1"  Part="1" 
F 0 "P1" H 20650 10700 60  0000 C CNN
F 1 "S100_MALE" V 21050 5650 60  0000 C CNN
F 2 "S100_MALE" H 20650 5650 60  0001 C CNN
	1    20650 5650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
