
# Flash Memory Adapter 

  This repo contains the kicad files for a flash memory adapter to enable the T-35 FPGA monitor to boot off a flash plugged into the 
  sram socket on the FPGA SBC Z80 board. 

  ![main window](flash_adapter_00.jpg)  
  ![main window](flash_adapter_01.jpg)  

