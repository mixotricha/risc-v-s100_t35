TARGET  = virtual_board.vpi
BUILD   = ghdl/build
VHDL = ghdl/vhdl
WORKDIR=ghdl/work 
VEXRISC  =  vexriscv
EFINITY_LOCAL_SRC = local_src
MONITOR = vexriscv/src/main/c/hoffman/monitor 
LISP = vexriscv/src/main/c/hoffman/lisp

SRC += ghdl/src/led.cc
SRC += ghdl/src/rgbled.cc
SRC += ghdl/src/segseven.cc
SRC += ghdl/src/push_button.cc
SRC += ghdl/src/switch.cc
SRC += ghdl/src/board_window.cc
SRC += ghdl/src/uart.cc
SRC += ghdl/src/virtual_board.cc
SRC += ghdl/src/VBMessage.cc
SRC += ghdl/src/Instance.cc
SRC += ghdl/src/InspectorWindow.cc
SRC += ghdl/src/vpi.cc

OBJS     = $(addprefix $(BUILD)/, $(notdir $(SRC:.cc=.o)))
OBJS    += $(BUILD)/resources.o
CFLAGS  += -W -Wall -Wno-write-strings -O2 -Isrc `pkg-config --cflags gtkmm-3.0`
LDFLAGS += `pkg-config --libs gtkmm-3.0` -lm -lrt
GHDL     = ghdl-gcc
GHDL_FLAGS = -fsynopsys -Whide -frelaxed-rules -Wspecs 

CXX      = g++
GHDLCXX  = $(GHDL) --vpi-compile $(CXX)
GHDLLD   = $(GHDL) --vpi-link $(CXX)
#SBT      = sbt "runMain vexriscv.demo.MuraxWithRamInit"
SBTEFINITY      = sbt "runMain vexriscv.hoffman.HoffmanWithMemoryInit"
SBTGHDL 	    = sbt "runMain vexriscv.hoffman.HoffmanWithMemoryInitGhdl"
#BRIEY 	        = sbt "runMain vexriscv.demo.BrieySim"
#WAVE 	        = sbt "runMain vexriscv.demo.WaveExample"

VHDL_FILE = Hoffman.vhd

#############################################
#All of the build for ghdl here 
EXEC_TOP_UNIT = Hoffman

## In VPI we should free the callback handels that we do not need (to cancel the callback) 
## to prevent memory leeks.
## However with GHDL <= v0.37.0, freeing the callback handle raises
## an exception when the callback is called
GHDL_VERSION = $(shell $(GHDL) -v | grep -o -e '^GHDL\s\+[0-9\.]\+' | grep -o -e '[0-9\.]\+')
ifneq (,$(filter $(GHDL_VERSION), 0.33 0.34 0.35))
$(error Please use a GHDL with version >= 0.36)
endif
ifneq (,$(filter $(GHDL_VERSION), 0.36 0.37 0.37.0))
CFLAGS += -DVPI_DO_NOT_FREE_CALLBACK_HANDLES
endif

all: $(BUILD)/$(TARGET)

$(BUILD)/resources.c: ghdl/src/resources.gresource.xml
	@mkdir -p $(@D)
	glib-compile-resources --internal --target=$@ --generate-source $<

$(BUILD)/resources.o: $(BUILD)/resources.c
	$(CXX) $(CFLAGS) -c $^ -o $@

$(BUILD)/%.o: ghdl/src/%.cc
	@mkdir -p $(@D)
	$(GHDLCXX) $(CFLAGS) -c -o $@ $<

$(BUILD)/$(TARGET): $(OBJS)
	$(GHDLLD) $(CFLAGS) $^ -o $@ $(LDFLAGS)

sim_run: $(BUILD)/$(TARGET)
	$(GHDL) -i $(GHDL_FLAGS) --workdir=$(WORKDIR) $(VHDL)/*.vhd  
	$(GHDL) -m $(GHDL_FLAGS) --workdir=$(WORKDIR) $(EXEC_TOP_UNIT) 
	$(GHDL) -r $(EXEC_TOP_UNIT) --ieee-asserts=disable --vpi=./$(BUILD)/$(TARGET)
	#$(GHDL) -r $(EXEC_TOP_UNIT) --vpi=./$(BUILD)/$(TARGET)

#debug:  $(BUILD)/$(TARGET)
#	$(GHDL) -i $(GHDL_FLAGS) --workdir=$(WORKDIR) $(VHDL)/*.vhd  
#	$(GHDL) -m $(GHDL_FLAGS) --workdir=$(WORKDIR) $(EXEC_TOP_UNIT)
#	gdbtui --args $(GHDL) -r $(EXEC_TOP_UNIT) --vpi=./$(BUILD)/$(TARGET)

sim_clean:
	@rm -rvf ghdl/$(BUILD)
	@rm -vf  ghdl/work-*.cf
	@rm -vf  ghdl/*.o
	@rm -vf  ghdl/test
	@rm -vf  ghdl/*.vcd
	@rm -vf  ghdl/trace.txt

#############################################
#All of the build for hoffman scala here 
hoffman_efinity_vhdl: 
	@echo "Changing to directory..."
	cd $(VEXRISC) && $(SBTEFINITY)
	@echo "Copying VHDL_FILE to directory..."
	cp $(VEXRISC)/$(VHDL_FILE) $(EFINITY_LOCAL_SRC)/. 
	@echo "Changing back to current directory..."

hoffman_sim_vhdl: 
	@echo "Changing to directory..."
	cd $(VEXRISC) && $(SBTGHDL)
	@echo "Copying VHDL_FILE to directory..."
	cp $(VEXRISC)/$(VHDL_FILE) $(VHDL)/. 
	@echo "Changing back to current directory..."

wave_sim: 
	@echo "Changing to directory..."
	cd $(VEXRISC) && $(WAVE)
	#@echo "Copying VHDL_FILE to directory..."
	#cp $(VEXRISC)/$(VHDL_FILE) $(VHDL)/. 
	#@echo "Changing back to current directory..."

#############################################
#All of the build for monitor here 
monitor_efinity:
	@echo "Changing to directory..."
	cd $(MONITOR) && make    
	@echo "Changing back to current directory..."

monitor_sim:
	@echo "Changing to directory..."
	cd $(MONITOR) && make SIMULATOR=1 
	@echo "Changing back to current directory..."

monitor_clean:
	@echo "Changing to directory..."
	cd $(MONITOR) && make clean  
	@echo "Changing back to current directory..."

#############################################
#All of the build for os here 

lisp_efinity:
	@echo "Changing to directory..."
	cd $(LISP) && make    
	@echo "Changing back to current directory..."

lisp_sim:
	@echo "Changing to directory..."
	cd $(LISP) && make SIMULATOR=1 
	@echo "Changing back to current directory..."

lisp_clean:
	@echo "Changing to directory..."
	cd $(LISP) && make clean  
	@echo "Changing back to current directory..."

#####################################################
#Synthesis for efinity from shell not implemented yet
efinity:  
	@echo "To Do..."

#export PATH=$PATH:/home/mixotricha/workspace/s100_stuff/software/riscv-gnu-toolchain/riscv/bin